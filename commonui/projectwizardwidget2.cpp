/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2006 by Tim Beaulen <tbscope@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "projectwizardwidget2.h"

#include <klocale.h>

#include <ui_projectwizardwidget2.h>

ProjectStep2::ProjectStep2(QWidget *parent)
    : QWizardPage(parent)
    , m_ui(new Ui::ProjectStep2Base)
{
    m_ui->setupUi(this);
    setTitle(i18n("Translation Files"));

    registerField("poDir", m_ui->_poDirEdit);
    registerField("potDir", m_ui->_potDirEdit);
}

ProjectStep2::~ProjectStep2()
{
    delete m_ui;
}

#include "projectwizardwidget2.moc"
