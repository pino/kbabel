/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2001 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2002-2003 by StanislavVsinovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "catalog.h"
#include "catalogsettings.h"
#include "editcmd.h"
#include "dictchooser.h"
#include "kbabeldictbox.h"
#include "regexpextractor.h"
#include "roughtransdlg.h"

#include <q3memarray.h>
#include <qcheckbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qradiobutton.h>
#include <qtimer.h>
#include <Q3PtrList>
#include <QFrame>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QApplication>
#include <QGroupBox>
#include <QAbstractButton>
#include <QProgressBar>
#include <QButtonGroup>

#include <kconfig.h>
#include <kglobal.h>
#include <klocale.h>
#include <kmessagebox.h>

#include <kdebug.h>
#include <kvbox.h>
#include <kconfiggroup.h>

using namespace KBabel;

RoughTransDlg::RoughTransDlg(KBabelDictBox *dict, Catalog *cat
                , QWidget *parent)
        : KDialog(parent)
        ,catalog(cat)
        ,active(false)
        ,stop(false)
        ,cancel(false)
        ,dictBox(dict)
        ,exactTransCounter(0)
        ,partTransCounter(0)
        ,totalTried(0)
{
    setCaption( i18nc("Caption of dialog","Rough Translation") );
    setButtons( User1|User2|User3|Close);
    setDefaultButton( User1 );
    setModal( true );
    setButtonsOrientation(Qt::Vertical);
    setButtonText(User1,i18n("&Start"));
    setButtonText(User2,i18n("S&top"));
    setButtonText(User3,i18n("C&ancel"));

    enableButton(User2,false);
    enableButton(User3,false);

    QWidget *mw = new QWidget(this);
    setMainWidget(mw);

    QVBoxLayout *mainLayout = new QVBoxLayout(mw);
    mainLayout->setMargin(0);

    configWidget = new KVBox(mw);
    mainLayout->addWidget(configWidget);

    QGroupBox *box = new QGroupBox(i18n("What to Translate"),configWidget);
    QBoxLayout *boxLay = new QHBoxLayout(box);

    whatGroup = new QButtonGroup(box);
    whatGroup->setExclusive(false);
    untransButton = new QCheckBox(i18n("U&ntranslated entries"),box);
    whatGroup->addButton(untransButton, 0);
    boxLay->addWidget(untransButton);
    fuzzyButton = new QCheckBox(i18n("&Fuzzy entries"),box);
    whatGroup->addButton(fuzzyButton, 1);
    boxLay->addWidget(fuzzyButton);
    transButton = new QCheckBox(i18n("T&ranslated entries"),box);
    whatGroup->addButton(transButton, 2);
    boxLay->addWidget(transButton);

    connect(whatGroup,SIGNAL(buttonClicked(int)),this,SLOT(msgButtonClicked(int)));

    box->setWhatsThis(i18n("<qt><p><b>What entries to translate</b></p>"
                "<p>Choose here, for which entries of the file KBabel "
                "tries to find a translation. Changed entries are always "
                "marked as fuzzy, no matter which option you choose.</p></qt>"));

    box = new QGroupBox(i18n("How to Translate"),configWidget);
    boxLay = new QHBoxLayout(box);

    searchMatchButton = new QCheckBox(i18n("&Use dictionary settings")
	    ,box);
    boxLay->addWidget(searchMatchButton);

    fuzzyMatchButton = new QCheckBox(i18n("Fu&zzy translation (slow)")
            ,box);
    boxLay->addWidget(fuzzyMatchButton);
    singleWordButton = new QCheckBox(i18n("&Single word translation")
            ,box);
    boxLay->addWidget(singleWordButton);

    box->setWhatsThis(i18n("<qt><p><b>How messages get translated</b></p>"
                "<p>Here you can define if a message can only get translated "
                "completely, if similar messages are acceptable or if KBabel "
                "is supposed to try translating "
                "the single words of a message if no translation of the "
                "complete message or similar message was found.</p></qt>"));


    box = new QGroupBox(i18n("Options"),configWidget);
    boxLay = new QVBoxLayout(box);

    markFuzzyButton = new QCheckBox(i18n("&Mark changed entries as fuzzy"),box);
    markFuzzyButton->setChecked(true);
    markFuzzyButton->setWhatsThis(
            i18n("<qt><p><b>Mark changed entries as fuzzy</b></p>"
          "<p>When a translation for a message is found, the entry "
          "will be marked <b>fuzzy</b> by default. This is because the "
          "translation is just guessed by KBabel and you should always "
          "check the results carefully. Deactivate this option only if "
          "you know what you are doing.</p></qt>"));
    boxLay->addWidget(markFuzzyButton);


    connect(markFuzzyButton, SIGNAL(toggled(bool))
            , this, SLOT(fuzzyButtonToggled(bool)));

    kdeButton = new QCheckBox(i18n("Initialize &KDE-specific entries"),box);
    kdeButton->setChecked(true);
    kdeButton->setWhatsThis(
            i18n("<qt><p><b>Initialize KDE-specific entries</b></p>"
          "<p>Initialize \"Comment=\" and \"Name=\" entries "
	  "if a translation is not found. Also, \"NAME OF TRANSLATORS\" "
	  "and \"EMAIL OF TRANSLATORS\" is filled with identity settings.</p></qt>"));
    boxLay->addWidget(kdeButton);

    QGroupBox *dBox = new QGroupBox(i18n("Dictionaries"),configWidget);
    boxLay = new QVBoxLayout(dBox);
    configWidget->setStretchFactor(dBox,1);

    QList<ModuleInfo *> moduleList = dict->moduleInfos();

    KSharedConfig::Ptr config = KGlobal::config();
    KConfigGroup gs(config,"RoughTranslation");
    QStringList selectedList=gs.readEntry("Selected",QStringList());
    if(selectedList.isEmpty())
    {
        int a = dict->activeModule();
        ModuleInfo *mi = moduleList.at(a);
        if(mi)
        {
            selectedList.append(mi->id);
        }
    }
    dictChooser = new DictChooser(dict,selectedList,dBox);
    dictChooser->setObjectName("dictChooser");

    dictChooser->setWhatsThis(i18n("<qt><p><b>Dictionaries</b></p>"
                "<p>Choose here, which dictionaries have to be used for "
                "finding a translation. If you select more than one "
                "dictionary, they are used in the same order as they "
                "are displayed in the list.</p>"
		"<p>The <b>Configure</b> button allows you to temporarily "
		"configure selected dictionary. The original settings "
		"will be restored after closing the dialog.</p></qt>"));
    boxLay->addWidget(dictChooser);

    QLabel* label = new QLabel( i18n("Messages:"), mw );
    progressbar = new QProgressBar( mw );
    progressbar->setTextVisible(true);
    progressbar->setFormat("%v/%m (%p%)");
    QHBoxLayout* pblayout= new QHBoxLayout();
    mainLayout->addLayout( pblayout );
    pblayout->addWidget(label);
    pblayout->addWidget(progressbar);

    transButton->setChecked(gs.readEntry("Translated",false));
    untransButton->setChecked(gs.readEntry("Untranslated",true));
    fuzzyButton->setChecked(gs.readEntry("Fuzzies",false));

    bool flag = gs.readEntry("fuzzyMatch",true);
    fuzzyMatchButton->setChecked(flag);

    flag = gs.readEntry("searchMatch",true);
    searchMatchButton->setChecked(flag);

    flag = gs.readEntry("singleWord",true);
    singleWordButton->setChecked(flag);

    flag = gs.readEntry("kdeSpecific",true);
    kdeButton->setChecked(flag);

    msgButtonClicked(0);
    connect(this,SIGNAL(user1Clicked()),this,SLOT(slotUser1()));
    connect(this,SIGNAL(user2Clicked()),this,SLOT(slotUser2()));
    connect(this,SIGNAL(user3Clicked()),this,SLOT(slotUser3()));
    connect(this,SIGNAL(closeClicked()),this,SLOT(slotClose()));
}

RoughTransDlg::~RoughTransDlg()
{
    KSharedConfig::Ptr config = KGlobal::config();
    KConfigGroup gs(config,"RoughTranslation");
    gs.writeEntry("Selected",dictChooser->selectedDicts());

    bool flag=transButton->isChecked();
    gs.writeEntry("Translated",flag);
    flag=untransButton->isChecked();
    gs.writeEntry("Untranslated",flag);
    flag=fuzzyButton->isChecked();
    gs.writeEntry("Fuzzies",flag);
    flag=singleWordButton->isChecked();
    gs.writeEntry("singleWord",flag);
    flag=fuzzyMatchButton->isChecked();
    gs.writeEntry("fuzzyMatch",flag);
    flag=searchMatchButton->isChecked();
    gs.writeEntry("searchMatch",flag);
    flag=kdeButton->isChecked();
    gs.writeEntry("kdeSpecific",flag);

}

void RoughTransDlg::slotUser1()
{
    configWidget->setEnabled(false);
    enableButton(User1,false);
    enableButton(Close,false);
    enableButton(User2,true);
    enableButton(User3,true);

    active=true;
    stop=false;
    cancel=false;

    exactTransCounter=0;
    partTransCounter=0;
    totalTried=0;

    QTimer::singleShot(0,this, SLOT(translate()));
}

void RoughTransDlg::translate()
{
    bool markFuzzy = markFuzzyButton->isChecked();
    bool translated = transButton->isChecked();
    bool untranslated = untransButton->isChecked();
    bool fuzzies = fuzzyButton->isChecked();
    bool kdeSpecific=kdeButton->isChecked();

    int total=catalog->numberOfEntries();
    progressbar->setRange( 0, total );

    QStringList dictList = dictChooser->selectedDicts();

    catalog->applyBeginCommand(0,Msgstr,0);

    bool singleWords=singleWordButton->isChecked();
    bool fuzzyMatch=fuzzyMatchButton->isChecked();
    bool searchMatch=searchMatchButton->isChecked();
    QRegExp contextReg=catalog->miscSettings().contextInfo;
    QChar accelMarker=catalog->miscSettings().accelMarker;
    QRegExp endPunctReg("[\\.?!: ]+$");


    for(int i = 0; i < total; i++)
    {
        progressbar->setValue( i + 1 );
        qApp->processEvents(QEventLoop::AllEvents, 100);

        if(stop || cancel) break;

        // FIXME: should care about plural forms
	QString msg=catalog->msgid(i,true).first();
        const QString context = catalog->msgctxt(i);
        QString translation;

        // this is KDE specific:
	if( kdeSpecific )
	{
	    if( catalog->pluralForm(i) == NoPluralForm )
	    {
		QString origTrans = catalog->msgstr(i).first();
    		if( msg.indexOf( "_: NAME OF TRANSLATORS\\n" ) == 0 || context == "NAME OF TRANSLATORS" )
    		{
		    QString authorName;
		    if( !catalog->identitySettings().authorLocalizedName.isEmpty() )
			authorName = catalog->identitySettings().authorLocalizedName;
		    else // fallback to non-localized name
			if( !catalog->identitySettings().authorName.isEmpty() )
			    authorName = catalog->identitySettings().authorName;
			else continue; // there is no name to be inserted

		    if( !origTrans.split( ',', QString::SkipEmptyParts ).contains( authorName ) )
		    {
        		if(origTrans.isEmpty() ) translation=authorName;
			else translation+=origTrans+','+authorName;
		    }
    		}
    		else if( msg.indexOf( "_: EMAIL OF TRANSLATORS\\n" ) == 0 || context == "EMAIL OF TRANSLATORS" )
    		{
		    // skip, if email is not specified in settings
		    if( catalog->identitySettings().authorEmail.isEmpty() ) continue;

		    if( !origTrans.split( ',', QString::SkipEmptyParts ).contains( catalog->identitySettings().authorEmail ) )
		    {
			if(origTrans.isEmpty() ) translation=catalog->identitySettings().authorEmail;
			else translation=origTrans+','+catalog->identitySettings().authorEmail;
		    }
    		}
                else if ( msg.indexOf("ROLES_OF_TRANSLATORS") == 0 )
                {
                  QString temp = "<othercredit role=\\\"translator\\\">\n<firstname></firstname>"
                    "<surname></surname>\n<affiliation><address><email>" +
                    catalog->identitySettings( ).authorEmail+"</email></address>\n"
                    "</affiliation><contrib></contrib></othercredit>";
                  if (origTrans.isEmpty( ))
                    translation = temp;
                  else if ( origTrans.indexOf( catalog->identitySettings().authorEmail ) < 0 )
                    translation = origTrans + '\n' + temp;
                }
                else if ( msg.indexOf( "CREDIT_FOR_TRANSLATORS" ) == 0 )
                {
                  QString authorName;
                  if (!catalog->identitySettings( ).authorLocalizedName.isEmpty( ))
                    authorName = catalog->identitySettings( ).authorLocalizedName;
                  else if (!catalog->identitySettings( ).authorName.isEmpty( ))
                    authorName = catalog->identitySettings( ).authorName;
                  QString temp = "<para>" + authorName + '\n' + "<email>" +
                    catalog->identitySettings( ).authorEmail + "</email></para>";
                  if (origTrans.isEmpty( ))
                    translation = temp;
                  else if ( origTrans.indexOf( authorName ) < 0 &&
                            origTrans.indexOf( catalog->identitySettings().authorEmail ) < 0 )
                    translation = origTrans + '\n' + temp;
                }
	    }
	}
	else  // not kdeSpecific
	{
	    // skip KDE specific texts
	    if( msg.indexOf( "_: EMAIL OF TRANSLATORS\\n" ) == 0 || msg.indexOf( "_: NAME OF TRANSLATORS\\n" ) == 0 ||
                msg.indexOf( "ROLES_OF_TRANSLATORS" ) == 0 || msg.indexOf( "CREDIT_FOR_TRANSLATORS" ) == 0 ||
                context == "NAME OF TRANSLATORS" || context == "EMAIL OF TRANSLATORS" )
		continue;
	}

	if( translation.isEmpty() ) // KDE-specific translation didn't work
	{
		if( !untranslated && catalog->isUntranslated(i) ) continue;
		if( !translated && !catalog->isUntranslated(i) && !catalog->isFuzzy(i) ) continue;
		if( !fuzzies && catalog->isFuzzy(i) ) continue;
	}

	totalTried++;

        if(msg.contains(contextReg))
        {
            msg.replace(contextReg,"");
        }

	// try exact translation
	QStringList::Iterator dit = dictList.begin();
	while(translation.isEmpty() && dit != dictList.end())
	  {
	    dictBox->setActiveModule(*dit);
	    translation = dictBox->translate(msg);

	    ++dit;
	  }

	if(!translation.isEmpty())
	  {
	    exactTransCounter++;
	  }

	// try search settings translation
	else if (searchMatch) {
	  QString tr;
	  int score, best_score = 0;
	  dit = dictList.begin();
	  while(dit != dictList.end())
	    {
	      dictBox->setActiveModule(*dit);
	      tr = dictBox->searchTranslation(msg,score);
	      kDebug() << "Found: " << tr << ", score " << score;

	      if (score > best_score) {
	        kDebug() << "Best score";
		translation = tr;
		best_score = score;
	      }

	      ++dit;
	    }

	  if(!translation.isEmpty())
	    {
	      partTransCounter++;
	    }
	}

	// try fuzzy translation
	else if (fuzzyMatch) {
	  QString tr;
	  int score, best_score = 0;
	  dit = dictList.begin();
	  while(dit != dictList.end())
	    {
	      dictBox->setActiveModule(*dit);
	      tr = dictBox->fuzzyTranslation(msg,score);

	      if (score > best_score) {
		translation = tr;
		best_score = score;
	      }

	      ++dit;
	    }

	  if(!translation.isEmpty())
	    {
	      partTransCounter++;
	    }
	}

	kDebug() << "Best translation so far: " << translation;

	// try single word translation
        if(translation.isEmpty() && singleWords)
	  {
            QStringList wordList;
            QChar accel;
            QString endingPunctuation;
            int pos = msg.lastIndexOf( endPunctReg );
            if(pos >= 0)
	      {
                endingPunctuation = msg.right(msg.length()-pos);
	      }

            msg=msg.simplified();
            msg=msg.trimmed();


            RegExpExtractor te(catalog->tagSettings().tagExpressions);
	    te.setString(msg);
            msg=te.matchesReplaced(" KBABELTAG ");

            QString word;
            int length = msg.length();
            QRegExp digitReg("^[0-9]*$");
            for(int index=0; index < length; index++)
            {
                QChar c=msg[index];

                if(c==accelMarker)
                {
                    index++;
                    if(index < length)
                    {
                        if(msg[index].isLetterOrNumber())
                        {
                            word+=msg[index];
                            accel=msg[index];
                        }
                        else if(!word.isEmpty() )
                        {
                            if(!word.contains(digitReg))
                                wordList.append(word);

                            word.clear();
                        }
                    }
                    else if(!word.isEmpty())
                    {
                        if(!word.contains(digitReg))
                            wordList.append(word);

                        word.clear();
                    }

                }
                else if(c.isLetterOrNumber())
                {
                    word+=c;
                }
                else if(c == '\\')
                {
                    if(index < length-2)
                    {
                        if(msg[index+1]=='n' && msg[index+2].isSpace())
                        {
                            if(!word.isEmpty() && !word.contains(digitReg))
                                wordList.append(word);

                            word.clear();

                            wordList.append("\\n\n");
                            index+=2;
                        }
                        else if(!word.isEmpty() )
                        {
                            if(!word.contains(digitReg))
                                wordList.append(word);

                            word.clear();
                        }
                    }
                    else if(!word.isEmpty())
                    {
                        if(!word.contains(digitReg))
                            wordList.append(word);

                        word.clear();
                    }
                }
                else if(!word.isEmpty())
                {
                    if(!word.contains(digitReg)) {
                        wordList.append(word);
		    }

                    word.clear();
                }
            }

	    // handle the last word as well
	    if( !word.isEmpty() ) wordList.append(word);

            dit = dictList.begin();
            int wordCounter=0;
            while(wordCounter==0 && dit != dictList.end())
            {
                dictBox->setActiveModule(*dit);

                for(QStringList::Iterator it=wordList.begin();
                        it!=wordList.end(); ++it)
                {
                    if( (*it)=="\\n\n" )
                    {
                        translation+="\\n\n";
                    }
                    else if( (*it)=="KBABELTAG" )
                    {
                        translation+=te.nextMatch();
                    }
                    else
                    {
                        QString trans = dictBox->translate(*it);

                        if(!trans.isEmpty())
                        {
                            wordCounter++;
                            if(!translation.isEmpty())
                            {
                                translation += ' ';
                            }
                            translation += trans;
                        }
                    }
                }

                if(wordCounter==0)
                    translation.clear();

                ++dit;
            }

            if(!translation.isEmpty())
            {
                partTransCounter++;
                // try to set the correct keyboard accelerator
                if(!accel.isNull())
                {
                    int index = translation.indexOf( accel, Qt::CaseInsensitive );
                    if(index >= 0)
                    {
                        translation.insert(index,accelMarker);
                    }
                }

                translation+=endingPunctuation;
            }
        }

	// this is KDE specific:
	if(kdeSpecific && translation.isEmpty())
	{
	    if( msg.startsWith("Name=") ) {
		translation="Name=";
		partTransCounter++;
	    }
	    if( msg.startsWith("Comment=") ) {
		translation="Comment=";
		partTransCounter++;
	    }
        }

        if(!translation.isEmpty())
        {
            if(!catalog->isUntranslated(i))
            {
		QStringList msgs = catalog->msgstr(i);
		uint counter = 0;
		for( QStringList::Iterator it = msgs.begin() ; it != msgs.end() ; ++it)
		{
            	    DelTextCmd* delCmd = new DelTextCmd(0
                        ,(*it),counter++);
            	    delCmd->setPart(Msgstr);
            	    delCmd->setIndex(i);
            	    catalog->applyEditCommand(delCmd,0);
		}
            }

	    for( int count=0; count < catalog->numberOfPluralForms(i) ; count++ )
	    {
		InsTextCmd* insCmd = new InsTextCmd(0,translation,count);
        	insCmd->setPart(Msgstr);
        	insCmd->setIndex(i);
        	catalog->applyEditCommand(insCmd,0);
	    }

            if(markFuzzy)
            {
                catalog->setFuzzy(i,true);
            }
        }
    }

    catalog->applyEndCommand(0,Msgstr,0);

    if(stop || cancel)
    {
        if(cancel)
        {
            catalog->undo();
        }
        else
        {
            msgButtonClicked(0);
        }
        progressbar->setValue( 0 );
        configWidget->setEnabled(true);
        active = false;

        enableButton(User1,true);
        enableButton(Close,true);
        enableButton(User2,false);
        enableButton(User3,false);

        return;
    }

    showStatistics();
}

void RoughTransDlg::showStatistics()
{
    int nothing=totalTried-partTransCounter-exactTransCounter;
    KLocale *locale = KGlobal::locale();
#ifdef __GNUC__
#warning "kde4: verify this code !"
#endif    
#if 0
	QString statMsg = ki18n("Result of the translation:\n"
            "Edited entries: %1\n"
            "Exact translations: %2 (%3%)\n"
            "Approximate translations: %4 (%5%)\n"
            "Nothing found: %6 (%7%)")
            .subs(tt)
            .subs(etc).subs(((double)(10000*etc/tt))/100)
            .subs(ptc).subs(((double)(10000*ptc/tt))/100)
            .subs(nothing).subs(((double)(10000*nothing/tt))/100)
            .toString();

    KMessageBox::information(this, statMsg
            , i18n("Rough Translation Statistics"));
#endif
    dictChooser->restoreConfig();
    accept();
}

void RoughTransDlg::slotClose()
{
    if(active)
    {
        cancel = true;
        return;
    }
    else
    {
       dictChooser->restoreConfig();
       accept();
    }
}

void RoughTransDlg::slotUser2()
{
    stop=true;
}

void RoughTransDlg::slotUser3()
{
    cancel=true;
}

void RoughTransDlg::msgButtonClicked(int id)
{
    if(whatGroup->checkedId() == -1)
    {
        whatGroup->button(id)->setChecked(true);
    }

    progressbar->setRange( 0, catalog->numberOfEntries() );

    enableButton(User1,catalog->numberOfEntries());
}

void RoughTransDlg::fuzzyButtonToggled(bool on)
{
    if(!on)
    {
        QString msg=i18n("<qt><p>"
          "When a translation for a message is found, the entry "
          "will be marked <b>fuzzy</b> by default. This is because the "
          "translation is just guessed by KBabel and you should always "
          "check the results carefully. Deactivate this option only if "
          "you know what you are doing.</p></qt>");

        KMessageBox::information(this, msg, QString(),"MarkFuzzyWarningInRoughTransDlg");
    }
}

void RoughTransDlg::statistics(int &total, int& exact, int& part) const
{
    total = totalTried;
    exact = exactTransCounter;
    part = partTransCounter;
}

#include "roughtransdlg.moc"
