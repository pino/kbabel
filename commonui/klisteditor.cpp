/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2006 by Beaulen Tim <tbscope@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "klisteditor.h"

#include <QWidget>


KListEditor::KListEditor( QWidget *parent ) : QWidget( parent )
{
    setupUi( this );
    layout()->setMargin( 0 );

    connect( _addButton, SIGNAL( pressed() ), this, SLOT( addToList() ) );
    connect( _removeButton, SIGNAL( pressed() ), this, SLOT( removeFromList() ) );
    connect( _upButton, SIGNAL( pressed() ), this, SLOT( upInList() ) );
    connect( _downButton, SIGNAL( pressed() ), this, SLOT( downInList() ) );
    connect( _edit, SIGNAL( returnPressed() ), this, SLOT( updateList() ) );
    connect( _edit, SIGNAL( textChanged( const QString& ) ), this, SLOT( editChanged( const QString& ) ) );
    connect( _list, SIGNAL( highlighted( int ) ), this, SLOT( updateButtons( int ) ) );
    connect( _list, SIGNAL( highlighted( const QString& ) ), _edit, SLOT( setText( const QString& ) ) );
}

KListEditor::~KListEditor()
{
}

void KListEditor::addToList()
{
    _list->insertItem( _edit->text() );
    _edit->clear();
    _removeButton->setEnabled( true );

    emit itemsChanged();
}

void KListEditor::downInList()
{
    int i = _list->currentItem();

    if( i < (int)_list->count() - 1 ) {
        QString ci = _list->currentText();
        _list->removeItem( i );
        _list->insertItem( ci, i + 1 );
        _list->setCurrentItem( i + 1 );
    }

    emit itemsChanged();
}

void KListEditor::removeFromList()
{
    _list->removeItem( _list->currentItem() );

    if( _list->count() == 0 ) 
        _edit->clear();

    _removeButton->setEnabled( _list->count() > 0 );

    emit itemsChanged();
}

void KListEditor::upInList()
{
    int i = _list->currentItem();

    if( i > 0 ) {
        QString ci = _list->currentText();
        _list->removeItem( i );
        _list->insertItem( ci, i - 1 );
        _list->setCurrentItem( i - 1 );
    }

    emit itemsChanged();
}

void KListEditor::updateButtons( int newIndex )
{
    _upButton->setEnabled( newIndex > 0 );
    _downButton->setEnabled( newIndex + 1 != (int)_list->count() );
    _removeButton->setEnabled( true );
}

void KListEditor::updateList()
{
    int i = _list->currentItem();

    if( i == -1 )
        addToList();
    else 
        _list->changeItem( _edit->text(), i );
}

void KListEditor::setList( const QStringList& contents )
{
    _list->clear();
    _list->insertStringList( contents );
    _list->setCurrentItem( 0 );
    _removeButton->setEnabled( !contents.isEmpty() );
}


void KListEditor::editChanged( const QString &s )
{
    _addButton->setEnabled( !s.isEmpty() );
}


void KListEditor::setTitle( const QString &s )
{
#ifdef __GNUC__
#warning FIXME Readd groupbox in designer
#endif	
    //setTitle( s );
}


QStringList KListEditor::list()
{
    QStringList result;

    for( uint i = 0; i < _list->count() ; i++ )
        result.append( _list->text( i ) );

    return result;
}

#include "klisteditor.moc"


