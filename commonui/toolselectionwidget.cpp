/* This file is part of KBabel
   Copyright (C) 2002 Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

*/

#include "toolselectionwidget.h"

#include "toolaction.h"

#include <kdatatool.h>
#include <kdebug.h>

#include <QListWidget>

ToolSelectionWidget::ToolSelectionWidget( QWidget * parent )
    : KActionSelector( parent )
{
}

void ToolSelectionWidget::loadTools( const QStringList &commands, 
    const QList<KDataToolInfo>& tools)
{
    if ( tools.isEmpty() ) return;
    
    _allTools = tools;
	
    foreach( const KDataToolInfo &info, tools )
    {
	QStringList userCommands = info.userCommands();
	QStringList toolCommands = info.commands();
	Q_ASSERT(!toolCommands.isEmpty());
	if ( toolCommands.count() != userCommands.count() )
	        kWarning() << "KDataTool desktop file error (" << info.service()
	        << "). " << toolCommands.count() << " commands and "
	        << userCommands.count() << " descriptions.";
	
	QStringList::ConstIterator uit = userCommands.begin();
	QStringList::ConstIterator cit = toolCommands.begin();
	for (; uit != userCommands.end() && cit != toolCommands.end(); ++uit, ++cit )
	{
	    if( commands.contains(*cit) )
	    {
		availableListWidget()->addItem( *uit );
	    }
        }
    }
}

void ToolSelectionWidget::setSelectedTools( const QStringList& tools )
{
    availableListWidget()->clear();
    selectedListWidget()->clear();
    foreach( const KDataToolInfo &info, _allTools )
    {
	QString uic = info.userCommands().at( info.commands().indexOf( "validate" ) );
	if( ToolAction::findTool( tools, info ) )
	    selectedListWidget()->addItem( uic );
	else
	    availableListWidget()->addItem( uic );
    }
}

QStringList ToolSelectionWidget::selectedTools()
{
    QStringList usedNames;
    for( int i=0; i<selectedListWidget()->count() ; i++ )
	usedNames += selectedListWidget()->item(i)->text();
	
    QStringList result;
    foreach( const KDataToolInfo &info, _allTools )
    {
	if( usedNames.contains( info.userCommands().at( info.commands().indexOf( "validate" ) ) ) )
	{
	    const QString id = info.service()->property( "X-KBabel-Id", QVariant::String ).toString();
	    if ( !id.isEmpty() )
		result += id;
	    else
	    {
		result += info.service()->library();
		if ( !info.service()->pluginKeyword().isEmpty() )
		    result.last().append( "#" + info.service()->pluginKeyword() );
	    }
	}
    }
    return result;
}

#include "toolselectionwidget.moc"
