/* This file is part of KBabel
   Copyright (C) 2002 Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
    you do not wish to do so, delete this exception statement from
   your version.

*/

#include "toolaction.h"

#include <kactioncollection.h>
#include <kdebug.h>
#include <kicon.h>

ToolAction::ToolAction( const QString & text, const KShortcut& cut, const KDataToolInfo & info, const QString & command,
                        QObject * parent )
    : KAction( KIcon( info.iconName() ), text, parent ),
      m_command( command ),
      m_info( info )
{
    setShortcut( cut );
    connect( this, SIGNAL( triggered( bool ) ), this, SLOT( slotActivated() ) );
}

void ToolAction::slotActivated()
{
    emit toolActivated( m_info, m_command );
}

QList<KAction *> ToolAction::dataToolActionList( const QList<KDataToolInfo> & tools, const QObject *receiver, const char* slot, const QStringList& command, bool excludeCommand, KActionCollection* parent, const QString& namePrefix )
{
    QList<KAction *> actionList;
    if ( tools.isEmpty() )
	return actionList;

    foreach( const KDataToolInfo &info, tools )
    {
	QStringList userCommands = info.userCommands();
	QStringList commands = info.commands();
	QStringList shortcuts = info.service()->property("Shortcuts").toStringList();
	Q_ASSERT(!commands.isEmpty());
	if ( commands.count() != userCommands.count() )
	        kWarning() << "KDataTool desktop file error (" << info.service()
	        << "). " << commands.count() << " commands and "
	        << userCommands.count() << " descriptions.";

	QStringList::ConstIterator uit = userCommands.begin();
	QStringList::ConstIterator cit = commands.begin();
	QStringList::ConstIterator sit = shortcuts.begin();
	for (; uit != userCommands.end() && cit != commands.end(); ++uit, ++cit)
	{
	    if( !excludeCommand == command.contains(*cit) )
	    {
		//FIXME: Qt4 porting, it might be something else causing a crash if not for this one:
                QString sc;
		if(shortcuts.size() !=0)
		    sc=*sit;
		ToolAction * action = new ToolAction( *uit, KShortcut(sc.isEmpty()?QString::null:sc), info, *cit, parent );	//krazy:exclude=nullstrassign for old broken gcc
                parent->addAction( QString(namePrefix+info.service()->library()+'_'+(*cit)).toUtf8(), action );
		connect( action, SIGNAL( toolActivated( const KDataToolInfo &, const QString & ) ),
                     receiver, slot );

        	actionList.append( action );
	    }
	    if( sit != shortcuts.end() ) sit++;
        }
    }

    return actionList;
}

QList<KDataToolInfo> ToolAction::validationTools()
{
    QList<KDataToolInfo> result;

    const QList<KDataToolInfo> tools = KDataToolInfo::query("CatalogItem", "application/x-kbabel-catalogitem", KGlobal::mainComponent());

    foreach( const KDataToolInfo &info, tools )
    {
	if( info.commands().contains("validate") )
	{
	    result.append( info );
	}
    }

    return result;
}

bool ToolAction::findTool( const QStringList& tools, const KDataToolInfo &info )
{
    foreach ( const QString &tool, tools )
    {
	const int index = tool.indexOf( '#' );
	if ( index < 0 )
	{
	    if ( tool == info.service()->property( "X-KBabel-Id", QVariant::String ).toString()
	         || tool == info.service()->library() )
		return true;
	}
	else
	{
	    if ( tool.left( index ) == info.service()->library()
	         && tool.mid( index + 1 ) == info.service()->pluginKeyword() )
		return true;
	}
    }
    return false;
}

#include "toolaction.moc"
