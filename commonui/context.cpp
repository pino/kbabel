/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2002-2005 by Stanislav Visnovsky <visnovsky@kde.org>
  Copyright (C) 2005,2006 by Nicolas GOUTTE <goutte@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "context.h"
#include "klisteditor.h"

#include <qcombobox.h>
#include <qfileinfo.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qlineedit.h>
#include <q3listbox.h>
#include <qpushbutton.h>
#include <qregexp.h>
#include <QDir>
//#include <qvgroupbox.h>

#include <QVBoxLayout>
#include <Q3ValueList>

#include <kconfig.h>
#include <kdebug.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kurl.h>
#include <kdeversion.h>
#include <kio/netaccess.h>

#include <ktexteditor/cursor.h>
#include <ktexteditor/range.h>
#include <ktexteditor/document.h>
#include <ktexteditor/editor.h>
#include <ktexteditor/view.h>
#include <ktexteditor/editorchooser.h>

SourceContext::SourceContext(QWidget *parent, KBabel::Project::Ptr project): QWidget(parent)
    , m_parent( parent )
    , m_doc(0)
    , m_view(0)
    , _referenceCombo(0)
    , _layout(0)
    , _project(project)
{
    _referenceList.clear();
    _referenceCombo = new QComboBox( this );
    connect( _referenceCombo, SIGNAL(activated(int)), this, SLOT(updateToSelected(int)));

    _layout= new QVBoxLayout(this);
    _layout->addWidget(_referenceCombo);
}

void SourceContext::setContext( const QString& packageDir, const QString& packageName, const QString& gettextComment, const KUrl& urlPoFile )
{
    if( !m_doc && !loadPart() ) return;
    _referenceCombo->clear();
    _referenceList.clear();
    _referenceList = resolvePath( packageDir, packageName, gettextComment, urlPoFile );

    for( Q3ValueList<ContextInfo>::const_iterator it = _referenceList.constBegin(); it != _referenceList.constEnd(); ++it )
        _referenceCombo->addItem( (*it).path );

    _referenceCombo->setEnabled( !_referenceList.isEmpty() );

    if( _referenceList.isEmpty() )
    {
        m_doc->setReadWrite( true );
        // We have to simulate a new document (like with File/New) by using openStream and closeStream
        m_doc->openStream( "text/plain", KUrl( "kbabel:error" ) ); // KBabel does not show the URL kbabel:error
        m_doc->closeStream();
        // KDE4: verify that the following line works
        m_doc->setText( i18n("Corresponding source file not found") );
        m_doc->setReadWrite(false);
	m_doc->setModified(false);
    }
    else
    {
	_referenceCombo->setCurrentIndex(0);
	updateToSelected(0);
    }
}

void SourceContext::updateToSelected(int index)
{
    if( !m_doc ) return;
    ContextInfo ci = *(_referenceList.at(index));
    KUrl newUrl( KUrl( ci.path ) );
    if( m_doc->url() != newUrl )
    {
        m_doc->setReadWrite( true );
        m_doc->openUrl( newUrl );
    }
    m_doc->setReadWrite( false );
    // ### KDE4: verify that the following 4 lines work
    KTextEditor::Cursor cursor( ci.line, 0 );
    m_view->setCursorPosition( cursor );
    KTextEditor::Range range( ci.line-1, 0, ci.line, 0 );
    m_view->setSelection( range );
}

Q3ValueList<ContextInfo> SourceContext::resolvePath( const QString& packageDir, const QString& packageName, const QString& gettextComment, const KUrl& urlPoFile )
{
    //kDebug() << "GETTEXTCOMMENT:" << gettextComment;

    // Find the directory name of the PO file, if the PO file is local
    // ### TODO: find a way to allow remote files too
    QString poDir;
    const KUrl localUrl( KIO::NetAccess::mostLocalUrl( urlPoFile, m_parent ) );
    if ( localUrl.isLocalFile() )
    {
        const QFileInfo fi( localUrl.path() );
        poDir = fi.dir().absolutePath();
    }
#if 0
    kDebug() << "CONTEXT VARIABLE START";
    kDebug() << "@CODEROOT@: " << _project->settings()->codeRoot();
    kDebug() << "@PACKAGEDIR@: " << packageDir ;
    kDebug() << "@PACKAGE@: " << packageName;
    kDebug() << "@POFILEDIR@: " << poDir ;
    kDebug() << "CONTEXT VARIABLE END";
#endif

    QStringList prefixes;
    const QStringList paths = _project->settings()->paths();

    for( QStringList::const_iterator it = paths.constBegin(); it!=paths.constEnd() ; ++it )
    {
	QString pref = (*it);

        if ( !poDir.isEmpty() )
        {
            pref.replace( "@POFILEDIR@", poDir );
        }
        else if ( pref.indexOf( "@POFILEDIR@ " ) != -1 )
            continue; // No need to keep this path pattern, as we have no PO file dir

        pref.replace( "@PACKAGEDIR@", packageDir);
        pref.replace( "@PACKAGE@", packageName);
	pref.replace( "@CODEROOT@", _project->settings()->codeRoot());
	prefixes.append(pref);
    }

    Q3ValueList<ContextInfo> rawRefList; // raw references
    QRegExp re("^\\s*(.+):(\\d+)\\s*$"); // Reg. exp. for Gettext references
    QRegExp rex( "^#. i18n: file (.+) line (\\d+)\\s*$" ); //Reg. exp. for KDE extractrc/extractattr references
    QRegExp res( "^# [Ff]ile: (.+), line(?: number)?: (\\d+)\\s*$"); // Reg. exp. for "strict" PO format
    const QStringList lines = gettextComment.split( "\n", QString::SkipEmptyParts );
    for ( QStringList::const_iterator it = lines.constBegin() ; it != lines.constEnd() ; ++it)
    {
	const QString curLine = (*it).trimmed();
	if( curLine.startsWith( "#:" ) )
        {
            // We have a Gettext line with references
            const QStringList references( curLine.mid( 2 ).split( " ", QString::SkipEmptyParts ) );
            for ( QStringList::const_iterator it = references.constBegin(); it != references.constEnd(); ++it )
            {
                if ( re.exactMatch( (*it) ) )
                {
                    ContextInfo ref;
                    ref.line = re.cap(2).toInt();
                    ref.path = re.cap(1);
                    // ### TODO KDE4: perhaps we should not do the replace if compiled for Windows
                    ref.path.replace( QChar( '\\' ), QChar( '/' ) );
                    rawRefList.append( ref );
                }
            }

        }
        else if ( curLine.startsWith( "#," ) )
        {
            // We have a Gettext option line. There is no source reference here.
            continue;
        }
        else if ( curLine.startsWith( "#. i18n:") )
        {
            // We might have a KDE reference from extractrc/extractattr
            if ( rex.exactMatch( (*it) ) )
            {
                ContextInfo ref;
                ref.line = rex.cap(2).toInt();
                ref.path = rex.cap(1);
                // KDE is not extracted on Windows, so no backslash conversion is needed.
                rawRefList.append( ref );
            }
        }
        else if ( curLine.startsWith( "# F" ) || curLine.startsWith( "# f" ) )
        {
            // We might have a "strict PO" reference
            if ( res.exactMatch( (*it) ) )
            {
                ContextInfo ref;
                ref.line = res.cap(2).toInt();
                ref.path = res.cap(1);
                // ### TODO KDE4: perhaps we should not do the replace if compiled for Windows
                ref.path.replace( QChar( '\\' ), QChar( '/' ) );
                rawRefList.append( ref );
            }
        }
        else
            continue;
    }

    // Now that we have gathered the references, we need to convert them to absolute paths
    Q3ValueList<ContextInfo> results;
    for ( Q3ValueList<ContextInfo>::const_iterator it = rawRefList.constBegin(); it != rawRefList.constEnd(); ++it )
    {
        const int lineNum = (*it).line;
        const QString fileName = (*it).path;
        for ( QStringList::const_iterator it1 = prefixes.constBegin(); it1 != prefixes.constEnd(); ++it1 )
        {
            QString path = (*it1);
            path.replace( "@COMMENTPATH@", fileName);

            //kDebug() << "CONTEXT PATH: " << path; // DEBUG
            QFileInfo pathInfo( path );
            if( pathInfo.exists() )
            {
                ContextInfo ref;
                ref.path = pathInfo.absoluteFilePath();
                ref.line = lineNum;
                results.append(ref);
            }
        }
    }

    return results;
}

bool SourceContext::loadPart()
{
#ifdef __GCC__
# warning "KDE4 KTextEditor API changed, please check the code!"
#endif
    KTextEditor::Editor* ed = KTextEditor::EditorChooser::editor();
    if ( ! ed )
    {
        // No editor! Something gone wrong, so abort!
	KMessageBox::error( this, i18n("KBabel cannot create an editor from "
            "a text editor component for the source context view.\n"
	    "Please check your KDE installation." ) );
	m_doc = 0;
	m_view = 0;
	return false;
    }
    m_doc = ed->createDocument( ed );
    if ( ! m_doc )
    {
        // No editor document! Something gone wrong, so abort!
	KMessageBox::error( this, i18n("KBabel cannot create a document "
	    "for the source context view." ) );
        delete ed;
	m_doc = 0;
	m_view = 0;
	return false;
    }
    m_view = qobject_cast<KTextEditor::View*> (m_doc->createView( this ) );
    if ( ! m_view )
    {
	KMessageBox::error( this, i18n("KBabel cannot create a view "
            "for the source context view.\n"
            "Perhaps the text editor component does not support creating views."
            ) );
        delete m_doc;
        delete ed;
	m_doc = 0;
	m_view = 0;
	return false;
    }
    _layout->addWidget( m_view, 1);
    m_view->show();

    return true;
}

void SourceContext::setProject( KBabel::Project::Ptr project )
{
    _project = project;
}

#include "context.moc"

// kate: space-indent on; indent-width 4; replace-tabs on;
