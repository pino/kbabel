/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2001-2005 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "klisteditor.h"
#include "toolselectionwidget.h"
#include "projectprefwidgets.h"
#include "resources.h"
#include "kbabeldictbox.h"
#include "toolaction.h"
#include "cmdedit.h"

#include <kcombobox.h>
#include <kdatatool.h>
#include <klocale.h>
#include <kdialog.h>
#include <kfiledialog.h>
#include <knuminput.h>
#include <kmessagebox.h>
#include <klineedit.h>
#include <kurlcompletion.h>
#include <kfontdialog.h>
#include <kcolorbutton.h>
#include <kparts/componentfactory.h>
#include <kregexpeditorinterface.h>
#include <k3sconfig.h>
#include <kurl.h>
#include <kurlrequester.h>
#include <kservicetypetrader.h>
#include <qlayout.h>
#include <qobject.h>
#include <qlabel.h>

#include <qlineedit.h>
#include <qcheckbox.h>
#include <q3groupbox.h>
#include <qpushbutton.h>
#include <qcombobox.h>
#include <qradiobutton.h>
#include <qspinbox.h>
#include <qtextcodec.h>

#include <QEvent>
#include <QKeyEvent>
#include <QDropEvent>
#include <QVBoxLayout>
#include <Q3HButtonGroup>
#include <Q3VButtonGroup>
#include <kvbox.h>

#include <ui_catmanpreferences.h>
#include <ui_identitypreferences.h>
#include <ui_viewpreferences.h>

using namespace KBabel;

static QSize sizeHintForWidget(const QWidget* widget)
{
  //
  // The size is computed by adding the sizeHint().height() of all
  // widget children and taking the width of the widest child and adding
  // layout()->margin() and layout()->spacing()
  //

  QSize size;

  int numChild = 0;
  QObjectList l = widget->children();

  for( int i=0; i < l.count(); i++ )
  {
    QObject *o = l.at(i);
    if( o->isWidgetType() )
    {
      numChild += 1;
      QWidget *w=((QWidget*)o);

      QSize s = w->sizeHint();
      if( s.isEmpty() == true )
      {
          s = QSize( 50, 100 ); // Default size
      }
      size.setHeight( size.height() + s.height() );
      if( s.width() > size.width() ) { size.setWidth( s.width() ); }
    }
  }

  if( numChild > 0 )
  {
    size.setHeight( size.height() + widget->layout()->spacing()*(numChild-1) );
    size += QSize( widget->layout()->margin()*2, widget->layout()->margin()*2 + 1 );
  }
  else
  {
    size = QSize( 1, 1 );
  }

  return( size );
}




SavePreferences::SavePreferences(QWidget *parent)
    :  QTabWidget(parent)
{
    QWidget* page = new QWidget(this);
    QVBoxLayout* layout=new QVBoxLayout(page);

    Q3GroupBox* box=new Q3GroupBox(1,Qt::Horizontal,page);
    layout->addWidget(box);

    _updateButton = new QCheckBox( i18n("&Update header when saving"), box );
    _updateButton->setObjectName( "kcfg_AutoUpdate" );
    _descriptionButton = new QCheckBox( i18n("Update &description comment when saving"), box );
    _descriptionButton->setObjectName( "kcfg_UpdateDescription" );
    _autoCheckButton = new QCheckBox( i18n("Chec&k syntax of file when saving"), box );
    _autoCheckButton->setObjectName( "kcfg_AutoSyntaxCheck" );
    _saveObsoleteButton = new QCheckBox( i18n("Save &obsolete entries"), box );
    _saveObsoleteButton->setObjectName( "kcfg_SaveObsolete" );

    Q3GroupBox* descBox=new Q3GroupBox(1,Qt::Horizontal,i18n("De&scription"),page);
    layout->addWidget(descBox);

    _descriptionEdit = new QLineEdit( descBox );
    _descriptionEdit->setObjectName( "kcfg_DescriptionString" );

    Q3GroupBox* encodingBox = new Q3GroupBox(1,Qt::Horizontal,i18n("Encoding")
                                  ,page);
    layout->addWidget(encodingBox);
    KHBox *b = new KHBox(encodingBox);

    QLabel* tempLabel = new QLabel( i18n("Default:"), b );
    _encodingBox = new QComboBox( b );
    _encodingBox->setObjectName( "kcfg_UseOldEncoding" );
    b->setStretchFactor(_encodingBox,2);

    QString defaultName=charsetString(ProjectSettingsBase::Locale);
    defaultName+=' '+i18n("(default)");
    QString utf8Name=charsetString(ProjectSettingsBase::UTF8);
    QString utf16Name=charsetString(ProjectSettingsBase::UTF16);

    _encodingBox->insertItem( (int)ProjectSettingsBase::Locale, defaultName );
    _encodingBox->insertItem( (int)ProjectSettingsBase::UTF8, utf8Name );

    // KBabel seems to crash somehow, when saving in utf16, so
    // it's better to disable this, since it is useless anyway
    // at the moment
    //_encodingBox->insertItem(utf16Name,(int)UTF16);

    tempLabel->setBuddy(_encodingBox);

    _oldEncodingButton = new QCheckBox( i18n("Kee&p the encoding of the file"), encodingBox );
    _oldEncodingButton->setObjectName( "kcfg_Encoding" );

    _autoSaveBox = new Q3GroupBox( 1, Qt::Horizontal, i18n( "Automatic Saving" ), page );
    layout->addWidget( _autoSaveBox );
    _autoSaveDelay = new KIntNumInput( _autoSaveBox);
    _autoSaveDelay->setRange( 0, 60 );
    _autoSaveDelay->setSuffix( i18nc( "Short for minutes", " min" ) );
    _autoSaveDelay->setSpecialValueText( i18n( "No autosave" ) );

    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));
    addTab(page, i18n("&General"));

    page = new QWidget(this);
    layout=new QVBoxLayout(page);

    Q3GroupBox* gridBox = new Q3GroupBox(2,Qt::Horizontal,i18n("Fields to Update"),page);
    layout->addWidget(gridBox);

    _revisionButton = new QCheckBox( i18n("Re&vision-Date"), gridBox );
    _revisionButton->setObjectName( "kcfg_UpdateRevisionDate" );
    _lastButton = new QCheckBox( i18n("Last-&Translator"), gridBox );
    _lastButton->setObjectName( "kcfg_UpdateLastTranslator" );
    _languageButton = new QCheckBox( i18n("&Language"), gridBox );
    _languageButton->setObjectName( "kcfg_UpdateLanguageTeam" );
    _charsetButton = new QCheckBox( i18n("Char&set"), gridBox );
    _charsetButton->setObjectName( "kcfg_UpdateCharset" );
    _encodingButton = new QCheckBox( i18n("&Encoding"), gridBox );
    _encodingButton->setObjectName( "kcfg_UpdateEncoding" );
    _projectButton = new QCheckBox( i18n("Pro&ject"), gridBox );
    _projectButton->setObjectName( "kcfg_UpdateProject" );

    Q3ButtonGroup* dateBox = new Q3ButtonGroup(2,Qt::Horizontal,i18n("Format of Revision-Date"),page, "kcfg_DateFormat");
    layout->addWidget(dateBox);

    // we remove/insert default date button to correctly map Qt::DateFormat to our Ids
    _defaultDateButton = new QRadioButton( i18n("De&fault date format"),dateBox );
    dateBox->remove (_defaultDateButton);
    _localDateButton = new QRadioButton( i18n("Local date fo&rmat"),dateBox );
    dateBox->remove (_localDateButton);
    _customDateButton = new QRadioButton( i18n("Custo&m date format:"),dateBox );

    dateBox->insert (_defaultDateButton);
    dateBox->insert (_localDateButton);

    _dateFormatEdit = new QLineEdit( dateBox );
    _dateFormatEdit->setObjectName( "kcfg_CustomDateFormat" );
    _dateFormatEdit->setEnabled( false );

    connect( _customDateButton, SIGNAL(toggled(bool)), this, SLOT( customDateActivated(bool) ) );

    Q3GroupBox* projectBox = new Q3GroupBox(1,Qt::Horizontal,i18n("Project String")
                                  ,page);
    layout->addWidget(projectBox);
    b = new KHBox(projectBox);

    tempLabel=new QLabel(i18n("Project-Id:"),b);
    _projectEdit = new QLineEdit( b );
    _projectEdit->setObjectName( "kcfg_ProjectString" );
    b->setStretchFactor(_projectEdit,2);
    tempLabel->setBuddy(_projectEdit);

    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));
    addTab(page, i18n("&Header"));

    page = new QWidget(this);
    layout=new QVBoxLayout(page);

    Q3GroupBox* translatorCopyrightBox = new Q3GroupBox(1,Qt::Horizontal, page);
    _translatorCopyrightButton = new QCheckBox( i18n("Update &translator copyright"), translatorCopyrightBox );
    _translatorCopyrightButton->setObjectName( "kcfg_UpdateTranslatorCopyright" );
    layout->addWidget(translatorCopyrightBox);

    Q3GroupBox* fsfBox=new Q3ButtonGroup(1,Qt::Horizontal,i18n("Free Software Foundation Copyright"),page, "kcfg_FSFCopyright");
    layout->addWidget(fsfBox);

    _removeFSFButton = new QRadioButton(i18n("&Remove copyright if empty"),fsfBox);
    _updateFSFButton = new QRadioButton(i18n("&Update copyright"),fsfBox);
    _nochangeFSFButton = new QRadioButton(i18n("Do &not change"),fsfBox);

    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));
    addTab(page, i18n("Cop&yright"));

    _updateButton->setWhatsThis(
        i18n("<qt><p><b>Update Header</b></p>\n"
             "<p>Check this button to update the header "
             "information of the file "
             "every time it is saved.</p>\n"
             "<p>The header normally keeps information about "
             "the date and time the file was last\n"
             "updated, the last translator etc.</p>\n"
             "<p>You can choose which information you want to update from the checkboxes below.\n"
             "Fields that do not exist are added to the header.\n"
             "If you want to add additional fields to the header, you can edit the header manually by choosing\n"
             "<b>Edit->Edit Header</b> in the editor window.</p></qt>"));

    gridBox->setWhatsThis(i18n("<qt><p><b>Fields to update</b></p>\n"
 "<p>Choose which fields in the header you want to have updated when saving.\n"
 "If a field does not exist, it is appended to the header.</p>\n"
 "<p>If you want to add other information to the header, you have to edit the header manually\n"
 "by choosing <b>Edit->Edit Header</b> in the editor window.</p>\n"
 "<p>Deactivate <b>Update Header</b> above if you do not want to have the header\n"
 "updated when saving.</p></qt>"));

    encodingBox->setWhatsThis(i18n("<qt><p><b>Encoding</b></p>"
"<p>Choose how to encode characters when saving to a file. If you are unsure "
"what encoding to use, please ask your translation coordinator.</p>"
"<ul><li><b>%1</b>: this is the encoding that fits the character "
"set of your system language.</li>"
"<li><b>%2</b>: uses Unicode (UTF-8) encoding.</li>"
"</ul></qt>", defaultName, utf8Name) );


    _oldEncodingButton->setWhatsThis(i18n("<qt><p><b>Keep the encoding of the file</b></p>"
        "<p>If this option is activated, files are always saved in the "
        "same encoding as they were read in. Files without charset "
        "information in the header (e.g. POT files) are saved in the "
        "encoding set above.</p></qt>"));

    _autoCheckButton->setWhatsThis(i18n("<qt><p><b>Check syntax of file when saving</b></p>\n"
"<p>Check this to automatically check syntax of file with \"msgfmt --statistics\"\n"
"when saving a file. You will only get a message, if an error occurred.</p></qt>"));

    _saveObsoleteButton->setWhatsThis(i18n("<qt><p><b>Save obsolete entries</b></p>\n"
"<p>If this option is activated, obsolete entries found when the file was open\n"
"will be saved back to the file. Obsolete entries are marked by #~ and are\n"
"created when the msgmerge does not need the translation anymore.\n"
"If the text will appear again, the obsolete entries will be activated again.\n"
"The main drawback is the size of the saved file.</p></qt>"));


    dateBox->setWhatsThis(i18n("<qt><p><b>Format of Revision-Date</b></p>"
"<p>Choose in which format the date and time of the header field\n"
"<i>PO-Revision-Date</i> is saved: <ul>\n"
"<li><b>Default</b> is the format normally used in PO files.</li>\n"
"<li><b>Local</b> is the format specific to your country.\n"
"It can be configured in KDE's Control Center.</li>\n"
"<li><b>Custom</b> lets you define your own format.</li></ul></p> "
"<p>It is recommended that you use the default format to avoid creating non-standard PO files.</p>"
"<p>For more information, see section <b>The Preferences Dialog</b> "
"in the online help.</p>"
"</qt>") );

    setMinimumSize(sizeHint());
}


void SavePreferences::defaults(const KBabel::SaveSettings& _settings)
{
   _updateButton->setChecked(_settings.autoUpdate);

   _lastButton->setChecked(_settings.updateLastTranslator);
   _revisionButton->setChecked(_settings.updateRevisionDate);
   _languageButton->setChecked(_settings.updateLanguageTeam);
   _charsetButton->setChecked(_settings.updateCharset);
   _encodingButton->setChecked(_settings.updateEncoding);
   _projectButton->setChecked(_settings.updateProject);

   _encodingBox->setCurrentIndex( _settings.encoding );
   _oldEncodingButton->setChecked(_settings.useOldEncoding);

   _projectEdit->setText(_settings.projectString);

   _descriptionButton->setChecked(_settings.updateDescription);
   _descriptionEdit->setText(_settings.descriptionString);
   _translatorCopyrightButton->setChecked(_settings.updateTranslatorCopyright);

   switch(_settings.FSFCopyright)
   {
      case ProjectSettingsBase::Update:
         _updateFSFButton->setChecked(true);
         break;
      case ProjectSettingsBase::Remove:
         _removeFSFButton->setChecked(true);
         break;
      case ProjectSettingsBase::NoChange:
         _nochangeFSFButton->setChecked(true);
         break;
      case ProjectSettingsBase::RemoveLine:
         break;
   }

   _autoCheckButton->setChecked(_settings.autoSyntaxCheck);
   _saveObsoleteButton->setChecked(_settings.saveObsolete);

   _dateFormatEdit->setText(_settings.customDateFormat);

   switch(_settings.dateFormat)
   {
      case Qt::ISODate:
         _defaultDateButton->setChecked(true);
         break;
      case Qt::LocalDate:
         _localDateButton->setChecked(true);
         break;
      case Qt::TextDate:
         _customDateButton->setChecked(true);
         break;
   }

   _autoSaveDelay->setValue( _settings.autoSaveDelay );
}


void SavePreferences::customDateActivated(bool on)
{
   _dateFormatEdit->setEnabled(on);
   _dateFormatEdit->setFocus();
}

void SavePreferences::setAutoSaveVisible( const bool on )
{
    if( on ) _autoSaveBox->show();
    else _autoSaveBox->hide();
}



IdentityPreferences::IdentityPreferences(QWidget* parent, const QString& project)
         : QWidget(parent)
         , _ui(new Ui::IdentityPreferencesBase)
{
    QWidget* page = this;
    _ui->setupUi(page);
    layout()->setMargin(0);

    if( !project.isEmpty() )
    {
	// show the project name in the widget at the top
	_ui->projectNameLabel->setText(i18n("<font size=\"+1\">Project: %1</font>", project));
    }
    else
    {
	_ui->projectNameLabel->hide();
    }

    connect(_ui->kcfg_LanguageCode,SIGNAL(textChanged(QString)),
            this, SLOT(checkTestPluralButton()));
    connect(_ui->kcfg_PluralForms,SIGNAL(valueChanged(int)),
            this, SLOT(checkTestPluralButton()));
    connect(_ui->testPluralButton, SIGNAL(clicked()),
            this, SLOT(testPluralForm()));
    connect(_ui->testGnuPluralFormButton, SIGNAL(clicked()),
            this, SLOT(lookupGnuPluralFormHeader()));

    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());

    _ui->kcfg_AuthorEmail->installEventFilter(this);
    _ui->kcfg_Mailinglist->installEventFilter(this);
}

IdentityPreferences::~IdentityPreferences()
{
    delete _ui;
}

void IdentityPreferences::defaults(const IdentitySettings& settings)
{
    _ui->kcfg_AuthorName->setText(settings.authorName);
    _ui->kcfg_LocalAuthorName->setText(settings.authorLocalizedName);
    _ui->kcfg_Language->setText(settings.languageName);
    _ui->kcfg_LanguageCode->setText(settings.languageCode);
    _ui->kcfg_Mailinglist->setText(settings.mailingList);
    _ui->kcfg_Timezone->setText(settings.timeZone);
    _ui->kcfg_PluralForms->setValue(settings.numberOfPluralForms);
    _ui->kcfg_PluralFormsHeader->setText(settings.gnuPluralFormHeader);
    _ui->kcfg_CheckPluralArgument->setChecked(settings.checkPluralArgument);
}

bool IdentityPreferences::eventFilter(QObject *o, QEvent *e)
{
    if(e->type() == QEvent::Drop)
    {
        QDropEvent *de = static_cast<QDropEvent*>(e);
        KUrl::List urlList = KUrl::List::fromMimeData( de->mimeData() );
        if(urlList.isEmpty())
        {
            KUrl url(urlList.first());
            if(url.protocol()== "mailto")
            {
                QString mail=url.path();

                bool handled=false;
                if(o == _ui->kcfg_AuthorEmail)
                {
                    handled=true;
                    _ui->kcfg_AuthorEmail->setText(mail);
                }
                else if(o == _ui->kcfg_Mailinglist)
                {
                    handled=true;
                    _ui->kcfg_Mailinglist->setText(mail);
                }

                if(handled)
                    return true;
            }
        }
    }

    return false;
}

void IdentityPreferences::checkTestPluralButton()
{
    int val = _ui->kcfg_PluralForms->value();
    QString lang = _ui->kcfg_LanguageCode->text();

    _ui->testPluralButton->setEnabled(val == 0 && !lang.isEmpty());
}

void IdentityPreferences::testPluralForm()
{
    QString lang = _ui->kcfg_LanguageCode->text();

    if(lang.isEmpty())
    {
        KMessageBox::sorry(this,i18n("Please insert a language code first."));
        return;
    }

    int number=Catalog::getNumberOfPluralForms(lang);

    QString msg;

    if(number < 0)
    {
        msg = i18n("It is not possible to find out the number "
                "of singular/plural forms automatically for the "
                "language code \"%1\".\n"
                "Do you have kdelibs4.po installed for this language?\n"
                "Please set the correct number manually.", lang);
    }
    else
    {
        msg = i18n("The number of singular/plural forms found for "
                "the language code \"%1\" is %2.", lang, number);
    }

    if(!msg.isEmpty())
    {
        KMessageBox::information(this,msg);
    }
}

void IdentityPreferences::lookupGnuPluralFormHeader()
{
    QString lang = _ui->kcfg_LanguageCode->text();

    if(lang.isEmpty())
    {
        KMessageBox::sorry(this,i18n("Please insert a language code first."));
        return;
    }

    QString header=GNUPluralForms(lang);

    if( header.isEmpty() )
    {
	KMessageBox::information(this, i18n("It was not possible to determine "
	"GNU header for plural forms. Maybe your GNU gettext tools are too "
	"old or they do not contain a suggested value for your language.") );
    }
    else
    {
	_ui->kcfg_PluralFormsHeader->setText( header );
    }
}


MiscPreferences::MiscPreferences(QWidget *parent)
                : QWidget(parent), _regExpEditDialog(0)
{
    QWidget* page = this;

    QVBoxLayout* layout=new QVBoxLayout(page);
    layout->setMargin(0);

    Q3GroupBox* box=new Q3GroupBox(1,Qt::Horizontal,page);
    layout->addWidget(box);

    KHBox *hbox = new KHBox(box);

    QLabel *label = new QLabel(i18n("&Marker for keyboard accelerator:"),hbox);
    accelMarkerEdit = new KLineEdit(hbox);
    accelMarkerEdit->setObjectName("kcfg_AccelMarker");
    accelMarkerEdit->setMaxLength(1);
    label->setBuddy(accelMarkerEdit);
    hbox->setStretchFactor(accelMarkerEdit,1);
    QString msg=i18n("<qt><p><b>Marker for keyboard accelerator</b></p>"
        "<p>Define here, what character marks the following "
        "character as keyboard accelerator. For example in Qt it is "
        "'&amp;' and in Gtk it is '_'.</p></qt>");
    label->setWhatsThis(msg);
    accelMarkerEdit->setWhatsThis(msg);


    hbox = new KHBox(box);

    label = new QLabel(i18n("&Regular expression for context information:")
            ,hbox);
    contextInfoEdit = new KLineEdit(hbox);
    contextInfoEdit->setObjectName("kcfg_ContextInfo");
    label->setBuddy(contextInfoEdit);
    hbox->setStretchFactor(contextInfoEdit,1);

    msg=i18n("<qt><p><b>Regular expression for context information</b></p>"
        "<p>Enter a regular expression here which defines what is "
        "context information in the message and must not get "
        "translated.</p></qt>");
    label->setWhatsThis(msg);
    contextInfoEdit->setWhatsThis(msg);

    if( !KServiceTypeTrader::self()->query("KRegExpEditor/KRegExpEditor").isEmpty() )
    {
	_regExpButton = new QPushButton( i18n("&Edit..."), hbox );
	connect( _regExpButton, SIGNAL( clicked() ), this, SLOT( regExpButtonClicked()));
    }


    // preferences for mail attachments
    Q3VButtonGroup* vbgroup = new Q3VButtonGroup(page);
    vbgroup->setTitle(i18n("Compression Method for Mail Attachments"));
    vbgroup->setRadioButtonExclusive(true);
    layout->addWidget(vbgroup);

    bzipButton = new QRadioButton( i18n("tar/&bzip2"), vbgroup );
    bzipButton->setObjectName( "kcfg_BZipCompression" );
    gzipButton = new QRadioButton(i18n("tar/&gzip"), vbgroup);

    compressSingle = new QCheckBox( i18n("&Use compression when sending a single file"), vbgroup );
    compressSingle->setObjectName( "kcfg_CompressSingleFile" );

    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));
}

void MiscPreferences::defaults(const MiscSettings& settings)
{
    accelMarkerEdit->setText(QString(settings.accelMarker));
    contextInfoEdit->setText(settings.contextInfo.pattern());
    if( settings.useBzip )
	bzipButton->setChecked (true);
    else
	gzipButton->setChecked (true);

    compressSingle->setChecked(settings.compressSingleFile);
}

QString MiscPreferences::contextInfo() const
{
    QString temp=contextInfoEdit->text();

    bool quoted=false;
    QString newStr;

    for(int i=0; i<temp.length(); i++)
    {
        if(temp[i]=='n')
        {
            quoted=!quoted;
            newStr+=temp[i];
        }
        else if(temp[i]=='n' && quoted)
        {
            newStr[newStr.length()-1]='\n';
            quoted=false;
        }
        else
        {
            quoted=false;
            newStr+=temp[i];
        }
    }

    return newStr;
}

void MiscPreferences::setContextInfo(QString reg)
{
    reg.replace("\n","\\n");
    contextInfoEdit->setText(reg);
}

void MiscPreferences::regExpButtonClicked()
{
    if ( _regExpEditDialog==0 )
      _regExpEditDialog = KServiceTypeTrader::createInstanceFromQuery<QDialog>
	("KRegExpEditor/KRegExpEditor", QString(), this );

    KRegExpEditorInterface *iface = qobject_cast<KRegExpEditorInterface *>( _regExpEditDialog );
    if( iface )
    {
	iface->setRegExp( contextInfoEdit->text() );
	if( _regExpEditDialog->exec() == QDialog::Accepted )
	    contextInfoEdit->setText( iface->regExp() );
    }
}


SpellPreferences::SpellPreferences(QWidget* parent)
         : QWidget(parent)
{
    QWidget* page = this;
    QVBoxLayout* layout=new QVBoxLayout(page);
    layout->setMargin(0);


    onFlyBtn = new QCheckBox( i18n("On the &fly spellchecking"), page );
    onFlyBtn->setObjectName( "kcfg_OnFlySpellCheck" );
    layout->addWidget(onFlyBtn);

    onFlyBtn->setWhatsThis(i18n("<qt><p><b>On the fly spellchecking</b></p>"
        "<p>Activate this to let KBabel spell check the text "
	"as you type. Mispelled words will be colored by the error color.</p></qt>"));

    spellConfig = new K3SpellConfig(page,0,false);
    spellConfig->setObjectName("spellConfigWidget");
    layout->addWidget(spellConfig);
    remIgnoredBtn = new QCheckBox( i18n("&Remember ignored words"), page );
    remIgnoredBtn->setObjectName( "kcfg_RememberIgnored" );
    layout->addWidget(remIgnoredBtn);

    connect( spellConfig, SIGNAL( configChanged() )
	, this, SIGNAL ( settingsChanged() ) );

    QLabel *tempLabel = new QLabel(i18n("F&ile to store ignored words:"),page);
    layout->addWidget(tempLabel);
    ignoreURLEdit = new KUrlRequester(page);
    ignoreURLEdit->setObjectName("kcfg_IgnoreURL");
    layout->addWidget(ignoreURLEdit);
    tempLabel->setBuddy(ignoreURLEdit);

    connect(remIgnoredBtn,SIGNAL(toggled(bool)),ignoreURLEdit
                        ,SLOT(setEnabled(bool)));


    QString msg = i18n("<qt><p><b>Remember ignored words</b></p>"
        "<p>Activate this, to let KBabel ignore the words, where you have "
        "chosen <i>Ignore All</i> in the spell check dialog, "
        "in every spell check.</p></qt>");

    remIgnoredBtn->setWhatsThis(msg);
    tempLabel->setWhatsThis(msg);
    ignoreURLEdit->setWhatsThis(msg);

    layout->addStretch(1);

    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());
}



void SpellPreferences::updateWidgets(const SpellcheckSettings& settings)
{
    spellConfig->setClient(settings.spellClient);
    spellConfig->setNoRootAffix(settings.noRootAffix);
    spellConfig->setRunTogether(settings.runTogether);
    spellConfig->setEncoding(settings.spellEncoding);
    spellConfig->setDictionary(settings.spellDict);
}


void SpellPreferences::mergeSettings(SpellcheckSettings& settings) const
{
    settings.noRootAffix=spellConfig->noRootAffix();
    settings.runTogether=spellConfig->runTogether();
    settings.spellClient=spellConfig->client();
    settings.spellEncoding=spellConfig->encoding();
    settings.spellDict=spellConfig->dictionary();

    settings.valid=true;
}

void SpellPreferences::defaults(const SpellcheckSettings& settings)
{
    remIgnoredBtn->setChecked(settings.rememberIgnored);
    ignoreURLEdit->setUrl(settings.ignoreURL);

    onFlyBtn->setChecked(settings.onFlySpellcheck);

    K3SpellConfig spCfg;
    *spellConfig = spCfg;
}

CatmanPreferences::CatmanPreferences(QWidget* parent)
         : QWidget(parent)
         , _ui(new Ui::CatmanPreferencesBase)
{
    QWidget* page = this;
    _ui->setupUi(page);
    layout()->setMargin(0);

    const KFile::Modes mode = KFile::Directory | KFile::ExistingOnly | KFile::LocalOnly;
    _ui->kcfg_PoBaseDir->setMode(mode);
    _ui->kcfg_PotBaseDir->setMode(mode);

    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());
}

CatmanPreferences::~CatmanPreferences()
{
    delete _ui;
}

void CatmanPreferences::defaults(const CatManSettings& settings)
{
   _ui->kcfg_PoBaseDir->setUrl(settings.poBaseDir);
   _ui->kcfg_PotBaseDir->setUrl(settings.potBaseDir);

   _ui->kcfg_OpenWindow->setChecked(settings.openWindow);

   _ui->kcfg_KillCmdOnExit->setChecked(settings.killCmdOnExit);
   _ui->kcfg_IndexWords->setChecked(settings.indexWords);
   _ui->kcfg_msgfmt->setChecked(settings.msgfmt);
}

DirCommandsPreferences::DirCommandsPreferences(QWidget* parent)
         : QWidget(parent)
{
    QWidget* page = this;

    QVBoxLayout* layout=new QVBoxLayout(page);
    layout->setMargin(0);

    Q3GroupBox* box = new Q3GroupBox( 1 , Qt::Horizontal , i18n("Commands for Folders") , page );
    layout->addWidget( box );

    _dirCmdEdit = new CmdEdit( box );
    new QLabel( i18n("Replaceables:\n@PACKAGE@, @PODIR@, @POTDIR@\n"
	"@POFILES@, @MARKEDPOFILES@"), box);

    connect (_dirCmdEdit, SIGNAL(widgetChanged()), this, SIGNAL(settingsChanged()));

    box->setWhatsThis(i18n("<qt><p><b>Commands for folders</b></p>"
"<p>Insert here the commands you want to execute in folders from "
"the Catalog Manager. The commands are then shown in the submenu "
"<b>Commands</b> in the Catalog Manager's context menu.</p>"
"<p>The following strings will be replaced in a command:<ul>"
"<li>@PACKAGE@: The name of the folder without path</li>"
"<li>@PODIR@: The name of the PO-folder with path</li>"
"<li>@POTDIR@: The name of the template folder with path</li>"
"<li>@POFILES@: The names of the PO files with path</li>"
"<li>@MARKEDPOFILES@: The names of the marked PO files with path</li>"
"</ul></p>"
"</qt>") );



    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());
}


DirCommandsPreferences::~DirCommandsPreferences()
{
}


void DirCommandsPreferences::updateWidgets(const CatManSettings& settings)
{
   _dirCmdEdit->setCommands( settings.dirCommands , settings.dirCommandNames );
}


void DirCommandsPreferences::mergeSettings(CatManSettings& settings) const
{
    _dirCmdEdit->commands( settings.dirCommands , settings.dirCommandNames );
}

void DirCommandsPreferences::defaults(const CatManSettings& settings)
{
   _dirCmdEdit->setCommands( settings.dirCommands, settings.dirCommandNames );
}


FileCommandsPreferences::FileCommandsPreferences(QWidget* parent)
         : QWidget(parent)
{
    QWidget* page = this;

    QVBoxLayout* layout=new QVBoxLayout(page);
    layout->setMargin(0);

    Q3GroupBox* box=new Q3GroupBox( 1 , Qt::Horizontal , i18n("Commands for Files") , page );
    layout->addWidget( box );

    _fileCmdEdit = new CmdEdit( box );
    new QLabel( i18n("Replaceables:\n"
"@PACKAGE@, @POFILE@,@POTFILE@,\n@PODIR@, @POTDIR@"), box);

    connect (_fileCmdEdit, SIGNAL(widgetChanged()), this, SIGNAL(settingsChanged()));

    box->setWhatsThis(i18n("<qt><p><b>Commands for files</b></p>"
"<p>Insert here the commands you want to execute on files from "
"the Catalog Manager. The commands are then shown in the submenu "
"<b>Commands</b> in the Catalog Manager's context menu.</p>"
"<p>The following strings will be replaced in a command:<ul>"
"<li>@PACKAGE@: The name of the file without path and extension</li>"
"<li>@POFILE@: The name of the PO-file with path and extension</li>"
"<li>@POTFILE@: The name of the corresponding template file with path "
"and extension</li>"
"<li>@POEMAIL@: The name and email address of the last translator</li>"
"<li>@PODIR@: The name of the folder the PO-file is in, with path</li>"
"<li>@POTDIR@: The name of the folder the template file is in, with "
"path</li></ul></p></qt>") );



    layout->addStretch(1);
    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());
}


FileCommandsPreferences::~FileCommandsPreferences()
{
}


void FileCommandsPreferences::updateWidgets(const CatManSettings& settings)
{
   _fileCmdEdit->setCommands( settings.fileCommands , settings.fileCommandNames );
}


void FileCommandsPreferences::mergeSettings(CatManSettings& settings) const
{
    _fileCmdEdit->commands( settings.fileCommands , settings.fileCommandNames );
}

void FileCommandsPreferences::defaults(const CatManSettings& settings)
{
   _fileCmdEdit->setCommands( settings.fileCommands, settings.fileCommandNames );
}

ViewPreferences::ViewPreferences(QWidget* parent)
         : QWidget(parent)
         , _ui(new Ui::ViewPreferencesBase)
{
    QWidget* page = this;
    _ui->setupUi(page);
    layout()->setMargin(0);

    page->setMinimumSize(sizeHintForWidget(page));

    setMinimumSize(sizeHint());
}

ViewPreferences::~ViewPreferences()
{
    delete _ui;
}

void ViewPreferences::defaults(const CatManSettings& _settings)
{
    _ui->kcfg_ShowFlagColumn->setChecked(_settings.flagColumn);
    _ui->kcfg_ShowFuzzyColumn->setChecked(_settings.fuzzyColumn);
    _ui->kcfg_ShowUntranslatedColumn->setChecked(_settings.untranslatedColumn);
    _ui->kcfg_ShowTotalColumn->setChecked(_settings.totalColumn);
    _ui->kcfg_ShowCVSColumn->setChecked(_settings.cvsColumn);
    _ui->kcfg_ShowRevisionColumn->setChecked(_settings.revisionColumn);
    _ui->kcfg_ShowTranslatorColumn->setChecked(_settings.translatorColumn);
}

SourceContextPreferences::SourceContextPreferences(QWidget* parent): QWidget(parent)
{
    QWidget* page = this;
    QVBoxLayout* layout=new QVBoxLayout(page);
    layout->setMargin(0);

    KHBox* box = new KHBox(page);
    QLabel* tempLabel=new QLabel(i18n("&Base folder for source code:"),box);

    const KFile::Modes mode = KFile::Directory | KFile::ExistingOnly | KFile::LocalOnly;
    _coderootEdit = new KUrlRequester ( box );
    _coderootEdit->setObjectName( "kcfg_CodeRoot" );
    _coderootEdit->setMode( mode );
    _coderootEdit->setMinimumSize( 250, _coderootEdit->sizeHint().height() );
    tempLabel->setBuddy( _coderootEdit );
    layout->addWidget(box);

    // FIXME: use KConfigXT
    _pathsEditor = new KListEditor(page);
    _pathsEditor->setTitle(i18n("Path Patterns"));
    layout->addWidget(_pathsEditor);

    connect ( _pathsEditor, SIGNAL (itemsChanged ())
	, this, SIGNAL (itemsChanged ()));

    _pathsEditor->installEventFilter(this);

    setMinimumSize(sizeHint());
}

SourceContextPreferences::~SourceContextPreferences()
{
}

void SourceContextPreferences::mergeSettings(KBabel::SourceContextSettings& settings) const
{
    settings.sourcePaths=_pathsEditor->list();
}

void SourceContextPreferences::updateWidgets(const KBabel::SourceContextSettings& settings)
{
    _pathsEditor->setList(settings.sourcePaths);
}

void SourceContextPreferences::defaults(const KBabel::SourceContextSettings& settings)
{
    _pathsEditor->setList(settings.sourcePaths);
}

bool SourceContextPreferences::eventFilter( QObject *, QEvent *e )
{
    if( e->type() == QEvent::KeyPress )
    {
        QKeyEvent *ke = static_cast<QKeyEvent*>(e);
        if( ke->key() == Qt::Key_Return || ke->key() == Qt::Key_Enter )
            return true;
    }
    return false;
}

#include "projectprefwidgets.moc"
