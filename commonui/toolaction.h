/* This file is part of KBabel
   Copyright (C) 2002 Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

*/

#ifndef TOOLACTION_H
#define TOOLACTION_H

#include <kbabel_export.h>
#include <qobject.h>
#include <qlist.h>
#include <kaction.h>
#include <kdatatool.h>

class KShortcut;
class KActionCollection;

class KBABELCOMMONUI_EXPORT ToolAction : public KAction
{
    Q_OBJECT
public:
    ToolAction( const QString & text, const KShortcut& cut, const KDataToolInfo & info, const QString & command, QObject *parent );

    /**
    * return the list of KActions created for a list of tools. @ref command
    * allows to specify rescriction of commands, for which the list should
    * or shouldn't be created according to the @ref excludeCommand flag.
    */
    static QList<KAction *> dataToolActionList( const QList<KDataToolInfo> & tools, const QObject *receiver, const char* slot, const QStringList& command, bool excludeCommand, KActionCollection* parent=0, const QString& namePrefix=QString() );

    /**
    * returns information about all available validation tools (KDataTools with support for CatalogItem
    * and the "validate" command.
    */
    static QList<KDataToolInfo> validationTools();

    static bool findTool( const QStringList& tools, const KDataToolInfo &info );

signals:
    void toolActivated( const KDataToolInfo & info, const QString & command );

private slots:
    void slotActivated();

private:
    QString m_command;
    KDataToolInfo m_info;
};

#endif
