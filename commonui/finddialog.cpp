/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2002	  by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "finddialog.h"

#include <q3buttongroup.h>
#include <kservicetypetrader.h>
#include <qcheckbox.h>
#include <q3groupbox.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlayout.h>
#include <klineedit.h>


#include <QVBoxLayout>

#include <kcombobox.h>
#include <kconfig.h>
#include <kglobal.h>
#include <klocale.h>
#include <kparts/componentfactory.h>
#include <kregexpeditorinterface.h>
#include <kvbox.h>
#include <kconfiggroup.h>

using namespace KBabel;

FindDialog::FindDialog(bool forReplace, QWidget* parent)
		:KDialog(parent)
		, _regExpEditDialog(0), _replaceDlg(forReplace)
{
    setButtons( Ok|Cancel );
    setDefaultButton( Ok);
    setModal( true );
	QWidget* page = new QWidget(this);
	QVBoxLayout *layout = new QVBoxLayout(page);
	layout->setSpacing(spacingHint());
	layout->setMargin(0);

	QLabel *label = new QLabel(i18n("&Find:"),page);
	layout->addWidget(label);

	_findCombo = new KComboBox(true, page);
        _findCombo->setObjectName("findCombo");
	_findCombo->setMaxCount(10);
	_findCombo->setInsertPolicy( QComboBox::InsertAtTop );
	layout->addWidget(_findCombo);
	label->setBuddy(_findCombo);

	QString msg=i18n("<qt><p><b>Find text</b></p>"
		"<p>Here you can enter the text you want to search for. "
		"If you want to search for a regular expression, "
		"enable <b>Use regular expression</b> below.</p></qt>");


	label->setWhatsThis(msg);
	_findCombo->setWhatsThis(msg);

	if(forReplace) {
		setCaption(i18n("Replace"));
                setButtonText(Ok, i18n("&Replace"));

		_replaceLabel = new QLabel(i18n("&Replace with:"),page);
		layout->addWidget(_replaceLabel);
		_replaceCombo = new KComboBox(true, page);
                _replaceCombo->setObjectName("replaceCombo");
		_replaceCombo->setMaxCount(10);
		_replaceCombo->setInsertPolicy( QComboBox::InsertAtTop );
		layout->addWidget(_replaceCombo);
		_replaceLabel->setBuddy(_replaceCombo);

	    	msg=i18n("<qt><p><b>Replace text</b></p>"
	    	"<p>Here you can enter the text you want the found text to get "
		"replaced with. The text is used as is. It is not possible to make a back "
		"reference, if you have searched for a regular expression.</p></qt>");

		_replaceLabel->setWhatsThis(msg);
		_replaceCombo->setWhatsThis(msg);
	}
	else {
		setCaption(i18n("Find"));
	        setButtonGuiItem(Ok, KGuiItem(i18n("&Find"),"edit-find"));

		_replaceLabel=0;
		_replaceCombo=0;
	}

	_buttonGrp = new Q3ButtonGroup(3, Qt::Horizontal, i18n("Where to Search"), page);
	connect(_buttonGrp,SIGNAL(clicked(int)), this, SLOT(inButtonsClicked(int)));
	layout->addWidget(_buttonGrp);

	_inMsgid = new QCheckBox(i18n("&Msgid"),_buttonGrp);
	_inMsgstr = new QCheckBox(i18n("M&sgstr"),_buttonGrp);
	_inComment = new QCheckBox(i18n("Comm&ent"),_buttonGrp);

	_buttonGrp->setWhatsThis(i18n("<qt><p><b>Where to search</b></p>"
		"<p>Select here in which parts of a catalog entry you want "
		"to search.</p></qt>"));


	Q3GroupBox* box = new Q3GroupBox(2, Qt::Horizontal, i18n("Options"), page);
	layout->addWidget(box);

	_caseSensitive = new QCheckBox(i18n("C&ase sensitive"),box);
	_wholeWords = new QCheckBox(i18n("O&nly whole words"),box);
	_ignoreAccelMarker = new QCheckBox(i18n("I&gnore marker for keyboard accelerator"),box);
	_ignoreContextInfo = new QCheckBox(i18n("Ignore con&text information"),box);
	_fromCursor = new QCheckBox(i18n("From c&ursor position"),box);
	_backwards = new QCheckBox(i18n("F&ind backwards"),box);

	KHBox *regexp = new KHBox(box);

	_isRegExp = new QCheckBox(i18n("Use regu&lar expression"),regexp);
	_regExpButton = 0;

	if( !KServiceTypeTrader::self()->query("KRegExpEditor/KRegExpEditor").isEmpty() )
	{
	    _regExpButton = new QPushButton( i18n("&Edit..."), regexp );
	    connect( _regExpButton, SIGNAL( clicked() ), this, SLOT( regExpButtonClicked()));
	    connect( _isRegExp, SIGNAL( toggled(bool) ), _regExpButton, SLOT(setEnabled(bool)));
	}

	if(forReplace)
	{
		_inMsgid->setEnabled(false);
		_askForReplace = new QCheckBox(i18n("As&k before replacing"),box);
        _ignoreContextInfo->setEnabled(false);

		box->setWhatsThis(i18n("<qt><p><b>Options</b></p>"
		"<p>Here you can finetune replacing:"
		"<ul><li><b>Case sensitive</b>: does case of entered text have to be respected?</li>"
		"<li><b>Only whole words</b>: text found must not be part of a longer word</li>"
		"<li><b>From cursor position</b>: start replacing at the part of the document where "
		"the cursor is. Otherwise replacing is started at the beginning or the end.</li>"
		"<li><b>Find backwards</b>: Should be self-explanatory.</li>"
		"<li><b>Use regular expression</b>: use text entered in field <b>Find</b> "
		"as a regular expression. This option has no effect with the replace text, especially "
		"no back references are possible.</li>"
		"<li><b>Ask before replacing</b>: Enable, if you want to have control about "
		"what is replaced. Otherwise all found text is replaced without asking.</li>"
		"</ul></p></qt>"));
	}
	else {
		_askForReplace=0;

		box->setWhatsThis(i18n("<qt><p><b>Options</b></p>"
		"<p>Here you can finetune the search:"
		"<ul><li><b>Case sensitive</b>: does case of entered text have to be respected?</li>"
		"<li><b>Only whole words</b>: text found must not be part of a longer word</li>"
		"<li><b>From cursor position</b>: start search at the part of the document, where "
		"the cursor is. Otherwise search is started at the beginning or the end.</li>"
		"<li><b>Find backwards</b>: Should be self-explanatory.</li>"
		"<li><b>Use regular expression</b>: use entered text as a regular expression.</li>"
		"</ul></p></qt>"));
	}


	readSettings();


	setMainWidget(page);
}

FindDialog::~FindDialog()
{
   saveSettings();
}

int FindDialog::show(const QString& initialStr)
{
   if( !initialStr.isEmpty() ) {
		_findCombo->setEditText( initialStr );
	}
	_findCombo->lineEdit()->selectAll();
   _findCombo->setFocus();


	KDialog::show();

	int r = result();

	if( r == QDialog::Accepted ) {
		if(_replaceDlg) {
			_replaceList.removeAt( _replaceList.indexOf( _replaceCombo->currentText() ) );
			_replaceList.prepend(_replaceCombo->currentText());
			if(_replaceList.count() > 10 )
				_replaceList.removeLast();

			_replaceCombo->clear();
			_replaceCombo->addItems( _replaceList );

			_replaceFindList.removeAt( _replaceFindList.indexOf( _findCombo->currentText() ) );
			_replaceFindList.prepend(_findCombo->currentText());
			if(_replaceFindList.count() > 10 )
				_replaceFindList.removeLast();

			_findCombo->clear();
			_findCombo->addItems( _replaceFindList );

			_replaceOptions.findStr = _findCombo->currentText();
			_replaceOptions.replaceStr = _replaceCombo->currentText();

			_replaceOptions.inMsgstr = _inMsgstr->isChecked();
			_replaceOptions.inComment = _inComment->isChecked();
			_replaceOptions.inMsgid = false;

			_replaceOptions.caseSensitive = _caseSensitive->isChecked();
			_replaceOptions.wholeWords = _wholeWords->isChecked();
			_replaceOptions.ignoreAccelMarker = _ignoreAccelMarker->isChecked();
			_replaceOptions.ignoreContextInfo = false;
			_replaceOptions.backwards = _backwards->isChecked();
			_replaceOptions.fromCursor = _fromCursor->isChecked();
			_replaceOptions.isRegExp = _isRegExp->isChecked();
			_replaceOptions.ask = _askForReplace->isChecked();
		}
		else {
			_findList.removeAt( _findList.indexOf( _findCombo->currentText() ) );
			_findList.prepend(_findCombo->currentText());
			if(_findList.count() > 10 )
				_findList.removeLast();

			_findCombo->clear();
			_findCombo->addItems( _findList );

			_findOptions.findStr = _findCombo->currentText();
			_findOptions.inMsgid = _inMsgid->isChecked();
			_findOptions.inMsgstr = _inMsgstr->isChecked();
			_findOptions.inComment = _inComment->isChecked();

			_findOptions.caseSensitive = _caseSensitive->isChecked();
			_findOptions.wholeWords = _wholeWords->isChecked();
			_findOptions.ignoreAccelMarker = _ignoreAccelMarker->isChecked();
			_findOptions.ignoreContextInfo = _ignoreContextInfo->isChecked();
			_findOptions.backwards = _backwards->isChecked();
			_findOptions.fromCursor = _fromCursor->isChecked();
			_findOptions.isRegExp = _isRegExp->isChecked();
		}
	}

	return r;
}

int FindDialog::exec(const QString& initialStr)
{
   if( !initialStr.isEmpty() ) {
		_findCombo->setEditText( initialStr );
	}
	_findCombo->lineEdit()->selectAll();
   _findCombo->setFocus();


	KDialog::exec();

	int r = result();

	if( r == QDialog::Accepted ) {
		if(_replaceDlg) {
			_replaceList.removeAt( _replaceList.indexOf( _replaceCombo->currentText() ) );
			_replaceList.prepend(_replaceCombo->currentText());
			if(_replaceList.count() > 10 )
				_replaceList.removeLast();

			_replaceCombo->clear();
			_replaceCombo->addItems( _replaceList );

			_replaceFindList.removeAt( _replaceFindList.indexOf( _findCombo->currentText() ) );
			_replaceFindList.prepend(_findCombo->currentText());
			if(_replaceFindList.count() > 10 )
				_replaceFindList.removeLast();

			_findCombo->clear();
			_findCombo->addItems( _replaceFindList );

			_replaceOptions.findStr = _findCombo->currentText();
			_replaceOptions.replaceStr = _replaceCombo->currentText();

			_replaceOptions.inMsgstr = _inMsgstr->isChecked();
			_replaceOptions.inComment = _inComment->isChecked();
			_replaceOptions.inMsgid = false;

			_replaceOptions.caseSensitive = _caseSensitive->isChecked();
			_replaceOptions.wholeWords = _wholeWords->isChecked();
			_replaceOptions.ignoreAccelMarker = _ignoreAccelMarker->isChecked();
			_replaceOptions.ignoreContextInfo = false;
			_replaceOptions.backwards = _backwards->isChecked();
			_replaceOptions.fromCursor = _fromCursor->isChecked();
			_replaceOptions.isRegExp = _isRegExp->isChecked();
			_replaceOptions.ask = _askForReplace->isChecked();
		}
		else {
			_findList.removeAt( _findList.indexOf( _findCombo->currentText() ) );
			_findList.prepend(_findCombo->currentText());
			if(_findList.count() > 10 )
				_findList.removeLast();

			_findCombo->clear();
			_findCombo->addItems( _findList );

			_findOptions.findStr = _findCombo->currentText();
			_findOptions.inMsgid = _inMsgid->isChecked();
			_findOptions.inMsgstr = _inMsgstr->isChecked();
			_findOptions.inComment = _inComment->isChecked();

			_findOptions.caseSensitive = _caseSensitive->isChecked();
			_findOptions.wholeWords = _wholeWords->isChecked();
			_findOptions.ignoreAccelMarker = _ignoreAccelMarker->isChecked();
			_findOptions.ignoreContextInfo = _ignoreContextInfo->isChecked();
			_findOptions.backwards = _backwards->isChecked();
			_findOptions.fromCursor = _fromCursor->isChecked();
			_findOptions.isRegExp = _isRegExp->isChecked();
		}
	}

	return r;
}

FindOptions FindDialog::findOpts()
{
   return _findOptions;
}

void FindDialog::setFindOpts(FindOptions options)
{
    _findOptions = options;
    _inMsgid->setChecked(_findOptions.inMsgid);
    _inMsgstr->setChecked(_findOptions.inMsgstr);
    _inComment->setChecked(_findOptions.inComment);

    _caseSensitive->setChecked(_findOptions.caseSensitive);
    _wholeWords->setChecked(_findOptions.wholeWords);
    _ignoreAccelMarker->setChecked(_findOptions.ignoreAccelMarker);
    _ignoreContextInfo->setChecked(_findOptions.ignoreContextInfo);
    _backwards->setChecked(_findOptions.backwards);
    _fromCursor->setChecked(_findOptions.fromCursor);
    _isRegExp->setChecked(_findOptions.isRegExp);
    if( _regExpButton ) _regExpButton->setEnabled( _findOptions.isRegExp );

    _findCombo->setEditText(_findOptions.findStr);
}

ReplaceOptions FindDialog::replaceOpts()
{
   return _replaceOptions;
}

void FindDialog::setReplaceOpts(ReplaceOptions options)
{
   _replaceOptions = options;
    _inMsgid->setChecked(_replaceOptions.inMsgid);
    _inMsgstr->setChecked(_replaceOptions.inMsgstr);
    _inComment->setChecked(_replaceOptions.inComment);

    _caseSensitive->setChecked(_replaceOptions.caseSensitive);
    _wholeWords->setChecked(_replaceOptions.wholeWords);
    _ignoreAccelMarker->setChecked(_replaceOptions.ignoreAccelMarker);
    _ignoreContextInfo->setChecked(_replaceOptions.ignoreContextInfo);
    _backwards->setChecked(_replaceOptions.backwards);
    _fromCursor->setChecked(_replaceOptions.fromCursor);
    _isRegExp->setChecked(_replaceOptions.isRegExp);
    _askForReplace->setChecked(_replaceOptions.ask);
    if( _regExpButton ) _regExpButton->setEnabled( _replaceOptions.isRegExp );

    _findCombo->setEditText(_replaceOptions.findStr);
    _replaceCombo->setEditText(_replaceOptions.replaceStr);
}

void FindDialog::readSettings()
{
	KSharedConfig::Ptr config = KGlobal::config();

	if(_replaceDlg) {
		KConfigGroup configGroup(config,"ReplaceDialog");
		_replaceOptions.inMsgstr = configGroup.readEntry("InMsgstr",true);
		_replaceOptions.inComment = configGroup.readEntry("InComment",false);

		_replaceOptions.caseSensitive =
            configGroup.readEntry("CaseSensitive",true);
		_replaceOptions.wholeWords = configGroup.readEntry("WholeWords",false);
		_replaceOptions.ignoreAccelMarker =
            configGroup.readEntry("IgnoreAccelMarker",true);
		_replaceOptions.backwards = configGroup.readEntry("Backwards",false);
		_replaceOptions.fromCursor = configGroup.readEntry("FromCursor",true);
		_replaceOptions.isRegExp = configGroup.readEntry("RegExp",false);
		_replaceOptions.ask = configGroup.readEntry("AskForReplace",true);
		_replaceFindList = configGroup.readEntry("FindList",QStringList());
		_replaceList = configGroup.readEntry("ReplaceList",QStringList());

		_inMsgstr->setChecked(_replaceOptions.inMsgstr);
		_inComment->setChecked(_replaceOptions.inComment);

		_caseSensitive->setChecked(_replaceOptions.caseSensitive);
		_wholeWords->setChecked(_replaceOptions.wholeWords);
		_ignoreAccelMarker->setChecked(_findOptions.ignoreAccelMarker);
		_backwards->setChecked(_replaceOptions.backwards);
		_fromCursor->setChecked(_replaceOptions.fromCursor);
		_isRegExp->setChecked(_replaceOptions.isRegExp);
		_askForReplace->setChecked(_replaceOptions.ask);
		if( _regExpButton ) _regExpButton->setEnabled( _findOptions.isRegExp );

		_replaceCombo->addItems( _replaceList );
		_findCombo->addItems( _replaceFindList );
	}
	else {
		KConfigGroup configGroup(config,"FindDialog");

		_findOptions.inMsgid = configGroup.readEntry("InMsgid",true);
		_findOptions.inMsgstr = configGroup.readEntry("InMsgstr",true);
		_findOptions.inComment = configGroup.readEntry("InComment",false);

		_findOptions.caseSensitive = configGroup.readEntry("CaseSensitive"
                ,false);
		_findOptions.wholeWords = configGroup.readEntry("WholeWords",false);
		_findOptions.ignoreAccelMarker =
            configGroup.readEntry("IgnoreAccelMarker",true);
		_findOptions.ignoreContextInfo =
            configGroup.readEntry("IgnoreContextInfo",true);
		_findOptions.backwards = configGroup.readEntry("Backwards",false);
		_findOptions.fromCursor = configGroup.readEntry("FromCursor",false);
		_findOptions.isRegExp = configGroup.readEntry("RegExp",false);
		_findList = configGroup.readEntry("List",QStringList());
		if( _regExpButton ) _regExpButton->setEnabled( _findOptions.isRegExp );

		_inMsgid->setChecked(_findOptions.inMsgid);
		_inMsgstr->setChecked(_findOptions.inMsgstr);
		_inComment->setChecked(_findOptions.inComment);

		_caseSensitive->setChecked(_findOptions.caseSensitive);
		_wholeWords->setChecked(_findOptions.wholeWords);
		_ignoreAccelMarker->setChecked(_findOptions.ignoreAccelMarker);
		_ignoreContextInfo->setChecked(_findOptions.ignoreContextInfo);
		_backwards->setChecked(_findOptions.backwards);
		_fromCursor->setChecked(_findOptions.fromCursor);
		_isRegExp->setChecked(_findOptions.isRegExp);

		_findCombo->addItems( _findList );
	}

}

void FindDialog::saveSettings()
{
	KSharedConfig::Ptr config = KGlobal::config();

	if(_replaceDlg) {
		KConfigGroup configGroup(config,"ReplaceDialog");
		configGroup.writeEntry("InMsgstr",_replaceOptions.inMsgstr);
		configGroup.writeEntry("InComment",_replaceOptions.inComment);

		configGroup.writeEntry("CaseSensitive",_replaceOptions.caseSensitive);
		configGroup.writeEntry("WholeWords",_replaceOptions.wholeWords);
		configGroup.writeEntry("IgnoreAccelMarker"
                ,_replaceOptions.ignoreAccelMarker);
		configGroup.writeEntry("Backwards",_replaceOptions.backwards);
		configGroup.writeEntry("FromCursor",_replaceOptions.fromCursor);
		configGroup.writeEntry("RegExp",_replaceOptions.isRegExp);
		configGroup.writeEntry("AskForReplace",_replaceOptions.ask);
		configGroup.writeEntry("FindList",_replaceFindList);
		configGroup.writeEntry("ReplaceList",_replaceList);
	}
	else {
		KConfigGroup configGroup(config,"FindDialog");

		configGroup.writeEntry("InMsgid",_findOptions.inMsgid);
		configGroup.writeEntry("InMsgstr",_findOptions.inMsgstr);
		configGroup.writeEntry("InComment",_findOptions.inComment);

		configGroup.writeEntry("CaseSensitive",_findOptions.caseSensitive);
		configGroup.writeEntry("WholeWords",_findOptions.wholeWords);
		configGroup.writeEntry("IgnoreAccelMarker"
                ,_findOptions.ignoreAccelMarker);
		configGroup.writeEntry("IgnoreContextInfo"
                ,_findOptions.ignoreContextInfo);
		configGroup.writeEntry("Backwards",_findOptions.backwards);
		configGroup.writeEntry("FromCursor",_findOptions.fromCursor);
		configGroup.writeEntry("RegExp",_findOptions.isRegExp);
		configGroup.writeEntry("List",_findList);
	}
}



void FindDialog::inButtonsClicked(int id)
{
 	// check if at least one button is checked
	if(! _buttonGrp->find(id)->isChecked() ) {
		if(!_inMsgstr->isChecked() && !_inComment->isChecked() ) {
			if(_inMsgid->isEnabled()) {
				if( !_inMsgid->isChecked() ) {
					_buttonGrp->setButton(id);
				}
			}
			else {
					_buttonGrp->setButton(id);
			}

		}
	}
}

void FindDialog::regExpButtonClicked()
{
    if ( _regExpEditDialog == 0 )
         _regExpEditDialog = KServiceTypeTrader::createInstanceFromQuery<QDialog>( "KRegExpEditor/KRegExpEditor", QString(), this );

    KRegExpEditorInterface *iface = qobject_cast<KRegExpEditorInterface *>( _regExpEditDialog );
    if( iface )
    {
	iface->setRegExp( _findCombo->currentText() );
	if( _regExpEditDialog->exec() == QDialog::Accepted )
	    _findCombo->setItemText( _findCombo->currentIndex(), iface->regExp() );
    }
}

ReplaceDialog::ReplaceDialog(QWidget* parent)
		:KDialog(parent)
{
    setButtons( Close|User1|User2|User3 );
    setDefaultButton( User1 );
    setButtonGuiItem( User1,KGuiItem(i18n("&Replace")) );
    setButtonGuiItem( User2,KGuiItem(i18n("&Goto Next")) );
    setButtonGuiItem( User3,KGuiItem(i18n("R&eplace All")) );

	QWidget* page = new QWidget( this );
        setMainWidget( page );
	QVBoxLayout *layout = new QVBoxLayout(page);
	layout->setSpacing(spacingHint());
	layout->setMargin(0);

	QLabel *label = new QLabel(i18n("Replace this string?"),page);
	layout->addWidget(label);

	connect(this,SIGNAL(user1Clicked()),this,SIGNAL(replace()));
	connect(this,SIGNAL(user2Clicked()),this,SIGNAL(next()));
	connect(this,SIGNAL(user3Clicked()),this,SIGNAL(replaceAll()));
}

ReplaceDialog::~ReplaceDialog()
{
}

#include "finddialog.moc"
