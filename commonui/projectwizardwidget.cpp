/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2006 by Tim Beaulen <tbscope@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "projectwizardwidget.h"

#include <kconfig.h>
#include <kglobal.h>
#include <klocale.h>
#include <kmessagebox.h>

#include <ui_projectwizardwidget.h>

ProjectStep1::ProjectStep1(QWidget *parent)
    : QWizardPage(parent)
    , m_ui(new Ui::ProjectStep1Base)
{
    m_ui->setupUi(this);
    setTitle(i18n("Basic Project Information"));

    // fill the known language codes
    KConfig all_languages( "all_languages", KConfig::NoGlobals, "locale" );
    QStringList lang_codes = KGlobal::locale()->allLanguagesList();
    for (QStringList::iterator it = lang_codes.begin();
	it != lang_codes.end(); ++it)
    {
	// we need untranslated entries here, because of Translation Robot!
	QString entry = (*it);
	const int i = entry.indexOf( '_' );
	if (i != -1)
		entry.replace( 0, i, entry.left( i ).toLower() );
	const KConfigGroup group = all_languages.group( entry );
	entry = group.readEntryUntranslated("Name");
	if( ! entry.isEmpty() )
	{
	    m_ui->_projectLanguage->addItem( entry );
	    m_language_codes[entry] = (*it);
	}
    }

    registerField("projectName*", m_ui->_projectName);
    registerField("projectFile*", m_ui->_projectFile,
                  "text", SIGNAL(textChanged(QString)));
    registerField("projectLanguage", m_ui->_projectLanguage,
                  "currentText", SIGNAL(editTextChanged(QString)));
    registerField("projectType", m_ui->_projectType);
}

ProjectStep1::~ProjectStep1()
{
    delete m_ui;
}

bool ProjectStep1::validatePage()
{
    const QString projectFile = field("projectFile").toString();

    // check if the file exists
    QFileInfo file(projectFile);

    if( file.exists() )
    {
	if (KMessageBox::warningContinueCancel(this, i18n("The file '%1' already exists.\n"
	    "Do you want to replace it?", projectFile), i18n("File Exists"), KGuiItem(i18n("Replace")) ) == KMessageBox::Cancel)
		return false;
    }

    return true;
}

QMap<QString, QString> ProjectStep1::languageCodes() const
{
    return m_language_codes;
}

#include "projectwizardwidget.moc"
