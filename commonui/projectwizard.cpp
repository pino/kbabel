/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2004 by StanislavVsinovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "projectwizard.h"
#include "projectwizardwidget.h"
#include "projectwizardwidget2.h"

#include "kbprojectmanager.h"

#include <ktoolinvocation.h>

using namespace KBabel;

ProjectWizard::ProjectWizard(QWidget *parent)
        : QWizard(parent)
{
    setOption(HaveHelpButton);
    connect(this, SIGNAL(helpRequested()), this, SLOT(slotHelpClicked()));

    ProjectStep1* _wizard = new ProjectStep1(this);
    _wizard->setObjectName( "project wizard widget" );
    m_language_codes = _wizard->languageCodes();
    addPage(_wizard);

    ProjectStep2* _wizard2 = new ProjectStep2(this);
    _wizard2->setObjectName( "project wizard widget2" );
    addPage(_wizard2);
}

Project::Ptr ProjectWizard::project()
{
    Project::Ptr p = ProjectManager::open(field("projectFile").toString());
    p->setName(field("projectName").toString());

    enum type { KDE, GNOME, TP, Other };

    type project_type = (type) field("projectType").toInt();

    KBabel::CatManSettings catman = p->catManSettings();
    catman.poBaseDir = field("poDir").toString();
    catman.potBaseDir = field("potDir").toString();
    p->setSettings (catman);

    KBabel::IdentitySettings identity = p->identitySettings();
    // Language
    identity.languageName = field("projectLanguage").toString();
    // LanguageCode
    identity.languageCode = m_language_codes[identity.languageName];
    p->setSettings (identity);

    KBabel::SaveSettings save = p->saveSettings();
    // autochecksyntax (not for KDE - it uses incompatible plural forms formatting)
    if( project_type == KDE )
    {
	save.autoSyntaxCheck = false;
    }
    p->setSettings (save);

    KBabel::MiscSettings misc = p->miscSettings();
    if (project_type == GNOME)
    {
	misc.accelMarker = '_';
    }
    p->setSettings (misc);

    return p;
}

Project::Ptr ProjectWizard::newProject()
{
    ProjectWizard* dialog = new ProjectWizard();
    if( dialog->exec() == QDialog::Accepted )
    {
	Project::Ptr res = dialog->project();
	delete dialog;
	res->config()->sync();
	return res;
    }

    return Project::Ptr();
}

void ProjectWizard::slotHelpClicked( void )
{
    KToolInvocation::invokeHelp( "preferences-project-wizard", "kbabel" );
}

#include "projectwizard.moc"
