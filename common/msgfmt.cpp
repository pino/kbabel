/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer <matthias.kiefer@gmx.de>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.
**************************************************************************** */

#include "msgfmt.h"

#include <kprocess.h>
#include <kshell.h>
#include <qregexp.h>
#include <qstring.h>

using namespace KBabel;

Msgfmt::Status Msgfmt::checkSyntax(const QString &file, QString &output, bool gnu)
{
    Status stat = Ok;
    // this method does not return the right return values at the moment :-(

    KProcess proc;
    proc.setOutputChannelMode(KProcess::MergedChannels);

    proc << "msgfmt" << "--statistics" << "-o" << "/dev/null" << file;

    if (gnu)
        proc << "-vc";

    proc.start();
    if (!proc.waitForStarted()) {
        stat = NoExecutable;
    } else if (!proc.waitForFinished()) {
        stat = Error;
    } else if (proc.exitStatus() == QProcess::NormalExit) {
        output = proc.readAllStandardOutput();
        if (proc.exitCode() || output.contains(QRegExp("^.+:\\d+:"))) // little workaround :-(
            stat = SyntaxError;
    } else {
        stat = Error;
    }

    return stat;
}

Msgfmt::Status Msgfmt::checkSyntaxInDir(const QString &dir, const QString &regexp, QString &output)
{
    Status stat = Ok;
    // this method does not return the right return values at the moment :-(

    KProcess proc;
    proc.setOutputChannelMode(KProcess::MergedChannels);

    QString cmd = "find %1 -name %2 -exec msgfmt --statistics -o /dev/null {} \\;";
    proc.setShellCommand(cmd.arg(KShell::quoteArg(dir), KShell::quoteArg(regexp)));

    proc.start();
    if (!proc.waitForStarted()) {
        stat = NoExecutable;
    } else if (!proc.waitForFinished()) {
        stat = Error;
    } else if (proc.exitStatus() == QProcess::NormalExit) {
        output = proc.readAllStandardOutput();
        if (proc.exitCode() || output.contains(QRegExp("^.+:\\d+:"))) // little workaround :-(
            stat = SyntaxError;
    } else {
        stat = Error;
    }

    return stat;
}

// kate: space-indent on; indent-width 4; replace-tabs on;
