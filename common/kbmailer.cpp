/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2002-2003 by Marco Wegner <mail@marcowegner.de>
                2004 by Stanislav Visnovsky <visnovsky@kde.org>
  Copyright (C) 2006 by Nicolas GOUTTE <goutte@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.
**************************************************************************** */

#include "kbmailer.h"

#include <qfileinfo.h>
#include <qregexp.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qwidget.h>

#include <kio/netaccess.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <ktar.h>
#include <kurl.h>
#include <ktoolinvocation.h>
#include <kinputdialog.h>

using namespace KBabel;

KBabelMailer::KBabelMailer(QWidget *parent, Project::Ptr project)
    : _project(project),
    m_parent(parent)
{
    readConfig();
}

KBabelMailer::~KBabelMailer()
{
    saveConfig();
}

void KBabelMailer::sendOneFile(const QString &fileName)
{
    if (!singleFileCompression) {
        KToolInvocation::invokeMailer(QString(), QString(), QString(), QString(), QString(), QString(), QStringList(fileName));
    } else {
        const QString archive(createArchive(QStringList(fileName), QFileInfo(fileName).baseName()));
        if (!archive.isEmpty()) {
            KToolInvocation::invokeMailer(QString(), QString(), QString(), QString(), QString(), QString(), QStringList(archive));
        }
    }
}

void KBabelMailer::sendOneFile(const KUrl &url)
{
    const KUrl localUrl(KIO::NetAccess::mostLocalUrl(url, m_parent));

    if (localUrl.isLocalFile()) {
        sendOneFile(localUrl.path());
        return;
    }

    if (!singleFileCompression) {
        QString fileName(url.fileName());

        if (fileName.isEmpty())
            fileName = "attachment";

        // ### TODO: the current implementation has the default to possibly overwrite an already existing temporary file
        QString tempName(m_tempDir.name() );
        tempName += fileName;

        if (KIO::NetAccess::download(url, tempName, m_parent))
            KToolInvocation::invokeMailer(QString(), QString(), QString(), QString(), QString(), QString(), QStringList(fileName));
        else
          KMessageBox::error(m_parent, i18n("Error while trying to download file %1.", url.prettyUrl()));
    } else {
        const QString archive(createArchive(QStringList(url.url()), url.fileName()));
        if (!archive.isEmpty())
            KToolInvocation::invokeMailer(QString(), QString(), QString(), QString(), QString(), QString(), QStringList(archive));
    }
}

void KBabelMailer::sendFiles(const QStringList &fileList, const QString &initialName)
{
    const QString archive(createArchive(fileList, initialName));

    if (!archive.isEmpty())
        KToolInvocation::invokeMailer(QString(), QString(), QString(), QString(), QString(), QString(), QStringList(archive));
}

QString KBabelMailer::createArchive(const QStringList &fileList, const QString &initialName)
{
    if (m_tempDir.name().isEmpty()) {
        kWarning() << "KBabelMailer does not have a valid temporary directory!";
        return QString();
    }

    // do nothing if there are no files in the list
    if (fileList.empty())
        return QString();

    // determine the name of the archive, do nothing if none is given
    // or Cancel was pressed
    // EBN fix: Avoid changing a given parameter, so we can use a const QString&
    //initialName = ( initialName.isEmpty() ? QString("translations") : initialName );
    bool ok = false;
    QStringList list(archiveList);
    list.prepend(initialName.isEmpty() ? QString("translations") : initialName );
    QString archiveName(KInputDialog::getItem(i18n("Archive Name"), i18n("Enter the name of the archive without file extension"), archiveList, 0, true, &ok, m_parent));
    if (!ok || archiveName.isEmpty())
        return QString();

    // file extensions are determined from the type of compression
    archiveName.remove(QRegExp("\\.tar\\.(gz|bz2)$"));

    // Update the list of archive names, keep only the ten most recent ones.
    archiveList.removeAt(archiveList.indexOf(archiveName));
    archiveList.prepend(archiveName);
    if (archiveList.count() > 10)
        archiveList.pop_back();

    // set the correct extension and mimetype
    QString mimetype;
    if (bzipCompression) {
        archiveName += ".tar.bz2";
        mimetype = "application/x-bzip";
    } else {
        archiveName += ".tar.gz";
        mimetype = "application/x-gzip";
    }
    return buildArchive(fileList, m_tempDir.name() + archiveName, mimetype, true);
}

QString KBabelMailer::buildArchive(const QStringList &fileList, const QString &archiveName, const QString &mimetype, bool remove)
{
    Q_UNUSED(remove)

    // create a new archive
    KTar archive(archiveName, mimetype);
    if (!archive.open(QIODevice::WriteOnly)) {
        KMessageBox::error(m_parent, i18n("Error while trying to create archive file."));
        return QString();
    }

    // add files to this archive
    QStringList::const_iterator it;
    for (it = fileList.constBegin(); it != fileList.constEnd(); ++it) {
        // Try to get a local URL instead of a remote one
        const KUrl url(KIO::NetAccess::mostLocalUrl(KUrl(*it), m_parent));
        QString poTempName;
        if (!KIO::NetAccess::download(url, poTempName, m_parent)) {
            KMessageBox::error(m_parent, i18n("Error while trying to read file %1.", url.prettyUrl()) );
            continue;
        }

        // The files in the archive are stored relatively to the PO base dir
        // but only if "PoBaseDir" in the config file is set and the files
        // actually reside in one of its subdirectories. Else they are stored
        // without relative path.
        QString poArchFileName = url.path();
        if (_poBaseDir.isEmpty() || poArchFileName.indexOf(_poBaseDir) != 0)
            poArchFileName = QFileInfo(poArchFileName).fileName();
        else
            poArchFileName.remove(QRegExp('^' + QRegExp::escape(_poBaseDir) + "/?"));

        if (!archive.addLocalFile(poTempName, poArchFileName))
            KMessageBox::error(m_parent, i18n("Error while trying to copy file %1 into archive.", url.prettyUrl()));

        KIO::NetAccess::removeTempFile(poTempName);
    }
    archive.close();

    return archive.fileName();
}

void KBabelMailer::readConfig()
{
    // The relevant variables are never stored in catalogmanagerrc but in
    // project config file. Therefore they are read from the project.

    MiscSettings _settings = _project->miscSettings();

    bzipCompression = _settings.useBzip;
    singleFileCompression = _settings.compressSingleFile;

    KConfigGroup miscGroup(_project->config(), "Misc");

    archiveList = miscGroup.readEntry("MailArchiveNames", QStringList());

    _poBaseDir = _project->catManSettings().poBaseDir;
}

void KBabelMailer::saveConfig()
{
    // For an explanation see readConfig( )
    KConfigGroup miscGroup(_project->config(), "Misc");

    miscGroup.writeEntry("MailArchiveNames", archiveList);
}

// kate: space-indent on; indent-width 4; replace-tabs on;
