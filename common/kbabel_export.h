/*
    This file is part of kbabel
    Copyright (c) 2006 Laurent Montel <montel@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
*/

#ifndef _KBABEL_EXPORT_H
#define _KBABEL_EXPORT_H

#include <kdemacros.h>

#ifdef Q_WS_WIN

#ifndef KBABELCOMMON_EXPORT
# ifdef MAKE_KBABELCOMMON_LIB
#  define KBABELCOMMON_EXPORT KDE_EXPORT
# else
#  define KBABELCOMMON_EXPORT KDE_IMPORT
# endif
#endif

#ifndef KBABELCOMMONUI_EXPORT
# ifdef MAKE_KBABELCOMMONUI_LIB
#  define KBABELCOMMONUI_EXPORT KDE_EXPORT
# else
#  define KBABELCOMMONUI_EXPORT KDE_IMPORT
# endif
#endif


#ifndef KBABELDICTPLUGIN_EXPORT
# ifdef MAKE_KBABELDICTPLUGIN_LIB
#  define KBABELDICTPLUGIN_EXPORT KDE_EXPORT
# else
#  define KBABELDICTPLUGIN_EXPORT KDE_IMPORT
# endif
#endif

#ifndef KBXLIFF_EXPORT
# ifdef MAKE_KBXLIFF_LIB
#  define KBXLIFF_EXPORT KDE_EXPORT
# else
#  define KBXLIFF_EXPORT KDE_IMPORT
# endif
#endif

#ifndef KBXLIFFUI_EXPORT
# ifdef MAKE_KBXLIFFUI_LIB
#  define KBXLIFFUI_EXPORT KDE_EXPORT
# else
#  define KBXLIFFUI_EXPORT KDE_IMPORT
# endif
#endif

#else // not windows

#define KBABELCOMMON_EXPORT KDE_EXPORT
#define KBABELCOMMONUI_EXPORT KDE_EXPORT
#define KBABELDICTPLUGIN_EXPORT KDE_EXPORT
#define KBXLIFF_EXPORT KDE_EXPORT
#define KBXLIFFUI_EXPORT KDE_EXPORT

#endif /* not windows */

#endif /* _KBABEL_EXPORT_H */

// kate: space-indent on; indent-width 4; replace-tabs on;
