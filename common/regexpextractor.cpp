/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2001 by Matthias Kiefer <matthias.kiefer@gmx.de>
                2002 by Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>

  based on code of Andrea Rizzi <rizzi@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.
**************************************************************************** */

#include "regexpextractor.h"

#include <kdebug.h>
#include <qregexp.h>

using namespace KBabel;

RegExpExtractor::RegExpExtractor(const QStringList &regexps)
    : _matchesIterator(-1), _regExpList(regexps)
{
    _string.clear();
}

void RegExpExtractor::setString(const QString &string)
{
    _string = string;
    processString();
}

uint RegExpExtractor::countMatches()
{
    return _matches.count();
}

QString RegExpExtractor::firstMatch()
{
    _matchesIterator = 0;

    return match(_matchesIterator);
}

QString RegExpExtractor::nextMatch()
{
    ++_matchesIterator;

    return match(_matchesIterator);
}

QString RegExpExtractor::match(uint tagnumber)
{
    if ((int) tagnumber < _matches.count())
        return _matches.at(tagnumber).extracted;

    return QString();
}

int RegExpExtractor::matchIndex(uint tagnumber)
{
    if ((int) tagnumber < _matches.count())
        return _matches.at(tagnumber).index;

    return -1;
}

QString RegExpExtractor::prevMatch()
{
    --_matchesIterator;

    return match(_matchesIterator);
}

QString RegExpExtractor::lastMatch()
{
    _matchesIterator = _matches.count() - 1;

    return match(_matchesIterator);
} 

QStringList RegExpExtractor::matches()
{
    QStringList list;

    foreach (const MatchedEntryInfo &ti, _matches)
        list.append(ti.extracted);

    return list;
}

QString RegExpExtractor::plainString(bool keepPos)
{
    QString tmp = _string;

    foreach (const MatchedEntryInfo &ti, _matches) {
        uint len = ti.extracted.length();
        QString s;

        for (uint i = 0; i < len; i++)
             s += ' ';

        tmp.replace(ti.index, len, s);
    }

    if (!keepPos)
        tmp = tmp.trimmed();

    return tmp;
}

QString RegExpExtractor::matchesReplaced(const QString &replace)
{
    QString tmp = _string;

    int posCorrection = 0;
    int replaceLen = replace.length();

    foreach (const MatchedEntryInfo &ti, _matches) {
        uint len = ti.extracted.length();
        tmp.replace(ti.index + posCorrection, len, replace);

        posCorrection += (replaceLen - len);
    }

    return tmp;
}

void RegExpExtractor::processString()
{
    _matches.clear();
    _matchesIterator = -1;

    // if there is no regexp to be matched, quit
    if (regExpList().empty())
        return;

    QList<MatchedEntryInfo> tmpList;

    bool found = false;
    QString tmp = _string;

    do {
        found = false;
        QStringList::Iterator it;

        for (it = _regExpList.begin(); it != _regExpList.end(); ++it) {
            int pos = -1;
            QString tag;
            QRegExp reg = QRegExp((*it));
            pos = reg.indexIn(tmp);
            int len = reg.matchedLength();

            if (pos >= 0)
                tag = tmp.mid(pos, len);

            if (pos >= 0) {
                found = true;
                MatchedEntryInfo ti;
                ti.index = pos;
                ti.extracted = tag;
                tmpList.append(ti);
                QString s;

                for (int i = 0; i < tag.length(); i++)
                    s += ' ';

                tmp.replace(pos, tag.length(), s);
                break;
            }
        }
    }
    while (found);

    while (!tmpList.isEmpty()) {
        uint n = 0;
        uint min = _string.length();
        uint counter = 0;

        foreach (const MatchedEntryInfo &entry, tmpList) {
            if (entry.index < min) {
                min = entry.index;
                n = counter;
            }
            counter++;
        }

        const MatchedEntryInfo entry = tmpList.at(n);

        _matches.append(entry);
        tmpList.removeAt(n);
    }
}

QStringList RegExpExtractor::regExpList()
{
    return _regExpList;
}

void RegExpExtractor::setRegExpList(const QStringList &regexps)
{
    _regExpList = regexps;
}

void RegExpExtractor::addRegExpIdentifier(const QString &regExp)
{
    _regExpList.append(regExp);
}

void RegExpExtractor::deleteRegExpIdentifier(const QString &regExp)
{
    _regExpList.removeAt(_regExpList.indexOf(regExp));
}

// kate: space-indent on; indent-width 4; encoding utf-8; replace-tabs on;
