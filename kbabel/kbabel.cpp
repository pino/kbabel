/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
                2002-2004 by Stanislav Visnovsky
                            <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "kbabel.h"

#include "kbabelsettings.h"
#include "kbabelpref.h"
#include "projectpref.h"
#include "regexpextractor.h"
#include "toolaction.h"
#include "commentview.h"
#include "contextview.h"
#include "charselectview.h"
#include "taglistview.h"
#include "sourceview.h"
#include "catalog_interface.h"

#include <q3dragobject.h>
#include <qlineedit.h>
#include <q3popupmenu.h>

#include <qsize.h>
#include <qtextcodec.h>

#include <qtimer.h>
#include <QWheelEvent>
#include <Q3CString>
#include <Q3PtrList>
#include <QDropEvent>
#include <QLabel>
#include <QDragEnterEvent>
#include <kicon.h>
#include <QDBusMessage>
#include <QDBusConnection>

#include <kactionmenu.h>
#include <ktoggleaction.h>
#include <kactioncollection.h>
#include <krecentfilesaction.h>
#include <kdatatool.h>
#include <kmenu.h>
#include <kstatusbar.h>
#include <kstandardshortcut.h>
#include <kedittoolbar.h>
#include <kglobal.h>
#include <kled.h>
#include <klocale.h>
#include <kiconloader.h>
#include <ktoolbar.h>
#include <kfiledialog.h>
#include <kconfig.h>
#include <kurl.h>
#include <kdialog.h>
#include <qprogressbar.h>
#include <kpushbutton.h>
#include <kmessagebox.h>
#include <kwindowsystem.h>
#include <kaction.h>
#include <kstandardaction.h>
#include <k3spelldlg.h>
#include <ksqueezedtextlabel.h>
#include <ktoolinvocation.h>
#include <kvbox.h>
#include <kxmlguifactory.h>
#include <kconfiggroup.h>

#include "resources.h"
#include "kbcatalog.h"
#include "dictionarymenu.h"
#include "kbabeldictbox.h"
#include "kbmailer.h"
#include "kbbookmarkhandler.h"
#include "kbprojectmanager.h"
#include "projectpref.h"
#include "projectwizard.h"

#include "version.h"

#define ID_STATUS_TOTAL 1
#define ID_STATUS_CURRENT 2
#define ID_STATUS_FUZZY 3
#define ID_STATUS_UNTRANS 4
#define ID_STATUS_EDITMODE 5
#define ID_STATUS_READONLY 6
#define ID_STATUS_CURSOR 7
#define ID_STATUS_PROGRESSBAR 8

// maximum number of recent files
#define MAX_RECENT 10

using namespace KBabel;

Q3PtrList<KBabelPreferences> KBabelMW::prefDialogs;

// ### TODO: is such a mini-class really needed?
class MyQProgress: public QProgressBar
{
public:
    MyQProgress( QWidget *parent ) : QProgressBar( parent )
    {
        setSizePolicy( QSizePolicy::Minimum, QSizePolicy::Preferred );
    }
    QSize sizeHint() const { return QSize( 1, 1);}
};

KBabelMW::KBabelMW(const QString& projectFile_)
        : KXmlGuiWindow ()
{
    QString projectFile( projectFile_ );
    if ( projectFile.isEmpty() )
	 projectFile = ProjectManager::defaultProjectName();

    _project = ProjectManager::open(projectFile);

    if ( !_project )
    {
	KMessageBox::error( this, i18n("Cannot open project file\n%1", projectFile)
            , i18n("Project File Error"));
	_project = ProjectManager::open(ProjectManager::defaultProjectName());
    }

    KBCatalog* catalog=new KBCatalog(projectFile);
    init(catalog);
}

KBabelMW::KBabelMW(KBCatalog* catalog, const QString& projectFile_)
        : KXmlGuiWindow ()
{
    QString projectFile( projectFile_ );
    if ( projectFile.isEmpty() )
	 projectFile = ProjectManager::defaultProjectName();
    _project = ProjectManager::open(projectFile);

    if ( !_project )
    {
	KMessageBox::error( this, i18n("Cannot open project file\n%1", projectFile)
            , i18n("Project File Error"));
	_project = ProjectManager::open(ProjectManager::defaultProjectName());
    }

    init(catalog);
}

void KBabelMW::init(KBCatalog* catalog)
{
    _config = KSharedConfig::openConfig( "kbabelrc" );

    _toolsShortcuts.clear();

    _fuzzyLed=0;
    _untransLed=0;
    _errorLed=0;

    _projectDialog=0;

    _prefDialog=0;
    prefDialogs.setAutoDelete(true);

    _statusbarTimer = new QTimer( this );
    _statusbarTimer->setObjectName( "statusbartimer" );
    connect(_statusbarTimer,SIGNAL(timeout()),this
        ,SLOT(clearStatusbarMsg()));

    // FIXME:
    Q_ASSERT(_project);

    m_view=new KBabelView(catalog,this, _project);

    setXMLFile ("kbabelui.rc");

    createGUI (0);

    // accept dnd
    setAcceptDrops(true);


    // setup our menubars and toolbars
    setupStatusBar();
    setupActions();
    stateChanged( "fileopened" , StateReverse );
    stateChanged( "readonly", StateNoReverse );

        {
        Q3PopupMenu* popup;
        popup = (Q3PopupMenu*)factory()->container("rmb_edit", this);
        if(popup)
        {
                m_view->setRMBEditMenu(popup);
        }
        }
        {
        QMenu* popup;
        popup = qobject_cast<QMenu *>(factory()->container("rmb_search", this));
        if(popup)
        {
                m_view->setRMBSearchMenu(popup);
        }
        }


    connect(catalog,SIGNAL(signalUndoAvailable(bool)),this
            ,SLOT(enableUndo(bool)));
    connect(catalog,SIGNAL(signalRedoAvailable(bool)),this
            ,SLOT(enableRedo(bool)));
    connect(catalog,SIGNAL(signalNumberOfFuzziesChanged(uint)),this
            ,SLOT(setNumberOfFuzzies(uint)));
    connect(catalog,SIGNAL(signalNumberOfUntranslatedChanged(uint)),this
            ,SLOT(setNumberOfUntranslated(uint)));
    connect(catalog,SIGNAL(signalTotalNumberChanged(uint)),this
            ,SLOT(setNumberOfTotal(uint)));
    connect(catalog,SIGNAL(signalProgress(int)),_progressBar,SLOT(setValue(int)));
    connect(catalog,SIGNAL(signalClearProgressBar()),this,SLOT(clearProgressBar()));
    connect(catalog,SIGNAL(signalResetProgressBar(QString,int))
           ,this,SLOT(prepareProgressBar(QString,int)));
    connect(catalog,SIGNAL(signalFileOpened(bool)),this,SLOT(enableDefaults(bool)));
    connect(catalog,SIGNAL(signalFileOpened(bool)),m_view,SLOT(newFileOpened(bool)));
    connect(catalog,SIGNAL(signalModified(bool)),this,SLOT(showModified(bool)));

    // allow the view to change the statusbar and caption
    connect(m_view, SIGNAL(signalChangeStatusbar(const QString&)),
            this,   SLOT(changeStatusbar(const QString&)));
    connect(m_view, SIGNAL(signalChangeCaption(const QString&)),
            this,   SLOT(changeCaption(const QString&)));
    connect(m_view,SIGNAL(signalFirstDisplayed(bool, bool)),this
           ,SLOT(firstEntryDisplayed(bool, bool)));
    connect(m_view,SIGNAL(signalLastDisplayed(bool, bool)),this
           ,SLOT(lastEntryDisplayed(bool, bool)));
    connect(m_view,SIGNAL(signalFuzzyDisplayed(bool)),this
           ,SLOT(fuzzyDisplayed(bool)));
    connect(m_view,SIGNAL(signalUntranslatedDisplayed(bool)),this
           ,SLOT(untranslatedDisplayed(bool)));
    connect(m_view,SIGNAL(signalFaultyDisplayed(bool)),this
           ,SLOT(faultyDisplayed(bool)));
    connect(m_view,SIGNAL(signalDisplayed(const KBabel::DocPosition&)),this
           ,SLOT(displayedEntryChanged(const KBabel::DocPosition&)));
    connect(m_view,SIGNAL(signalFuzzyAfterwards(bool)),this
            ,SLOT(hasFuzzyAfterwards(bool)));
    connect(m_view,SIGNAL(signalFuzzyInFront(bool)),this
            ,SLOT(hasFuzzyInFront(bool)));
    connect(m_view,SIGNAL(signalUntranslatedAfterwards(bool)),this
            ,SLOT(hasUntranslatedAfterwards(bool)));
    connect(m_view,SIGNAL(signalUntranslatedInFront(bool)),this
            ,SLOT(hasUntranslatedInFront(bool)));
    connect(m_view,SIGNAL(signalErrorAfterwards(bool)),this
            ,SLOT(hasErrorAfterwards(bool)));
    connect(m_view,SIGNAL(signalErrorInFront(bool)),this
            ,SLOT(hasErrorInFront(bool)));
    connect(m_view,SIGNAL(signalBackHistory(bool)),this
            ,SLOT(enableBackHistory(bool)));
    connect(m_view,SIGNAL(signalForwardHistory(bool)),this
            ,SLOT(enableForwardHistory(bool)));


    connect(m_view,SIGNAL(ledColorChanged(const QColor&)),this
                  ,SLOT(setLedColor(const QColor&)));


    connect(m_view,SIGNAL(signalSearchActive(bool)),this,SLOT(enableStop(bool)));

    connect(m_view,SIGNAL(signalProgress(int)),_progressBar,SLOT(setValue(int)));
    connect(m_view,SIGNAL(signalClearProgressBar()),this,SLOT(clearProgressBar()));
    connect(m_view,SIGNAL(signalResetProgressBar(QString,int))
           ,this,SLOT(prepareProgressBar(QString,int)));

    connect(m_view,SIGNAL(signalDictionariesChanged())
           , this, SLOT(buildDictMenus()));
    connect(m_view,SIGNAL(signalCursorPosChanged(int,int)), this
            , SLOT(updateCursorPosition(int,int)));

    if(!catalog->currentURL().isEmpty())
    {
       enableDefaults(catalog->isReadOnly());
       setNumberOfFuzzies(catalog->numberOfFuzzies());
       setNumberOfUntranslated(catalog->numberOfUntranslated());
       setNumberOfTotal(catalog->numberOfEntries());

       enableUndo(catalog->isUndoAvailable());
       enableUndo(catalog->isRedoAvailable());

       m_view->emitEntryState();

       changeCaption(catalog->currentURL().prettyUrl() );
    }

    mailer = new KBabelMailer( this, _project );

    bmHandler = new KBabelBookmarkHandler(qobject_cast<QMenu *>(factory()->container("bookmark", this)));
    // the earlier created KAction for "clear_bookmarks" is now reconnected
    QAction* action = actionCollection()->action("clear_bookmarks");
    if (action) {
      action->disconnect(SIGNAL(activated()));
      connect(action, SIGNAL(activated()),
              bmHandler, SLOT(slotClearBookmarks()));
    }
    connect(bmHandler, SIGNAL(signalBookmarkSelected(int)),
            this, SLOT(slotOpenBookmark(int)));
    connect(m_view, SIGNAL(signalNewFileOpened(KUrl)),
            bmHandler, SLOT(slotClearBookmarks()));

    KConfigGroup kbg( _config, "KBabel" );
    restoreSettings();

    if(!kbg.hasKey("Version"))
    {
      QString encodingStr;
      switch(catalog->saveSettings().encoding)
      {
         case KBabel::ProjectSettingsBase::UTF8:
            encodingStr=QTextCodec::codecForName("UTF-8")->name();
            break;
         case KBabel::ProjectSettingsBase::UTF16:
            encodingStr=QTextCodec::codecForName("UTF-16")->name();
            break;
         default:
            encodingStr=QTextCodec::codecForLocale()->name();
      }

      KMessageBox::information(0,i18n("You have not run KBabel before. "
                   "To allow KBabel to work correctly you must enter some "
                   "information in the preferences dialog first.\n"
                   "The minimum requirement is to fill out the Identity page.\n"
                   "Also check the encoding on the Save page, which is currently "
                   "set to %1. You may want to change this setting "
                   "according to the settings of your language team.", encodingStr));

      QTimer::singleShot(1,this,SLOT(projectConfigure()));
    }

    kbg.writeEntry("Version",KBABEL_VERSION);
    kbg.sync();

}

KBabelMW::~KBabelMW()
{
   if(_prefDialog)
   {
      prefDialogs.remove(_prefDialog);
   }
   if(_projectDialog)
   {
      delete _projectDialog;
   }
   delete mailer;
   delete bmHandler;
}


void KBabelMW::setSettings(SaveSettings saveOpts,IdentitySettings idOpts)
{
   m_view->updateSettings();
   m_view->catalog()->setSettings(saveOpts);
   m_view->catalog()->setSettings(idOpts);

   if(_fuzzyLed)
   {
      _fuzzyLed->setColor(KBabelSettings::ledColor());
   }
   if(_untransLed)
   {
      _untransLed->setColor(KBabelSettings::ledColor());
   }
   if(_errorLed)
   {
      _errorLed->setColor(KBabelSettings::ledColor());
   }

}

void KBabelMW::updateSettings()
{
   m_view->updateSettings();

   if(_fuzzyLed)
   {
      _fuzzyLed->setColor(KBabelSettings::ledColor());
   }
   if(_untransLed)
   {
      _untransLed->setColor(KBabelSettings::ledColor());
   }
   if(_errorLed)
   {
      _errorLed->setColor(KBabelSettings::ledColor());
   }

}


void KBabelMW::setupActions()
{
   KAction* action;

   // the file menu
   action = KStandardAction::open(this, SLOT(fileOpen()), actionCollection());

   a_recent = KStandardAction::openRecent(this, SLOT(openRecent(const KUrl&)), actionCollection());
   a_recent->setMaxItems(MAX_RECENT);

   action = KStandardAction::revert(m_view,SLOT(revertToSaved()),actionCollection());
   action = KStandardAction::save(this, SLOT(fileSave()), actionCollection());
   action = KStandardAction::saveAs(this, SLOT(fileSaveAs()), actionCollection());

   KAction *actionSaveSpecial = actionCollection()->addAction( "save_special" );
   actionSaveSpecial->setText( i18n("Save Sp&ecial...") );
   connect( actionSaveSpecial, SIGNAL( triggered() ), this, SLOT( fileSaveSpecial() ) );

   KAction *actionSetPackage = actionCollection()->addAction( "set_package" );
   actionSetPackage->setText( i18n("Set &Package...") );
   connect( actionSetPackage, SIGNAL( triggered() ), m_view, SLOT( setFilePackage() ) );

   action = KStandardAction::mail(this, SLOT(fileMail()), actionCollection());

   KAction *actionNewView = actionCollection()->addAction( "file_new_view" );
   actionNewView->setText( i18n("&New View") );
   connect( actionNewView, SIGNAL( triggered() ), this, SLOT( fileNewView() ) );

   KAction *actionNewWindow = actionCollection()->addAction( "file_new_window" );
   actionNewWindow->setText( i18n("New &Window") );
   actionNewWindow->setShortcuts( KStandardShortcut::openNew() );
   connect( actionNewWindow, SIGNAL( triggered() ), this, SLOT( fileNewWindow() ) );

   action = KStandardAction::quit(this, SLOT(quit()), actionCollection());



   // the edit menu
   action = KStandardAction::undo(m_view, SLOT(undo()), actionCollection());
   action = KStandardAction::redo(m_view, SLOT(redo()), actionCollection());
   action = KStandardAction::cut(m_view, SIGNAL(signalCut()), actionCollection());
   action = KStandardAction::copy(m_view, SIGNAL(signalCopy()), actionCollection());
   action = KStandardAction::paste(m_view, SIGNAL(signalPaste()), actionCollection());
   action = KStandardAction::selectAll(m_view, SIGNAL(signalSelectAll()), actionCollection());
   action = KStandardAction::find(m_view, SLOT(find()), actionCollection());
   action = KStandardAction::findNext(m_view, SLOT(findNext()), actionCollection());
   action = KStandardAction::findPrev(m_view, SLOT(findPrev()), actionCollection());
   action = KStandardAction::replace(m_view, SLOT(replace()), actionCollection());

   action = actionCollection()->addAction(KStandardAction::Clear, "clear", m_view, SLOT(clear()));

   KAction *actionMsgid2Msgstr = actionCollection()->addAction( "msgid2msgstr" );
   actionMsgid2Msgstr->setIcon( KIcon( "msgid2msgstr" ) );
   actionMsgid2Msgstr->setText( i18n("Cop&y Msgid to Msgstr") );
   actionMsgid2Msgstr->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Space ) );
   connect( actionMsgid2Msgstr, SIGNAL( triggered() ), m_view, SLOT( msgid2msgstr() ) );

   KAction *actionSearch2Msgstr = actionCollection()->addAction( "search2msgstr" );
   actionSearch2Msgstr->setIcon( KIcon( "search2msgstr" ) );
   actionSearch2Msgstr->setText( i18n("Copy Searc&h Result to Msgstr") );
   actionSearch2Msgstr->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_Space ) );
   connect( actionSearch2Msgstr, SIGNAL( triggered() ), m_view, SLOT( search2msgstr() ) );

   KAction *actionPlural2Msgstr = actionCollection()->addAction( "plural2msgstr" );
   actionPlural2Msgstr->setText( i18n("Copy Msgstr to Other &Plurals") );
   actionPlural2Msgstr->setShortcut( QKeySequence( Qt::Key_F11 ) );
   connect( actionPlural2Msgstr, SIGNAL( triggered() ), m_view, SLOT( plural2msgstr() ) );

   KAction *actionSelectedChar2Msgstr = actionCollection()->addAction( "char2msgstr" );
   actionSelectedChar2Msgstr->setText( i18n("Copy Selected Character to Msgstr") );
   actionSelectedChar2Msgstr->setShortcut( QKeySequence( Qt::Key_F10 ) );
   connect( actionSelectedChar2Msgstr, SIGNAL( triggered() ), m_view, SLOT( char2msgstr() ) );

   a_unsetFuzzy = actionCollection()->addAction( "edit_toggle_fuzzy" );
   a_unsetFuzzy->setIcon( KIcon( "togglefuzzy" ) );
   a_unsetFuzzy->setText( i18n("To&ggle Fuzzy Status") );
   a_unsetFuzzy->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_U ) );
   connect( a_unsetFuzzy, SIGNAL( triggered() ), m_view, SLOT( removeFuzzyStatus() ) );

   KAction *actionEditHeader = actionCollection()->addAction( "edit_edit_header" );
   actionEditHeader->setText( i18n("&Edit Header...") );
   connect( actionEditHeader, SIGNAL( triggered() ), m_view, SLOT( editHeader() ) );

   KAction *actionInsertNextTag = actionCollection()->addAction( "insert_next_tag" );
   actionInsertNextTag->setIcon( KIcon( "insert_tag" ) );
   actionInsertNextTag->setText( i18n("&Insert Next Tag") );
   actionInsertNextTag->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_N ) );
   connect( actionInsertNextTag, SIGNAL( triggered() ), m_view, SLOT( insertNextTag() ) );

   connect( m_view, SIGNAL( signalNextTagAvailable( bool ) ), action, SLOT( setEnabled( bool ) ) );

   KAction *actionInsertNextTagMsgid = actionCollection()->addAction( "insert_next_tag_msgid" );
   actionInsertNextTagMsgid->setIcon( KIcon( "insert_tag" ) );
   actionInsertNextTagMsgid->setText( i18n("Insert Next Tag From Msgid P&osition") );
   actionInsertNextTagMsgid->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_M ) );
   connect( actionInsertNextTagMsgid, SIGNAL( triggered() ), m_view, SLOT( insertNextTagMsgid() ) );

   connect( m_view, SIGNAL( signalNextTagAvailable( bool ) ), action, SLOT( setEnabled( bool ) ) );

   KActionMenu *actionMenu= actionCollection()->add<KActionMenu>("insert_tag");
   actionMenu->setIcon(KIcon("insert_tag"));
   actionMenu->setText(i18n("Inser&t Tag"));
   m_view->setTagsMenu(qobject_cast<QMenu *>(actionMenu->menu()));
   connect(m_view,SIGNAL(signalTagsAvailable(bool)),actionMenu
           ,SLOT(setEnabled(bool)));
   connect(actionMenu,SIGNAL(activated()),m_view,SLOT(insertNextTag()));

   KAction *actionShowTagsMenu = actionCollection()->addAction( "show_tags_menu" );
   actionShowTagsMenu->setText( i18n("Show Tags Menu") );
   actionShowTagsMenu->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Less ) );
   actionShowTagsMenu->setEnabled( false );
   connect( actionShowTagsMenu, SIGNAL( triggered() ), m_view, SLOT( showTagsMenu() ) );

   connect( m_view, SIGNAL( signalTagsAvailable( bool ) ), action, SLOT( setEnabled( bool ) ) );

   KAction *actionMoveToNextTag = actionCollection()->addAction( "move_to_next_tag" );
   actionMoveToNextTag->setText( i18n("Move to Next Tag") );
   actionMoveToNextTag->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_M ) );
   connect( actionMoveToNextTag, SIGNAL( triggered() ), m_view, SLOT( skipToNextTag() ) );

   KAction *actionMoveToPrevTag = actionCollection()->addAction( "move_to_prev_tag" );
   actionMoveToPrevTag->setText( i18n("Move to Previous Tag") );
   actionMoveToPrevTag->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_B ) );
   connect( actionMoveToPrevTag, SIGNAL( triggered() ), m_view, SLOT( skipToPreviousTag() ) );

   KAction *actionInsertNextArg = actionCollection()->addAction( "insert_next_arg" );
   actionInsertNextArg->setIcon( KIcon( "insert_arg" ) );
   actionInsertNextArg->setText( i18n("Insert Next Argument") );
   actionInsertNextArg->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_G ) );
   connect( actionInsertNextArg, SIGNAL( triggered() ), m_view, SLOT( insertNextArg() ) );

   connect( m_view, SIGNAL( signalNextArgAvailable( bool ) ), action, SLOT( setEnabled( bool ) ) );

   actionMenu= actionCollection()->add<KActionMenu>("insert_arg");
   actionMenu->setIcon(KIcon("insert_arg"));
   actionMenu->setText(i18n("Inser&t Argument"));
   m_view->setArgsMenu(qobject_cast<QMenu *>(actionMenu->menu()));
   connect(m_view,SIGNAL(signalArgsAvailable(bool)),actionMenu
           ,SLOT(setEnabled(bool)));
   connect(actionMenu,SIGNAL(activated()),m_view,SLOT(insertNextArg()));

   KAction *actionShowArgsMenu = actionCollection()->addAction( "show_args_menu" );
   actionShowArgsMenu->setText( i18n("Show Arguments Menu") );
   actionShowArgsMenu->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Percent ) );
   actionShowArgsMenu->setEnabled( false );
   connect( actionShowArgsMenu, SIGNAL( triggered() ), m_view, SLOT( showArgsMenu() ) );

   connect(m_view,SIGNAL(signalArgsAvailable(bool)),action
           ,SLOT(setEnabled(bool)));

    // next, the go-menu
   KAction *actionPrevious = actionCollection()->addAction( "go_prev_entry" );
   actionPrevious->setIcon( KIcon( "go-previous" ) );
   actionPrevious->setText( i18n("&Previous") );
   actionPrevious->setShortcuts( KStandardShortcut::shortcut(KStandardShortcut::Prior) );
   connect( actionPrevious, SIGNAL( triggered() ), m_view , SLOT( gotoPrev() ) );

   KAction *actionNext = actionCollection()->addAction( "go_next_entry" );
   actionNext->setIcon( KIcon( "go-next" ) );
   actionNext->setText( i18n("&Next") );
   actionNext->setShortcuts( KStandardShortcut::shortcut(KStandardShortcut::Next) );
   connect( actionNext, SIGNAL( triggered() ), m_view , SLOT( gotoNext() ) );

   action = KStandardAction::goTo(m_view, SLOT(gotoEntry()), actionCollection());
   action->setShortcuts(KStandardShortcut::gotoLine());
   action = KStandardAction::firstPage(m_view, SLOT(gotoFirst()),actionCollection());
   action->setText(i18n("&First Entry"));
   action->setShortcut(QKeySequence(Qt::CTRL+Qt::ALT+Qt::Key_Home));
   action = KStandardAction::lastPage(m_view, SLOT(gotoLast()),actionCollection());
   action->setText(i18n("&Last Entry"));
   action->setShortcut(QKeySequence(Qt::CTRL+Qt::ALT+Qt::Key_End));

   a_prevFoU = actionCollection()->addAction( "go_prev_fuzzyUntr" );
   a_prevFoU->setIcon( KIcon( "prevfuzzyuntrans" ) );
   a_prevFoU->setText( i18n("P&revious Fuzzy or Untranslated") );
   a_prevFoU->setShortcut( QKeySequence( Qt::CTRL+Qt::SHIFT+Qt::Key_PageUp ) );
   connect( a_prevFoU, SIGNAL( triggered() ), m_view, SLOT( gotoPrevFuzzyOrUntrans() ) );

   a_nextFoU = actionCollection()->addAction( "go_next_fuzzyUntr" );
   a_nextFoU->setIcon( KIcon( "nextfuzzyuntrans" ) );
   a_nextFoU->setText( i18n("N&ext Fuzzy or Untranslated") );
   a_nextFoU->setShortcut( QKeySequence( Qt::CTRL+Qt::SHIFT+Qt::Key_PageDown ) );
   connect( a_nextFoU, SIGNAL( triggered() ), m_view, SLOT( gotoNextFuzzyOrUntrans() ) );

   a_prevFuzzy = actionCollection()->addAction( "go_prev_fuzzy" );
   a_prevFuzzy->setIcon( KIcon( "prevfuzzy" ) );
   a_prevFuzzy->setText( i18n("Pre&vious Fuzzy") );
   a_prevFuzzy->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_PageUp ) );
   connect( a_prevFuzzy, SIGNAL( triggered() ), m_view, SLOT( gotoPrevFuzzy() ) );

   a_nextFuzzy = actionCollection()->addAction( "go_next_fuzzy" );
   a_nextFuzzy->setIcon( KIcon( "nextfuzzy" ) );
   a_nextFuzzy->setText( i18n("Ne&xt Fuzzy") );
   a_nextFuzzy->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_PageDown ) );
   connect( a_nextFuzzy, SIGNAL( triggered() ), m_view, SLOT( gotoNextFuzzy() ) );

   a_prevUntrans = actionCollection()->addAction( "go_prev_untrans" );
   a_prevUntrans->setIcon( KIcon( "prevuntranslated" ) );
   a_prevUntrans->setText( i18n("Prev&ious Untranslated") );
   a_prevUntrans->setShortcut( QKeySequence( Qt::ALT+Qt::Key_PageUp ) );
   connect( a_prevUntrans, SIGNAL( triggered() ), m_view, SLOT( gotoPrevUntranslated() ) );

   a_nextUntrans = actionCollection()->addAction( "go_next_untrans" );
   a_nextUntrans->setIcon( KIcon( "nextuntranslated" ) );
   a_nextUntrans->setText( i18n("Nex&t Untranslated") );
   a_nextUntrans->setShortcut( QKeySequence( Qt::ALT+Qt::Key_PageDown ) );
   connect( a_nextUntrans, SIGNAL( triggered() ), m_view, SLOT( gotoNextUntranslated() ) );

   KAction *actionPrevError = actionCollection()->addAction( "go_prev_error" );
   actionPrevError->setIcon( KIcon( "preverror" ) );
   actionPrevError->setText( i18n("Previo&us Error") );
   actionPrevError->setShortcut( QKeySequence( Qt::SHIFT+Qt::Key_PageUp ) );
   connect( actionPrevError, SIGNAL( triggered() ), m_view, SLOT( gotoPrevError() ) );

   KAction *actionNextError = actionCollection()->addAction( "go_next_error" );
   actionNextError->setIcon( KIcon( "nexterror" ) );
   actionNextError->setText( i18n("Next Err&or") );
   actionNextError->setShortcut( QKeySequence( Qt::SHIFT+Qt::Key_PageDown ) );
   connect( actionNextError, SIGNAL( triggered() ), m_view, SLOT( gotoNextError() ) );

   KAction *actionBackHistory = actionCollection()->addAction( "go_back_history" );
   actionBackHistory->setIcon( KIcon( "go-previous" ) );
   actionBackHistory->setText( i18n("&Back in History") );
   actionBackHistory->setShortcut( QKeySequence( Qt::ALT+Qt::Key_Left ) );
   connect( actionBackHistory, SIGNAL( triggered() ), m_view, SLOT( backHistory() ) );

   KAction *actionForwardHistory = actionCollection()->addAction( "go_forward_history" );
   actionForwardHistory->setIcon( KIcon( "go-next" ) );
   actionForwardHistory->setText( i18n("For&ward in History") );
   actionForwardHistory->setShortcut( QKeySequence( Qt::ALT+Qt::Key_Right ) );
   connect( actionForwardHistory, SIGNAL( triggered() ), m_view, SLOT( forwardHistory() ) );

    // the search menu
   actionMenu=actionCollection()->add<KActionMenu>("dict_search_all");
   actionMenu->setIcon(KIcon("transsearch"));
   actionMenu->setText(i18n("&Find Text"));
   connect(actionMenu,SIGNAL(activated()),m_view,SLOT(startSearch()));
   dictMenu = new DictionaryMenu(actionMenu->menu(),actionCollection(),this);
   connect(dictMenu,SIGNAL(activated(const QString)), m_view
           , SLOT(startSearch(const QString)));

   actionMenu=actionCollection()->add<KActionMenu>("dict_search_selected");
   actionMenu->setIcon(KIcon("transsearch"));
   actionMenu->setText(i18n("F&ind Selected Text"));
   connect(actionMenu,SIGNAL(activated()),m_view,SLOT(startSelectionSearch()));
   selectionDictMenu = new DictionaryMenu(actionMenu->menu(),actionCollection(),this);
   connect(selectionDictMenu,SIGNAL(activated(const QString)), m_view
           , SLOT(startSelectionSearch(const QString)));

   actionMenu=actionCollection()->add<KActionMenu>("dict_edit");
   actionMenu->setIcon(KIcon("transsearch"));
   actionMenu->setText(i18n("&Edit Dictionary"));
   editDictMenu = new DictionaryMenu(actionMenu->menu(),actionCollection(),this);
   connect(editDictMenu,SIGNAL(activated(const QString)), m_view
           , SLOT(editDictionary(const QString)));


   actionMenu=actionCollection()->add<KActionMenu>("dict_configure");
   actionMenu->setIcon(KIcon("transsearch"));
   actionMenu->setText(i18n("Con&figure Dictionary"));
   configDictMenu = new DictionaryMenu(actionMenu->menu(),actionCollection(),this);
   connect(configDictMenu,SIGNAL(activated(const QString)), m_view
           , SLOT(configureDictionary(const QString)));

   actionMenu=actionCollection()->add<KActionMenu>( "dict_about");
   actionMenu->setIcon(KIcon("transsearch"));
   actionMenu->setText(i18n("About Dictionary"));
   aboutDictMenu = new DictionaryMenu(actionMenu->menu(),actionCollection(),this);
   connect(aboutDictMenu,SIGNAL(activated(const QString)), m_view
           , SLOT(aboutDictionary(const QString)));

   buildDictMenus();

    // the project menu
   action = actionCollection()->addAction("project_new");
   action->setIcon(KIcon("document-new"));
   action->setText(i18n("&New..."));
   connect(action, SIGNAL(triggered ( bool)),this, SLOT(projectNew()));

   action = actionCollection()->addAction("project_open");
   action->setIcon(KIcon("document-open"));
   action->setText(i18n("&Open..."));
   connect(action, SIGNAL(triggered ( bool)),this, SLOT(projectOpen()));

   action = actionCollection()->addAction("project_close");
   action->setIcon(KIcon("window-close"));
   action->setText(i18n("C&lose"));
   connect(action, SIGNAL(triggered ( bool)),this, SLOT(projectClose()));
   action->setEnabled (_project->filename() != ProjectManager::defaultProjectName() );

   action = actionCollection()->addAction("project_settings");
   action->setIcon(KIcon("configure"));
   action->setText(i18n("&Configure..."));
   connect(action, SIGNAL(triggered ( bool)),this, SLOT(projectConfigure()));

   a_recentprojects = actionCollection()->add<KRecentFilesAction>( "recent_projects" );
   a_recentprojects->setText( i18n("Open &Recent") );
   connect( a_recentprojects, SIGNAL( urlSelected( const KUrl& ) ), this, SLOT(projectOpenRecent( const KUrl& ) ) );

    // the tools menu
   KAction *actionSpellCheckCommon = actionCollection()->addAction( "spellcheck_common" );
   actionSpellCheckCommon->setIcon( KIcon( "tools-check-spelling" ) );
   actionSpellCheckCommon->setText( i18n("&Spell Check...") );
   actionSpellCheckCommon->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_I ) );
   connect( actionSpellCheckCommon, SIGNAL( triggered() ), m_view, SLOT( spellcheckCommon() ) );

   KAction *actionSpellCheckAll = actionCollection()->addAction( "spellcheck_all" );
   actionSpellCheckAll->setIcon( KIcon( "spellcheck_all" ) );
   actionSpellCheckAll->setText( i18n("&Check All...") );
   connect( actionSpellCheckAll, SIGNAL( triggered() ), m_view, SLOT( spellcheckAll() ) );

   KAction *actionSpellCheckFromCursor = actionCollection()->addAction( "spellcheck_from_cursor" );
   actionSpellCheckFromCursor->setIcon( KIcon( "spellcheck_from_cursor" ) );
   actionSpellCheckFromCursor->setText( i18n("C&heck From Cursor Position...") );
   connect( actionSpellCheckFromCursor, SIGNAL( triggered() ), m_view, SLOT( spellcheckFromCursor() ) );

   KAction *actionSpellCheckCurrent = actionCollection()->addAction( "spellcheck_current" );
   actionSpellCheckCurrent->setIcon( KIcon( "spellcheck_actual" ) );
   actionSpellCheckCurrent->setText( i18n("Ch&eck Current...") );
   connect( actionSpellCheckCurrent, SIGNAL( triggered() ), m_view, SLOT( spellcheckCurrent() ) );

   KAction *actionSpellCheckFromCurrent = actionCollection()->addAction( "spellcheck_from_current" );
   actionSpellCheckFromCurrent->setText( i18n("Check Fro&m Current to End of File...") );
   connect( actionSpellCheckFromCurrent, SIGNAL( triggered() ), m_view, SLOT( spellcheckFromCurrent() ) );

   KAction *actionSpellCheckSelected = actionCollection()->addAction( "spellcheck_marked" );
   actionSpellCheckSelected->setIcon( KIcon( "spellcheck_selected" ) );
   actionSpellCheckSelected->setText( i18n("Chec&k Selected Text...") );
   connect( actionSpellCheckSelected, SIGNAL( triggered() ), m_view, SLOT( spellcheckMarked() ) );

   KToggleAction *toggleAction;

   toggleAction = actionCollection()->add<KToggleAction>( "diff_toggleDiff" );
   toggleAction->setIcon( KIcon( "autodiff" ) );
   toggleAction->setText( i18n("&Diffmode") );
   connect(toggleAction,SIGNAL(toggled(bool)), m_view, SLOT(toggleAutoDiff(bool)));
   connect(m_view,SIGNAL(signalDiffEnabled(bool)), toggleAction
               , SLOT(setChecked(bool)));
   toggleAction->setChecked(m_view->autoDiffEnabled());

   KAction *actionShowDiff = actionCollection()->addAction( "diff_diff" );
   actionShowDiff->setIcon( KIcon( "diff" ) );
   actionShowDiff->setText( i18n("&Show Diff") );
   actionShowDiff->setShortcut( QKeySequence( Qt::Key_F5 ) );
   connect( actionShowDiff, SIGNAL( triggered() ), m_view, SLOT( diff() ) );

   KAction *actionShowOriginal = actionCollection()->addAction( "diff_showOrig" );
   actionShowOriginal->setIcon( KIcon( "help-contents" ) );
   actionShowOriginal->setText( i18n("S&how Original Text") );
   actionShowOriginal->setShortcut( QKeySequence( Qt::Key_F6 ) );
   connect( actionShowOriginal, SIGNAL( triggered() ), m_view, SLOT( diffShowOrig() ) );

   KAction *actionDiffOpenFile = actionCollection()->addAction( "diff_openFile" );
   actionDiffOpenFile->setIcon( KIcon( "document-open" ) );
   actionDiffOpenFile->setText( i18n("&Open File for Diff") );
   connect( actionDiffOpenFile, SIGNAL( triggered() ), m_view, SLOT( openDiffFile() ) );

   KAction *actionRoughTranslation = actionCollection()->addAction( "rough_translation" );
   actionRoughTranslation->setText( i18n("&Rough Translation...") );
   connect( actionRoughTranslation, SIGNAL( triggered() ), m_view, SLOT( roughTranslation() ) );

   KAction *actionCatalogManager = actionCollection()->addAction( "open_catalog_manager" );
   actionCatalogManager->setIcon( KIcon( "catalogmanager" ) );
   actionCatalogManager->setText( i18n("&Catalog Manager...") );
   connect( actionCatalogManager, SIGNAL( triggered() ), this, SLOT( openCatalogManager() ) );

   KAction *actionToggleEditMode = actionCollection()->addAction( "toggle_insert_mode" );
   actionToggleEditMode->setText( i18n("Toggle Edit Mode") );
   actionToggleEditMode->setShortcut( QKeySequence( Qt::Key_Insert ) );
   connect( actionToggleEditMode, SIGNAL( triggered() ), this, SLOT( toggleEditMode() ) );

   KAction *actionWordCount = actionCollection()->addAction( "word_count" );
   actionWordCount->setText( i18n("&Word Count") );
   connect( actionWordCount, SIGNAL( triggered() ), m_view, SLOT( wordCount() ) );

   // next, the settings menu
   createStandardStatusBarAction();

   KStandardAction::configureToolbars(this,SLOT(optionsEditToolbars()),actionCollection());

   KStandardAction::keyBindings(guiFactory(),SLOT(configureShortcuts()),actionCollection());
   KStandardAction::preferences(this,SLOT(optionsPreferences()),actionCollection());

   setStandardToolBarMenuEnabled ( true );

   KAction *actionStopSearching = actionCollection()->addAction( "stop_search" );
   actionStopSearching->setIcon( KIcon( "process-stop" ) );
   actionStopSearching->setText( i18n("&Stop Searching") );
   actionStopSearching->setShortcut( QKeySequence( Qt::Key_Escape ) );
   actionStopSearching->setEnabled( false );
   connect( actionStopSearching, SIGNAL( triggered() ), m_view, SLOT( stopSearch() ) );

   KAction *actionGettextInfo = actionCollection()->addAction( "help_gettext" );
   actionGettextInfo->setText( i18n("&Gettext Info") );
   connect( actionGettextInfo, SIGNAL( triggered() ), this, SLOT( gettextHelp() ) );


   // the bookmarks menu

   KAction *actionAddBookmark = actionCollection()->addAction(KStandardAction::AddBookmark, "add_bookmark",
                                                              this, SLOT(slotAddBookmark()));
   actionAddBookmark->setEnabled( false );
   // this action is now connected to dummySlot(), and later reconnected
   // to bmHandler after that object actually is created
   KAction *actionClearBookmarks = actionCollection()->addAction( "clear_bookmarks" );
   actionClearBookmarks->setText( i18n("Clear Bookmarks") );
   connect( actionClearBookmarks, SIGNAL( triggered() ), this, SLOT( dummySlot() ) );

   setupDynamicActions();

   createGUI(0);

   QMenu *popup = static_cast<QMenu*>( factory()->container( "settings", this ) );
   popup->addMenu( m_view->viewMenu() );
}


void KBabelMW::setupStatusBar()
{
    statusBar()->insertItem(i18n("Current: 0"),ID_STATUS_CURRENT);
    statusBar()->insertItem(i18n("Total: 0"),ID_STATUS_TOTAL);
    statusBar()->insertItem(i18n("Fuzzy: 0"),ID_STATUS_FUZZY);
    statusBar()->insertItem(i18n("Untranslated: 0"),ID_STATUS_UNTRANS);

    if(KBabelSettings::ledInStatusbar())
    {
       QColor ledColor=KBabelSettings::ledColor();
       KHBox* statusBox = new KHBox(statusBar());
       statusBox->setSpacing(2);
       new QLabel(' '+i18n("Status: "),statusBox);
       _fuzzyLed = new KLed(ledColor,KLed::Off,KLed::Sunken,KLed::Rectangular
                   ,statusBox);
       _fuzzyLed->setFixedSize(15,12);
       new QLabel(i18n("fuzzy")+' ',statusBox);
       _untransLed = new KLed(ledColor,KLed::Off,KLed::Sunken,KLed::Rectangular
                   ,statusBox);
       _untransLed->setFixedSize(15,12);
       new QLabel(i18n("untranslated")+' ',statusBox);
       _errorLed = new KLed(ledColor,KLed::Off,KLed::Sunken,KLed::Rectangular
                   ,statusBox);
       _errorLed->setFixedSize(15,12);
       new QLabel(i18n("faulty")+' ',statusBox);

       statusBox->setFixedWidth(statusBox->sizeHint().width());
       statusBar()->addWidget(statusBox);
    }

    statusBar()->insertItem(i18n("INS"),ID_STATUS_EDITMODE);

    statusBar()->insertItem(i18n("RW"),ID_STATUS_READONLY);

    statusBar()->insertItem(i18n("Line: %1 Col: %2", 1, 1)
            ,ID_STATUS_CURSOR);

    KHBox* progressBox = new KHBox(statusBar());
    progressBox->setSpacing(2);
    _progressLabel = new KSqueezedTextLabel( "", progressBox );
    _progressBar = new MyQProgress(progressBox);
    _progressBar->setObjectName("progressbar");
    _progressBar->hide();
    progressBox->setStretchFactor(_progressBar,1);

    statusBar()->insertPermanentWidget(ID_STATUS_PROGRESSBAR,progressBox,1);

    statusBar()->setWhatsThis(
       i18n("<qt><p><b>Statusbar</b></p>\n\
<p>The statusbar displays some information about the opened file,\n\
like the total number of entries and the number of fuzzy and untranslated\n\
messages. Also the index and the status of the currently displayed entry is shown.</p></qt>"));

}

void KBabelMW::setupDynamicActions()
{
   // dynamic validation tools
   QList<KDataToolInfo> tools = ToolAction::validationTools();

   QList<KAction *> actions = ToolAction::dataToolActionList(
      tools, m_view, SLOT(validateUsingTool( const KDataToolInfo &, const QString & )),
      QStringList("validate"), false, actionCollection() );

   KActionMenu* m_menu = actionCollection()->add<KActionMenu>("dynamic_validation_tools");
   m_menu->setText(i18n("&Validation"));

   KAction* ac = actionCollection()->addAction( "check_all" );
   ac->setText( i18n("Perform &All Checks") );
   ac->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_E ) );
   ac->setEnabled( false );
   connect( ac, SIGNAL( triggered() ), m_view, SLOT( checkAll() ) );
   m_menu->addAction( ac );

   ac = new KAction( this );
   ac->setSeparator( true );
   m_menu->addAction( ac );

   ac = actionCollection()->addAction( "check_syntax" );
   ac->setText( i18n("C&heck Syntax") );
   ac->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_T ) );
   ac->setEnabled( false );
   connect( ac, SIGNAL( triggered() ), m_view, SLOT( checkSyntax() ) );
   m_menu->addAction( ac );

   foreach ( ac, actions )
   {
        m_menu->addAction(ac);
   }

   // dynamic modify tools

   // query available tools
   QList<KDataToolInfo> allTools = KDataToolInfo::query
        ("CatalogItem", "application/x-kbabel-catalogitem", KGlobal::mainComponent());

   // skip read-only tools for single items
   QList<KDataToolInfo> modifyTools;

   foreach( const KDataToolInfo &info, allTools )
   {
        if( !info.isReadOnly() )
        {
            modifyTools.append( info );
        }
   }

   // create corresponding actions
   actions = ToolAction::dataToolActionList(
      modifyTools, m_view, SLOT(modifyUsingTool( const KDataToolInfo &, const QString & )),
      QStringList("validate"), true, actionCollection() );

   // skip validation actions
   foreach ( ac, actions )
   {
        m_menu->addAction(ac);
   }

   // insert tools
   m_menu = actionCollection()->add<KActionMenu>("dynamic_modify_tools");
   m_menu->setText(i18n("&Modify"));
   foreach ( ac, actions )
   {
        m_menu->addAction(ac);
   }

   // query available tools for whole catalog
   allTools = KDataToolInfo::query
        ("Catalog", "application/x-kbabel-catalog", KGlobal::mainComponent());

   // skip read-only tools
   foreach( const KDataToolInfo &info, allTools )
   {
        if( !info.isReadOnly() )
        {
            modifyTools.append( info );
        }
   }

   // create corresponding actions
   actions = ToolAction::dataToolActionList(
      modifyTools, m_view, SLOT(modifyUsingTool( const KDataToolInfo &, const QString & )),
      QStringList("validate"), true, actionCollection() );

   // skip validation actions
   foreach ( ac, actions )
   {
        m_menu->addAction(ac);
   }

   // create corresponding actions
   actions = ToolAction::dataToolActionList(
      modifyTools, m_view, SLOT(modifyCatalogUsingTool( const KDataToolInfo &, const QString & )),
      QStringList("validate"), true, actionCollection() );

   // insert tools
   m_menu = actionCollection()->add<KActionMenu>("dynamic_modify_tools");
   m_menu->setText(i18n("&Modify"));
   foreach ( ac, actions )
   {
        m_menu->addAction(ac);
   }
}

void KBabelMW::saveSettings()
{
    {
        saveMainWindowSettings( _config->group( "View") );
    }

    {
       a_recent->saveEntries( _config->group( QString() ) );
       a_recentprojects->saveEntries( _config->group( "Project") );
    }

    _config->sync();
}

void KBabelMW::restoreSettings()
{
    {
       KConfigGroup group( _config, "View" );
       applyMainWindowSettings( group );
    }

    {
       a_recent->loadEntries(_config->group( QString() ));

       a_recentprojects->loadEntries( _config->group( "Project") );
    }
}



void KBabelMW::saveProperties(KConfigGroup &config)
{
   m_view->saveSession(config);
}

void KBabelMW::readProperties(const KConfigGroup &config)
{
   m_view->restoreSession(config);

   // need to ensure that the windows is propertly setup also
   // for new views-only
   if(!m_view->currentURL().isEmpty())
   {
       KBCatalog* catalog=m_view->catalog();
       enableDefaults(catalog->isReadOnly());
       setNumberOfFuzzies(catalog->numberOfFuzzies());
       setNumberOfUntranslated(catalog->numberOfUntranslated());
       setNumberOfTotal(catalog->numberOfEntries());

       enableUndo(catalog->isUndoAvailable());
       enableUndo(catalog->isRedoAvailable());

       m_view->emitEntryState();

       changeCaption(catalog->currentURL().prettyUrl() );
    }
}

bool KBabelMW::queryClose()
{
   if(m_view->isSearching())
   {
       connect(m_view,SIGNAL(signalSearchActive(bool)),this,SLOT(quit()));
       m_view->stopSearch();
       return false;
   }

   if(m_view->catalog()->isActive())
   {
       // stop the activity and try again
       m_view->catalog()->stop();
       QTimer::singleShot(0, this, SLOT( close() ));
       return false;
   }

   if(m_view->isModified())
   {
      switch(KMessageBox::warningYesNoCancel(this,
      i18n("The document contains unsaved changes.\n\
Do you want to save your changes or discard them?"),i18n("Warning"),
      KStandardGuiItem::save(),KStandardGuiItem::discard()))
      {
         case KMessageBox::Yes:
         {
            return m_view->saveFile();
         }
         case KMessageBox::No:
            return true;
         default:
            return false;
      }
   }

  return true;
}

bool KBabelMW::queryExit()
{
   saveSettings();
   m_view->saveSettings();
   return true;
}

void KBabelMW::quit()
{
    close();
}


void KBabelMW::dragEnterEvent(QDragEnterEvent *event)
{
    // accept uri drops only
    event->setAccepted(KUrl::List::canDecode(event->mimeData()));
}

void KBabelMW::dropEvent(QDropEvent *event)
{
    KUrl::List uri = KUrl::List::fromMimeData(event->mimeData());
    // see if we can decode a URI.. if not, just ignore it
    if (!uri.isEmpty())
    {
       m_view->processUriDrop(uri,mapToGlobal(event->pos()));
    }
}

void KBabelMW::wheelEvent(QWheelEvent *e)
{
    m_view->wheelEvent(e);
}

void KBabelMW::openRecent(const KUrl& url)
{
   KBabelView *view = KBabelView::viewForURL(url, QString());
   if(view)
   {
#ifdef Q_OS_UNIX
       KWindowSystem::activateWindow(view->topLevelWidget()->winId());
#endif
       return;
   }

   m_view->open(url);
}

void KBabelMW::open(const KUrl& url)
{
   open(url, QString(), false);
}

void KBabelMW::open(const KUrl& url, const QString package, bool newWindow)
{
   kDebug(KBABEL) << "opening file with project:" << _project->filename();
   kDebug(KBABEL) << "URL:" << url.prettyUrl();
   KBabelView *view = KBabelView::viewForURL(url, _project->filename());
   if(view)
   {
        kDebug(KBABEL) << "there is a such view";
#ifdef Q_OS_UNIX
       KWindowSystem::activateWindow(view->topLevelWidget()->winId());
#endif
       return;
   }

   addToRecentFiles(url);

   if(newWindow)
   {
        kDebug(KBABEL) << "creating new window"<< endl;
      fileNewWindow()->open(url, package,false);
   }
   else
   {
        m_view->open(url,package);
   }
}

void KBabelMW::openTemplate(const KUrl& openURL,const KUrl& saveURL,const QString& package, bool newWindow)
{
   if(newWindow)
   {
      fileNewWindow()->openTemplate(openURL,saveURL,package,false);
   }
   else
   {
      m_view->openTemplate(openURL,saveURL);
      m_view->catalog()->setPackage(package);
   }
}

void KBabelMW::fileOpen()
{
    m_view->open();

    KUrl url=m_view->currentURL();
    addToRecentFiles(url);
}


void KBabelMW::addToRecentFiles(const KUrl& url)
{
   if( url.isValid() && ! url.isEmpty() )
        a_recent->addUrl(url);
}

void KBabelMW::fileSave()
{
    // do it asynchronously due to kdelibs bug
    QTimer::singleShot( 0, this, SLOT( fileSave_internal() ));
}

void KBabelMW::fileSave_internal()
{
    // this slot is called whenever the File->Save menu is selected,
    // the Save shortcut is pressed (usually CTRL+S) or the Save toolbar
    // button is clicked

    if(!m_view->isModified())
    {
       statusBar()->showMessage( i18n("There are no changes to save."), 2000 );
    }
    else
    {
       // disable save
       QAction* saveAction=actionCollection()->action( KStandardAction::name( KStandardAction::Save) );
       saveAction->setEnabled(false);

       m_view->saveFile();

       KUrl url=m_view->currentURL();

       QByteArray arg = (url.directory(KUrl::AppendTrailingSlash)+url.fileName()).utf8();
       QDBusMessage message =
          QDBusMessage::createSignal("/CatalogManager", "org.kde.kbabel.catalogmanager", "updateFiles");
       message << arg;
       if(!QDBusConnection::sessionBus().send(message))
	       kDebug(KBABEL) << "Unable to send file update info via D-Bus";

       // reenable save action
       saveAction->setEnabled(true);
    }
}

void KBabelMW::fileSaveAs()
{
    m_view->saveFileAs();
    KUrl url=m_view->currentURL();
    QByteArray arg = (url.directory(KUrl::AppendTrailingSlash)+url.fileName()).utf8();
    QDBusMessage message =
        QDBusMessage::createSignal("/CatalogManager", "org.kde.kbabel.catalogmanager", "updateFiles");
    message << arg;
    if(!QDBusConnection::sessionBus().send(message))
            kDebug(KBABEL) << "Unable to send file update info via D-Bus";



}

void KBabelMW::fileSaveSpecial()
{
    if( !m_view->saveFileSpecial() ) return;

    KUrl url=m_view->currentURL();
    QByteArray arg = (url.directory(KUrl::AppendTrailingSlash)+url.fileName()).utf8();
    QDBusMessage message =
        QDBusMessage::createSignal("/CatalogManager", "org.kde.kbabel.catalogmanager", "updateFiles");
    message << arg;
    if(!QDBusConnection::sessionBus().send(message))
            kDebug(KBABEL) << "Unable to send file update info via D-Bus";

}

void KBabelMW::fileMail()
{
    if( m_view->isModified() ) fileSave();
    mailer->sendOneFile( m_view->currentURL() );
}

void KBabelMW::fileNewView()
{
   KBabelMW* b=new KBabelMW(m_view->catalog(),_project->filename());
   b->updateSettings();
   b->initBookmarks(bmHandler->bookmarks());
   b->show();
}

KBabelMW* KBabelMW::fileNewWindow()
{
   KBabelMW* b=new KBabelMW(_project->filename());
   b->setSettings(m_view->catalog()->saveSettings(),m_view->catalog()->identitySettings());
   b->show();

   return b;
}

void KBabelMW::toggleEditMode()
{
   bool ovr=!m_view->isOverwriteMode();
//TODO get rid of it
//   m_view->setOverwriteMode(ovr);

   if(ovr)
      statusBar()->changeItem(i18n("OVR"),ID_STATUS_EDITMODE);
   else
      statusBar()->changeItem(i18n("INS"),ID_STATUS_EDITMODE);

}

void KBabelMW::optionsShowStatusbar(bool on)
{
   if(on)
   {
      statusBar()->show();
   }
   else
   {
      statusBar()->hide();
   }
}

void KBabelMW::optionsEditToolbars()
{
   saveMainWindowSettings( KGlobal::config()->group( "View" ) );
   KEditToolBar dlg(actionCollection());
   connect(&dlg, SIGNAL(newToolbarConfig()), this, SLOT(newToolbarConfig()));
   dlg.exec();
}

void KBabelMW::newToolbarConfig()
{
    createGUI(0);
    applyMainWindowSettings( KGlobal::config()->group( "View" ) );
}

void KBabelMW::optionsPreferences()
{
    if(!_prefDialog)
    {
        _prefDialog = new KBabelPreferences(m_view->dictionaries());
        prefDialogs.append(_prefDialog);

        connect(_prefDialog,SIGNAL(settingsChanged(const QString&))
                ,m_view,SLOT(updateSettings()));
    }

    int prefHeight=_prefDialog->height();
    int prefWidth=_prefDialog->width();
    int width=this->width();
    int height=this->height();

    int x=width/2-prefWidth/2;
    int y=height/2-prefHeight/2;

    _prefDialog->move(mapToGlobal(QPoint(x,y)));

    if(!_prefDialog->isVisible())
    {
       _prefDialog->show();
    }

    _prefDialog->raise();
#ifdef Q_OS_UNIX
    KWindowSystem::activateWindow(_prefDialog->winId());
#endif
}

void KBabelMW::setLedColor(const QColor& color)
{
   if(_fuzzyLed)
   {
      _fuzzyLed->setColor(color);
   }
   if(_untransLed)
   {
      _untransLed->setColor(color);
   }
   if(_errorLed)
   {
      _errorLed->setColor(color);
   }
}

void KBabelMW::openCatalogManager()
{
   QDBusConnection dbus = QDBusConnection::sessionBus();
   QDBusReply<QStringList> reply = dbus.interface()->registeredServiceNames();
   if ( !reply.isValid() )
      return;

   const QStringList allServices = reply;
   for ( QStringList::const_iterator it = allServices.begin(), end = allServices.end() ; it != end ; ++it ) {
	const QString service = *it;
        if ( service.startsWith( "org.kde.catalogmanager" ) ) {
		org::kde::kbabel::catalogmanager catalog( service, "/CatalogManager", dbus );
		QDBusReply<void> rep = catalog.setPreferredWindow(this->winId());
    		if( !rep.isValid())
			kDebug(KBABEL) << "Unable to set preferred window via D-Bus";
		return;
	}
   }

   QString prog = "catalogmanager";
   QString url = "";
   QString service ="";
   QString result="";
   if( KToolInvocation::startServiceByDesktopName(prog,url, &result,&service))
   {
        KMessageBox::error(this, i18n("Unable to use KLauncher to start "
           "Catalog Manager. You should check the installation of KDE.\n"
           "Please start Catalog Manager manually."));
   }

}



void KBabelMW::firstEntryDisplayed(bool firstEntry, bool firstForm)
{
   QAction* firstAction=actionCollection()->action(KStandardAction::name(KStandardAction::FirstPage));
   QAction* prevAction=actionCollection()->action("go_prev_entry");

   firstAction->setEnabled(!firstEntry);
   prevAction->setEnabled(!(firstEntry && firstForm));

}

void KBabelMW::lastEntryDisplayed(bool lastEntry, bool lastForm)
{
   QAction* lastAction=actionCollection()->action(KStandardAction::name(KStandardAction::LastPage));
   QAction* nextAction=actionCollection()->action("go_next_entry");

   lastAction->setEnabled(!lastEntry);
   nextAction->setEnabled(!(lastEntry && lastForm));
}

void KBabelMW::fuzzyDisplayed(bool flag)
{
    if(!_fuzzyLed)
       return;

    if(flag)
    {
       if(_fuzzyLed->state()==KLed::Off)
       {
          _fuzzyLed->on();
       }
    }
    else
    {
       if(_fuzzyLed->state()==KLed::On)
           _fuzzyLed->off();
    }
}

void KBabelMW::untranslatedDisplayed(bool flag)
{
    if(!_untransLed)
       return;

    // do not allow fuzzy toggle for untranslated
    QAction *action=actionCollection()->action("edit_toggle_fuzzy");
    if(action)
        action->setEnabled(!flag);


    if(flag)
    {
       if(_untransLed->state()==KLed::Off)
          _untransLed->on();
    }
    else
    {
       if(_untransLed->state()==KLed::On)
          _untransLed->off();
    }
}


void KBabelMW::faultyDisplayed(bool flag)
{
    if(!_errorLed)
       return;

    if(flag)
    {
       if(_errorLed->state()==KLed::Off)
          _errorLed->on();
    }
    else
    {
       if(_errorLed->state()==KLed::On)
          _errorLed->off();
    }
}


void KBabelMW::displayedEntryChanged(const KBabel::DocPosition& pos)
{
   statusBar()->changeItem(i18n("Current: %1", pos.item+1),ID_STATUS_CURRENT);
  _currentIndex = pos.item;
}

void KBabelMW::setNumberOfTotal(uint number)
{
   statusBar()->changeItem(i18n("Total: %1", number),ID_STATUS_TOTAL);
}

void KBabelMW::setNumberOfFuzzies(uint number)
{
   statusBar()->changeItem(i18n("Fuzzy: %1", number),ID_STATUS_FUZZY);
}

void KBabelMW::setNumberOfUntranslated(uint number)
{
   statusBar()->changeItem(i18n("Untranslated: %1", number),ID_STATUS_UNTRANS);
}

void KBabelMW::hasFuzzyAfterwards(bool flag)
{
   a_nextFuzzy->setEnabled(flag);

   // check if there is  a fuzzy or untranslated afterwards
   if( flag || a_nextUntrans->isEnabled() )
   {
       a_nextFoU->setEnabled(true);
   }
   else
   {
       a_nextFoU->setEnabled(false);
   }

}

void KBabelMW::hasFuzzyInFront(bool flag)
{
   a_prevFuzzy->setEnabled(flag);

   // check if there is  a fuzzy or untranslated in front
   if( flag || a_prevUntrans->isEnabled() )
   {
       a_prevFoU->setEnabled(true);
   }
   else
   {
       a_prevFoU->setEnabled(false);
   }
}

void KBabelMW::hasUntranslatedAfterwards(bool flag)
{
   a_nextUntrans->setEnabled(flag);

   // check if there is a fuzzy or untranslated afterwards
   if( flag || a_nextFuzzy->isEnabled() )
   {
       a_nextFoU->setEnabled(true);
   }
   else
   {
       a_nextFoU->setEnabled(false);
   }
}

void KBabelMW::hasUntranslatedInFront(bool flag)
{
   a_prevUntrans->setEnabled(flag);

   // check if there is  a fuzzy or translated in front
   if( flag || a_prevFuzzy->isEnabled() )
   {
       a_prevFoU->setEnabled(true);
   }
   else
   {
       a_prevFoU->setEnabled(false);
   }
}



void KBabelMW::hasErrorAfterwards(bool flag)
{
   QAction* action=actionCollection()->action("go_next_error");
   action->setEnabled(flag);
}

void KBabelMW::hasErrorInFront(bool flag)
{
   QAction* action=actionCollection()->action("go_prev_error");
   action->setEnabled(flag);
}


void KBabelMW::enableBackHistory(bool on)
{
   QAction* action=actionCollection()->action("go_back_history");
   action->setEnabled(on);
}

void KBabelMW::enableForwardHistory(bool on)
{
   QAction* action=actionCollection()->action("go_forward_history");
   action->setEnabled(on);
}


void KBabelMW::prepareProgressBar(const QString& msg,int max)
{
   if(_statusbarTimer->isActive())
        _statusbarTimer->stop();

   _progressBar->show();
   _progressLabel->setText(' '+msg);
   _progressBar->setRange( 0, max );
   _progressBar->setValue( 0 );

}

void KBabelMW::clearProgressBar()
{
   _progressBar->setValue( 0 );
   _progressBar->hide();
   _progressLabel->setText("      ");
}


void KBabelMW::changeStatusbar(const QString& text)
{
    // display the text on the statusbar
    _progressLabel->setText(' '+text);

    if(_statusbarTimer->isActive())
        _statusbarTimer->stop();

    _statusbarTimer->setSingleShot( true );
    _statusbarTimer->start( 5000 );
}

void KBabelMW::clearStatusbarMsg()
{
    _progressLabel->setText("");
}

void KBabelMW::changeCaption(const QString& text)
{
    // display the text on the caption
    setCaption(text + ( _project->filename () != KBabel::ProjectManager::defaultProjectName() ?
            " (" + _project->name() + ')' : "" /* KDE 3.4: i18n("(No project)")*/ )
        ,m_view->isModified());
}


void KBabelMW::showModified(bool on)
{
    // display the text on the caption
    setCaption(m_view->catalog()->package(),on);

    QAction *action=actionCollection()->action(
            KStandardAction::name(KStandardAction::Save));
    action->setEnabled(on);

    action=actionCollection()->action(KStandardAction::name(KStandardAction::Revert));
    action->setEnabled(on);
}


void KBabelMW::enableDefaults(bool readOnly)
{
    stateChanged( "readonly", readOnly ? StateNoReverse : StateReverse );
    stateChanged( "fileopened", StateNoReverse );

    if(readOnly)
       statusBar()->changeItem(i18n("RO"),ID_STATUS_READONLY);
    else
       statusBar()->changeItem(i18n("RW"),ID_STATUS_READONLY);
}

void KBabelMW::enableUndo(bool on)
{
   QAction* action=actionCollection()->action(KStandardAction::name(KStandardAction::Undo));
   action->setEnabled(on);
}

void KBabelMW::enableRedo(bool on)
{
   QAction* action=actionCollection()->action(KStandardAction::name(KStandardAction::Redo));
   action->setEnabled(on);
}

void KBabelMW::enableStop(bool flag)
{
   QAction* action=actionCollection()->action("stop_search");
   action->setEnabled(flag);
}

void KBabelMW::gettextHelp()
{
    QString error;
    KToolInvocation::startServiceByDesktopName("khelpcenter",
                QString("info:/gettext"), &error);

    if(!error.isEmpty())
    {
        KMessageBox::sorry(this,i18n("An error occurred while "
                "trying to open the gettext info page:\n%1", error));
    }
}

void KBabelMW::buildDictMenus()
{
   QList<ModuleInfo *> dictList = m_view->dictionaries();

   dictMenu->clear();
   selectionDictMenu->clear();
   configDictMenu->clear();
   editDictMenu->clear();
   aboutDictMenu->clear();

   foreach(ModuleInfo *info, dictList)
   {
      QString accel="Ctrl+Alt+%1";
      dictMenu->add(info->name,info->id, accel);

      accel=QString("Ctrl+%1");
      selectionDictMenu->add(info->name,info->id, accel);

      configDictMenu->add(info->name,info->id);
      aboutDictMenu->add(info->name,info->id);

      if(info->editable)
      {
         dictMenu->add(info->name,info->id);
      }
   }

   qDeleteAll(dictList);
}

void KBabelMW::updateCursorPosition(int line, int col)
{
    statusBar()->changeItem(i18n("Line: %1 Col: %2", line+1, col+1)
            ,ID_STATUS_CURSOR);
}


KBabelMW *KBabelMW::winForURL(const KUrl& url, const QString& project)
{
    KBabelMW *kb=0;

    KBabelView *v = KBabelView::viewForURL(url,project);
    if(v)
    {
        QObject *p = v->parent();
        while(p && !p->inherits("KBabelMW"))
        {
            p = p->parent();
        }

        if(p)
            kb = static_cast<KBabelMW*>(p);
    }

    return kb;
}

KBabelMW *KBabelMW::emptyWin(const QString& project)
{
    KBabelMW *kb=0;

    KBabelView *v = KBabelView::emptyView(project);
    if(v)
    {
        QObject *p = v->parent();
        while(p && !p->inherits("KBabelMW"))
        {
            p = p->parent();
        }

        if(p)
            kb = static_cast<KBabelMW*>(p);
    }

    return kb;
}

void KBabelMW::spellcheckMoreFiles(const QStringList& filelist)
{
    if( filelist.isEmpty() ) return;
    _toSpellcheck = filelist;
    connect( m_view, SIGNAL( signalSpellcheckDone(int) ), this, SLOT( spellcheckDone(int)));
    spellcheckDone( KS_IGNORE ); // use something else than KS_STOP
}

void KBabelMW::spellcheckDone( int result)
{
    if( _toSpellcheck.isEmpty() || result == KS_STOP)
    {
        disconnect( m_view, SIGNAL( signalSpellcheckDone(int)), this, SLOT(spellcheckDone( int)));
        KMessageBox::information( this, i18nc("MessageBox text", "Spellchecking of multiple files is finished."),
            i18nc("MessageBox caption", "Spellcheck Done"));
    }
    else
    {
        QString file = _toSpellcheck.first();
        _toSpellcheck.pop_front();
        if( m_view->isModified() ) fileSave();
        open(KUrl( file ), QString(), false);
        kDebug(KBABEL) << "Starting another spellcheck";
        QTimer::singleShot( 1, m_view, SLOT(spellcheckAllMulti()));
    }
}

void KBabelMW::initBookmarks(const QList<KBabelBookmark *>& list)
{
  bmHandler->setBookmarks(list);
}

void KBabelMW::slotAddBookmark()
{
  bmHandler->addBookmark(_currentIndex,
    m_view->catalog()->msgid(_currentIndex).first());
}

void KBabelMW::slotOpenBookmark(int index)
{
  DocPosition pos;
  pos.item=index;
  pos.form=0;
  m_view->gotoEntry(pos);
}

void KBabelMW::projectNew()
{
    KBabel::Project::Ptr p = KBabel::ProjectWizard::newProject();
    if( p )
    {
        _project = p;
        m_view->useProject(p);
        changeProjectActions(p->filename());
    }
}

void KBabelMW::projectOpen()
{
    QString oldproject = m_view->project();
    if( oldproject == ProjectManager::defaultProjectName() )
    {
        oldproject = QString();
    }
    const QString file = KFileDialog::getOpenFileName(oldproject, QString(), this);
    if (file.isEmpty())
    {
        return;
    }

    projectOpen (file);
}

void KBabelMW::projectOpenRecent(const KUrl& url)
{
    projectOpen (url.path());
    KBabel::Project::Ptr p = ProjectManager::open(url.path());
    if( p )
    {
        _project = p;
        m_view->useProject(p);
        changeProjectActions(url.path());
    }
}

void KBabelMW::projectOpen(const QString& file)
{
    QString oldproject = m_view->project();
    if( oldproject == ProjectManager::defaultProjectName() )
    {
        oldproject = "";
    }
    if (file.isEmpty())
    {
        return;
    }
    KBabel::Project::Ptr p = KBabel::ProjectManager::open(file);
    if( p )
    {
        _project = p;
        m_view->useProject(p);
        changeProjectActions(file);
    }
    else
    {
	KMessageBox::error( this, i18n("Cannot open project file\n%1", file)
            , i18n("Project File Error"));
	_project = ProjectManager::open(ProjectManager::defaultProjectName());
	m_view->useProject(_project);
	changeProjectActions(ProjectManager::defaultProjectName());
    }
}

void KBabelMW::projectClose()
{
    m_view->useProject( ProjectManager::open(ProjectManager::defaultProjectName()) );
    _project = ProjectManager::open(ProjectManager::defaultProjectName());
    changeProjectActions(ProjectManager::defaultProjectName());
}

void KBabelMW::changeProjectActions(const QString& project)
{
    bool def = project == ProjectManager::defaultProjectName();

    QAction* saveAction=actionCollection()->action( "project_close" );
    saveAction->setEnabled( ! def );

    if (!def)
    {
	addToRecentProjects(project);
    }

    // if there is a project dialog, delete it (we have a different project now
    if (_projectDialog)
    {
	delete _projectDialog;
	_projectDialog = NULL;
    }
}

void KBabelMW::projectConfigure()
{
    if(!_projectDialog)
    {
        _projectDialog = new ProjectDialog(_project);
	connect (_projectDialog, SIGNAL (settingsChanged(const QString&))
	    , m_view, SLOT (updateProjectSettings()));
    }

    int prefHeight=_projectDialog->height();
    int prefWidth=_projectDialog->width();
    int width=this->width();
    int height=this->height();

    int x=width/2-prefWidth/2;
    int y=height/2-prefHeight/2;

    _projectDialog->move(mapToGlobal(QPoint(x,y)));

    if(!_projectDialog->isVisible())
    {
       _projectDialog->show();
    }

    _projectDialog->raise();
#ifdef Q_OS_UNIX
    KWindowSystem::activateWindow(_projectDialog->winId());
#endif
}

void KBabelMW::addToRecentProjects(const KUrl& url)
{
   if( url.isValid() && ! url.isEmpty() )
	a_recentprojects->addUrl(url);
}


#include "kbabel.moc"
