

#ifndef KBABELINTERFACE_H
#define KBABELINTERFACE_H

#include <kwindowsystem.h>
#include <QByteArray>
#include <kurl.h>
#include <kbabel.h>
#include <findoptions.h>

class KBabelInterface : public QObject	
{
    Q_OBJECT
public:
    KBabelInterface();

    virtual void openUrl(QByteArray url, QByteArray package, WId window, int newWindow);
    virtual void openUrl(QByteArray url, QByteArray package, WId window, int newWindow, QString projectFile);
    virtual void openTemplate(QByteArray openFilename, QByteArray saveFilename, QByteArray package, int newWindow );
    virtual void openTemplate(QByteArray openFilename, QByteArray saveFilename, QByteArray package, int newWindow, QString projectFile );
    virtual void gotoFileEntry(QString url, QString msgid);
    virtual void gotoFileEntry(QString url, QString package, int msgid);
    virtual void gotoFileEntry(QString url, QString package, int msgid, QString projectFile);
    virtual bool findInFile(QString fileSource, QString url,
        QString findStr, int caseSensitive, int wholeWords, int isRegExp,
        int inMsgid, int inMsgstr, int inComment,
        int ignoreAccelMarker, int ignoreContextInfo, int askForNextFile, int askForSave);
    virtual bool replaceInFile(QString fileSource, QString url,
        QString findStr, QString replaceStr, int caseSensitive, int wholeWords, int isRegExp,
        int inMsgid, int inMsgstr, int inComment,
        int ignoreAccelMarker, int ignoreContextInfo, int ask, int askForNextFile, int askForSave);
    virtual void spellcheck(const QStringList &fileList);
private:
    KBabelMW* findInstance( const KUrl& url, const QString& project, const QString& package) const;
};
#endif

