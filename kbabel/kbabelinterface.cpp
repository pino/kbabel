
#include <kbabelinterface.h>
#include "kbprojectmanager.h"

KBabelInterface::KBabelInterface()
    /*: DCOPObject("KBabelIFace")*/
{
}

void KBabelInterface::openUrl(QByteArray url, QByteArray package, WId window, int newWindow)
{
    openUrl( url,package,window,newWindow, KBabel::ProjectManager::defaultProjectName().toLocal8Bit() );
}

void KBabelInterface::openUrl(QByteArray url, QByteArray package, WId window, int newWindow, QString projectFile)
{
    const QString project( projectFile  );

    kDebug() << "openUrl " << url;

    KUrl u(QString::fromLocal8Bit(url));

    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Suspending DCOP";
    // kapp->dcopClient()->suspend();

    KBabelMW *kb = KBabelMW::winForURL(u,project);
    if(kb)
    {
#ifdef Q_OS_UNIX
        KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
    }
    else
    {
        if( !KMainWindow::memberList().isEmpty() )
        {
            // first, try to lookup correct winid

            // (As we have a QList, use an index instead of an iterator)
            // (We are assuming that the list can change.)
            for ( int index = 0; index < KMainWindow::memberList().size(); ++index )
            {
                KMainWindow* mw = KMainWindow::memberList().at( index );
                if( mw && mw->inherits("KBabelMW") && mw->winId() == window )
                {
                    kb = static_cast<KBabelMW*>(mw);
#ifdef Q_OS_UNIX
                    KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
                    kb->open(u, QString::fromUtf8(package),newWindow);

                    // TODO: There's no replacement for suspend or resume in QtDBus
                    // kDebug () << "Resuming DCOP";
                    // kapp->dcopClient()->resume();

                    return ;
                 }
            }
        }

        // now, the empty window
        kb = KBabelMW::emptyWin(projectFile);
        if (kb)
        {
            // here, we don't care about "open in new window", because
            // it's empty
#ifdef Q_OS_UNIX
            KWindowSystem::activateWindow( kb->topLevelWidget()->winId() );
#endif
            kb->projectOpen(projectFile);
            kb->open(u,QString::fromUtf8(package),false);

            // TODO: There's no replacement for suspend or resume in QtDBus
            // kDebug () << "Resuming DCOP";
            // kapp->dcopClient()->resume();

            return;
        }

        // ### TODO: why again? We have tried before and failed!
        if( !KMainWindow::memberList().isEmpty() )
        {
            // first, try to lookup correct winid

            // (As we have a QList, use an index instead of an iterator)
            // (We are assuming that the list can change.)
            for ( int index = 0; index < KMainWindow::memberList().size(); ++index )
            {
                KMainWindow* mw = KMainWindow::memberList().at( index );
                if( mw && mw->inherits("KBabelMW") && mw->winId() == window )
                {
                    kb = static_cast<KBabelMW*>(mw);
#ifdef Q_OS_UNIX
                    KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
                    kb->open(u, QString::fromUtf8(package),newWindow);

                    // TODO: There's no replacement for suspend or resume in QtDBus
                    // kDebug () << "Resuming DCOP";
                    // kapp->dcopClient()->resume();

                    return ;
                 }
            }
        }

        kb = new KBabelMW(project);
        kb->show();
#ifdef Q_OS_UNIX
        KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
        kb->open(u,QString::fromUtf8(package),newWindow);
    }

    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Resuming DCOP";
    // kapp->dcopClient()->resume();
}

void KBabelInterface::openTemplate(QByteArray openFilename, QByteArray saveFilename, QByteArray package, int newWindow)
{
    openTemplate( openFilename, saveFilename, package, newWindow,KBabel::ProjectManager::defaultProjectName().toLocal8Bit() );
}

void KBabelInterface::openTemplate(QByteArray openFilename, QByteArray saveFilename, QByteArray package, int newWindow, QString projectFile)
{
    const QString project( projectFile  );

    const KUrl u( QString::fromLocal8Bit( saveFilename ) );
    const KUrl t( QString::fromLocal8Bit( openFilename ) );

    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Suspending DCOP";
    // kapp->dcopClient()->suspend();

    KBabelMW *kb = KBabelMW::winForURL(u, project);
    if(kb)
    {
#ifdef Q_OS_UNIX
        KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
    }
    else
    {
        // ### TODO: Why is the code so much different than for opening a file? And why are we only testing the first main window and not even trying to find the first right window?
        kb = 0;
        // CAREFUL: QList::first() is undefined if the list is empty!
        if ( !KMainWindow::memberList().isEmpty() )
        {
            KMainWindow* mw = KMainWindow::memberList().first();

            if( mw && mw->inherits("KBabelMW") && static_cast<KBabelMW*>(mw)->project() == project )
            {
                kb = static_cast<KBabelMW*>(mw);
#ifdef Q_OS_UNIX
                KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
                kb->projectOpen(projectFile);
                kb->openTemplate(t,u,QString::fromUtf8(package),newWindow);
             }
        }
        if ( !kb )
        {
            // We have no main window or we have not found the correct one
            kb = new KBabelMW(project);
            kb->show();
#ifdef Q_OS_UNIX
            KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
            kb->openTemplate(t,u,QString::fromUtf8(package));
        }
    }

    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Resuming DCOP";
    // kapp->dcopClient()->resume();
}

void KBabelInterface::gotoFileEntry(QString url, QString m)
{
    const KUrl u( url );
    KBabelMW *kb = findInstance( u, KBabel::ProjectManager::defaultProjectName(), QString() );

    if(!kb) return;

    QString msgid = m;
    int index = kb->m_view->catalog()->indexForMsgid(msgid);
    if(index >= 0)
    {
        KBabel::DocPosition pos;
        pos.item=index;
        pos.form=0;
        kb->m_view->gotoEntry(pos);
    }
}

void KBabelInterface::gotoFileEntry(QString url, QString package, int m)
{
    gotoFileEntry( url, package, m, KBabel::ProjectManager::defaultProjectName().toLocal8Bit() );
}

void KBabelInterface::gotoFileEntry(QString url, QString package, int m, QString projectFile)
{
    const KUrl u ( url );
    const QString p ( package ); // ### VERIFY encoding!
    KBabelMW *kb = findInstance( u, projectFile, p );

    if(!kb) return;

    KBabel::DocPosition pos;
    pos.item=m;
    pos.form=0;
    kb->m_view->gotoEntry(pos);
}
bool KBabelInterface::findInFile(QString fileSource, QString url,
        QString findStr, int caseSensitive, int wholeWords, int isRegExp,
        int inMsgid, int inMsgstr, int inComment,
        int ignoreAccelMarker, int ignoreContextInfo, int askForNextFile, int askForSave)
{
    kDebug(KBABEL) << "findInFile (" <<fileSource<< "): " << url << " for " << findStr;

    const KUrl u( url );
    KBabelMW *kb = findInstance( u, KBabel::ProjectManager::defaultProjectName(), QString() );

    if(!kb) return false;

    KBabel::FindOptions options;
    options.findStr = findStr;
    options.caseSensitive = (caseSensitive>0);
    options.fromCursor = false;
    options.backwards = false;
    options.wholeWords = (wholeWords>0);
    options.isRegExp = (isRegExp>0);
    options.inMsgid = (inMsgid>0);
    options.inMsgstr = (inMsgstr>0);
    options.inComment = (inComment>0);
    options.ignoreAccelMarker = (ignoreAccelMarker>0);
    options.ignoreContextInfo = (ignoreContextInfo>0);
    options.askForNextFile = (askForNextFile>0);
    options.askForSave = (askForSave>0);
    kb->m_view->findInFile(fileSource.toLocal8Bit(), options);

    return true;
}

bool KBabelInterface::replaceInFile(QString fileSource, QString url,
        QString findStr, QString replaceStr, int caseSensitive, int wholeWords, int isRegExp,
        int inMsgid, int inMsgstr, int inComment,
        int ignoreAccelMarker, int ignoreContextInfo, int ask, int askForNextFile, int askForSave)
{
    kDebug(KBABEL) << "replaceInFile (" <<fileSource<< "): " << url << " for " << findStr;

    const KUrl u( url );
    KBabelMW *kb = findInstance( u, KBabel::ProjectManager::defaultProjectName(), QString() );

    if( !kb ) return false;

    KBabel::ReplaceOptions options;
    options.findStr = findStr;
    options.replaceStr = replaceStr;
    options.caseSensitive = (caseSensitive>0);
    options.fromCursor = false;
    options.backwards = false;
    options.wholeWords = (wholeWords>0);
    options.isRegExp = (isRegExp>0);
    options.inMsgid = (inMsgid>0);
    options.inMsgstr = (inMsgstr>0);
    options.inComment = (inComment>0);
    options.ignoreAccelMarker = (ignoreAccelMarker>0);
    options.ignoreContextInfo = (ignoreContextInfo>0);
    options.ask = (ask>0);
    options.askForNextFile = (askForNextFile>0);
    options.askForSave = (askForSave>0);
    kb->m_view->replaceInFile(fileSource.toLocal8Bit(), options);

    return true;
}

void KBabelInterface::spellcheck(const QStringList &fileList)
{
    // ### FIXME: the default project might use the wrong language!
    KBabelMW *kb = findInstance( KUrl(), KBabel::ProjectManager::defaultProjectName(), QString() );
    kb->show();
    kb->spellcheckMoreFiles( fileList );
}

KBabelMW* KBabelInterface::findInstance( const KUrl& url, const QString& project, const QString& package) const
{
    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Suspending DCOP";
    // kapp->dcopClient()->suspend();

    KBabelMW *kb = 0;
    if( !url.isEmpty() )
    {
        kb = KBabelMW::winForURL( url, project );

        if(kb)
        {
#ifdef Q_OS_UNIX
            KWindowSystem::activateWindow(kb->topLevelWidget()->winId());
#endif
        }
    }

    if( !kb )
    {
        kb = KBabelMW::emptyWin(project);
        if( !kb )
        {
            kb = new KBabelMW(project);
        }
        else
        {
            kb->projectOpen(project);
        }

        kb->show();
        if ( !url.isEmpty() )
            kb->open( url, package, false );
    }

    // TODO: There's no replacement for suspend or resume in QtDBus
    // kDebug () << "Resuming DCOP";
    // kapp->dcopClient()->resume();

    return kb;
}

#include <kbabelinterface.moc>
