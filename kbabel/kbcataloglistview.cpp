/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2004 by Asgeir Frimannsson
                            <asgeirf@redhat.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

**************************************************************************** */

#include "kbcataloglistview.h"
#include "kbcatalog.h"
#include "editcmd.h"

#include <klocale.h>
#include <qtreeview.h>
#include <qheaderview.h>
#include <qstandarditemmodel.h>
#include <qitemselectionmodel.h>
#include <qlayout.h>

using namespace KBabel;

enum {
    SortRole = Qt::UserRole + 1
};

template <typename T>
class SortableStandardItem : public QStandardItem
{
public:
    SortableStandardItem(const T& sortKey, int whichRole, const QString& text)
       : QStandardItem(text)
       , m_sortKey(sortKey)
       , m_whichRole(whichRole)
    {
    }

    virtual QVariant data(int role = Qt::UserRole + 1) const
    {
        if(role == m_whichRole)
            return m_sortKey;
        return QStandardItem::data(role);
    }

private:
    const T m_sortKey;
    const int m_whichRole;
};


KBCatalogListView::KBCatalogListView(KBCatalog* catalog, QWidget *parent, KBabel::Project::Ptr project)
 : QWidget(parent)
{
    m_catalog= catalog;
    QVBoxLayout* layout=new QVBoxLayout(this);
    
    m_listview = new QTreeView(this);
    m_listview->setObjectName( "catalogListView" );
    m_listview->setAllColumnsShowFocus(true);
    m_listview->setRootIsDecorated(false);
    m_listview->setAlternatingRowColors(true);
    m_listview->setWordWrap(true);
    m_listview->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_listview->setSortingEnabled(true);
    m_listview->setSelectionMode(QAbstractItemView::SingleSelection);

    m_model = new QStandardItemModel(m_listview);
    m_listview->setModel(m_model);
    m_model->setSortRole(SortRole);
    QStringList headers;
    headers << i18n("Id");
    headers << i18n("Original String");
    headers << i18n("Translated String");
    m_model->setHorizontalHeaderLabels(headers);

    QHeaderView *header = m_listview->header();
    header->setResizeMode(0, QHeaderView::ResizeToContents);
    header->setResizeMode(1, QHeaderView::Stretch);
    header->setResizeMode(2, QHeaderView::Stretch);
    header->show();

    layout->addWidget(m_listview);
    layout->setStretchFactor(m_listview,1);
    
    connect(m_listview->selectionModel(), SIGNAL(currentChanged(QModelIndex,QModelIndex)),
            this, SLOT(slotCurrentChanged(QModelIndex,QModelIndex)));
}

KBCatalogListView::~KBCatalogListView()
{
}


void KBCatalogListView::slotCurrentChanged(const QModelIndex& current, const QModelIndex& /*previous*/)
{
    DocPosition pos;
    int number = current.isValid() ? current.row() : 0;
    
    pos.item=number;
    pos.form=0;
    
    emit signalSelectionChanged(pos);
}

void KBCatalogListView::setSelectedItem(int index)
{
    QModelIndex item = m_model->index(index, 0);

    // block signals - don't reemit the selected item signal
    blockSignals(true);
    m_listview->setCurrentIndex(item);
    blockSignals(false);
    
    m_listview->scrollTo(item);
}

void KBCatalogListView::update(EditCommand* cmd, bool undo)
{
    /*
   if((int)_currentIndex==cmd->index())
   {
      emitEntryState();

      if(cmd->part()==Msgstr)
      {
         msgstrEdit->processCommand(cmd,undo);
	 emit signalMsgstrChanged();
      }
   }
   */
}

void KBCatalogListView::msgstrChanged(const QString& str)
{
    m_model->item(m_listview->currentIndex().row(), 2)->setText(str);
}

void KBCatalogListView::slotNewFileOpened()
{
    m_model->clear();
    QStringList headers;
    headers << i18n("Id");
    headers << i18n("Original String");
    headers << i18n("Translated String");
    m_model->setHorizontalHeaderLabels(headers);
    if(m_catalog)
    {
        for(uint i=0;i<m_catalog->numberOfEntries();i++)
        {
            QList<QStandardItem *> items;
            items.append(new SortableStandardItem<int>(i + 1, SortRole, QString::number(i + 1)));
            QString msgid(m_catalog->msgid(i).at(0) );
            items.append(new SortableStandardItem<QString>(msgid, SortRole, msgid));
            QString msgstr(m_catalog->msgstr(i).at(0) );
            items.append(new SortableStandardItem<QString>(msgstr, SortRole, msgstr));
            m_model->appendRow(items);
        }
    }
    
    QHeaderView *header = m_listview->header();
    header->setResizeMode(0, QHeaderView::ResizeToContents);
    header->setResizeMode(1, QHeaderView::Stretch);
    header->setResizeMode(2, QHeaderView::Stretch);


}


#include "kbcataloglistview.moc"
