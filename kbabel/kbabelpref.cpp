/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2004-2005  by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "kbabelpref.h"
#include "kbabeldictbox.h"
#include "kbabelsettings.h"
#include "fontpreferences.h"
#include "editordiffpreferences.h"
#include "editorpreferences.h"
#include "searchpreferences.h"
#include "colorpreferences.h"
#include "toolaction.h"
#include "toolselectionwidget.h"
#include "qcombobox.h"

#include <Q3PtrList>

#include <klocale.h>
#include <ktoolinvocation.h>

using namespace KBabel;

KBabelPreferences::KBabelPreferences(const QList<ModuleInfo*> &ml)
    : KConfigDialog(0, "Preferences", KBabelSettings::self())
{
    _editorPage = new EditorPreferences( 0 );
    _editorPage->setObjectName( "editor" );
    addPage( _editorPage
	, i18nc("title of page in preferences dialog","Edit")
	, "document-edit"
        , i18n("Options for Editing"));

    // this contains a custom widget for tool selection, set it up	
    QList<KDataToolInfo> tools = ToolAction::validationTools();
    _editorPage->_kcfg_AutoCheckTools->loadTools( QStringList("validate"), tools );
    connect( _editorPage->_kcfg_AutoCheckTools, SIGNAL( added( QListWidgetItem * ) ),
	this, SLOT (updateButtons()));
    connect( _editorPage->_kcfg_AutoCheckTools, SIGNAL( removed( QListWidgetItem * ) ),
	this, SLOT (updateButtons()));

    _searchPage = new SearchPreferences( 0 );
    _searchPage->setObjectName( "search" );
    addPage( _searchPage
	, i18nc("title of page in preferences dialog","Search")
	, "transsearch"
        , i18n("Options for Searching Similar Translations"));

    // setup the dictionary combobox contents
    foreach(ModuleInfo *info, ml)
    {
        _searchPage->_kcfg_DefaultModule->addItem( info->name );
    }
    moduleList = ml;
    connect( _searchPage->_kcfg_DefaultModule, SIGNAL( activated(int) ),
	this, SLOT (updateButtons()));

    EditorDiffPreferences *editorDiffPreferences = new EditorDiffPreferences( 0 );
    editorDiffPreferences->setObjectName( "diff" );
    addPage( editorDiffPreferences
	,i18nc("title of page in preferences dialog","Diff")
	, "diff"
        , i18n("Options for Showing Differences"));

    FontPreferences *fontPreferences = new FontPreferences( 0 );
    fontPreferences->setObjectName( "fonts" );
    addPage( fontPreferences
	, i18nc("name of page in preferences dialog icon list","Fonts")
	, "preferences-desktop-font"
        , i18nc("title of page in preferences dialog","Font Settings"));

    ColorPreferences *colorPreferences = new ColorPreferences( 0 );
    colorPreferences->setObjectName( "colors" );
    addPage( colorPreferences
	, i18nc("name of page in preferences dialog icon list","Colors")
	, "preferences-desktop-color"
        , i18nc("title of page in preferences dialog","Color Settings"));
    
    adjustSize();
}


void KBabelPreferences::slotHelp()
{
   //TODO
   KToolInvocation::invokeHelp("Preferences","");
}

bool KBabelPreferences::hasChanged()
{
    ModuleInfo *info = moduleList.at( _searchPage->_kcfg_DefaultModule->currentIndex() );
    
    bool module_ret = true;
    if( info )
    {
	module_ret = info->id != KBabelSettings::defaultModule();
    }
    
    return KConfigDialog::hasChanged() 
	|| (_editorPage->_kcfg_AutoCheckTools->selectedTools() != KBabelSettings::autoCheckTools())
	|| (module_ret);
}

bool KBabelPreferences::isDefault()
{
    bool old_useDefault = KBabelSettings::self()->useDefaults(true);
    
    ModuleInfo *info = moduleList.at( _searchPage->_kcfg_DefaultModule->currentIndex() );
    bool module_ret = ( info && info->id == KBabelSettings::defaultModule() );

    bool ret = KConfigDialog::isDefault() 
	&& (_editorPage->_kcfg_AutoCheckTools->selectedTools().empty())
	&& (module_ret);
	
    KBabelSettings::self()->useDefaults(old_useDefault);
    return ret;
}

void KBabelPreferences::updateSettings()
{
    KConfigDialog::updateSettings();
    
    KBabelSettings::setAutoCheckTools(_editorPage->_kcfg_AutoCheckTools->selectedTools());

    int i = _searchPage->_kcfg_DefaultModule->currentIndex();
    ModuleInfo *info = moduleList.at(i);

    if(info)
    {
	KBabelSettings::setDefaultModule(info->id);
    }
    
#ifdef __GNUC__
# warning "Verify info->id"
#endif
    emit settingsChanged( info->id );
}

void KBabelPreferences::updateWidgets()
{
    KConfigDialog::updateWidgets();
    _editorPage->_kcfg_AutoCheckTools->setSelectedTools(KBabelSettings::autoCheckTools());
    
    int i=0;
    foreach(ModuleInfo *info, moduleList)
    {
        if(KBabelSettings::defaultModule() == info->id)
            break;

        i++;
    }
    _searchPage->_kcfg_DefaultModule->setCurrentIndex(i);
}

void KBabelPreferences::updateWidgetsDefault()
{
    KConfigDialog::updateWidgetsDefault();
    
    bool old_useDefault = KBabelSettings::self()->useDefaults(true);

    kDebug () << "Default tools: " << KBabelSettings::autoCheckTools();
    
    _editorPage->_kcfg_AutoCheckTools->setSelectedTools(KBabelSettings::autoCheckTools()); 

    int i=0;
    foreach(ModuleInfo *info, moduleList)
    {
        if(KBabelSettings::defaultModule() == info->id)
            break;

        i++;
    }

    _searchPage->_kcfg_DefaultModule->setCurrentIndex(i);
    
    KBabelSettings::self()->useDefaults(old_useDefault);
}

#include "kbabelpref.moc"
