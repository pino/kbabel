/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2001-2004 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  Alt+123 feature idea taken from KOffice by David Faure <david@mandrakesoft.com>.
  Word wrap support by Jarno Elonen <elonen@iki.fi>, 2003

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include "mymultilineedit.h"
#include "editcmd.h"
#include "resources.h"

#include <qpixmap.h>
#include <q3valuelist.h>
#include <qstringlist.h>
#include <qregexp.h>
#include <qclipboard.h>
#include <qapplication.h>
#include <q3dragobject.h>

#include <QWheelEvent>
#include <QFocusEvent>
#include <QKeyEvent>
#include <QContextMenuEvent>

//#include <private/qrichtext_p.h>
#include <q3popupmenu.h>

#include <kdebug.h>
#include <kglobal.h>
#include <kglobalsettings.h>
#include <kmessagebox.h>
#include <kstandardshortcut.h>

using namespace KBabel;

MyMultiLineEdit::MyMultiLineEdit(int ID, QWidget* parent,const char* name)
                :K3TextEdit(parent), emitUndo(true), _dontUpdate(false), _myID (ID),
		 _menu(0)
{
   setUndoRedoEnabled(false); // we handle this ourselves
   setWordWrap( WidgetWidth );
   viewport()->setAcceptDrops( false ); // we need our parent to get drops
}

void MyMultiLineEdit::processCommand(EditCommand* cmd, bool undo)
{
   if(cmd->terminator()!=0)
      return;

   DelTextCmd* delcmd = (DelTextCmd*) cmd;
   bool ins =  true;
   if (delcmd->type() == EditCommand::Delete )
      ins = undo;
   else if (delcmd->type() == EditCommand::Insert )
      ins = !undo;
   else
   {
      return;
   }

  // avoid duplicate update of catalog
  bool oldEmitUndo = emitUndo;
  emitUndo = false;

   if ( ins )
   {
      int row, col;

      offset2Pos( delcmd->offset, row, col );
      setCursorPosition( row, col );


      K3TextEdit::insert( delcmd->str ); 

      offset2Pos( delcmd->offset+delcmd->str.length(), row, col );
      setCursorPosition( row, col);
   }
   else
   { // del

      int row, col, rowEnd, colEnd;

      offset2Pos( delcmd->offset, row, col );
      offset2Pos( delcmd->offset + delcmd->str.length(), rowEnd, colEnd );

      setSelection( row, col, rowEnd, colEnd, 0 );
      K3TextEdit::removeSelectedText();
   }


   emitUndo = oldEmitUndo;
      
   emitCursorPosition();
}

int MyMultiLineEdit::beginOfMarkedText()
{
	int beginX=0;
	int beginY=0;
	int endX=0;
	int endY=0;

	int pos=-1;
	
	getSelection(&beginY,&beginX,&endY,&endX);
	if( hasSelectedText() )
	{
		pos = pos2Offset(beginY,beginX);
	}

	return pos;
}

void MyMultiLineEdit::emitCursorPosition()
{
    int line=0;
    int col=0;
    getCursorPosition(&line,&col);
    
    emit cursorPositionChanged(line, col);
}

void MyMultiLineEdit::wheelEvent(QWheelEvent *e)
{
    e->ignore();
}

void MyMultiLineEdit::focusInEvent(QFocusEvent *e)
{
    K3TextEdit::focusInEvent(e);
    emitCursorPosition();
}

void MyMultiLineEdit::contentsContextMenuEvent( QContextMenuEvent * e)
{
    e->accept();
    if( _menu ) _menu->exec( e->globalPos() );
}

Q3PopupMenu * MyMultiLineEdit::createPopupMenu()
{
    return _menu;
}

Q3PopupMenu * MyMultiLineEdit::createPopupMenu(const QPoint &)
{
    return 0;
}

void MyMultiLineEdit::setContextMenu( Q3PopupMenu * menu )
{
    _menu = menu;
}

void MyMultiLineEdit::setText(const QString& s)
{
    // workaround, since insert does not interpret markup
    setTextFormat( Qt::PlainText );
    K3TextEdit::setText(s);
    setTextFormat( Qt::AutoText );
    emit textChanged();
}

void MyMultiLineEdit::insertAt( const QString & s, int line, int col, bool mark )
{
   // it will invoke insert, don't need to send InsTextCmd
   K3TextEdit::insertAt(s,line,col);
   
   // code from QMultiLineEdit
   if( mark )  
	setSelection( line, col, line, col + s.length(), 0 );
   // end of copied code
   
   emitCursorPosition();
}

void MyMultiLineEdit::insert( const QString & text, bool indent, bool checkNewLine, bool removeSelected )
{
   int row,col;

   bool noSelectionRemoved = true;
   setUpdatesEnabled(false);
   if( removeSelected && hasSelectedText() )
   {
	int endRow,endCol;
	getSelection(&row,&col,&endRow,&endCol);

	removeSelectedText();
	noSelectionRemoved = false;
   }
	
   getCursorPosition(&row,&col);

   if( emitUndo)
   {
	emit signalUndoCmd( new BeginCommand(-1,UndefPart));
	// reimplemented overwrite
	
	emit signalUndoCmd( new InsTextCmd(currentIndex(), text, _myID) );
	emit signalUndoCmd( new EndCommand(-1,UndefPart));
   }
   
   int n = text.indexOf( "\n" );

   K3TextEdit::insert(text, indent, checkNewLine, removeSelected);
 
   setUpdatesEnabled(true);

}

void MyMultiLineEdit::removeLine ( int line )
{
   kDebug(KBABEL) << "removeLine invoked";

   K3TextEdit::removeParagraph(line);
   emitCursorPosition();
}

void MyMultiLineEdit::clear()
{
    _dontUpdate=true;

   QString s = text();
   if( !s.isEmpty() && emitUndo ) {
	emit signalUndoCmd( new BeginCommand(-1,UndefPart) );
	emit signalUndoCmd( new DelTextCmd(0,s,_myID) );
	emit signalUndoCmd( new EndCommand(-1,UndefPart) );
   }
   
   K3TextEdit::clear();
   
   _dontUpdate=false;
   
   emitCursorPosition();
}

void MyMultiLineEdit::removeSelectedText(int selNum)
{
    if( selNum != 0 ) 
    {
	
	K3TextEdit::removeSelectedText(selNum);
    }
    else 
    {
	  int paraFrom, idxFrom, paraTo, idxTo;
	  K3TextEdit::getSelection( &paraFrom, &idxFrom, &paraTo, &idxTo );
	  
	  int offset = pos2Offset( paraFrom, idxFrom );
	  emit signalUndoCmd(new DelTextCmd( offset, selectedText(), _myID ) );
	  K3TextEdit::removeSelectedText(selNum);
    }

    emitCursorPosition();
}

void MyMultiLineEdit::paste()
{
    K3TextEdit::paste();    
    emitCursorPosition();
}

int MyMultiLineEdit::currentIndex()
{
    int para; // paragraph of current position
    int index; // index in the current paragraph
    
    K3TextEdit::getCursorPosition(&para,&index);
    
    return pos2Offset( para, index );
}


void MyMultiLineEdit::offset2Pos(int offset, int &paragraph, int &index) const
{
    if (offset <= 0)
    {
	paragraph = 0;
	index = 0;
	return;
    }
    else
    {
       int charsLeft = offset;
       int i;

       for( i = 0; i < paragraphs(); ++i )
       {
           if (paragraphLength( i ) < charsLeft)
              charsLeft -= paragraphLength( i );
           else
           {
              paragraph = i;
              index = charsLeft;
              return;
           }
	   --charsLeft;
       }
    
          paragraph = i-1;
          index = charsLeft;
       return;
    }
}

int MyMultiLineEdit::pos2Offset(uint paragraph, uint index)
{
    paragraph = qMax( qMin( (int)paragraph, paragraphs() - 1), 0 ); // Sanity check
    index  = qMax( qMin( (int)index,  paragraphLength( paragraph )), 0 ); // Sanity check

    {
        uint lastI;
	lastI  = paragraphLength( paragraph );
	uint i = 0; 
	uint tmp = 0;

	for( ;i < paragraph ; i++ )
	{
	    tmp += paragraphLength( i ) + 1;
	}
	
	tmp += qMin( lastI, index );

	return tmp;
    }
}

void MyMultiLineEdit::setReadOnly(bool on)
{
	// I want this backgroundmode, also when readonly==true
	if(on) 
	{
		setBackgroundRole( QPalette::Base );
	}
	
	Q3TextEdit::setReadOnly(on);
}

/*******************************************************************************/
MsgMultiLineEdit::MsgMultiLineEdit(int ID, K3Spell* spell, QWidget* parent,const char* name)
                :MyMultiLineEdit(ID, parent,name),
                _quotes(false),
                _cleverEditing(false),
//                _cformatColor(Qt::blue),
//                _accelColor(Qt::darkMagenta),
                _showDiff(false),
                _diffUnderlineAdd(true),
                _diffStrikeOutDel(true),
                _diffAddColor(Qt::darkGreen),
                _diffDelColor(Qt::darkRed),
		_currentUnicodeNumber(0)
{
   diffPos.setAutoDelete(true);
   diffPos.clear();

}

MsgMultiLineEdit::~MsgMultiLineEdit ()
{
}

void MsgMultiLineEdit::setText(const QString& s)
{
    QString str = s;
    
    if(_showDiff)
    {
        diffPos.clear();
        int lines = s.count('\n');
        diffPos.resize(lines+1);

        QStringList lineList = s.split( '\n', QString::KeepEmptyParts );
        
        int lineCounter=-1;
        bool haveAdd=false;
        bool haveDel=false;
        bool multiline=false;
        QStringList::Iterator it;
        for(it = lineList.begin(); it != lineList.end(); ++it)
        {
            lineCounter++;
            
            int lastPos=0;
            bool atEnd=false;

            while(!atEnd)
            {            
                int addPos=-1;
                int delPos=-1;
         
                if(haveAdd && multiline)
                {
                    addPos=0;
                }
                else
                {
                    addPos = (*it).indexOf( "<KBABELADD>", lastPos );
                }
            
                if(haveDel && multiline)
                {
                    delPos=0;
                }
                else
                {
                    delPos = (*it).indexOf( "<KBABELDEL>", lastPos );
                }

                if(delPos >= 0 && addPos >= 0)
                {
                    if(delPos <= addPos)
                    {
                        haveDel=true;
                        haveAdd=false;
                    }
                    else
                    {
                        haveDel=false;
                        haveAdd=true;
                    }
                }
                else if(delPos >= 0)
                {
                    haveDel=true;
                    haveAdd=false;
                }
                else if(addPos >= 0)
                {
                    haveDel=false;
                    haveAdd=true;
                }
                else
                {
                    atEnd=true;
                    haveAdd=false;
                    haveDel=false;
                }
                
                DiffInfo di;
                di.begin=-1;
                
                if(haveAdd)
                {
                    if(!multiline)
                    {
                        (*it).remove(addPos,11);
                    }

                    int endPos = (*it).indexOf( "</KBABELADD>", addPos );
                    if(endPos < 0)
                    {
                        endPos = (*it).length();
                        atEnd=true;
                        multiline=true;
                    }
                    else
                    {
                        (*it).remove(endPos,12);
                        haveAdd=false;
                        multiline=false;
                    }
                    
                    lastPos=endPos;

                    di.begin=addPos;
                    di.end=endPos-1;
                    di.add=true;
                }
                else if(haveDel)
                {
                    if(!multiline)
                    {
                        (*it).remove(delPos,11); 
                    }

                    int endPos = (*it).indexOf( "</KBABELDEL>", delPos );
                    if(endPos < 0)
                    {
                        endPos = (*it).length();
                        atEnd=true;
                        multiline=true;
                    }
                    else
                    {
                        (*it).remove(endPos,12);
                        haveDel=false;
                        multiline=false;
                    }
                    
                    lastPos=endPos;

                    di.begin=delPos;
                    di.end=endPos-1;
                    di.add=false;
                }

                if(di.begin >= 0)
                {
                    Q3ValueList<DiffInfo> *list = diffPos[lineCounter];
                    if(!list)
                    {
                        list = new Q3ValueList<DiffInfo>;
                        diffPos.insert(lineCounter,list);
                    }

                    list->append(di);
                }
                
            }
        }
        
        QRegExp reg("</?KBABELADD>");
        str.replace(reg,"");
        reg.setPattern("</?KBABELDEL>");
        str.replace(reg,"");
    }
    
    MyMultiLineEdit::setText(str);
}

void MsgMultiLineEdit::setQuotes(bool on)
{
   _quotes=on;
   update();
}

void MsgMultiLineEdit::setCleverEditing(bool on)
{
   _cleverEditing=on;
}

void MsgMultiLineEdit::setDiffDisplayMode(bool addUnderline, bool delStrikeOut)
{
    _diffUnderlineAdd = addUnderline;
    _diffStrikeOutDel = delStrikeOut;
    
    if(_showDiff)
        update();
}

void MsgMultiLineEdit::setDiffColors(const QColor& addColor
        , const QColor& delColor)
{
    _diffAddColor = addColor;
    _diffDelColor = delColor;

    if(_showDiff)
        update();
}

void MsgMultiLineEdit::setSpellChecker(K3Spell* spell)
{//TODO qt4
//    highlighter->setSpellChecker(spell);
}

void MsgMultiLineEdit::ensureCursorVisible()
{
    if( updatesEnabled() )
	MyMultiLineEdit::ensureCursorVisible();
}

void MsgMultiLineEdit::keyPressEvent(QKeyEvent *e)
{
    if(!_cleverEditing || isReadOnly())
    {
        MyMultiLineEdit::keyPressEvent(e);
        return;
    }

    
    if(e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    { 
        emit signalUndoCmd(new BeginCommand(-1,UndefPart));

        int row, col;
        getCursorPosition(&row,&col);
        QString str=text(row);

        if( e->modifiers() & Qt::ShiftModifier )
        {
            if(col > 0 && !str.isEmpty())
            {
                if(str.at(col-1) == '\\' && !isMasked(&str,col-1))
                {
                    insert("n",false);
                }
                else
                {
                    insert("\\n",false);
                }
            }
            else
            {
                insert("\\n",false);
            }
        }    
        else if( !( e->modifiers() & Qt::ControlModifier ) )
        {
            if(col > 0 && !str.isEmpty() && !str.at(col-1).isSpace())
            {
                if(str.at(col-1)=='\\' && !isMasked(&str,col-1))
                {
                    insert("\\",false);
                }
                
                // if there is not a new line at the end
                if(col < 2 || str.mid(col-2,2)!="\\n")
                {
                    insert(" ",false);
                }
            }
            else if(str.isEmpty())
            {
                insert("\\n",false);
            }
        }    
     
        if( !str.isEmpty())
        {
	    // construct new event without modifiers
    	    MyMultiLineEdit::keyPressEvent( new QKeyEvent( e->type(), e->key(), 0,
		e->text(), e->isAutoRepeat(), e->count() ) );
	    e->accept();
        }

        emit signalUndoCmd(new EndCommand(-1,UndefPart));
        return;
    }
    else if(e->key() == Qt::Key_Tab)
    {
        insert("\\t",false);
        emit textChanged();
        e->accept();
        return;
    }
    else if( ( e->key() == Qt::Key_Delete && !( e->modifiers() & Qt::ControlModifier ) )
            || ( ( e->modifiers() & Qt::ControlModifier ) && e->key() == Qt::Key_D ) )
    {
        emit signalUndoCmd(new BeginCommand(-1,UndefPart));
        
        if(!hasSelectedText())
        {
            int row, col;
            getCursorPosition(&row,&col);
            QString str=text(row);

            if(!str.isEmpty() && col < (int)str.length() && str.at(col) == '\\'
                        && !isMasked(&str,col))
            {
                QString spclChars="abfnrtv'\"?\\";
                if(col < (int)str.length()-1 
                        && spclChars.contains(str.at(col+1)))
                {
                    del();
                }
            }
        }
        
        del();

        emit signalUndoCmd(new EndCommand(-1,UndefPart));
        emit textChanged();
        e->accept();
        return;
    }
    else if( e->key() == Qt::Key_Backspace
            || ( ( e->modifiers() & Qt::ControlModifier ) && e->key() == Qt::Key_H ) )
    {
        emit signalUndoCmd(new BeginCommand(-1,UndefPart)); 
        
        if(!hasSelectedText())
        {
            int row, col;
            getCursorPosition(&row,&col);
            QString str=text(row);

            QString spclChars="abfnrtv'\"?\\";
            if(!str.isEmpty() && col > 0 && spclChars.contains(str.at(col-1)))
            {
                if(col > 1 && str.at(col-2)=='\\' && !isMasked(&str,col-2))
                {
                    MyMultiLineEdit::keyPressEvent(e);
                }
            }

        }
        
        MyMultiLineEdit::keyPressEvent(e);

        emit signalUndoCmd(new EndCommand(-1,UndefPart));

        e->accept();
        return;
    }
    else if(e->text() == "\"")   
    {
        emit signalUndoCmd(new BeginCommand(-1,UndefPart));

        int row, col;
        getCursorPosition(&row,&col);
        QString str=text(row);
    
        if(col == 0 || str.at(col-1) != '\\' || isMasked(&str,col-1) )
        {
            insert("\\\"",false);
        }
        else
        {
            insert("\"",false);
        }

        e->accept();

        emit signalUndoCmd(new EndCommand(-1,UndefPart));
        return;
    }
    else if( e->key() == Qt::Key_Space && ( e->modifiers() & Qt::AltModifier ) )
    {
	insert( QString(QChar( 0x00a0U )) );
	e->accept();
	return;
    }
    // ALT+123 feature
    else if( ( e->modifiers() & Qt::AltModifier ) && e->text()[0].isDigit() )
    {
	QString text=e->text();
	while ( text[0].isDigit() ) {
	    _currentUnicodeNumber = 10*_currentUnicodeNumber+(text[0].digitValue());
	    text.remove( 0, 1 );
	}
    }
    else
    { 
        MyMultiLineEdit::keyPressEvent(e);
    }
}

void MsgMultiLineEdit::keyReleaseEvent(QKeyEvent* e)
{
    if ( e->key() == Qt::Key_Alt && _currentUnicodeNumber >= 32 )
    {
        QString text = QString(QChar( _currentUnicodeNumber ));
	_currentUnicodeNumber=0;
        insert( text );
    }
}

void MsgMultiLineEdit::setDiffMode(bool on)
{
    _showDiff=on;
    
    if(!on)
    {
        diffPos.clear();
    }
}

bool MsgMultiLineEdit::isMasked(QString *str, uint col)
{
    if(col == 0 || !str)
        return false;

    uint counter=0;
    int pos=col;
    
    while(pos >= 0 && str->at(pos) == '\\')
    {
        counter++;
        pos--;
    }
    
    return !(bool)(counter%2);
}

void MsgMultiLineEdit::selectTag(int start, int length)
{
    //TODO qt4
    /*setUpdatesEnabled(false);
    setSelection( _tagStartPara, _tagStartIndex, _tagEndPara, _tagEndIndex);
    setBold( false );

    offset2Pos(start, _tagStartPara, _tagStartIndex);
    offset2Pos(start+length, _tagEndPara, _tagEndIndex);
    
    setSelection( _tagStartPara, _tagStartIndex, _tagEndPara, _tagEndIndex);
    setBold( true );
    setUpdatesEnabled(true);*/
}

#include "mymultilineedit.moc"
