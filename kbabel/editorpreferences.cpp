/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2006 by Tim Beaulen <tbscope@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "editorpreferences.h"

#include <kmessagebox.h>
#include <QVBoxLayout>

EditorPreferences::EditorPreferences( QWidget *parent ) : QWidget( parent )
{
    setupUi( this );
    layout()->setMargin( 0 );

    // Add the tool selection widget
    _kcfg_AutoCheckTools = new ToolSelectionWidget( this );
    static_cast<QVBoxLayout*>(groupBox->layout())->insertWidget( 0, _kcfg_AutoCheckTools );

    connect( kcfg_LedInStatusbar, SIGNAL( toggled( bool ) ), this, SLOT( ledWarning( bool ) ) );
    connect( radioButton2, SIGNAL( toggled( bool ) ), this, SLOT( ledWarning( bool ) ) );
    connect( kcfg_LedInStatusbar, SIGNAL( toggled( bool ) ), this, SLOT( toggleOther( bool ) ) );
}

EditorPreferences::~EditorPreferences()
{
}

void EditorPreferences::ledWarning( bool show )
{
    if( show )
        KMessageBox::information( this, i18n("This option takes no effect until KBabel is restarted.") );
}


void EditorPreferences::toggleOther( bool other )
{
    radioButton2->setChecked( !other );
}

#include "editorpreferences.moc"
