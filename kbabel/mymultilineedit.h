/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2001-2003 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#ifndef MYMULTILINEEDIT_H
#define MYMULTILINEEDIT_H

#include <q3ptrvector.h>
//Added by qt3to4:
#include <QWheelEvent>
#include <QPixmap>
#include <QFocusEvent>
#include <QKeyEvent>
#include <Q3ValueList>
#include <QContextMenuEvent>
#include <Q3PopupMenu>

#include <k3textedit.h>

namespace KBabel
{
    class EditCommand;
}

class K3Spell;
class QPixmap;

class MyMultiLineEdit : public K3TextEdit
{
   Q_OBJECT
public:
   MyMultiLineEdit(int ID,QWidget* parent,const char* name=0);

   /**
      applies cmd to the displayed text, but does not emit
      signalUndoCommand
   */
   void processCommand(KBabel::EditCommand* cmd, bool undo=false);

   /** 
	* @returns the position in text, where the marked text begins
	* -1, if there is no marked text
	*/
   int beginOfMarkedText();

   virtual void insertAt ( const QString & s, int line, int col, bool mark = false );
   virtual void removeLine ( int line );

   int pos2Offset(uint paragraph, uint index);
   void offset2Pos(int offset, int &row, int &col) const;
   /** 
	* @returns the current position in text, where the cursor is
	*/

   int currentIndex();

   /**
    * need to override deleting of popup menus :-(
    */
   void contentsContextMenuEvent( QContextMenuEvent *e );

public slots:
   virtual void clear();
   virtual void paste();
   virtual void setReadOnly(bool on);
   virtual void setContextMenu( Q3PopupMenu *menu );
   virtual void setText(const QString& s);
   virtual void removeSelectedText(int selNum = 0);
   
protected:

   virtual void focusInEvent(QFocusEvent*);
   virtual Q3PopupMenu *createPopupMenu();
   virtual Q3PopupMenu *createPopupMenu(const QPoint &pos);
   
   /* the parent handles this */
   virtual void wheelEvent(QWheelEvent*);

   bool emitUndo;

   /* flag to skip any work on updating, since it will be more changes */
   bool _dontUpdate;

protected slots:
   virtual void insert ( const QString & text, bool indent = false, bool checkNewLine = true, bool removeSelected = true );
   virtual void emitCursorPosition();

signals:
   void signalUndoCmd(KBabel::EditCommand*);

protected:
   int _myID;
   
private:
   Q3PopupMenu *_menu;
};


class MsgMultiLineEdit : public MyMultiLineEdit
{   
   Q_OBJECT
public:
   enum TextColor { NormalColor, ErrorColor };

   explicit MsgMultiLineEdit(int ID, K3Spell* spell=0, QWidget* parent=0,const char* name=0);
   virtual ~MsgMultiLineEdit();

   /** is displaying surrounding quotes enabled? */
   bool quotes() const { return _quotes;}
   /** enable or disable displaying of surrounding quotes */
   void setQuotes(bool on);

   /** is clever editing enabled? */
   bool cleverEditing() const { return _cleverEditing; }
   /** enable or disable clever editing */
   void setCleverEditing(bool on);

//   void setFont(const QFont& font);

   void setDiffMode(bool on);
   void setDiffDisplayMode(bool underlineAdded, bool strikeOutDeleted);
   void setDiffColors(const QColor& addColor, const QColor& delColor);
   
   void setTextColor(const QColor &color);
   void setErrorColor(const QColor &color);
   
   void setSpellChecker(K3Spell* spell);
   
   void selectTag(int start, int length);
   
public slots:
   virtual void setText(const QString& s);

   /**
   * reimplemented to skip in case of disabled updates
   */
   void ensureCursorVisible();
   
protected:
   virtual void keyPressEvent(QKeyEvent*);
   virtual void keyReleaseEvent(QKeyEvent*);

private:

   /**
    * tests if the character in string str at position col is masked with
    * '\' by counting the number of '\' backwards
    */
   static bool isMasked(QString *str,uint col);

private:
   bool _quotes;
   bool _cleverEditing;

   int _wsOffsetX;
   int _wsOffsetY;

   struct DiffInfo
   {
       bool add;
       int begin;
       int end;
   };

   Q3PtrVector< Q3ValueList<DiffInfo> > diffPos;
   bool _showDiff;
   bool _diffUnderlineAdd;
   bool _diffStrikeOutDel;
   QColor _diffAddColor;
   QColor _diffDelColor;
   
   // for Alt+123 feature
   int _currentUnicodeNumber;
   
};

#endif // MYMULTILINEEDIT_H
