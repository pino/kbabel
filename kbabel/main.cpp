/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2001 by Matthias Kiefer <matthias.kiefer@gmx.de>
		2002-2005 by Stanislav Visnovsky <visnovsky@kde.org>
  Copyright (C) 2006 by Nicolas GOUTTE <goutte@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "kbabel.h"
#include "kbabelinterface.h"
#include "kbabeladaptor.h"
#include "catalog.h"
#include "findoptions.h"
#include "kbprojectmanager.h"

#include "version.h"

#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <kcursor.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kuniqueapplication.h>

#include <kwindowsystem.h>
#include <QtDBus/QtDBus>

#include <qfile.h>
#include <qfileinfo.h>
#include <qtimer.h>


class KBabelApp : public KUniqueApplication
{
public:
    KBabelApp();
    virtual ~KBabelApp();

    virtual int newInstance();

private:
    KBabelInterface *kbInterface;
};

KBabelApp::KBabelApp()
    : KUniqueApplication()
{
    kbInterface = new KBabelInterface;
    new KbabelAdaptor(kbInterface);
    QDBusConnection::sessionBus().registerObject(QLatin1String("/KBabel"), kbInterface);
}

KBabelApp::~KBabelApp()
{
    delete kbInterface;
}

int KBabelApp::newInstance()
{
    const bool first = KBabelMW::memberList().isEmpty();

    // see if we are starting with session management
    if (!restoringSession())
    {
        // TODO: There's no replacement for suspend in QtDBus
	// kDebug () << "Suspending DCOP";
	// kapp->dcopClient()->suspend();

        KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

        if( first )
        {
            setOverrideCursor(Qt::WaitCursor);
        }

        QString projectFile=args->getOption("project");
	if( !projectFile.isEmpty() )
	{
	    QFileInfo fi(projectFile);
	    projectFile = fi.absoluteFilePath();
	} else
	{
	    projectFile = KBabel::ProjectManager::defaultProjectName();
	}

	kDebug() << "Project: " << projectFile;
	QString msgid=args->getOption("gotomsgid");
        if(!msgid.isEmpty() && args->count() > 0)
        {
            kDebug(KBABEL) << "gotomsgid";
            QString m = msgid;

            // TODO: There's no replacement for suspend or resume in QtDBus
	    // kDebug () << "Resuming DCOP";
	    // kapp->dcopClient()->resume();
            kbInterface->gotoFileEntry( args->url(0).url().toLocal8Bit(), m.toUtf8() );
        }
        else
        {
            // no session.. just start up normally
    	    KBabelMW *widget=0;
            if(args->count() > 0)
            {
                KUrl u = args->url(0);
                widget=KBabelMW::winForURL(u,projectFile);
            }

            if(!widget)
                widget=KBabelMW::emptyWin(projectFile);

            if(!widget)
                widget=new KBabelMW(projectFile);

            widget->show();
            for (int i=0; i < args->count(); i++)
            {
               widget->open( args->url(i) , QString(), i != 0 );
            }

            // TODO: There's no replacement for suspend or resume in QtDBus
	    // kDebug () << "Resuming DCOP";
	    // kapp->dcopClient()->resume();
        }


        args->clear();

        if( first )
        {
            KApplication::restoreOverrideCursor();
            /*
            KMessageBox::information(0,
                    "This is a development version of KBabel!\n"
                    "Please double check the files you edit "
                    "and save with this version for correctness.\n"
                    "Please report any bug you find to kiefer@kde.org.\n"
                    "Thanks.", "Warning");
            */
        }
    }

    return 0;
}


int main(int argc, char **argv)
{
    KAboutData about("kbabel", 0,ki18n("KBabel"),KBABEL_VERSION,
       ki18n("An advanced PO file editor"),KAboutData::License_GPL,
       ki18n("(c) 1999,2000,2001,2002,2003,2004,2005,2006 The KBabel developers"),KLocalizedString(),"http://kbabel.kde.org");

    about.addAuthor(ki18n("Matthias Kiefer"),ki18n("Original author"),"kiefer@kde.org");
    about.addAuthor(ki18n("Wolfram Diestel")
         ,ki18n("Wrote diff algorithm, fixed K3Spell and gave a lot "
         "of useful hints."),"wolfram@steloj.de");
    about.addAuthor(ki18n("Andrea Rizzi"),ki18n("Wrote the dictionary plugin "
		"for searching in a database and some other code.")
            ,"rizzi@kde.org");
    about.addAuthor(ki18n("Stanislav Visnovsky"),ki18n("Current maintainer, porting to KDE3/Qt3.")
	,"visnovsky@kde.org");
    about.addAuthor(ki18n("Marco Wegner"),ki18n("Bug fixes, KFilePlugin for PO files, CVS support, mailing files")
         ,"dubbleu@web.de");
    about.addAuthor(ki18n("Asgeir Frimannsson"),ki18n("Translation List View")
         ,"asgeirf@redhat.com");
    about.addAuthor(ki18n("Nicolas Goutte"), ki18n("Filter improvements"), "goutte@kde.org");

    about.addCredit(ki18n("Claudiu Costin"),ki18n("Wrote documentation and sent "
		"many bug reports and suggestions for improvements.")
         ,"claudiuc@geocities.com");
    about.addCredit(ki18n("Thomas Diehl"),ki18n("Gave many suggestions for the GUI "
         "and the behavior of KBabel. He also contributed the beautiful splash screen.")
            ,"thd@kde.org");
    about.addCredit(ki18n("Stephan Kulow"),ki18n("Helped keep KBabel up to date "
		"with the KDE API and gave a lot of other help."),"coolo@kde.org");
    about.addCredit(ki18n("Stefan Asserhall"),ki18n("Implemented XML validation/highlighting "
	 "plus other small fixes.") ,"stefan.asserhall@telia.com");
    about.addCredit(ki18n("Dwayne Bailey"),ki18n("Various validation plugins.")
	 ,"dwayne@translate.org.za");
	about.addCredit(ki18n("SuSE GmbH")
					,ki18n("Sponsored development of KBabel for a while.")
					,"suse@suse.de","http://www.suse.de");
    about.addCredit(ki18n("Trolltech"), ki18n("KBabel contains code from Qt"), 0, "http://www.trolltech.com");

    about.addCredit(ki18n("Eva Brucherseifer"), ki18n("String distance algorithm implementation"), "eva@kde.org");

    about.addCredit(ki18n("Albert Cervera Areny"), ki18n("Error list for current entry, regexp data tool"), "albertca@hotpop.com");

    about.addCredit(ki18n("Nick Shaforostoff"), ki18n("Word-by-word string difference algorithm implementation"), "shafff@ukr.net");

    // Initialize command line args
    KCmdLineArgs::init(argc, argv, &about);

    // Tell which options are supported

    KCmdLineOptions options;
    options.add("gotomsgid <msgid>", ki18n("Go to entry with msgid <msgid>"));
    options.add("nosplash", ki18n("Disable splashscreen at startup"));
    options.add("project <configfile>", ki18n("File to load configuration from"));
    options.add("+[file]", ki18n("Files to open"));
    KCmdLineArgs::addCmdLineOptions( options );

    // Add options from other components
    KUniqueApplication::addCmdLineOptions();


    if(!KUniqueApplication::start())
    {
        return 0;
    }

    KBabelApp app;

    if( app.isSessionRestored() )
    {
	RESTORE(KBabelMW)
    }

    return app.exec();
}
