/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2004  by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "charselectview.h"

#include <qlayout.h>

#include <kconfig.h>
#include <klocale.h>
#include <kcharselect.h>
#include <kconfiggroup.h>

#include "kbcatalog.h"

using namespace KBabel;

CharacterSelectorView::CharacterSelectorView(KBCatalog* catalog,QWidget *parent, Project::Ptr project)
    : KBCatalogView(catalog,parent,project)
{
    QVBoxLayout* layout = new QVBoxLayout( this );
    layout->setMargin( 0 );

    _table = new KCharSelect(this, 0, KCharSelect::BlockCombos|KCharSelect::CharacterTable);
    layout->addWidget(_table);
    
    connect( _table, SIGNAL( charSelected(QChar) ), this, SIGNAL( characterDoubleClicked(QChar) ) );
    
    connect( _catalog, SIGNAL( signalFileOpened(bool) ), this, SLOT (setDisabled (bool)));

    setWhatsThis(
       i18n("<qt><p><b>Character Selector</b></p>"
         "<p>This tool allows to insert special characters using "
         "double click.</p></qt>"));
}

void CharacterSelectorView::saveSettings(KConfig* config)
{
    KConfigGroup group(config, "KBCharSelector" );
	
    group.writeEntry( "SelectedChar", QString(_table->currentChar()) );
}

void CharacterSelectorView::restoreSettings(KConfig* config)
{
    KConfigGroup group(config, "KBCharSelector" );
    
    _table->setCurrentChar( group.readEntry("SelectedChar"," ").at(0));
}

QChar CharacterSelectorView::currentChar() const
{
    return _table->currentChar();
}

#include "charselectview.moc"
