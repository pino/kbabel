/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2001-2004 by Stanislav Visnovsky <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "catmanresource.h"
#include "catalogmanager.h"
#include "catalog.h"
#include "catalogmanagerapp.h"
#include "findinfilesdialog.h"
#include "kbabeldictbox.h"
#include "resources.h"
#include "projectpref.h"
#include "kbprojectmanager.h"
#include "projectwizard.h"
#include "msgfmt.h"
#include "toolaction.h"

#include <qlabel.h>
#include <qpainter.h>

#include <Q3CString>
#include <Q3PtrList>
#include <QHBoxLayout>
#include <Q3ValueList>
#include <QVBoxLayout>
#include <QMenu>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusConnectionInterface>
#include <QProgressBar>

#include <kaction.h>
#include <kconfig.h>
#include <kcursor.h>
#include <kdatatool.h>
#include <kactioncollection.h>
#include <ktoggleaction.h>
#include <kactionmenu.h>
//#include <kedittoolbar.h>
#include <kfiledialog.h>
#include <kglobal.h>
#include <kglobalsettings.h>
#include <kiconloader.h>
#include <kmessagebox.h>
#include <klocale.h>
#include <kmenu.h>
#include <kstandardshortcut.h>
#include <kstandardaction.h>
#include <kstandarddirs.h>
#include <kstatusbar.h>
#include <ktoolbar.h>
#include <kwindowsystem.h>
#include <kxmlguifactory.h>

#include <qfileinfo.h>
#include <qdir.h>
#include <qtimer.h>
#include <qbitmap.h>
#include <q3header.h>
#include <q3dragobject.h>
#include <qlayout.h>
#include <kicon.h>

#include <ktoolinvocation.h>
#include <kvbox.h>
#include <kbabel_interface.h>

#include <unistd.h>

using namespace KBabel;

WId CatalogManagerApp::_preferredWindow = 0;

QStringList CatalogManager::_foundFilesList;
QStringList CatalogManager::_toBeSearched;

CatalogManager::CatalogManager( const QString& configFile )
    : KXmlGuiWindow( 0 )
{
    if ( configFile.isEmpty() )
	_configFile = ProjectManager::defaultProjectName();
    else
   	_configFile = configFile;

   init();
   restoreSettings();
   updateSettings();
}

CatalogManager::~CatalogManager()
{
   saveView();
   saveSettings(_configFile);
   delete config;
}

void CatalogManager::init()
{
    _foundToBeSent = 0;
    _totalFound = 0;
    _foundFilesList.clear();
    _toBeSearched.clear();
    _timerFind = new QTimer( this );
    connect(_timerFind, SIGNAL( timeout() ), this, SLOT(findNextFile()) );
    _searchStopped = false;

   _prefDialog=0;
   _findDialog=0;
   _replaceDialog=0;

   _project = KBabel::ProjectManager::open(_configFile);

   if ( _project.isNull() )
   {
	KMessageBox::error( this, i18n("Cannot open project file\n%1", _configFile)
	    , i18n("Project File Error"));

	_project = KBabel::ProjectManager::open(KBabel::ProjectManager::defaultProjectName());
   }

   connect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	, this, SLOT (updateSettings()));

   QWidget *view = new QWidget(this);
   QVBoxLayout* layout= new QVBoxLayout(view);
   layout->setMargin(0);

   _catalogManager=new CatalogManagerView(_project, view);
   _catalogManager->setObjectName("catalog manager");
   layout->addWidget(_catalogManager);
   layout->setStretchFactor(_catalogManager,1);

   connect(this,SIGNAL(settingsChanged(KBabel::CatManSettings))
            ,_catalogManager,SLOT(setSettings(KBabel::CatManSettings)));
   connect(_catalogManager,SIGNAL(openFile(QByteArray,QByteArray))
           ,this,SLOT(openFile(QByteArray,QByteArray)));
   connect(_catalogManager,SIGNAL(openFileInNewWindow(QByteArray,QByteArray))
           ,this,SLOT(openFileInNewWindow(QByteArray,QByteArray)));
   connect(_catalogManager,SIGNAL(openTemplate(QByteArray,QByteArray,QByteArray))
           ,this,SLOT(openTemplate(QByteArray,QByteArray,QByteArray)));
   connect(_catalogManager,SIGNAL(openTemplateInNewWindow(QByteArray,QByteArray,QByteArray))
           ,this,SLOT(openTemplateInNewWindow(QByteArray,QByteArray,QByteArray)));
   connect(_catalogManager,SIGNAL(gotoFileEntry(QByteArray,QByteArray,int))
           ,this,SLOT(openFile(QByteArray,QByteArray,int)));
   connect(_catalogManager, SIGNAL(selectedChanged(uint)),
           this, SLOT(selectedChanged(uint)));
#ifdef Q_OS_UNIX
   KWindowSystem::setIcons(winId(),BarIcon("catalogmanager",32)
           ,SmallIcon("catalogmanager"));
#endif
   QHBoxLayout* hBoxL = new QHBoxLayout();
   layout->addLayout( hBoxL );
   _progressLabel = new QLabel(view);
   hBoxL->addWidget(_progressLabel);
   _progressBar=new QProgressBar( view );
   hBoxL->addWidget(_progressBar);
   hBoxL->setStretchFactor(_progressBar,1);

   _progressLabel->hide();
   _progressBar->hide();

   connect(_catalogManager,SIGNAL(prepareProgressBar(QString,int))
           , this, SLOT(prepareProgressBar(QString,int)));
   connect(_catalogManager,SIGNAL(clearProgressBar())
           , this, SLOT(clearProgressBar()));
   connect(_catalogManager,SIGNAL(progress(int))
           , _progressBar, SLOT(setValue(int)));
//   connect(_catalogManager, SIGNAL(signalBuildTree(bool))
//	   , this, SLOT(enableMenuForFiles(bool)));
   connect(_catalogManager, SIGNAL(signalBuildTree(bool))
	   , this, SLOT(enableActions(bool)));
   connect(this, SIGNAL(searchStopped())
	   , _catalogManager, SLOT(stopSearch()));
   connect(_catalogManager, SIGNAL(prepareFindProgressBar(int))
	   , this, SLOT(prepareStatusProgressBar(int)));

   setCentralWidget(view);
   resize( 600,300);

   setupStatusBar();
   setupActions();


   QMenu* popup;
   popup = (QMenu*)(factory()->container("rmb_file", this));
   if(popup)
   {
       _catalogManager->setRMBMenuFile(popup);
   }
   popup = (QMenu*)(factory()->container("rmb_dir", this));
   if(popup)
   {
       _catalogManager->setRMBMenuDir(popup);
   }

   connect(_catalogManager, SIGNAL(signalSearchedFile(int))
           , _statusProgressBar, SLOT(setValue(int)));

   restoreView();
}

void CatalogManager::setupActions()
{
    KIconLoader::global()->addAppDir("kbabel");

    KAction *action;

    // the file menu
    KAction *actionOpen = actionCollection()->addAction( "open" );
    actionOpen->setText( i18n("&Open") );
    actionOpen->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_O ) );
    actionOpen->setEnabled( false );
    connect( actionOpen, SIGNAL( triggered() ), _catalogManager, SLOT( slotOpenFile() ) );

    KAction *actionOpenTemplate = actionCollection()->addAction( "open_template" );
    actionOpenTemplate->setText( i18n("&Open Template") );
    actionOpenTemplate->setShortcut( QKeySequence( Qt::Key_Space ) );
    actionOpenTemplate->setEnabled( false );
    connect( actionOpenTemplate, SIGNAL( triggered() ), _catalogManager, SLOT( slotOpenTemplate() ) );

    KAction *actionOpenNewWindow = actionCollection()->addAction( "open_new_window" );
    actionOpenNewWindow->setText( i18n("Open in &New Window") );
    actionOpenNewWindow->setShortcut( QKeySequence( Qt::CTRL+Qt::SHIFT+Qt::Key_O ) );
    actionOpenNewWindow->setEnabled( false );
    connect( actionOpenNewWindow, SIGNAL( triggered() ), _catalogManager, SLOT( slotOpenFileInNewWindow() ) );

    action = KStandardAction::quit(kapp, SLOT (closeAllWindows()), actionCollection());

    actionMap["open_template"] = NEEDS_POT;

    // the edit menu
    KAction *actionFindInFiles = actionCollection()->addAction( "find_in_files" );
    actionFindInFiles->setText( i18n("Fi&nd in Files...") );
    actionFindInFiles->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_F ) );
    actionFindInFiles->setEnabled( false );
    connect( actionFindInFiles, SIGNAL( triggered() ), this, SLOT( find() ) );

    KAction *actionReplaceInFiles = actionCollection()->addAction( "replace_in_files" );
    actionReplaceInFiles->setText( i18n("Re&place in Files...") );
    actionReplaceInFiles->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_R ) );
    actionReplaceInFiles->setEnabled( false );
    connect( actionReplaceInFiles, SIGNAL( triggered() ), this, SLOT( replace() ) );

    KAction *actionStopSearching = actionCollection()->addAction( "stop_search" );
    actionStopSearching->setIcon( KIcon( "process-stop" ) );
    actionStopSearching->setText( i18n("&Stop Searching") );
    actionStopSearching->setShortcut( QKeySequence( Qt::Key_Escape ) );
    actionStopSearching->setEnabled( false );
    connect( actionStopSearching, SIGNAL( triggered() ), this, SLOT( stopSearching() ) );

    KAction *actionReload = actionCollection()->addAction( "reload" );
    actionReload->setIcon( KIcon( "view-refresh" ) );
    actionReload->setText( i18n("&Reload") );
    actionReload->setShortcuts( KStandardShortcut::reload() );
    actionReload->setEnabled( false );
    connect( actionReload, SIGNAL( triggered() ), _catalogManager, SLOT( updateCurrent() ) );

    // the marking menu
    KAction *actionToggleMarking = actionCollection()->addAction( "toggle_marking" );
    actionToggleMarking->setText( i18n("&Toggle Marking") );
    actionToggleMarking->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_M ) );
    actionToggleMarking->setEnabled( false );
    connect( actionToggleMarking, SIGNAL( triggered() ), _catalogManager, SLOT( toggleMark() ) );

    KAction *actionRemoveMarking = actionCollection()->addAction( "remove_marking" );
    actionRemoveMarking->setText( i18n("Remove Marking") );
    actionRemoveMarking->setEnabled( false );
    connect( actionRemoveMarking, SIGNAL( triggered() ), _catalogManager, SLOT( slotClearMarksInDir() ) );

    KAction *actionToggleAllMarkings = actionCollection()->addAction( "toggle_all_marking" );
    actionToggleAllMarkings->setText( i18n("Toggle All Markings") );
    actionToggleAllMarkings->setEnabled( false );
    connect( actionToggleAllMarkings, SIGNAL( triggered() ), _catalogManager, SLOT( toggleAllMarks() ) );

    KAction *actionRemoveAllMarkings = actionCollection()->addAction( "remove_all_marking" );
    actionRemoveAllMarkings->setText( i18n("Remove All Markings") );
    actionRemoveAllMarkings->setEnabled( false );
    connect( actionRemoveAllMarkings, SIGNAL( triggered() ), _catalogManager, SLOT( clearAllMarks() ) );

    KAction *actionMarkModifiedFiles = actionCollection()->addAction( "mark_modified_files" );
    actionMarkModifiedFiles->setText( i18n("Mark Modified Files") );
    // fixme to enabling this when loading is done using updateFinished() signal
    actionMarkModifiedFiles->setEnabled( true );
    connect( actionMarkModifiedFiles, SIGNAL( triggered() ), _catalogManager, SLOT( markModifiedFiles() ) );

    KAction *actionLoadMarkings = actionCollection()->addAction( "load_marking" );
    actionLoadMarkings->setText( i18n("&Load Markings...") );
    actionLoadMarkings->setEnabled( false );
    connect( actionLoadMarkings, SIGNAL( triggered() ), _catalogManager, SLOT( loadMarks() ) );

    KAction *actionSaveMarkings = actionCollection()->addAction( "save_marking" );
    actionSaveMarkings->setText( i18n("&Save Markings...") );
    actionSaveMarkings->setEnabled( false );
    connect( actionSaveMarkings, SIGNAL( triggered() ), _catalogManager, SLOT( saveMarks() ) );

    KAction *actionMarkFiles = actionCollection()->addAction( "mark_pattern" );
    actionMarkFiles->setText( i18n("&Mark Files...") );
    connect( actionMarkFiles, SIGNAL( triggered() ), _catalogManager, SLOT( slotMarkPattern() ) );

    KAction *actionUnmarkFiles = actionCollection()->addAction( "unmark_pattern" );
    actionUnmarkFiles->setText( i18n("&Unmark Files...") );
    connect( actionUnmarkFiles, SIGNAL( triggered() ), _catalogManager, SLOT( slotUnmarkPattern() ) );

    actionMap["remove_marking"]     = NEEDS_MARK;
    actionMap["remove_all_marking"] = NEEDS_MARK;
    actionMap["mark_pattern"]       = NEEDS_DIR;
    actionMap["unmark_pattern"]     = NEEDS_DIR | NEEDS_MARK;

    // go menu
    KAction *actionNextUntranslated = actionCollection()->addAction( "go_next_untrans" );
    actionNextUntranslated->setIcon( KIcon( "nextuntranslated" ) );
    actionNextUntranslated->setText( i18n("Nex&t Untranslated") );
    actionNextUntranslated->setShortcut( QKeySequence( Qt::ALT+Qt::Key_PageDown ) );
    actionNextUntranslated->setEnabled( false );
    connect( actionNextUntranslated, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextUntranslated() ) );

    KAction *actionPreviousUntranslated = actionCollection()->addAction( "go_prev_untrans" );
    actionPreviousUntranslated->setIcon( KIcon( "prevuntranslated" ) );
    actionPreviousUntranslated->setText( i18n("Prev&ious Untranslated") );
    actionPreviousUntranslated->setShortcut( QKeySequence( Qt::ALT+Qt::Key_PageUp ) );
    actionPreviousUntranslated->setEnabled( false );
    connect( actionPreviousUntranslated, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousUntranslated() ) );

    KAction *actionNextFuzzy = actionCollection()->addAction( "go_next_fuzzy" );
    actionNextFuzzy->setIcon( KIcon( "nextfuzzy" ) );
    actionNextFuzzy->setText( i18n("Ne&xt Fuzzy") );
    actionNextFuzzy->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_PageDown ) );
    actionNextFuzzy->setEnabled( false );
    connect( actionNextFuzzy, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextFuzzy() ) );

    KAction *actionPreviousFuzzy = actionCollection()->addAction( "go_prev_fuzzy" );
    actionPreviousFuzzy->setIcon( KIcon( "prevfuzzy" ) );
    actionPreviousFuzzy->setText( i18n("Pre&vious Fuzzy") );
    actionPreviousFuzzy->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_PageUp ) );
    actionPreviousFuzzy->setEnabled( false );
    connect( actionPreviousFuzzy, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousFuzzy() ) );

    KAction *actionNextFuzzyOrTranslated = actionCollection()->addAction( "go_next_fuzzyUntr" );
    actionNextFuzzyOrTranslated->setIcon( KIcon( "nextfuzzyuntrans" ) );
    actionNextFuzzyOrTranslated->setText( i18n("N&ext Fuzzy or Untranslated") );
    actionNextFuzzyOrTranslated->setShortcut( QKeySequence( Qt::CTRL+Qt::SHIFT+Qt::Key_PageDown ) );
    actionNextFuzzyOrTranslated->setEnabled( false );
    connect( actionNextFuzzyOrTranslated, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextFuzzyOrUntranslated() ) );

    KAction *actionPreviousFuzzyOrTranslated = actionCollection()->addAction( "go_prev_fuzzyUntr" );
    actionPreviousFuzzyOrTranslated->setIcon( KIcon( "prevfuzzyuntrans" ) );
    actionPreviousFuzzyOrTranslated->setText( i18n("P&revious Fuzzy or Untranslated") );
    actionPreviousFuzzyOrTranslated->setShortcut( QKeySequence( Qt::CTRL+Qt::SHIFT+Qt::Key_PageUp ) );
    actionPreviousFuzzyOrTranslated->setEnabled( false );
    connect( actionPreviousFuzzyOrTranslated, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousFuzzyOrUntranslated() ) );

    KAction *actionNextError = actionCollection()->addAction( "go_next_error" );
    actionNextError->setIcon( KIcon( "nexterror" ) );
    actionNextError->setText( i18n("Next Err&or") );
    actionNextError->setShortcut( QKeySequence( Qt::ALT+Qt::SHIFT+Qt::Key_PageDown ) );
    actionNextError->setEnabled( false );
    connect( actionNextError, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextError() ) );

    KAction *actionPreviousError = actionCollection()->addAction( "go_prev_error" );
    actionPreviousError->setIcon( KIcon( "preverror" ) );
    actionPreviousError->setText( i18n("Previo&us Error") );
    actionPreviousError->setShortcut( QKeySequence( Qt::ALT+Qt::SHIFT+Qt::Key_PageUp ) );
    actionPreviousError->setEnabled( false );
    connect( actionPreviousError, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousError() ) );

    KAction *actionNextTemplateOnly = actionCollection()->addAction( "go_next_template" );
    actionNextTemplateOnly->setIcon( KIcon( "nexttemplate" ) );
    actionNextTemplateOnly->setText( i18n("Next Te&mplate Only") );
    actionNextTemplateOnly->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Down ) );
    actionNextTemplateOnly->setEnabled( false );
    connect( actionNextTemplateOnly, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextTemplate() ) );

    KAction *actionPreviousTemplateOnly = actionCollection()->addAction( "go_prev_template" );
    actionPreviousTemplateOnly->setIcon( KIcon( "prevtemplate" ) );
    actionPreviousTemplateOnly->setText( i18n("Previous Temp&late Only") );
    actionPreviousTemplateOnly->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Up ) );
    actionPreviousTemplateOnly->setEnabled( false );
    connect( actionPreviousTemplateOnly, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousTemplate() ) );

    KAction *actionNextTranslationExists = actionCollection()->addAction( "go_next_po" );
    actionNextTranslationExists->setIcon( KIcon( "nextpo" ) );
    actionNextTranslationExists->setText( i18n("Next Tran&slation Exists") );
    actionNextTranslationExists->setShortcut( QKeySequence( Qt::ALT+Qt::Key_Down ) );
    actionNextTranslationExists->setEnabled( false );
    connect( actionNextTranslationExists, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextPo() ) );

    KAction *actionPreviousTranslationExists = actionCollection()->addAction( "go_prev_po" );
    actionPreviousTranslationExists->setIcon( KIcon( "prevpo" ) );
    actionPreviousTranslationExists->setText( i18n("Previous Transl&ation Exists") );
    actionPreviousTranslationExists->setShortcut( QKeySequence( Qt::ALT+Qt::Key_Up ) );
    actionPreviousTranslationExists->setEnabled( false );
    connect( actionPreviousTranslationExists, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousPo() ) );

    KAction *actionPreviousMarked = actionCollection()->addAction( "go_prev_marked" );
    actionPreviousMarked->setIcon( KIcon( "prevmarked" ) );
    actionPreviousMarked->setText( i18n("Previous Marke&d") );
    actionPreviousMarked->setShortcut( QKeySequence( Qt::SHIFT+Qt::Key_Up ) );
    actionPreviousMarked->setEnabled( false );
    connect( actionPreviousMarked, SIGNAL( triggered() ), _catalogManager, SLOT( gotoPreviousMarked() ) );

    KAction *actionNextMarked = actionCollection()->addAction( "go_next_marked" );
    actionNextMarked->setIcon( KIcon( "nextmarked" ) );
    actionNextMarked->setText( i18n("Next &Marked") );
    actionNextMarked->setShortcut( QKeySequence( Qt::SHIFT+Qt::Key_Down ) );
    actionNextMarked->setEnabled( false );
    connect( actionNextMarked, SIGNAL( triggered() ), _catalogManager, SLOT( gotoNextMarked() ) );

    // project menu
    // the project menu
    action = actionCollection()->addAction( "project_new" );
    action->setIcon( KIcon("document-new") );
    action->setText( i18n("&New...") );
    connect(action,SIGNAL(triggered(bool)), this, SLOT(projectNew()));

    action = actionCollection()->addAction( "project_open" );
    action->setIcon( KIcon("document-open") );
    action->setText( i18n("&Open...") );
    connect(action,SIGNAL(triggered(bool)), this, SLOT(projectOpen()));

    action = actionCollection()->addAction( "project_close" );
    action->setIcon( KIcon("window-close") );
    action->setText( i18n("C&lose") );
    action->setEnabled (_project->filename() != KBabel::ProjectManager::defaultProjectName());
    connect(action,SIGNAL(triggered(bool)), this, SLOT(projectClose()));

    action = actionCollection()->addAction( "project_settings" );
    action->setIcon( KIcon( "configure") );
    action->setText( i18n("&Configure...") );
    connect(action,SIGNAL(triggered(bool)), this, SLOT(projectConfigure()));

    // tools menu
    KAction *actionStatistics = actionCollection()->addAction( "statistics" );
    actionStatistics->setIcon( KIcon( "statistics" ) );
    actionStatistics->setText( i18n("&Statistics") );
    actionStatistics->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_S ) );
    actionStatistics->setEnabled( false );
    connect( actionStatistics, SIGNAL( triggered() ), _catalogManager, SLOT( statistics() ) );

    KAction *actionStatisticsInMarked = actionCollection()->addAction( "statistics_marked" );
    actionStatisticsInMarked->setIcon( KIcon( "statistics" ) );
    actionStatisticsInMarked->setText( i18n("S&tatistics in Marked") );
    actionStatisticsInMarked->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_S ) );
    actionStatisticsInMarked->setEnabled( false );
    connect( actionStatisticsInMarked, SIGNAL( triggered() ), _catalogManager, SLOT( markedStatistics() ) );

    KAction *actionCheckSyntax = actionCollection()->addAction( "syntax" );
    actionCheckSyntax->setIcon( KIcon( "syntax" ) );
    actionCheckSyntax->setText( i18n("Check S&yntax") );
    actionCheckSyntax->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_Y ) );
    actionCheckSyntax->setEnabled( false );
    connect( actionCheckSyntax, SIGNAL( triggered() ), _catalogManager, SLOT( checkSyntax() ) );

    KAction *actionSpellCheck = actionCollection()->addAction( "spellcheck" );
    actionSpellCheck->setIcon( KIcon( "tools-check-spelling" ) );
    actionSpellCheck->setText( i18n("S&pell Check") );
    actionSpellCheck->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_I ) );
    actionSpellCheck->setEnabled( false );
    connect( actionSpellCheck, SIGNAL( triggered() ), this, SLOT( spellcheck() ) );

    KAction *actionSpellCheckInMarked = actionCollection()->addAction( "spellcheck_marked" );
    actionSpellCheckInMarked->setIcon( KIcon( "tools-check-spelling" ) );
    actionSpellCheckInMarked->setText( i18n("Spell Check in &Marked") );
    actionSpellCheckInMarked->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_I ) );
    actionSpellCheckInMarked->setEnabled( false );
    connect( actionSpellCheckInMarked, SIGNAL( triggered() ), this, SLOT( markedSpellcheck() ) );

    KAction *actionRoughTranslation = actionCollection()->addAction( "rough_translation" );
    actionRoughTranslation->setText( i18n("&Rough Translation") );
    actionRoughTranslation->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_T ) );
    actionRoughTranslation->setEnabled( false );
    connect( actionRoughTranslation, SIGNAL( triggered() ), _catalogManager, SLOT( roughTranslation() ) );

    KAction *actionRoughTranslationInMarked = actionCollection()->addAction( "rough_translation_marked" );
    actionRoughTranslationInMarked->setText( i18n("Rough Translation in M&arked") );
    actionRoughTranslationInMarked->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_T ) );
    actionRoughTranslationInMarked->setEnabled( false );
    connect( actionRoughTranslationInMarked, SIGNAL( triggered() ), _catalogManager, SLOT( markedRoughTranslation() ) );

    KAction *actionMail = actionCollection()->addAction( "mail_file" );
    actionMail->setIcon( KIcon( "mail-send" ) );
    actionMail->setText( i18n("Mai&l") );
    actionMail->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_A ) );
    actionMail->setEnabled( false );
    connect( actionMail, SIGNAL( triggered() ), _catalogManager, SLOT( mailFiles() ) );

    KAction *actionMailMarked = actionCollection()->addAction( "mail_file_marked" );
    actionMailMarked->setIcon( KIcon( "mail-send" ) );
    actionMailMarked->setText( i18n("Mail Mar&ked") );
    actionMailMarked->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_A ) );
    actionMailMarked->setEnabled( false );
    connect( actionMailMarked, SIGNAL( triggered() ), _catalogManager, SLOT( mailMarkedFiles() ) );

    KAction *actionPack = actionCollection()->addAction( "package_file" );
    actionPack->setIcon( KIcon( "tar" ) );
    actionPack->setText( i18n("&Pack") );
    actionPack->setShortcut( QKeySequence( Qt::CTRL+Qt::Key_B ) );
    connect( actionPack, SIGNAL( triggered() ), _catalogManager, SLOT( packageFiles() ) );

    KAction *actionPackMarked = actionCollection()->addAction( "package_file_marked" );
    actionPackMarked->setIcon( KIcon( "tar" ) );
    actionPackMarked->setText( i18n("Pack &Marked") );
    actionPackMarked->setShortcut( QKeySequence( Qt::CTRL+Qt::ALT+Qt::Key_B ) );
    actionPackMarked->setEnabled(false);
    connect( actionPackMarked, SIGNAL( triggered() ), _catalogManager, SLOT( packageMarkedFiles() ) );

    actionMap["statistics_marked"]        = NEEDS_DIR | NEEDS_MARK;
    actionMap["syntax"]                   = NEEDS_PO;
    actionMap["spellcheck"]               = NEEDS_PO;
    actionMap["spellcheck_marked"]        = NEEDS_PO | NEEDS_MARK;
    actionMap["rough_translation_marked"] = NEEDS_MARK;
    actionMap["mail_file"]                = NEEDS_PO;
    actionMap["mail_file_marked"]         = NEEDS_PO | NEEDS_MARK;
    actionMap["package_file_marked"]      = NEEDS_PO | NEEDS_MARK;

    // dynamic tools
    QList<KDataToolInfo> tools = ToolAction::validationTools();

    QList<KAction *> actions = ToolAction::dataToolActionList(
	tools, _catalogManager, SLOT(validateUsingTool( const KDataToolInfo &, const QString& ))
	,QStringList("validate"), false, actionCollection() );

    KActionMenu* m_menu = actionCollection()->add<KActionMenu>("dynamic_validation");
    m_menu->setText(i18n("&Validation"));

    foreach (KAction *ac, actions)
    {
	m_menu->addAction(ac);
    }

    actions = ToolAction::dataToolActionList(
	tools, _catalogManager, SLOT(validateMarkedUsingTool( const KDataToolInfo &, const QString& ))
	,QStringList("validate"), false, actionCollection(), "marked_" );
    m_menu = actionCollection()->add<KActionMenu>("dynamic_validation_marked");
    m_menu->setText(i18n("V&alidation Marked"));

    foreach (KAction *ac, actions)
    {
	m_menu->addAction(ac);
    }

    actionMap["dynamic_validation"]        = NEEDS_PO;
    actionMap["dynamic_validation_marked"] = NEEDS_PO | NEEDS_MARK;

    // CVS submenu
    // Actions for PO files
    KAction *actionCVSUpdate = actionCollection()->addAction( "cvs_update" );
    actionCVSUpdate->setIcon( KIcon( "go-down" ) );
    actionCVSUpdate->setText( i18n( "Update" ) );
    connect( actionCVSUpdate, SIGNAL( triggered() ), _catalogManager, SLOT( cvsUpdate() ) );

    KAction *actionCVSUpdateMarked = actionCollection()->addAction( "cvs_update_marked" );
    actionCVSUpdateMarked->setText( i18n( "Update Marked" ) );
    connect( actionCVSUpdateMarked, SIGNAL( triggered() ), _catalogManager, SLOT( cvsUpdateMarked() ) );

    KAction *actionCVSCommit = actionCollection()->addAction( "cvs_commit" );
    actionCVSCommit->setIcon( KIcon( "go-up" ) );
    actionCVSCommit->setText( i18n( "Commit" ) );
    connect( actionCVSCommit, SIGNAL( triggered() ), _catalogManager, SLOT( cvsCommit() ) );

    KAction *actionCVSCommitMarked = actionCollection()->addAction( "cvs_commit_marked" );
    actionCVSCommitMarked->setText( i18n( "Commit Marked" ) );
    connect( actionCVSCommitMarked, SIGNAL( triggered() ), _catalogManager, SLOT( cvsCommitMarked() ) );

    KAction *actionCVSStatus = actionCollection()->addAction( "cvs_status" );
    actionCVSStatus->setText( i18n( "Status" ) );
    connect( actionCVSStatus, SIGNAL( triggered() ), _catalogManager, SLOT( cvsStatus() ) );

    KAction *actionCVSStatusMarked = actionCollection()->addAction( "cvs_status_marked" );
    actionCVSStatusMarked->setText( i18n( "Status for Marked" ) );
    connect( actionCVSStatusMarked, SIGNAL( triggered() ), _catalogManager, SLOT( cvsStatusMarked() ) );

    KAction *actionCVSShowDiff = actionCollection()->addAction( "cvs_diff" );
    actionCVSShowDiff->setText( i18n( "Show Diff" ) );
    connect( actionCVSShowDiff, SIGNAL( triggered() ), _catalogManager, SLOT( cvsDiff() ) );

    // CVS
    actionMap["cvs_update"]        = NEEDS_PO | NEEDS_PO_CVS;
    actionMap["cvs_update_marked"] = NEEDS_PO | NEEDS_PO_CVS | NEEDS_MARK;
    actionMap["cvs_commit"]        = NEEDS_PO | NEEDS_PO_CVS;
    actionMap["cvs_commit_marked"] = NEEDS_PO | NEEDS_PO_CVS | NEEDS_MARK;
    actionMap["cvs_status"]        = NEEDS_PO | NEEDS_PO_CVS;
    actionMap["cvs_status_marked"] = NEEDS_PO | NEEDS_PO_CVS | NEEDS_MARK;
    actionMap["cvs_diff"]          = NEEDS_PO | NEEDS_PO_CVS;

    // SVN submenu
    // Actions for PO files
    KAction *actionSVNUpdate = actionCollection()->addAction( "svn_update" );
    actionSVNUpdate->setIcon( KIcon( "go-down" ) );
    actionSVNUpdate->setText( i18n( "Update" ) );
    connect( actionSVNUpdate, SIGNAL( triggered() ), _catalogManager, SLOT( svnUpdate() ) );

    KAction *actionSVNUpdateMarked = actionCollection()->addAction( "svn_update_marked" );
    actionSVNUpdateMarked->setText( i18n( "Update Marked" ) );
    connect( actionSVNUpdateMarked, SIGNAL( triggered() ), _catalogManager, SLOT( svnUpdateMarked() ) );

    KAction *actionSVNCommit = actionCollection()->addAction( "svn_commit" );
    actionSVNCommit->setIcon( KIcon( "go-up" ) );
    actionSVNCommit->setText( i18n( "Commit" ) );
    connect( actionSVNCommit, SIGNAL( triggered() ), _catalogManager, SLOT( svnCommit() ) );

    KAction *actionSVNCommitMarked = actionCollection()->addAction( "svn_commit_marked" );
    actionSVNCommitMarked->setText( i18n( "Commit Marked" ) );
    connect( actionSVNCommitMarked, SIGNAL( triggered() ), _catalogManager, SLOT( svnCommitMarked() ) );

    KAction *actionSVNLocalStatus = actionCollection()->addAction( "svn_status_local" );
    actionSVNLocalStatus->setText( i18n( "Status (Local)" ) );
    connect( actionSVNLocalStatus, SIGNAL( triggered() ), _catalogManager, SLOT( svnStatusLocal() ) );

    KAction *actionSVNLocalStatusMarked = actionCollection()->addAction( "svn_status_local_marked" );
    actionSVNLocalStatusMarked->setText( i18n( "Status (Local) for Marked" ) );
    connect( actionSVNLocalStatusMarked, SIGNAL( triggered() ), _catalogManager, SLOT( svnStatusLocalMarked() ) );

    KAction *actionSVNRemoteStatus = actionCollection()->addAction( "svn_status_remote" );
    actionSVNRemoteStatus->setText( i18n( "Status (Remote)" ) );
    connect( actionSVNRemoteStatus, SIGNAL( triggered() ), _catalogManager, SLOT( svnStatusRemote() ) );

    KAction *actionSVNRemoteStatusMarked = actionCollection()->addAction( "svn_status_remote_marked" );
    actionSVNRemoteStatusMarked->setText( i18n( "Status (Remote) for Marked" ) );
    connect( actionSVNRemoteStatusMarked, SIGNAL( triggered() ), _catalogManager, SLOT( svnStatusRemoteMarked() ) );

    KAction *actionSVNShowDiff = actionCollection()->addAction( "svn_diff" );
    actionSVNShowDiff->setText( i18n( "Show Diff" ) );
    connect( actionSVNShowDiff, SIGNAL( triggered() ), _catalogManager, SLOT( svnDiff() ) );

    KAction *actionSVNShowInformation = actionCollection()->addAction( "svn_info" );
    actionSVNShowInformation->setText( i18n( "Show Information" ) );
    connect( actionSVNShowInformation, SIGNAL( triggered() ), _catalogManager, SLOT( svnInfo() ) );

    KAction *actionSVNShowInformationMarked = actionCollection()->addAction( "svn_info_marked" );
    actionSVNShowInformationMarked->setText( i18n( "Show Information for Marked" ) );
    connect( actionSVNShowInformationMarked, SIGNAL( triggered() ), _catalogManager, SLOT( svnInfoMarked() ) );

    // SVN
    actionMap["svn_update"]        = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_update_marked"] = NEEDS_PO | NEEDS_PO_SVN | NEEDS_MARK;
    actionMap["svn_commit"]        = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_commit_marked"] = NEEDS_PO | NEEDS_PO_SVN | NEEDS_MARK;
    actionMap["svn_status_local"]        = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_status_local_marked"] = NEEDS_PO | NEEDS_PO_SVN | NEEDS_MARK;
    actionMap["svn_status_remote"]        = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_status_remote_marked"] = NEEDS_PO | NEEDS_PO_SVN | NEEDS_MARK;
    actionMap["svn_diff"]          = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_info"]          = NEEDS_PO | NEEDS_PO_SVN;
    actionMap["svn_info_marked"]   = NEEDS_PO | NEEDS_PO_SVN | NEEDS_MARK;

    // CVS Actions for POT files
    KAction *actionCVSUpdateTemplates = actionCollection()->addAction( "cvs_update_template" );
    actionCVSUpdateTemplates->setText( i18n( "Update Templates" ) );
    connect( actionCVSUpdateTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( cvsUpdateTemplate() ) );

    KAction *actionCVSUpdateMarkedTemplates = actionCollection()->addAction( "cvs_update_marked_template" );
    actionCVSUpdateMarkedTemplates->setText( i18n( "Update Marked Templates" ) );
    connect( actionCVSUpdateMarkedTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( cvsUpdateMarkedTemplate() ) );

    KAction *actionCVSCommitTemplates = actionCollection()->addAction( "cvs_commit_template" );
    actionCVSCommitTemplates->setText( i18n( "Commit Templates" ) );
    connect( actionCVSCommitTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( cvsCommitTemplate() ) );

    KAction *actionCVSCommitMarkedTemplates = actionCollection()->addAction( "cvs_commit_marked_template" );
    actionCVSCommitMarkedTemplates->setText( i18n( "Commit Marked Templates" ) );
    connect( actionCVSCommitMarkedTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( cvsCommitMarkedTemplate() ) );

    actionMap["cvs_update_template"]        = NEEDS_POT | NEEDS_POT_CVS;
    actionMap["cvs_update_marked_template"] = NEEDS_POT | NEEDS_POT_CVS | NEEDS_MARK;
    actionMap["cvs_commit_template"]        = NEEDS_POT | NEEDS_POT_CVS;
    actionMap["cvs_commit_marked_template"] = NEEDS_POT | NEEDS_POT_CVS | NEEDS_MARK;

    // SVN Actions for POT files
    KAction *actionSVNUpdateTemplates = actionCollection()->addAction( "svn_update_template" );
    actionSVNUpdateTemplates->setText( i18n( "Update Templates" ) );
    connect( actionSVNUpdateTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( svnUpdateTemplate() ) );

    KAction *actionSVNUpdateMarkedTemplates = actionCollection()->addAction( "svn_update_marked_template" );
    actionSVNUpdateMarkedTemplates->setText( i18n( "Update Marked Templates" ) );
    connect( actionSVNUpdateMarkedTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( svnUpdateMarkedTemplate() ) );

    KAction *actionSVNCommitTemplates = actionCollection()->addAction( "svn_commit_template" );
    actionSVNCommitTemplates->setText( i18n( "Commit Templates" ) );
    connect( actionSVNCommitTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( svnCommitTemplate() ) );

    KAction *actionSVNCommitMarkedTemplates = actionCollection()->addAction( "svn_commit_marked_template" );
    actionSVNCommitMarkedTemplates->setText( i18n( "Commit Marked Templates" ) );
    connect( actionSVNCommitMarkedTemplates, SIGNAL( triggered() ), _catalogManager, SLOT( svnCommitMarkedTemplate() ) );

    actionMap["svn_update_template"]        = NEEDS_POT | NEEDS_POT_SVN;
    actionMap["svn_update_marked_template"] = NEEDS_POT | NEEDS_POT_SVN | NEEDS_MARK;
    actionMap["svn_commit_template"]        = NEEDS_POT | NEEDS_POT_SVN;
    actionMap["svn_commit_marked_template"] = NEEDS_POT | NEEDS_POT_SVN | NEEDS_MARK;

    // settings menu
    // FIXME: KStandardAction::preferences(this, SLOT( optionsPreferences()), actionCollection());

    createStandardStatusBarAction();

    setStandardToolBarMenuEnabled ( true );

    // commands menus
    KActionMenu* actionMenu= actionCollection()->add<KActionMenu>("dir_commands");
    actionMenu->setText(i18n("Commands"));
    _catalogManager->setDirCommandsMenu( actionMenu->menu() );

    actionMenu=actionCollection()->add<KActionMenu>("file_commands");
    actionMenu->setText(i18n("Commands"));
    _catalogManager->setFileCommandsMenu( actionMenu->menu() );

    KAction *actionDelete = actionCollection()->addAction( "delete" );
    actionDelete->setText( i18n("&Delete") );
    actionDelete->setShortcut( QKeySequence( Qt::Key_Delete ) );
    actionDelete->setEnabled( false );
    connect( actionDelete, SIGNAL( triggered() ), _catalogManager, SLOT( slotDeleteFile() ) );

    setupGUI();
}

void CatalogManager::setupStatusBar()
{
    _foundLabel = new QLabel( "          ", statusBar());
    statusBar()->addWidget(_foundLabel,0);

    KHBox* progressBox = new KHBox(statusBar() );
    progressBox->setSpacing(2);
    _statusProgressLabel = new QLabel( "", progressBox );
    _statusProgressBar = new QProgressBar( progressBox );
    _statusProgressBar->hide();

    statusBar()->addWidget(progressBox,1);
    statusBar()->setMinimumHeight(_statusProgressBar->sizeHint().height());

    statusBar()->setWhatsThis(
	i18n("<qt><p><b>Statusbar</b></p>\n"
         "<p>The statusbar displays information about progress of"
         " the current find or replace operation. The first number in <b>Found:</b>"
         " displays the number of files with an occurrence of the searched text not"
         " yet shown in the KBabel window. The second shows the total number of files"
         " containing the searched text found so far.</p></qt>"));
}

void CatalogManager::enableMenuForFiles(bool enable)
{
    stateChanged( "treeBuilt", enable ? StateNoReverse: StateReverse );
}

void CatalogManager::selectedChanged(uint actionValue)
{
  QMap<QString,uint>::Iterator it;
  for (it = actionMap.begin( ); it != actionMap.end( ); ++it) {
    QAction *action = actionCollection()->action( it.key()/*.toLatin1()*/ );
    if ( action ) action->setEnabled( ( actionValue & it.value() ) == it.value() );
  }
}

CatManSettings CatalogManager::settings() const
{
    return _catalogManager->settings();
}

void CatalogManager::updateSettings()
{
    _settings = _project->catManSettings();
    _catalogManager->setSettings(_settings);
   _openNewWindow=_settings.openWindow;
}

void CatalogManager::saveSettings( const QString& configFile )
{
    _settings = _catalogManager->settings(); // restore settings from the view

    _project->setSettings( _settings );

    config = new KConfig(configFile);

    _catalogManager->saveView(config);

    config->sync();
}

void CatalogManager::restoreSettings()
{
    _settings = _project->catManSettings();
    _openNewWindow=_settings.openWindow;
    _catalogManager->restoreView(_project->config());
}

void CatalogManager::setPreferredWindow(WId window)
{
    _preferredWindow = window;
    kDebug(KBABEL_CATMAN) << "setPrefereedWindow set to :" << _preferredWindow;
}

void CatalogManager::updateFile(const QString& fileWithPath)
{
    _catalogManager->updateFile(fileWithPath,true); //force update
}

void CatalogManager::updateAfterSave(const QString& fileWithPath, PoInfo &info)
{
    _catalogManager->updateAfterSave(fileWithPath, info);
}

CatalogManagerView *CatalogManager::view()
{
    return _catalogManager;
}

void CatalogManager::openFile(const QByteArray& filename, const QByteArray& package)
{

    if( startKBabel() )
    {
        kDebug(KBABEL_CATMAN) << "Open file with project " << _configFile;

	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");
        bool valid=false;
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());

        if(_configFile != "kbabelrc" )
        {
            QDBusReply<void> reply = kbabel.openUrl( filename, package, qlonglong( CatalogManagerApp::_preferredWindow ), ( _openNewWindow ? 1 : 0 ), _configFile );
	    valid = reply.isValid();
        }
        else
        {
            QDBusReply<void> reply = kbabel.openUrl(filename, package, qlonglong( CatalogManagerApp::_preferredWindow ), ( _openNewWindow ? 1 : 0 ) );
	    valid = reply.isValid();
        }
        if( !valid)
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::openFile(const QByteArray& filename, const QByteArray& package, int msgid)
{

    if( startKBabel() )
    {
        kDebug(KBABEL_CATMAN) << "Open file with project " << _configFile;

	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
        bool valid = false;
	if(_configFile != "kbabelrc" )
        {
            QDBusReply<void> reply = kbabel.gotoFileEntry(filename, package, msgid, _configFile );
	    valid = reply.isValid();
        }
        else
        {
            QDBusReply<void> reply = kbabel.gotoFileEntry(filename, package, msgid );
	    valid = reply.isValid();
        }
        if( !valid)
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::openFileInNewWindow(const QByteArray& filename, const QByteArray& package)
{

    if( startKBabel() )
    {
	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
	bool valid = false;
        if(_configFile != "kbabelrc" )
        {
            QDBusReply<void> reply = kbabel.openUrl(filename, package, qlonglong( CatalogManagerApp::_preferredWindow ), 1 , _configFile );
	    valid = reply.isValid();
        }
        else
        {
            QDBusReply<void> reply = kbabel.openUrl(filename, package, qlonglong( CatalogManagerApp::_preferredWindow ), 1  );
	    valid = reply.isValid();
        }

        if( !valid )
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::openTemplate(const QByteArray& openFilename, const QByteArray& saveFilename, const QByteArray& package)
{

    if( startKBabel() ) {

	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
        bool valid = false;
	if(_configFile != "kbabelrc" )
        {
            QDBusReply<void> reply = kbabel.openTemplate( openFilename, saveFilename, package, ( _openNewWindow ? 1 : 0 ), _configFile );
	    valid = reply.isValid();
        }
        else
        {
            QDBusReply<void> reply = kbabel.openTemplate( openFilename, saveFilename, package, ( _openNewWindow ? 1 : 0 ) );
	    valid = reply.isValid();
        }

        if( !valid )
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::openTemplateInNewWindow(const QByteArray& openFilename, const QByteArray& saveFilename, const QByteArray& package)
{

    if( startKBabel() ) {

	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
	bool valid = false;
        if(_configFile != "kbabelrc" )
        {
            QDBusReply<void> reply = kbabel.openTemplate(openFilename, saveFilename, package, 1, _configFile );
	    valid = reply.isValid();
        }
        else
        {
            QDBusReply<void> reply = kbabel.openTemplate( openFilename, saveFilename, package, 1 );
	    valid = reply.isValid();
        }

        if( !valid )
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::spellcheck()
{

    QStringList fileList = _catalogManager->current();

    if( startKBabel() ) {
	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
        QDBusReply<void> reply = kbabel.spellcheck(fileList );

	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");

        if( !reply.isValid() )
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

void CatalogManager::markedSpellcheck()
{

    QStringList fileList = _catalogManager->marked();

    if( startKBabel() ) {
	// update the user timestamp for KBabel to get it a focus
	kapp->updateRemoteUserTimestamp ("org.kde.kbabel");

	org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
	QDBusReply<void> reply = kbabel.spellcheck( fileList );

        if( !reply.isValid() )
            KMessageBox::error(this, i18n("Cannot send a D-Bus message to KBabel.\n"
                                          "Please check your installation of KDE."));
    }
}

bool CatalogManager::startKBabel()
{
    // if there is no running kbabel, start one
    if( !QDBusConnection::sessionBus().interface()->isServiceRegistered("org.kde.kbabel") )
    {
        QString app = "kbabel";
        QString url = "";
        QString service, result;
        if( KToolInvocation::startServiceByDesktopName(app,url, &result, &service))
        {
            KMessageBox::error( this, i18n("Unable to use KLauncher to start KBabel.\n"
                                           "You should check the installation of KDE.\n"
                                           "Please start KBabel manually."));
            return false;
        } else sleep(1);
    }

    return true;
}


void CatalogManager::prepareProgressBar(const QString& msg, int max)
{
   _progressBar->setRange( 0, max );
   _progressBar->setValue( 0 );
   _progressLabel->setText(msg);

   _progressBar->show();
   _progressLabel->show();
}

void CatalogManager::clearProgressBar()
{
   _progressBar->setValue( 0 );

   _progressBar->hide();
   _progressLabel->hide();
}

void CatalogManager::prepareStatusProgressBar(const QString& msg, int max)
{
   _totalFound = 0;
   _foundToBeSent = 0;
   _statusProgressBar->setRange( 0, max );
   _statusProgressLabel->setText(msg);
   _foundLabel->setText( i18n("Found: 0/0") );

   _statusProgressBar->show();
   _statusProgressLabel->show();
}

void CatalogManager::prepareStatusProgressBar(int max)
{
   _statusProgressBar->setRange( 0, max );
}

void CatalogManager::clearStatusProgressBar()
{
   _statusProgressBar->setValue(0);

   _statusProgressBar->hide();
   _statusProgressLabel->hide();
   _foundLabel->setText("          ");
}

void CatalogManager::setNumberOfFound(int toBeSent, int total)
{
    _foundLabel->setText(i18n("Found: %1/%2", toBeSent, total));
}

void CatalogManager::decreaseNumberOfFound()
{
    if( _foundToBeSent > 0 ) {
        _foundToBeSent--;
        setNumberOfFound( _foundToBeSent, _totalFound );
    }
}

void CatalogManager::slotHelp()
{
   KToolInvocation::invokeHelp("CATALOGMANAGER","kbabel");
}

void CatalogManager::find()
{
    if( !_findDialog ) _findDialog = new FindInFilesDialog(false,this);

    if( _findDialog->exec("") == QDialog::Accepted )
    {
        _timerFind->stop();
        _searchStopped = false;
        _catalogManager->stop(false); // surely we are not in process of quitting, since there is no window and user cannot invoke Find
        prepareStatusProgressBar(i18n("Searching"),1); // just show the progress bar

        // enable stop action to stop searching
        KAction *action = (KAction*)actionCollection()->action("stop_search");
        action->setEnabled(true);

        _findOptions = _findDialog->findOpts();

        // get from options the information for ignoring text parts
        _findOptions.contextInfo = QRegExp( _project->miscSettings().contextInfo );
        _findOptions.accelMarker = _project->miscSettings().accelMarker;

        _foundFilesList.clear();
        kDebug(KBABEL_CATMAN) << "Calling catalogmanagerview::find";
        QString url = _catalogManager->find(_findOptions, _toBeSearched );

        if( _catalogManager->isStopped() ) return;
        if( !url.isEmpty() )
        {
            if( startKBabel() )
            {
                QDBusInterface kbabel("org.kde.kbabel", "/KBabel", "org.kde.kbabel.kbabel");

                QList<QVariant> arguments;
                arguments << QDBusConnection::sessionBus().baseService();
                arguments << url.toUtf8();
                arguments << _findOptions.findStr;
                arguments << (_findOptions.caseSensitive ? 1 : 0);
                arguments << (_findOptions.wholeWords ? 1 : 0);
                arguments << (_findOptions.isRegExp ? 1 : 0);
                arguments << (_findOptions.inMsgid ? 1 : 0);
                arguments << (_findOptions.inMsgstr ? 1 : 0);
                arguments << (_findOptions.inComment ? 1 : 0);
                arguments << (_findOptions.ignoreAccelMarker ? 1 : 0);
                arguments << (_findOptions.ignoreContextInfo ? 1 : 0);
                arguments << (_findOptions.askForNextFile ? 1 : 0);
                arguments << (_findOptions.askForSave ? 1 : 0);
                QDBusReply<bool> reply = kbabel.callWithArgumentList( QDBus::NoBlock, "findInFile", arguments );
                if( !reply.isValid())
                {
                    KMessageBox::error( this, i18n("D-Bus communication with KBabel failed."), i18n("D-Bus Communication Error"));
                    stopSearching();
                    return;
                }

                if( !_toBeSearched.isEmpty() )
                {
                    _totalFound = 1;
                    _foundToBeSent = 0;
                    setNumberOfFound( 0, 1 );	// one found, but already sent
                    _timerFind->setSingleShot( true );
                    _timerFind->start( 100 );
                } else stopSearching();
            }
            else
            {
                KMessageBox::error( this, i18n("KBabel cannot be started."), i18n("Cannot Start KBabel"));
                stopSearching();
            }

        }
        else
        {
            if( !_searchStopped) KMessageBox::information(this, i18n("Search string not found."));
            stopSearching();
        }
    }
}

void CatalogManager::replace()
{
    if( !_replaceDialog ) _replaceDialog = new FindInFilesDialog(true,this);


    if( _replaceDialog->exec("") == QDialog::Accepted )
    {
        _timerFind->stop();
        _searchStopped = false;
        _catalogManager->stop(false); // surely we are not in process of quitting, since there is no window and user cannot invoke Find
        prepareStatusProgressBar(i18n("Searching"),1); // just show the progress bar

        // enable stop action to stop searching
        KAction *action = (KAction*)actionCollection()->action("stop_search");
        action->setEnabled(true);

        ReplaceOptions options = _replaceDialog->replaceOpts();

        _findOptions = options;

        // get from options the information for ignoring text parts
        options.contextInfo = QRegExp( _project->miscSettings().contextInfo );
        options.accelMarker = _project->miscSettings().accelMarker;

        _foundFilesList.clear();
        QString url = _catalogManager->find(options, _toBeSearched );

        if( _catalogManager->isStopped() ) return;
        if( !url.isEmpty() )
        {
            if( startKBabel() )
            {
                QDBusInterface kbabel("org.kde.kbabel", "/KBabel", "org.kde.kbabel.kbabel");
                QDBusReply<bool> reply;
                QList<QVariant> arg;
                arg << QDBusConnection::sessionBus().baseService();
                arg << url.toUtf8();
                arg << options.findStr;
                arg << options.replaceStr;
                arg << (options.caseSensitive ? 1 : 0);
                arg << (options.wholeWords ? 1 : 0);
                arg << (options.isRegExp ? 1 : 0);
                arg << (options.inMsgid ? 1 : 0);
                arg << (options.inMsgstr ? 1 : 0);
                arg << (options.inComment ? 1 : 0);
                arg << (options.ignoreAccelMarker ? 1 : 0);
                arg << (options.ignoreContextInfo ? 1 : 0);
                arg << (options.ask ? 1 : 0);
                arg << (options.askForNextFile ? 1 : 0);
                arg << (options.askForSave ? 1 : 0);
                reply = kbabel.callWithArgumentList( QDBus::NoBlock, "replaceInFile", arg );
                if( !reply.isValid() )
                {
                    KMessageBox::error( this, i18n("D-Bus communication with KBabel failed."), i18n("D-Bus Communication Error"));
                    stopSearching();
                    return;
                }

                if( !_toBeSearched.isEmpty() )
                {
                    _totalFound = 1;
                    setNumberOfFound( 0, 1 );
                    _timerFind->setSingleShot( true );
                    _timerFind->start( 100 );
                } else stopSearching();
            }
            else
            {
                KMessageBox::error( this, i18n("KBabel cannot be started."), i18n("Cannot Start KBabel"));
                stopSearching(); // update window
            }

        }
        else
        {
            if( !_searchStopped ) KMessageBox::information(this, i18n("Search string not found."));
            stopSearching(); // update window
        }
    }
}

void CatalogManager::findNextFile()
{
    _timerFind->stop(); // stop the timer for lookup time
    if(_toBeSearched.empty() )
    {
        stopSearching();
        return;
    }
    QString file = _toBeSearched.first();
    _toBeSearched.pop_front();
    if( PoInfo::findInFile( file, _findOptions ) )
    {
        _foundFilesList.append(file);
        _totalFound++;
        _foundToBeSent++;
        setNumberOfFound(_foundToBeSent,_totalFound);
    }
    _statusProgressBar->setValue(_statusProgressBar->value()+1);
    if( !_toBeSearched.empty() ) {
        _timerFind->setSingleShot( true );
        _timerFind->start( 100 ); // if there is more files to be searched, start the timer again
    }
    else
        stopSearching();
}

void CatalogManager::stopSearching()
{
    _searchStopped = true;
    emit searchStopped();
    // clear the list of files to be searched
    _toBeSearched.clear();

    // fake that we are over (fake, because findNextFile can still be running for the last file
    clearStatusProgressBar(); // clear the status bar, we are finished
    // disable stop action as well
    KAction *action = (KAction*)actionCollection()->action("stop_search");
    action->setEnabled(false);
}

void CatalogManager::optionsPreferences()
{
   if(!_prefDialog)
   {
      _prefDialog = new KBabel::ProjectDialog(_project);
   }

   _prefDialog->exec();
}

void CatalogManager::newToolbarConfig()
{
    createGUI();
    restoreView();
}

void CatalogManager::optionsShowStatusbar(bool on)
{
    if( on )
        statusBar()->show();
    else
        statusBar()->hide();
}

bool CatalogManager::queryClose()
{
    _catalogManager->stop();
    saveView();
    saveSettings(_configFile);
    return true;
}

void CatalogManager::saveView()
{
    saveMainWindowSettings( KGlobal::config()->group( "View") );
}


void CatalogManager::restoreView()
{
    applyMainWindowSettings( KGlobal::config()->group( "View") );

    KToggleAction * toggle = (KToggleAction*)actionCollection()->
	action(KStandardAction::name(KStandardAction::ShowStatusbar));
    toggle->setChecked(!statusBar()->isHidden() );
}


void CatalogManager::projectNew()
{
    KBabel::Project::Ptr p = KBabel::ProjectWizard::newProject();
    if( p )
    {
	disconnect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));
        _project = p;
	connect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));

	_configFile = _project->filename();
        restoreSettings();
	updateSettings();
        changeProjectActions(p->filename());
	emit settingsChanged(_settings);
    }
}

void CatalogManager::projectOpen()
{
    QString oldproject = _project->filename();
    if( oldproject == ProjectManager::defaultProjectName() )
    {
        oldproject = QString();
    }
    const QString file = KFileDialog::getOpenFileName(oldproject, QString(), this);
    if (file.isEmpty())
    {
        return;
    }
    KBabel::Project::Ptr p = KBabel::ProjectManager::open(file);
    if( p )
    {
	disconnect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));
        _project = p;
	connect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));

	_configFile = p->filename();
        restoreSettings();
	updateSettings();
        changeProjectActions(file);
	emit settingsChanged(_settings);
    }
    else
    {
	KMessageBox::error (this, i18n("Cannot open project file %1", file));
    }
}

void CatalogManager::projectClose()
{
    disconnect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));
    _project = KBabel::ProjectManager::open(KBabel::ProjectManager::defaultProjectName());
    connect( _project.data(), SIGNAL (signalCatManSettingsChanged())
	    , this, SLOT (updateSettings()));
    _configFile = _project->filename();
    restoreSettings();
    updateSettings();
    changeProjectActions(ProjectManager::defaultProjectName());
    emit settingsChanged(_settings);
}

void CatalogManager::changeProjectActions(const QString& project)
{
    bool def = project == ProjectManager::defaultProjectName();

    KAction* saveAction=(KAction*)actionCollection()->action( "project_close" );
    saveAction->setEnabled( ! def );
}

void CatalogManager::projectConfigure()
{
    KBabel::ProjectDialog* _projectDialog = new ProjectDialog(_project);

    connect (_projectDialog, SIGNAL (settingsChanged(const QString&))
	, this, SLOT (updateSettings()));

    // settings are updated via signals
    _projectDialog->exec();

    delete _projectDialog;
}

void CatalogManager::enableActions()
{
    enableActions(true);
}

void CatalogManager::disableActions()
{
    enableActions(false);
}

void CatalogManager::enableActions(bool enable)
{
    KAction* action;
    // the file menu

    action = (KAction*)actionCollection()->action( "open" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "open_new_window" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "find_in_files" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "replace_in_files" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "reload" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "toggle_marking" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "toggle_all_marking" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "mark_modified_files" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "load_marking" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "save_marking" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_untrans" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_untrans" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_fuzzy" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_fuzzy" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_fuzzyUntr" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_fuzzyUntr" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_error" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_error" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_template" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_template" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_po" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_po" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_next_marked" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "go_prev_marked" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "statistics" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "package_file" );
    action->setEnabled(enable);

    action = (KAction*)actionCollection()->action( "rough_translation" );
    action->setEnabled(enable);
}

#include "catalogmanager.moc"
