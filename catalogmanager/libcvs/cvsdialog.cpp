/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2002-2003 by Marco Wegner <mail@marcowegner.de>
  Copyright (C) 2005, 2006 by Nicolas GOUTTE <goutte@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


// Qt include files
#include <qcheckbox.h>
#include <qcombobox.h>
#include <qfileinfo.h>
#include <qframe.h>
#include <qlabel.h>
#include <qlayout.h>
#include <Q3ListBox>
#include <qpushbutton.h>
#include <qstring.h>
#include <qstringlist.h>
#include <QTextEdit>
#include <q3textedit.h>
#include <QTextCodec>
#include <QTextStream>
// KDE include files
#include <kconfig.h>
#include <kdebug.h>
#include <kglobal.h>
#include <klocale.h>
#include <k3process.h>
#include <kshell.h>
#include <ktemporaryfile.h>
#include <kmessagebox.h>
#include <kstringhandler.h>
#include <kcombobox.h>
#include <kcharsets.h>
// Project specific include files
#include "cvsdialog.h"

CVSDialog::CVSDialog( CVS::Command cmd, QWidget * parent, KSharedConfig* config )
  : KDialog( parent ), m_tempFile( 0 ), m_config( config )
{
  _cmd = cmd;
  p=0L;
  setCaption( i18n( "CVS Dialog" ) );

  QString temp;

  QVBoxLayout * layout = new QVBoxLayout( this );
  layout->setObjectName( "MAIN LAYOUT" );
  layout->setSpacing( 6 );
  layout->setMargin( 6 );

  // Set the label's text depending on the CVS command.
  switch ( cmd ) {
    case CVS::Update:
      temp = i18n( "Update the following files:" );
      break;
    case CVS::Commit:
      temp = i18n( "Commit the following files:" );
      break;
    case CVS::Status:
      temp = i18n( "Get status for the following files:" );
      break;
    case CVS::Diff:
      temp = i18n( "Get diff for the following files:" );
      break;
  }
  layout->addWidget( new QLabel( temp, this ) );

  // Widget for showing the list of files.
  filebox = new Q3ListBox( this );
  layout->addWidget( filebox );

  // Add special widgets for 'cvs commit'.
  if ( cmd == CVS::Commit ) {
    QLabel * label;

    // Combobox for displaying old log messages.
    label = new QLabel( i18n( "&Old messages:" ), this );
    oldMessages = new QComboBox( this );
    oldMessages->setDuplicatesEnabled( false );
    label->setBuddy( oldMessages );
    layout->addWidget( label );
    layout->addWidget( oldMessages );

    // Textfield for entering a log message.
    label = new QLabel( i18n( "&Log message:" ), this );
    logedit = new QTextEdit( this );
    label->setBuddy( logedit );
    layout->addWidget( label );
    layout->addWidget( logedit );

    label = new QLabel( i18n( "E&ncoding:" ), this );
    m_encodingComboBox = new KComboBox( this );
    label->setBuddy( m_encodingComboBox );
    layout->addWidget( label );
    layout->addWidget( m_encodingComboBox );
    QStringList encodingList;
    // The last encoding will be added at the top of the list, when the seetings will be read.
    encodingList << i18nc( "Descriptive encoding name", "Recommended ( %1 )", QString( "UTF-8" ) );
    encodingList << i18nc( "Descriptive encoding name", "Locale ( %1 )", QString(QTextCodec::codecForLocale()->name()) );
    encodingList += KGlobal::charsets()->descriptiveEncodingNames();
    m_encodingComboBox->addItems( encodingList );
    
    connect( oldMessages, SIGNAL( activated( int ) ),
      this, SLOT( slotComboActivated( int ) ) );
  }

  QHBoxLayout * buttons = new QHBoxLayout();
  buttons->setObjectName( "BUTTON LAYOUT" );
  buttons->setSpacing( 6 );
  buttons->setMargin( 0 );
  // Add special buttons for 'cvs commit'.
  if ( cmd == CVS::Commit ) {
    autoAddBox = new QCheckBox( i18n( "Auto&matically add files if necessary" ), this );
    buttons->addWidget( autoAddBox );
  }
  buttons->addItem( new QSpacerItem( 1, 0, QSizePolicy::Expanding, QSizePolicy::Minimum ) );

  // Set the main button's text depending on the CVS comand.
  switch ( cmd ) {
    case CVS::Update:
      temp = i18n( "&Update" );
      break;
    case CVS::Commit:
      temp = i18n( "&Commit" );
      break;
    case CVS::Status:
      temp = i18n( "&Get Status" );
      break;
    case CVS::Diff:
      temp = i18n( "&Get Diff" );
      break;
  }
  mainBtn = new QPushButton( temp, this );
  mainBtn->setDefault( true );
  buttons->addWidget( mainBtn );

  cancelBtn = new QPushButton( i18n( "C&ancel" ), this );
  buttons->addWidget( cancelBtn );
  layout->addLayout( buttons );

  QFrame * line = new QFrame( this );
  line->setFrameStyle( QFrame::HLine | QFrame::Sunken );
  layout->addWidget( line );

  layout->addWidget( new QLabel( i18n( "Command output:" ), this ) );

  output = new Q3TextEdit( this );
  output->setReadOnly( true );
  layout->addWidget( output );

  resize( QSize( 600, 450 ).expandedTo( minimumSizeHint( ) ) );

  if ( cmd == CVS::Commit )
    logedit->setFocus( );

  readSettings( );

  connect( mainBtn, SIGNAL( clicked( ) ), this, SLOT( slotExecuteCommand( ) ) );
  connect( cancelBtn, SIGNAL( clicked( ) ), this, SLOT( reject( ) ) );
}

void CVSDialog::slotComboActivated( int index )
{
  if ( index < 0 || index >= m_logMessages.count() )
    return;
  logedit->setText( m_logMessages[index] );
}

CVSDialog::~CVSDialog()
{
    delete m_tempFile;
    delete p;
}

void CVSDialog::accept( )
{
  saveSettings( );
  KDialog::accept( );
}

void CVSDialog::setFiles( const QStringList& files )
{
  filebox->insertStringList( files );
}

void CVSDialog::setCommandLine( const QString& command )
{
  _commandLine = command;
}

void CVSDialog::setAddCommand( const QString& command )
{
  _addCommand = command;
}

void CVSDialog::slotExecuteCommand( )
{
  // Nothing to do here.
  if ( _commandLine.isEmpty( ) ) return;

  kDebug() << "Preparing K3Process";
  
  // Create a new shell process
  p = new K3Process;
  p->setUseShell( true, "/bin/sh" );

  if ( _cmd == CVS::Commit ) {
    // Include command for 'cvs add'.
    if ( autoAddBox->isChecked( ) && !_addCommand.isEmpty( ) )
      _commandLine.prepend( _addCommand );

    const QString msg( logedit->text() );

    if ( msg.isEmpty() )
    {
        // A good commit should never have an empty comment, so ask the user if he really wants it.
        const int res = KMessageBox::warningContinueCancel( this,
            i18n( "The commit log message is empty. Do you want to continue?" ) );
        if ( res != KMessageBox::Continue )
            return;
    }

    m_encoding = KGlobal::charsets()->encodingForName( m_encodingComboBox->currentText() );
    QTextCodec* codec = QTextCodec::codecForName( m_encoding.toUtf8() );

    if ( !codec )
    {
        KMessageBox::error( this, i18n( "Cannot find encoding: %1", m_encoding ) );
        return;
    }
    else if ( !codec->canEncode( msg ) )
    {
        const int res = KMessageBox::warningContinueCancel( this,
            i18n( "The commit log message cannot be encoded in the selected encoding: %1.\n"
            "Do you want to continue?", m_encoding ) );
        if ( res != KMessageBox::Continue )
            return;
    }
    
    // Write the commit log message from the input field to a temporary file
    m_tempFile = new KTemporaryFile;
    if ( !m_tempFile->open() )
    {
        kError() << "Could not create KTemporaryFile";
        delete m_tempFile;
        m_tempFile = 0;
        KMessageBox::error( this, i18n( "Cannot open temporary file for writing. Aborting.") );
        return;
    }
    QTextStream stream ( m_tempFile );
    stream.setCodec( codec );
    stream << msg;
    stream.flush();
    
    if ( m_tempFile->error() != QFile::NoError )
    {
        kError() << "Could not write to file " << m_tempFile->fileName();
        delete m_tempFile;
        m_tempFile = 0;
        KMessageBox::error( this, i18n( "Cannot write to temporary file. Aborting.") );
        return;
    }
    
    // Change the command line to have the real name of the temporary file
    _commandLine.replace( "@LOG@FILE@", KShell::quoteArg( m_tempFile->fileName() ) );

    // Update the list of log messages
    if ( !msg.isEmpty() ) {
      const QString shortLog = KStringHandler::csqueeze( msg, 80 );
      

      // Remove the message from the list if it already exists
      m_logMessages.removeAll( msg );
      // Prepend the current message to the list
      m_logMessages.prepend( msg );

      // At this time of the process, we do not need the combobox anymore, so we do not squeeze the changed strings.
    }
  }

  // Set the K3Process' command line.
  *p << _commandLine;

  connect( p, SIGNAL( receivedStdout( K3Process*, char*, int ) ),
    this, SLOT ( slotProcessStdout( K3Process*, char*, int ) ) );
  connect( p, SIGNAL( receivedStderr( K3Process*, char*, int ) ),
    this, SLOT ( slotProcessStderr( K3Process*, char*, int ) ) );
  connect( p, SIGNAL( processExited( K3Process* ) ),
    this, SLOT( slotProcessExited( K3Process* ) ) );

  output->append( i18n( "[ Starting command ]" ) );

  if ( p->start( K3Process::NotifyOnExit, K3Process::Communication( K3Process::AllOutput ) ) ) {
    // Disable the main button (and the log edit if in commit mode) to
    // indicate activity.
    mainBtn->setEnabled( false );
    if ( _cmd == CVS::Commit )
      logedit->setEnabled( false );
  } else
  {
      kError() << "Process could not be started.";
      KMessageBox::error( this, i18n( "The process could not be started." ) );
  }
}

void CVSDialog::slotProcessStdout( K3Process*, char * buffer, int len )
{
  output->append( QString::fromLocal8Bit( buffer, len ) );
  // Set the cursor's position at the end of the output.
  output->setCursorPosition( output->lines( ), 0 );

  // If the command is 'cvs status' or 'cvs diff' collect the output of stdout.
  if ( (_cmd == CVS::Status) || (_cmd == CVS::Diff) )
    _statusOutput += QString::fromLocal8Bit( buffer, len );
}

void CVSDialog::slotProcessStderr( K3Process*, char * buffer, int len )
{
  // If an error occurs while executing the command display stderr in
  // another color.
  QColor oldColor( output->color( ) );
  output->setColor( Qt::red );
  output->append( QString::fromLocal8Bit( buffer, len ) );
  output->setColor( oldColor );
  output->setCursorPosition( output->lines( ), 0 );
}

void CVSDialog::slotProcessExited( K3Process * p )
{
  if ( p->exitStatus( ) )
    output->append( i18n( "[ Exited with status %1 ]", p->exitStatus( ) ) );
  else
    output->append( i18n( "[ Finished ]" ) );

  // The command is finished. Now we can reconnect the main button.
  disconnect( mainBtn, 0, 0, 0 );
  if ( _cmd == CVS::Diff )
    mainBtn->setText( i18n( "&Show Diff" ) );
  else
    mainBtn->setText( i18n( "&Close" ) );
  connect( mainBtn, SIGNAL( clicked( ) ), this, SLOT( accept( ) ) );

  // Reenable the button and the log edit now that the process is finished.
  mainBtn->setEnabled( true );
  if ( _cmd == CVS::Commit )
    logedit->setEnabled( true );
}

QString CVSDialog::statusOutput( )
{
  return _statusOutput;
}

void CVSDialog::readSettings( )
{
  KSharedConfig * config = m_config;
  const KConfigGroup group = config->group( "CVSSupport" );

  if ( _cmd == CVS::Commit ) {
    autoAddBox->setChecked( group.readEntry( "AutoAddFiles", true ) );

    // Fill the combobox with old messages.
    m_logMessages.clear();
    m_squeezedLogMessages.clear();
    for ( int cnt = 0; cnt < 10; cnt++ )
      if ( group.hasKey( QString( "CommitLogMessage%1" ).arg( cnt ) ) )
    {
      const QString logMessage = group.readEntry( QString( "CommitLogMessage%1" ).arg( cnt ),QString() );
      if ( !logMessage.isEmpty() )
      {
          // If the message is too long, cut it to 80 characters (or the combo box becomes too wide)
          // ### FIXME: if the string matches the squeezed 80 chars, it might overwrite another entry
        const QString shortLog = KStringHandler::csqueeze( logMessage );
        m_logMessages.append( logMessage );
        m_squeezedLogMessages.append( shortLog );
        oldMessages->addItem( shortLog );
      }
    }

    m_encoding = group.readEntry( "CVSEncoding", "UTF-8" );
    m_encodingComboBox->addItem( i18nc( "Descriptive encoding name", "Last choice ( %1 )", m_encoding ), 0);
  }
}

void CVSDialog::saveSettings( )
{
  KConfigGroup config = m_config->group( "CVSSupport" );
  if ( _cmd == CVS::Commit ) {
    config.writeEntry( "AutoAddFiles", autoAddBox->isChecked( ) );

    // Write the log messages to the config file.
    int cnt = 0;
    QStringList::const_iterator it;
    for ( it = m_logMessages.constBegin( ); it != m_logMessages.constEnd( ) && cnt < 10 ; ++it, ++cnt )
      config.writeEntry( QString( "CommitLogMessage%1" ).arg( cnt ), *it );

    config.writeEntry( "CVSEncoding", m_encoding );
  }
  m_config->sync();
}

#include "cvsdialog.moc"
