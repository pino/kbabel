/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2001 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2001-2004 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "kbprojectmanager.h"
#include "catalogmanager.h"
#include "catalog.h"
#include "catalogmanagerapp.h"
#include "catalogmanagerapp.moc"
#include "catalogmanageradaptor.h"
#include "poinfo.h"

#include "version.h"

#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <kcursor.h>
#include <klocale.h>
#include <kiconloader.h>
#include <kmessagebox.h>
#include <kapplication.h>
#include <kwindowsystem.h>
#include <kxmlguiwindow.h>

#include <qfile.h>
#include <qfileinfo.h>
#include <qregexp.h>
#include <qtimer.h>
#include <QByteArray>

CatalogManager *CatalogManagerApp::_view = 0;

CatalogManagerApp::CatalogManagerApp()
    : KApplication()
{
    kbInterface = new CatalogManagerInterface;
    _view = 0;
    _preferredWindow = 0;
}

CatalogManagerApp::~CatalogManagerApp()
{
    delete kbInterface;
    KBabel::PoInfo::cacheWrite();
}

void CatalogManagerApp::setPreferredWindow(WId id)
{
    _preferredWindow = id;
    if( _view )
    {
	_view->raise();
#ifdef Q_OS_UNIX
	KWindowSystem::activateWindow(_view->winId());
#endif
    }
}

void CatalogManagerApp::updatedFile(QByteArray url)
{
    if( _view )
	_view->updateFile(url);
}

QByteArray CatalogManagerApp::findNextFile()
{
    QString reply = "";
    if( !CatalogManager::_foundFilesList.isEmpty() )
    {
	reply = CatalogManager::_foundFilesList.first();
	CatalogManager::_foundFilesList.pop_front();
	if( _view ) _view->decreaseNumberOfFound();
    } else
    {
	if( !CatalogManager::_toBeSearched.isEmpty() )
	    reply = QByteArray(""); // nothing found yet
	else
	    return QByteArray(); // not found definitely
    }

    return reply.toUtf8();
}

int CatalogManagerApp::newInstance()
{
    if( isSessionRestored() )
    {
	int n = 1;
	while (KMainWindow::canBeRestored(n)){
	    CatalogManager* cm = new CatalogManager();
	    cm->restore(n);
    	    n++;

	    // this view will be used as DCOP dispatcher
	    if( !_view )
		_view = cm;
	}
    }
    else
    {
	KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

	QString configfile = args->getOption("project");

	if( !configfile.isEmpty() )
	{
	    QFileInfo fi( configfile );
	    configfile = fi.absoluteFilePath();
	}
	else
	{
	    configfile = KBabel::ProjectManager::defaultProjectName();
	}

	_view=new CatalogManager(configfile);

	_view->setPreferredWindow( _preferredWindow );
	_view->show();
	_view->raise();
#ifdef Q_OS_UNIX
	KWindowSystem::activateWindow(_view->winId());
#endif
	args->clear();
    }

    return 0;
}

CatalogManagerInterface::CatalogManagerInterface()
{
      (void) new CatalogmanagerAdaptor( this );
      QDBusConnection dbus = QDBusConnection::sessionBus();
      dbus.registerObject( "/CatalogManager", this );
      dbus.connect(QString(), "/CatalogManager", "org.kde.kbabel.catalogmanager", "updateFiles", this, SLOT(slotUpdateFiles(QByteArray)));
}

void CatalogManagerInterface::setPreferredWindow( WId id )
{
    CatalogManagerApp::setPreferredWindow(id);
}

QByteArray CatalogManagerInterface::findNextFile()
{
    return CatalogManagerApp::findNextFile();
}

void CatalogManagerInterface::slotUpdateFiles(const QByteArray &url )
{
   updatedFile(url);
}

void CatalogManagerInterface::updatedFile( QByteArray url )
{
    CatalogManagerApp::updatedFile(url);
}


int main(int argc, char **argv)
{
    KLocale::setMainCatalog("kbabel");
    KAboutData about("catalogmanager", 0,ki18n("KBabel - Catalog Manager"),KBABEL_VERSION,
                     ki18n("An advanced catalog manager for KBabel"),KAboutData::License_GPL,
                     ki18n("(c) 1999,2000,2001,2002,2003,2004,2005,2006 The KBabel developers"),KLocalizedString(),"http://kbabel.kde.org");

    about.addAuthor(ki18n("Matthias Kiefer"),ki18n("Original author"),"kiefer@kde.org");
    about.addAuthor(ki18n("Stanislav Visnovsky"),ki18n("Current maintainer, porting to KDE3/Qt3.")
                    ,"visnovsky@kde.org");
    about.addAuthor(ki18n("Nicolas Goutte"), ki18n("Improved CVS support and added SVN support"), "goutte@kde.org");

    about.addCredit(ki18n("Claudiu Costin"),ki18n("Wrote documentation and sent "
                                               "many bug reports and suggestions for improvements.")
                    ,"claudiuc@kde.org");
    about.addCredit(ki18n("Thomas Diehl"),ki18n("Gave many suggestions for the GUI "
                                             "and the behavior of KBabel. He also contributed the beautiful splash screen.")
                    ,"thd@kde.org");
    about.addCredit(ki18n("Wolfram Diestel")
                    ,ki18n("Wrote diff algorithm, fixed KSpell and gave a lot "
                               "of useful hints."),"wolfram@steloj.de");
    about.addCredit(ki18n("Stephan Kulow"),ki18n("Helped keep KBabel up to date "
                                              "with the KDE API and gave a lot of other help."),"coolo@kde.org");
    about.addCredit(ki18n("Dwayne Bailey"),ki18n("Various validation plugins.")
	 ,"dwayne@translate.org.za");
	about.addCredit(ki18n("SuSE GmbH")
					,ki18n("Sponsored development of KBabel for a while.")
					,"suse@suse.de","http://www.suse.de");
    about.addCredit(ki18n("Bram Schoenmakers"),ki18n("Support for making diffs and some minor "
                                                  "improvements."),"bramschoenmakers@kde.nl");

    about.addCredit(ki18n("Trolltech"), ki18n("KBabel contains code from Qt"), 0, "http://www.trolltech.com");

    about.addCredit(ki18n("GNU gettext"), ki18n("KBabel contains code from GNU gettext"), 0, "http://www.gnu.org/software/gettext/");

    // Initialize command line args
    KCmdLineArgs::init(argc, argv, &about);

    // Tell which options are supported

    KCmdLineOptions options;
    options.add("project <configfile>", ki18n("File to load configuration from"));
    KCmdLineArgs::addCmdLineOptions( options );

    // Add options from other components
    KCmdLineArgs::addStdCmdLineOptions();

    CatalogManagerApp app;

    app.newInstance();

    return app.exec();
}
