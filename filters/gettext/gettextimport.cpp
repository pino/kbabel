// kate: space-indent on; indent-width 3; replace-tabs on;

/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2000 by Matthias Kiefer <matthias.kiefer@gmx.de>
		2001-2003 by Stanislav Visnovsky <visnovsky@kde.org>
  Copyright (C) 2006 by Nicolas GOUTTE <nicolasg@snafu.de>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "gettextimport.h"

#include <catalogitem.h>
#include <resources.h>

#include <qfile.h>
#include <qfileinfo.h>
#include <qregexp.h>
#include <qtextcodec.h>
#include <QList>
#include <QTextStream>
#include <QEventLoop>
#include <QApplication>

#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>

K_PLUGIN_FACTORY( GettextImportPluginFactory, registerPlugin< GettextImportPlugin >(); )
K_EXPORT_PLUGIN( GettextImportPluginFactory( "kbabelgettextimportfilter" ) )

using namespace KBabel;

GettextImportPlugin::GettextImportPlugin(QObject* parent, const QVariantList &) : CatalogImportPlugin(parent)
{
    setObjectName("GettextImportPlugin");
}

ConversionStatus GettextImportPlugin::load(const QString& filename, const QString&)
{
   kDebug( KBABEL );
   
   if ( filename.isEmpty() ) {
      kDebug(KBABEL) << "fatal error: empty filename to open";
      return NO_FILE;
   }

   QFileInfo info(filename);

   if(!info.exists() || info.isDir())
      return NO_FILE;

   if(!info.isReadable())
      return NO_PERMISSIONS;

   QFile file(filename);

   if ( !file.open( QIODevice::ReadOnly ) )
      return NO_PERMISSIONS;
   
   uint oldPercent = 0;
   emit signalResetProgressBar(i18n("loading file"),100);

   QByteArray ba = file.readAll();
   file.close();

   // find codec for file
   bool hadCodec;
   QTextCodec* codec=codecForArray( ba, &hadCodec );
   
   bool recoveredErrorInHeader = false;

   QTextStream stream(ba,QIODevice::ReadOnly);

   if(codec)
      stream.setCodec(codec);
   else
   {
      kWarning() << "No encoding declared or found, using UTF-8";
      stream.setCodec( "UTF-8" );;
#ifdef __GNUC__
# warning Default UTF-8 encoding needs to be improved
#endif
      // Templates define CHARSET, so if e make it a recoverable error, the template is not loadable anymore, as for templates recoverable errors are disqualifying.
      //recoveredErrorInHeader = true;
   }
   
   QIODevice *dev = stream.device();
   int fileSize = dev->size();

   // if somethings goes wrong with the parsing, we don't have deleted the old contents
   CatalogItem tempHeader;
   QStringList tempObsolete;


   kDebug(KBABEL) << "start parsing...";

   // first read header
   const ConversionStatus status = readHeader(stream);


   if ( status == RECOVERED_PARSE_ERROR )
   {
      kDebug( KBABEL ) << "Recovered error in header entry";
      recoveredErrorInHeader = true;
   }
   else if ( status != OK )
   {
      emit signalClearProgressBar();

      kDebug( KBABEL ) << "Parse error in header entry";
      return status;
   }

   kDebug() << "HEADER MSGID: " << _msgid;
   kDebug() << "HEADER MSGSTR: " << _msgstr;
   if ( !_msgid.isEmpty() && !_msgid.first().isEmpty() )
   {
      // The header must have an empty msgid
      kWarning(KBABEL) << "Header entry has non-empty msgid. Creating a temporary header! " << _msgid;
      tempHeader.setMsgid( QStringList() );
      QStringList tmp;
      tmp.append(
         "Content-Type: text/plain; charset=UTF-8\\n" // Unknown charset
         "Content-Transfer-Encoding: 8bit\\n"
         "Mime-Version: 1.0" );
      tempHeader.setMsgstr( tmp );
      // We keep the comment of the first entry, as it might really be a header comment (at least partially)
      const QString comment( "# Header entry was created by KBabel!\n#\n" + _comment );
      tempHeader.setComment( comment );
      recoveredErrorInHeader = true;
   }
   else
   {
      tempHeader.setMsgid( _msgid );
      tempHeader.setMsgstr( _msgstr );
      tempHeader.setComment( _comment );
   }
   if(tempHeader.isFuzzy())
   {
      tempHeader.removeFuzzy();
   }

   // check if header seems to indicate docbook content generated by xml2pot
   const bool docbookContent = tempHeader.msgstr().contains( "application/x-xml2pot" );

   // now parse the rest of the file
   uint counter=0;
   QList<uint> errorIndex;
   bool recoveredError=false;
   bool docbookFile=false;
   	
   while( !stream.atEnd() )
   {
      qApp->processEvents(QEventLoop::AllEvents, 10);
      if( isStopped() )
      {
         return STOPPED;
      }

      const ConversionStatus success=readEntry(stream);

      if(success==OK)
      {
         if( _obsolete )
         {
               tempObsolete.append(_comment);
         }
         else
         {
               CatalogItem tempCatItem;
               tempCatItem.setMsgctxt( _msgctxt );
               tempCatItem.setMsgid( _msgid );
               tempCatItem.setMsgstr( _msgstr );
               tempCatItem.setComment( _comment );
               tempCatItem.setGettextPluralForm( _gettextPluralForm );
               
               // add new entry to the list of entries
               appendCatalogItem(tempCatItem);
               // check if first comment seems to indicate a docbook source file
               if(counter==0)
                  docbookFile = ( tempCatItem.comment().indexOf( ".docbook" ) != -1 );
         }
      }
      else if(success==RECOVERED_PARSE_ERROR)
      {
         kDebug( KBABEL ) << "Recovered parse error in entry: " << counter;
         recoveredError=true;
         errorIndex.append(counter);
         
         CatalogItem tempCatItem;
         tempCatItem.setMsgctxt( _msgctxt );
         tempCatItem.setMsgid( _msgid );
         tempCatItem.setMsgstr( _msgstr );
         tempCatItem.setComment( _comment );
         tempCatItem.setGettextPluralForm( _gettextPluralForm );

         // add new entry to the list of entries
         appendCatalogItem(tempCatItem);
      }
      else if ( success == PARSE_ERROR )
      {
         kDebug( KBABEL ) << "Parse error in entry: " << counter;
         return PARSE_ERROR;
      }
      else
      {
         kWarning( KBABEL ) << "Unknown success status, assumig parse error " << success;
         return PARSE_ERROR;
      }
      counter++;

      const uint newPercent = ( 100 * dev->pos() ) / fileSize;
      if(newPercent > oldPercent)
      {
         oldPercent = newPercent;
         emit signalProgress(oldPercent);
      }
   }


   // to be sure it is set to 100, if someone don't connect to
   // signalClearProgressBar()
   emit signalProgress(100);

   emit signalClearProgressBar();


   // ### TODO: can we check that there is no useful entry?
   if ( !counter )
   {
      // Empty file? (Otherwise, there would be a try of getting an entry and the count would be 1 !)
      kDebug( KBABEL ) << " Empty file?";
      return PARSE_ERROR;
   }

   kDebug(KBABEL) << " ready";

   // We have successfully loaded the file (perhaps with recovered errors)
   
   setGeneratedFromDocbook(docbookContent || docbookFile);

   setHeader(tempHeader);
   setCatalogExtraData(tempObsolete);
   setErrorIndex(errorIndex);

   if(hadCodec)
      setFileCodec(codec);
   else
      setFileCodec(0);
   
   setMimeTypes( "text/x-gettext-translation" );

   if ( recoveredErrorInHeader )
   {
      kDebug( KBABEL ) << " Returning: header error";
      return RECOVERED_HEADER_ERROR;
   }
   else if ( recoveredError )
   {
      kDebug( KBABEL ) << " Returning: recovered parse error";
      return RECOVERED_PARSE_ERROR;
   }
   else
   {
      kDebug( KBABEL ) << " Returning: OK! :-)";
      return OK;
   }
}

QTextCodec* GettextImportPlugin::codecForArray(QByteArray& array, bool* hadCodec)
{
   if(hadCodec)
   {
      *hadCodec=false;
   }

   QTextStream stream( array, QIODevice::ReadOnly );
   // ### TODO Qt4: see if it can be done with QByteArray alone, in an encoding-neutral way.
   // Set ISO-8859-1 as it is a relatively neutral encoding when reading (compared to UTF-8  or a random locale encoing)
   stream.setCodec( "ISO-8859-1" );

   // first read header
   ConversionStatus status = readHeader(stream);
   if(status!=OK && status != RECOVERED_PARSE_ERROR)
   {
       kDebug(KBABEL) << "wasn't able to read header";
       return 0;
   }

   const QString head = _msgstr.first();

   QRegExp regexp("Content-Type:\\s*\\w+/[-\\w]+;?\\s*charset\\s*=\\s*(\\S+)\\s*\\\\n");
   if( regexp.indexIn( head ) == -1 )
   {
      kDebug(KBABEL) << "no charset entry found";
      return 0;
   }
   
   const QString charset = regexp.cap(1);
   kDebug(KBABEL) << "charset: " << charset;

   QTextCodec* codec=0;

   if(!charset.isEmpty())
   {
      // "CHARSET" is the default charset entry in a template (pot).
      // characters in a template should be either pure ascii or 
      // at least utf8, so utf8-codec can be used for both.
      if( charset == "CHARSET")
      {
          if(hadCodec)
             *hadCodec=false;

          codec=QTextCodec::codecForName("utf8");
          kDebug(KBABEL) << "file seems to be a template: using utf-8 encoding.";
      }
      else
      {
         codec = QTextCodec::codecForName( charset.toLatin1() );
         if(hadCodec)
            *hadCodec=true;
      }

      if(!codec)
      {
         kWarning(KBABEL) << "charset found, but no codec available, using UTF-8 instead";
      }
   }
   else
   {
      // No charset? So it is probably ASCII, therefore UTF-8
      kWarning(KBABEL) << "No charset defined! Assuming UTF-8!";
   }
         

   return codec;
}

ConversionStatus GettextImportPlugin::readHeader(QTextStream& stream)
{
   CatalogItem temp;
   int filePos = stream.device()->pos();
   ConversionStatus status=readEntry(stream);

   if(status==OK || status==RECOVERED_PARSE_ERROR)
   {
      // test if this is the header
      if(!_msgid.first().isEmpty())
      {
         stream.device()->seek( filePos );
      }

      return status;
   }

   return PARSE_ERROR;
}

ConversionStatus GettextImportPlugin::readEntry(QTextStream& stream)
{
   //kDebug( KBABEL ) << " START";
   enum {Begin,Comment,Msgctxt,Msgid,Msgstr} part=Begin;

   QString line;
   bool error=false;
   bool recoverableError=false;
   bool seenMsgctxt=false;
   _msgstr.clear();
   _msgstr.append(QString());
   _msgid.clear();
   _msgid.append(QString());
   _msgctxt=QString();
   _comment=QString();
   _gettextPluralForm=false;
   _obsolete=false;
   
   QStringList::Iterator msgstrIt=_msgstr.begin();
   
   while( !stream.atEnd() )
   {
       line=stream.readLine();

       //kDebug() << "Parsing line: " << line;

       // ### Qt4: no need of a such a check
       if(line.isNull()) // file end
          break;
       else if ( line.startsWith( "<<<<<<<" ) || line.startsWith( "=======" ) || line.startsWith( ">>>>>>>" ) )
       {
          // We have found a CVS/SVN conflict marker. Abort.
          // (It cannot be any useful data of the PO file, as otherwise the line would start with at least a quote)
          kError(KBABEL) << "CVS/SVN conflict marker found! Aborting!" << endl << line;
          return PARSE_ERROR;
       }

       // remove whitespaces from beginning and end of line
       line = line.trimmed();

       if(part==Begin)
       {
           // ignore trailing newlines
           if(line.isEmpty())
              continue;

           if(line.startsWith("#~"))
           {
              _obsolete=true;
	      part=Comment;
	      _comment=line;
           }
           else if(line.startsWith('#'))
           {
               part=Comment;
               _comment=line;
           }
           else if( line.indexOf( QRegExp( "^msgctxt\\s*\".*\"$" ) ) != -1 )
           {
               part=Msgctxt;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgctxt\\s*\""));
               line.remove(QRegExp("\"$"));
               _msgctxt=line;
               seenMsgctxt=true;
           }
           else if( line.indexOf( QRegExp( "^msgid\\s*\".*\"$" ) ) != -1 )
           {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\""));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;
           }
		     // one of the quotation marks is missing
           else if( line.indexOf( QRegExp( "^msgid\\s*\"?.*\"?$" ) ) != -1 )
           {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;

               if(!line.isEmpty())
                  recoverableError=true;
           }
           else
           {
               kDebug(KBABEL) << "no comment, msgctxt or msgid found after a comment: " << line;
               error=true;
               break;
           }
       }
       else if(part==Comment)
       {
            if(line.isEmpty() && _obsolete ) return OK;
	    if(line.isEmpty() )
	       continue;
            else if(line.startsWith("#~"))
            {
               _comment+=('\n'+line);
	       _obsolete=true;
            }
            else if(line.startsWith('#'))
            {
               _comment+=('\n'+line);
            }
            else if( line.indexOf( QRegExp( "^msgctxt\\s*\".*\"$" ) ) != -1 )
            {
               part=Msgctxt;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgctxt\\s*\""));
               line.remove(QRegExp("\"$"));
               _msgctxt=line;
               seenMsgctxt=true;
            }
            else if( line.indexOf( QRegExp( "^msgid\\s*\".*\"$" ) ) != -1 )
            {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\""));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;
            }
            // one of the quotation marks is missing
            else if( line.indexOf( QRegExp( "^msgid\\s*\"?.*\"?$" ) ) != -1 )
            {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;
			   
               if(!line.isEmpty())
                     recoverableError=true;
            }
            else
            {
               kDebug(KBABEL) << "no comment or msgid found after a comment while parsing: " << _comment;
               error=true;
               break;
            }
        }
        else if(part==Msgctxt)
        {
            if(line.isEmpty())
               continue;
            else if( line.indexOf( QRegExp( "^\".*\\n?\"$" ) ) != -1 )
            {
               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^\""));
               line.remove(QRegExp("\"$"));
               
               // add Msgctxt line to item
               if(_msgctxt.isEmpty())
                  _msgctxt=line;
               else
                  _msgctxt+=('\n'+line);
            }
            else if( line.indexOf( QRegExp( "^msgid\\s*\".*\"$" ) ) != -1 )
            {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\""));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;
            }
            // one of the quotation marks is missing
            else if( line.indexOf( QRegExp( "^msgid\\s*\"?.*\"?$" ) ) != -1 )
            {
               part=Msgid;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*(_msgid).begin())=line;
			   
               if(!line.isEmpty())
                     recoverableError=true;
            }
            else
            {
               kDebug(KBABEL) << "no msgid found after a msgctxt while parsing: " << _msgctxt;
               error=true;
               break;
            }
        }
        else if(part==Msgid)
        {
            if(line.isEmpty())
               continue;
            else if( line.indexOf( QRegExp( "^\".*\\n?\"$" ) ) != -1 )
            {
               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^\""));
               line.remove(QRegExp("\"$"));

               QStringList::Iterator it;
               if(_gettextPluralForm) {
                   it = _msgid.end();
                   --it; // end() returns the imaginary item after the last one, in other words, it doesn't exist, so get the real last one.
               }
               else
                   it = _msgid.begin();
               
               // add Msgid line to item
               if((*it).isEmpty())
                  (*it)=line;
               else
                  (*it)+=('\n'+line);
            }
            else if( line.indexOf( QRegExp( "^msgid_plural\\s*\".*\"$" ) ) != -1 )
            {
               part=Msgid;
               _gettextPluralForm = true;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid_plural\\s*\""));
               line.remove(QRegExp("\"$"));

               _msgid.append(line);
            }
            // one of the quotation marks is missing
            else if( line.indexOf( QRegExp( "^msgid_plural\\s*\"?.*\"?$" ) ) != -1 )
            {
               part=Msgid;
               _gettextPluralForm = true;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgid_plural\\s*\"?"));
               line.remove(QRegExp("\"$"));

               _msgid.append(line);
			   
               if(!line.isEmpty())
                  recoverableError=true;
            }
           else if( !_gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\s*\".*\\n?\"$" ) ) != -1 ) )
            {
               part=Msgstr;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*msgstrIt)=line;
            }
            else if( !_gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\s*\"?.*\\n?\"?$" ) ) != -1 ) )
            {
               part=Msgstr;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*msgstrIt)=line;

               if(!line.isEmpty())
                  recoverableError=true;
            }
            else if( _gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\[0\\]\\s*\".*\\n?\"$" ) ) != -1 ) )
            {
               part=Msgstr;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\[0\\]\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*msgstrIt)=line;
            }
            else if( _gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\[0\\]\\s*\"?.*\\n?\"?$" ) ) != -1 ) )
            {
               part=Msgstr;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\[0\\]\\s*\"?"));
               line.remove(QRegExp("\"$"));

               (*msgstrIt)=line;

               if(!line.isEmpty())
                  recoverableError=true;
            }
            else if ( line.startsWith( "#" ) )
            {
               // ### TODO: could this be considered recoverable?
               kDebug(KBABEL) << "comment found after a msgid while parsing: " << _msgid.first();
               error=true;
               break;
            }
            else if ( line.startsWith( "msgid" ) )
            {
               kDebug(KBABEL) << "Another msgid found after a msgid while parsing: " << _msgid.first();
               error=true;
               break;
            }
            // a line of the msgid with a missing quotation mark
            else if( line.indexOf( QRegExp( "^\"?.+\\n?\"?$" ) ) != -1 )
            {
               recoverableError=true;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^\""));
               line.remove(QRegExp("\"$"));

               QStringList::Iterator it;
               if( _gettextPluralForm ) {
                   it = _msgid.end();
                   --it;
               }
               else
                   it = _msgid.begin();
               
               // add Msgid line to item
               if((*it).isEmpty())
                  (*it)=line;
               else
                  (*it)+=('\n'+line);
            }
            else
            {
               kDebug(KBABEL) << "no msgstr found after a msgid while parsing: " << _msgid.first();
               error=true;
               break;
            }
        }
        else if(part==Msgstr)
        {
            if(line.isEmpty())
               break;
            // another line of the msgstr
            else if( line.indexOf( QRegExp( "^\".*\\n?\"$" ) ) != -1 )
            {
               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^\""));
               line.remove(QRegExp("\"$"));

               if((*msgstrIt).isEmpty())
                  (*msgstrIt)=line;
               else
                  (*msgstrIt)+=('\n'+line);
            }
            else if( _gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\[[0-9]+\\]\\s*\".*\\n?\"$" ) ) != -1 ) )
            {
               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\[[0-9]+\\]\\s*\"?"));
               line.remove(QRegExp("\"$"));

               _msgstr.append(line);
               msgstrIt= _msgstr.end();
               --msgstrIt;
            }
	    else if( _gettextPluralForm && ( line.indexOf( QRegExp( "^msgstr\\[[0-9]\\]\\s*\"?.*\\n?\"?$" ) ) != -1 ) )
            {
               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^msgstr\\[[0-9]\\]\\s*\"?"));
               line.remove(QRegExp("\"$"));

               _msgstr.append(line);
               msgstrIt= _msgstr.end();
               --msgstrIt;

               if(!line.isEmpty())
                  recoverableError=true;
            }/*Avoid non-seq access, couse it does not seem to work...
            else if( ( line.indexOf( QRegExp( "^\\s*msgid" ) ) != -1 ) || ( line.indexOf( QRegExp( "^\\s*#" ) ) != -1 ) )
            {
               // We have read successfully one entry, so end loop.
               //stream.device()->seek(pos);// reset position in stream to beginning of this line
               if (stream.device()->seek(pos))
                  kDebug(KBABEL) << "_______OK______ ";// reset position in stream to beginning of this line
               else
                   kDebug(KBABEL) << "_______SHIT______ ";// reset
               
               kDebug(KBABEL) << "_______IT______ " << stream.readLine();// reset
               stream.device()->seek(pos);
               
               break;
            }*/
            else if(line.startsWith("msgstr"))
            {
               kDebug(KBABEL) << "Another msgstr found after a msgstr while parsing: " << _msgstr.last();
               error=true;
               break;
            }
            // another line of the msgstr with a missing quotation mark
            else if( line.indexOf( QRegExp( "^\"?.+\\n?\"?$" ) ) != -1 )
            {
               recoverableError=true;

               // remove quotes at beginning and the end of the lines
               line.remove(QRegExp("^\""));
               line.remove(QRegExp("\"$"));

               if((*msgstrIt).isEmpty())
                  (*msgstrIt)=line;
               else
                  (*msgstrIt)+=('\n'+line);
            }
            else
            {
               kDebug(KBABEL) << "no msgid or comment found after a msgstr while parsing: " << _msgstr.last();
               error=true;
               break;
            }
        }
    }

/*
   if(_gettextPluralForm)
   {
       kDebug(KBABEL) << "gettext plural form:\n"
                 << "msgid:\n" << _msgid.first() << "\n"
                 << "msgid_plural:\n" << _msgid.last() << "\n";
       int counter=0;
       for(QStringList::Iterator it = _msgstr.begin(); it != _msgstr.end(); ++it)
       {
           kDebug(KBABEL) << "msgstr[" << counter << "]:\n" 
                     << (*it);
           counter++;
       }
   }
  */

    //kDebug( KBABEL ) << " NEAR RETURN";
    if(error)
       return PARSE_ERROR;
	else if(recoverableError)
		return RECOVERED_PARSE_ERROR;
    else
    {
      return OK;
    }
}
