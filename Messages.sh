#! /bin/sh
$EXTRACTRC `find . -name \*.rc -o -name \*.ui` >> rc.cpp || exit 11
$XGETTEXT `find . -name \*.cpp -o -name \*.cc` -o $podir/kbabel.pot
$XGETTEXT -L Python -j `find . -name \*.py` -o $podir/kbabel.pot
rm -f rc.cpp
