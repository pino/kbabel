/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include "xliffresourceitemdelegate.h"
#include "qabstracttextdocumentlayout.h"
#include "xliffresource.h"
#include "xlifftransunit.h"
#include "xliffsource.h"
#include "xlifftarget.h"
#include "xlifftextedit.h"
#include "kdebug.h"
#include <QtGui>

XliffResourceItemDelegate::XliffResourceItemDelegate(QObject *parent)
 : QItemDelegate(parent)
{
}


XliffResourceItemDelegate::~XliffResourceItemDelegate()
{
}


QWidget *XliffResourceItemDelegate::createEditor(QWidget *parent,
    const QStyleOptionViewItem &/* option */,
    const QModelIndex &/* index */) const
{
    XliffTextEdit *editor = new XliffTextEdit(parent);
    editor->installEventFilter(const_cast<XliffResourceItemDelegate*>(this));

    return editor;
}

void XliffResourceItemDelegate::setEditorData(QWidget *editor,
                                    const QModelIndex &index) const
{
    XliffTransUnit *tuItem = static_cast<XliffTransUnit*>(index.internalPointer());

    XliffTextEdit *xEdit = static_cast<XliffTextEdit*>(editor);
    xEdit->setDocument(tuItem->target()->content()->clone());
}

void XliffResourceItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                   const QModelIndex &index) const
{
    XliffTransUnit *tuItem = static_cast<XliffTransUnit*>(index.internalPointer());

    XliffTextEdit *xEdit = static_cast<XliffTextEdit*>(editor);
    tuItem->target()->setContent(xEdit->document());
}

void XliffResourceItemDelegate::updateEditorGeometry(QWidget *editor,
    const QStyleOptionViewItem &option, const QModelIndex &/* index */) const
{
    QRect rect = option.rect;
    rect.setHeight(rect.height());
    editor->setGeometry(rect);
}


void XliffResourceItemDelegate::paintTransUnit( QPainter * painter, 
    const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    XliffTransUnit *tuItem = static_cast<XliffTransUnit*>(index.internalPointer());
    
    if(index.column() == 3 || index.column() == 4){ // ignore painting of anything but source/target
    
        Q_ASSERT(index.isValid());
        const QAbstractItemModel *model = index.model();
        Q_ASSERT(model);
    
        QVariant value;
        // draw the background color
        if (option.showDecorationSelected && (option.state & QStyle::State_Selected)) {
            QPalette::ColorGroup cg = option.state & QStyle::State_Enabled
                                    ? QPalette::Normal : QPalette::Disabled;
            painter->fillRect(option.rect, option.palette.brush(cg, QPalette::Highlight));
        } else {
            value = model->data(index, Qt::BackgroundColorRole);
            if (value.isValid() && qvariant_cast<QColor>(value).isValid())
                painter->fillRect(option.rect, qvariant_cast<QColor>(value));
        }
        
        QTextDocument *doc;
        if(index.column() == 3)
            if(tuItem->source() != NULL)
                doc = tuItem->source()->content()->clone();
            else
                doc = new QTextDocument;            
        else
            if(tuItem->target() != NULL)
                doc = tuItem->target()->content()->clone();
            else
                doc = new QTextDocument;            
        
        doc->setPageSize(QSizeF(option.rect.size()));
        
        QAbstractTextDocumentLayout::PaintContext context;
        
        painter->translate(option.rect.left(),option.rect.top());
        doc->documentLayout()->draw(painter, context);
        painter->translate(-option.rect.left(),-option.rect.top());
    }
    else
        return QItemDelegate::paint(painter,option,index);
}

void XliffResourceItemDelegate::paint ( QPainter * painter, 
    const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
    Q_ASSERT(index.isValid());

    XliffResource *resItem = static_cast<XliffResource*>(index.internalPointer());
    
    XliffTransUnit *tuItem = qobject_cast<XliffTransUnit*>(resItem);
    if(tuItem)
        return paintTransUnit(painter,option,index);
    else
        return QItemDelegate::paint(painter,option,index);
}

QSize XliffResourceItemDelegate::sizeHint ( const QStyleOptionViewItem & option, 
    const QModelIndex & index ) const
{
    Q_ASSERT(index.isValid());
    const QAbstractItemModel *model = index.model();
    Q_ASSERT(model);
    
    XliffResource *resItem = static_cast<XliffResource*>(index.internalPointer());
    XliffTransUnit *tuItem = qobject_cast<XliffTransUnit*>(resItem);
    QSize sHint = QItemDelegate::sizeHint(option, index);
    if(!tuItem)
        return sHint;
        
    // TODO: Figure out how to calculate the size of the text...
    return QSize(sHint.width(), 50);
/*
    QTextDocument *doc = tuItem->source()->content();
    
    kDebug() << "calculated size: "<< sHint.width() << " x "<< sHint.height() << endl;
    
    QSizeF oldPageSize = doc->pageSize();
    kDebug() << "old page size:   "<< oldPageSize.width() << " x "<< oldPageSize.height() << endl;
    
    doc->setPageSize(QSizeF(sHint));
    
    QSizeF newPageSize = doc->pageSize();
    kDebug() << "new page size : "<< newPageSize.width() << " x "<< newPageSize.height() << endl;
    
    QSizeF ps = doc->documentLayout()->documentSize();
    kDebug() << "doc size : "<< ps.width() << " x "<< ps.height() << endl;
    
    QSizeF final = ps.expandedTo(sHint);
    kDebug() << "new size : "<< final.width() << " x "<< final.height() << endl;
  */  

}

