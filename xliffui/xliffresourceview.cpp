/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "xliffresourceview.h"
#include "kdebug.h"
#include "xliffresourceitemdelegate.h"
#include "xliffresource.h"
#include "xlifftransunit.h"
#include "xliffresourcemodel.h"

#include <QHeaderView>
#include <QResizeEvent>

XliffResourceView::XliffResourceView(QWidget *parent)
    :QTreeView(parent)
{
    setAlternatingRowColors(true);
    
    XliffResourceItemDelegate *delegate = new XliffResourceItemDelegate;
    setItemDelegate(delegate);
    
    header()->setStretchLastSection(true);
    header()->setResizeMode(QHeaderView::Interactive);
    connect(this, SIGNAL(clicked(QModelIndex)),
        this, SLOT(toggleState(QModelIndex)));
}


XliffResourceView::~XliffResourceView()
{
}

void XliffResourceView::toggleState ( const QModelIndex & index)
{
    if(!index.isValid())
        return;
        
    if(index.column() != 2)
        return;
        
    XliffResource *item = static_cast<XliffResource*>(index.internalPointer());
    XliffTransUnit *tuItem = qobject_cast<XliffTransUnit*>(item);
    if(tuItem){
        if(!tuItem->translate()) // item is locked
            return;
        
        tuItem->setApproved(!tuItem->approved());
        
        XliffResourceModel *mod = qobject_cast<XliffResourceModel*>(model());
        if(!mod)
            kDebug() << "Error casting model" << endl;
        
        mod->updateItem(index);
    }
}

void XliffResourceView::resizeEvent ( QResizeEvent * event )
{
    kDebug() << "Resizing ok"<< endl;
    int size3 = header()->sectionSize(3);
    int size4 = header()->sectionSize(4);
    
    int size = (size3+size4)/2;
    header()->resizeSection(3,size);
    header()->resizeSection(4,size);
    QTreeView::resizeEvent(event);
}

void XliffResourceView::setItemsExpandedRecursive(const QModelIndex & parent, bool expanded)
{
    setExpanded(parent,expanded);
    
    int children = model()->rowCount(parent);
    
    for(int i=0;i<children;i++){
        QModelIndex childIndex = model()->index(i,0,parent);//parent.child(i,parent.column());
        kDebug() << " data for " << i << ": " << model()->data(childIndex, Qt::DisplayRole) << endl;
        if(childIndex.isValid())
        setItemsExpandedRecursive(childIndex,expanded);
    }
    
}

