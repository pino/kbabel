/* This file is part of the KDE project
 * Copyright (C) 2007 Montel Laurent <montel@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 */

#define STRIGI_IMPORT_API
#include <strigi/streamthroughanalyzer.h>
#include <strigi/analyzerplugin.h>
#include <strigi/fieldtypes.h>
#include <strigi/analysisresult.h>

//kbabel include
#include "poinfo.h"
#include "catalogfileplugin.h"

//kde include
#include <KAboutData>
#include <KComponentData>
#include <KLocale>
#include <KUrl>
//Qt include
#include <QStringList>

using namespace std;
using namespace Strigi;
using namespace KBabel;

class PoThroughAnalyzerFactory;
class PoThroughAnalyzer : public StreamThroughAnalyzer {
    private:
        const PoThroughAnalyzerFactory* factory;
        AnalysisResult* idx;

        void setIndexable( AnalysisResult *i ) {
            idx = i;
        }
        InputStream* connectInputStream( InputStream *in );
        bool isReadyWithStream() { return true; }
        const char* name() const {
           return "PoThroughAnalyzer";
        }

    public:
        PoThroughAnalyzer( const PoThroughAnalyzerFactory* f );
};

class PoThroughAnalyzerFactory : public StreamThroughAnalyzerFactory {
private:
    const char* name() const {
        return "PoThroughAnalyzer";
    }
    StreamThroughAnalyzer* newInstance() const {
        return new PoThroughAnalyzer(this);
    }
    void registerFields( FieldRegister& );

    static const std::string totalFieldName;
    static const std::string fuzzyFieldName;
    static const std::string untranslatedFieldName;
    static const std::string lasttranslatorFieldName;
    static const std::string languageteamFieldName;
    static const std::string revisionFieldName;
public:
    const RegisteredField* totalField;
    const RegisteredField* fuzzyField;
    const RegisteredField* untranslatedField;
    const RegisteredField* lasttranslatorField;
    const RegisteredField* languageteamField;
    const RegisteredField* revisionField;
};

const std::string PoThroughAnalyzerFactory::totalFieldName( "total" );
const std::string PoThroughAnalyzerFactory::fuzzyFieldName( "fuzzy" );
const std::string PoThroughAnalyzerFactory::untranslatedFieldName( "untranslated" );
const std::string PoThroughAnalyzerFactory::lasttranslatorFieldName( "last translator" );
const std::string PoThroughAnalyzerFactory::languageteamFieldName( "language team" );
const std::string PoThroughAnalyzerFactory::revisionFieldName( "revision" );

void PoThroughAnalyzerFactory::registerFields( FieldRegister& reg ) {
	totalField = reg.registerField( totalFieldName, FieldRegister::integerType, 1, 0 );
	fuzzyField = reg.registerField( fuzzyFieldName, FieldRegister::integerType, 1, 0 );
	untranslatedField = reg.registerField(untranslatedFieldName, FieldRegister::integerType, 1, 0 );
	lasttranslatorField = reg.registerField(lasttranslatorFieldName, FieldRegister::stringType, 1, 0 );
        languageteamField = reg.registerField(languageteamFieldName, FieldRegister::stringType, 1, 0 );
	revisionField = reg.registerField(revisionFieldName, FieldRegister::stringType, 1, 0 );
}

PoThroughAnalyzer::PoThroughAnalyzer( const PoThroughAnalyzerFactory* f ) : factory( f )
{
    KAboutData about("strigipothroughanalyzer","",ki18n("StrigiPoThroughAnalyzer"), "0.01pre");
    KComponentData kcdata(&about);
}

InputStream* PoThroughAnalyzer::connectInputStream( InputStream* in ) {
    if( !in )
        return in;
    const string& path = idx->path();
    PoInfo poInfo;
    QStringList wordList;
    KUrl url(path.c_str());
    ConversionStatus status = PoInfo::info(url.path(), poInfo, wordList, false, false, false);
    if (status == OK) {
	idx->addValue( factory->totalField,poInfo.total);
	idx->addValue( factory->fuzzyField,poInfo.fuzzy);
	idx->addValue( factory->untranslatedField,poInfo.untranslated);
	idx->addValue( factory->lasttranslatorField,(const char*)poInfo.lastTranslator.toUtf8());
	idx->addValue( factory->languageteamField, (const char*)poInfo.languageTeam.toUtf8());
	idx->addValue( factory->revisionField,(const char*)poInfo.revision.toUtf8());
    }
    return in;
}

class Factory : public AnalyzerFactoryFactory {
public:
    std::list<StreamThroughAnalyzerFactory*>
    streamThroughAnalyzerFactories() const {
        std::list<StreamThroughAnalyzerFactory*> af;
        af.push_back(new PoThroughAnalyzerFactory());
        return af;
    }
};

STRIGI_ANALYZER_FACTORY(Factory) 

