/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include <QtGui>

#include "mdichild.h"

#include "xliffdocument.h"
#include "xlifftransunit.h"
#include "xliffresource.h"
#include "xliffsource.h"
#include "xlifftarget.h"


#include "kdebug.h"

#include "xliffresourceview.h"
#include "xliffresourcemodel.h"
#include "xlifftextedit.h"

MdiChild::MdiChild()
{
    setAttribute(Qt::WA_DeleteOnClose);
    m_resView = new XliffResourceView(this);
    
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(0);
    
    QSplitter *splitter = new QSplitter;
    splitter->setOrientation(Qt::Vertical);
    splitter->addWidget(m_resView);
    
    QSplitter *splitter2 = new QSplitter;
    splitter->addWidget(splitter2);
    
    m_contextView = new QTextEdit;
    m_contextView->setReadOnly(true);
    m_contextView->setHtml("<b>testing:</b> All good!");
    
    m_sourceEditor = new XliffTextEdit;
    m_sourceEditor->setReadOnly(true);
    m_targetEditor = new XliffTextEdit;
    
    QVBoxLayout *layout2 = new QVBoxLayout;
    layout2->setMargin(0);
    layout2->addWidget(m_sourceEditor);
    layout2->addWidget(m_targetEditor);
    
    QWidget * editorW = new QWidget;
    editorW->setLayout(layout2);
    
    splitter2->addWidget(editorW);
    splitter2->addWidget(m_contextView);
    
    layout->addWidget(splitter);
    setLayout(layout);
}

void MdiChild::newFile()
{
    static int sequenceNumber = 1;

    isUntitled = true;
    curFile = tr("document%1.txt").arg(sequenceNumber++);
    setWindowTitle(curFile + "[*]");
}

bool MdiChild::loadFile(const QString &fileName)
{

    QApplication::setOverrideCursor(Qt::WaitCursor);

    QString errorMsg;
    int errorLine, errorColumn;

    m_doc = XliffDocument::parse(fileName, &errorMsg, &errorLine, &errorColumn);
    
    if (!m_doc) {
        QApplication::restoreOverrideCursor();
        QMessageBox::warning(this, tr("Error opening file"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(fileName)
                             .arg(errorMsg));
        return false;
    }

     m_resView->setModel(new XliffResourceModel(m_doc));
    
    m_resView->setItemsExpandedRecursive(m_resView->rootIndex());
    
    connect(m_resView->selectionModel(), SIGNAL(currentRowChanged(const QModelIndex &,const QModelIndex &)),
            this, SLOT(selectionChanged(const QModelIndex &,const QModelIndex &)));
    
    QApplication::restoreOverrideCursor();

    setCurrentFile(fileName);
    return true;
}
void MdiChild::selectionChanged( const QModelIndex & current, const QModelIndex & previous )
{
    if(current.isValid()){
        m_contextView->clear();
        m_sourceEditor->setDocument(0);
        m_targetEditor->setDocument(0);
    }
    
    XliffResource *item = static_cast<XliffResource*>(current.internalPointer());
    XliffTransUnit *tuItem = qobject_cast<XliffTransUnit*>(item);
    if(tuItem){
        m_contextView->setHtml("Context info etc. goes here eventually :)");
        m_sourceEditor->setDocument(tuItem->source()->content()->clone());
        m_targetEditor->setDocument(tuItem->target()->content());
    }
    else{
        m_contextView->clear();
        m_sourceEditor->setDocument(0);
        m_targetEditor->setDocument(0);
    }
    
    
}
bool MdiChild::save()
{
    if (isUntitled) {
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

bool MdiChild::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                                                    curFile);
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName);
}

bool MdiChild::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("MDI"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(fileName)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << m_doc->xmlDoc()->toString();
    QApplication::restoreOverrideCursor();

    setCurrentFile(fileName);
    return true;
}

QString MdiChild::userFriendlyCurrentFile()
{
    return strippedName(curFile);
}

void MdiChild::closeEvent(QCloseEvent *event)
{
    if (maybeSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void MdiChild::documentWasModified()
{
//    setWindowModified(document()->isModified());
}

bool MdiChild::maybeSave()
{
/*    if (document()->isModified()) {
        int ret = QMessageBox::warning(this, tr("MDI"),
                     tr("'%1' has been modified.\n"
                        "Do you want to save your changes?")
                     .arg(userFriendlyCurrentFile()),
                     QMessageBox::Yes | QMessageBox::Default,
                     QMessageBox::No,
                     QMessageBox::Cancel | QMessageBox::Escape);
        if (ret == QMessageBox::Yes)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
*/
    return true;
}

void MdiChild::setCurrentFile(const QString &fileName)
{
    curFile = QFileInfo(fileName).canonicalFilePath();
 //   document()->setModified(false);
    setWindowModified(false);
    setWindowTitle(userFriendlyCurrentFile() + "[*]");
}

QString MdiChild::strippedName(const QString &fullFileName)
{
    return QFileInfo(fullFileName).fileName();
}

XliffDocument* MdiChild::xliffDocument()
{
    return m_doc;
}

XliffResourceView* MdiChild::resView()
{
    return m_resView;
}

