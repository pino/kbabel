/* This file is part of KBabel
   Copyright (C) 2013 Pino Toscano <pino@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

*/

#ifndef __main_h__
#define __main_h__

#include <kpluginfactory.h>
#include <kdatatool.h>

namespace Kross
{

class Action;

}

namespace KBabel
{

class CatalogItem;

}

class KrossPluginFactory : public KPluginFactory
{
    Q_OBJECT

public:
    KrossPluginFactory();

protected:
    virtual QObject* create( const char* iface, QWidget* parentWidget, QObject* parent, const QVariantList& args, const QString& keyword );
};


class KrossTool : public KDataTool
{
    Q_OBJECT

public:
    KrossTool( Kross::Action* action, QObject* parent, const QVariantList & );
    virtual bool run( const QString& command, void* data, const QString& datatype, const QString& mimetype);

private:
    bool do_validate_CatalogItem( KBabel::CatalogItem* item );

    Kross::Action* m_action;
};

#endif
