/* This file is part of KBabel
   Copyright (C) 2013 Pino Toscano <pino@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

*/

#ifndef KROSSCATALOGITEM_H
#define KROSSCATALOGITEM_H

#include <qobject.h>

namespace KBabel
{

class CatalogItem;

}

class KrossCatalogItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QString comment READ comment )
    Q_PROPERTY( QString msgctxt READ msgctxt )
    Q_PROPERTY( QStringList msgid READ msgid )
    Q_PROPERTY( QStringList msgstr READ msgstr )
    Q_PROPERTY( QStringList rawMsgid READ rawMsgid )
    Q_PROPERTY( QStringList rawMsgstr READ rawMsgstr )

public:
    KrossCatalogItem( KBabel::CatalogItem* item, QObject* parent = 0 );

    Q_SCRIPTABLE bool isValid() const;
    Q_SCRIPTABLE bool isUntranslated() const;
    Q_SCRIPTABLE bool isFuzzy() const;
    Q_SCRIPTABLE bool isCformat() const;
    Q_SCRIPTABLE bool isNoCformat() const;
    Q_SCRIPTABLE bool isQtformat() const;
    Q_SCRIPTABLE bool isNoQtformat() const;

private:
    QString comment() const;
    QString msgctxt() const;
    QStringList msgid() const;
    QStringList msgstr() const;
    QStringList rawMsgid() const;
    QStringList rawMsgstr() const;

    KBabel::CatalogItem* m_item;
};

#endif
