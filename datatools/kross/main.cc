/* This file is part of KBabel
   Copyright (C) 2013 Pino Toscano <pino@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

*/

#include "main.h"

#include "krosscatalogitem.h"

#include <catalogitem.h>
#include <resources.h>

#include <kconfig.h>
#include <kdebug.h>
#include <klocale.h>
#include <kstandarddirs.h>
#include <kross/core/action.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_EXPORT_PLUGIN( KrossPluginFactory() )

KrossPluginFactory::KrossPluginFactory()
    : KPluginFactory( "kbabeldatatool" )
{
}

QObject* KrossPluginFactory::create( const char* iface, QWidget* parentWidget, QObject* parent, const QVariantList& args, const QString& keyword )
{
    if ( keyword.isEmpty() )
    {
	kDebug(KBABEL) << "Empty plugin keyword";
	return 0;
    }
    QString script = KStandardDirs::locate( "data", "kbabel/datatools/" + keyword );
    if ( script.isEmpty() )
    {
	kDebug(KBABEL) << "Cannot find script:" << keyword;
	return 0;
    }

    Kross::Action* action = new Kross::Action( 0, keyword );
    if ( !action->setFile( script ) )
    {
	kDebug(KBABEL) << "Cannot set the file in Kross::Action";
	delete action;
	return 0;
    }

    return new KrossTool( action, parentWidget ? parentWidget : parent, args );
}


using namespace KBabel;

namespace
{

bool variantToBool( const QVariant& v, bool* valid )
{
    *valid = false;
    if ( !v.isValid() )
	return false;
    if ( v.type() == QVariant::Bool )
    {
	*valid = true;
	return v.toBool();
    }
    if ( v.canConvert< int >() )
    {
	*valid = true;
	return v.toInt() != 0;
    }
    return false;
}

}

KrossTool::KrossTool( Kross::Action* action, QObject* parent, const QVariantList & )
    : KDataTool( parent )
    , m_action( action )
{
    m_action->setParent( this );
}

bool KrossTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{

    if ( command == "validate" && datatype == "CatalogItem" && mimetype == "application/x-kbabel-catalogitem" )
    {
	return do_validate_CatalogItem( (CatalogItem*)(data) );
    }

    kDebug(KBABEL) << "Unknown combination:" << command << datatype << mimetype;
    return false;
}

bool KrossTool::do_validate_CatalogItem( CatalogItem* item )
{
    const QString errorString = m_action->callFunction( "errorString" ).toString();
    if ( m_action->hadError() )
    {
	kDebug(KBABEL) << "Error while executing errorString:" << m_action->errorMessage();
	kDebug(KBABEL) << "Trace:" << endl << m_action->errorTrace();
	m_action->clearError();
	return true;
    }
    if ( errorString.isEmpty() )
    {
	kDebug(KBABEL) << "Empty error string (errorString)";
	return true;
    }

    KrossCatalogItem kci( item );
    QVariantList args;
    args << qVariantFromValue( qobject_cast< QObject* >( &kci ) );
    const QVariant result = m_action->callFunction( "validateItem", args );
    bool valid;
    bool hasError = !variantToBool( result, &valid );
    if ( m_action->hadError() )
    {
	kDebug(KBABEL) << "Error while executing validateItem:" << m_action->errorMessage();
	kDebug(KBABEL) << "Trace:" << endl << m_action->errorTrace();
	m_action->clearError();
	return true;
    }
    if ( !valid )
    {
	kDebug(KBABEL) << "validateItem result is invalid or not bool:" << result;
	return true;
    }

    if (hasError)
	item->appendError( errorString );
    else
	item->removeError( errorString );

    return !hasError;
}

#include "main.moc"
