/* This file is part of KBabel
   based Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
			2002	Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
   you do not wish to do so, delete this exception statement from
   your version.

*/

#include <resources.h>
#include "catalog.h"
#include "catalogitem.h"
#include "catalogsettings.h"
#include "main.h"

#include <kconfig.h>
#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>
#include <kmessagebox.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( PluralsToolFactory, registerPlugin< PluralsTool >(); )
K_EXPORT_PLUGIN( PluralsToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

namespace {

int numberOfPluralForms(Project::Ptr project)
{
    IdentitySettings options = project->identitySettings();
    if (options.numberOfPluralForms > 0) {
        return options.numberOfPluralForms;
    }
    QString lang = options.languageCode;
    if (lang.isEmpty()) {
        return -1;
    }
    return Catalog::getNumberOfPluralForms(lang);
}

}

PluralsTool::PluralsTool( QObject* parent, const QVariantList & )
    : KDataTool( parent ), _cache_origin( 0 ), _neededForms(-1)
{
    i18nc("what check found errors", "plural forms");
}

bool PluralsTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
    if ( command != "validate" )
    {
	kDebug(KBABEL) << "Plural Forms Tool does only accept the command 'validate' and 'shortcut'";
	kDebug(KBABEL) << "   The commands " << command << " is not accepted";
	return false;
    }
    
    // Check wether we can accept the data
    if ( datatype != "CatalogItem" )
    {
	kDebug(KBABEL) << "Plural Forms Tool only accepts datatype CatalogItem";
	return false;
    }

    if ( mimetype != "application/x-kbabel-catalogitem" )
    {
	kDebug(KBABEL) << "Plural Forms Tool only accepts mimetype application/x-kbabel-catalogitem";
	return false;
    }
    
    if( command == "validate" )
    {
	CatalogItem* item = (CatalogItem*)(data);
	
	if( _neededForms < 0 )
	{
	    _neededForms = numberOfPluralForms(item->project());
	}
	
	if( _cache_origin != item->project() )
	{
	    _plurals = item->project()->miscSettings().singularPlural;
	    _cache_origin = item->project();
	}
	
	bool hasError = false;

	if(!item->isUntranslated() && item->pluralForm() == KDESpecific )
	{       
	    if(_neededForms <= 0 || item->msgstr().first().contains(_plurals))
	    {
		hasError = true;
	    }
	    else if( item->msgstr().first().count(QString("\\n"))+1 != _neededForms )
	    {
		hasError = true;
	    }
	}
	
	if(hasError)
	{
	    item->appendError( "plural forms" );
	}
	else
	{
	    item->removeError( "plural forms" );
	}
	
	return !hasError;
    }
    return false;
}

#include "main.moc"
