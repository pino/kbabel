/*	Copyright (C) 2005	Albert Cervera i Areny <albertca at hotpop dot com>

	Based on Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
		    2002 	Stanislav Visnovsky <visnovsky@kde.org>
			2003	Dwayne Bailey <dwayne@translate.org.za>

	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Library General Public
	License as published by the Free Software Foundation; either
	version 2 of the License, or (at your option) any later version.

	This library is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
	Library General Public License for more details.

	You should have received a copy of the GNU Library General Public License
	along with this library; see the file COPYING.LIB.  If not, write to
	the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
	Boston, MA 02110-1301, USA.
*/

#include <resources.h>
#include "catalog.h"
#include "catalogitem.h"
#include "catalogsettings.h"
#include "main.h"
#include <math.h>

#include <qdir.h>
#include <qfile.h>
#include <qdom.h>
#include <qstringlist.h>
#include <qregexp.h>
#include <kconfig.h>
#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kstandarddirs.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( RegExpToolFactory, registerPlugin< RegExpTool >(); )
K_EXPORT_PLUGIN( RegExpToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

RegExpTool::RegExpTool( QObject* parent, const QVariantList & )
    : KDataTool( parent )
{
	i18nc("which check found errors","translation has inconsistent length");
	loadExpressions();
	if ( ! _error.isNull() )
		KMessageBox::error( (QWidget*)parent, i18n( "Error loading data (%1)", _error ) );
}

bool RegExpTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
	if ( command != "validate" )
	{
		kDebug(KBABEL) << "RegExpTool only accepts the 'validate' command";
		kDebug(KBABEL) << "   The command " << command << " is not accepted";
		return false;
	}
	// Check wether we can accept the data
	if ( datatype != "CatalogItem" )
	{
		kDebug(KBABEL) << "RegExpTool only accepts the CatalogItem datatype";
		return false;
	}
	if ( mimetype != "application/x-kbabel-catalogitem" )
	{
		kDebug(KBABEL) << "RegExpTool only accepts the 'application/x-kbabel-catalogitem' mimetype";
		return false;
	}
	
	bool hasError = false;
	if( command == "validate" )
	{
		CatalogItem* item = (CatalogItem*)(data);

		if(!item->isUntranslated()) {
			ExpressionList::Iterator it( _list.begin() );
			ExpressionList::Iterator end( _list.end() );
			QStringList msgs = item->msgstr();
			QStringList results;
			for ( ; it != end; ++it ) {
				results.clear();
				results = msgs.filter( (*it).regExp() );
				if ( results.size() > 0 ) {
					hasError = true;
					break;
				}
			}
		}
		if(hasError) {
			item->appendError( "regexp" );
		} else {
			item->removeError( "regexp" );
		}
	}
	return !hasError;
}


void RegExpTool::loadExpressions()
{
	const QString filePath = KStandardDirs::locateLocal( "data" , "kbabel/regexplist.xml" );
	if ( filePath.isEmpty() ) {
		kDebug() << "File not found";
		_error = i18n( "File not found" );
		return;
	}
	QFile file( filePath );
	QDomDocument doc;
	
	if ( ! file.open( QIODevice::ReadOnly ) ) {
		kDebug() << "File could not be opened";
		_error = i18n( "File opening error" );
		return;
	}
	if ( ! doc.setContent( &file ) ) {
		kDebug() << "Could not set content of xml file";
		_error = i18n( "The file is not an XML" );
		return;
	}
	file.close();

	QDomElement docElem = doc.documentElement();
	QDomNode n = docElem.firstChild();
	while( !n.isNull() ) {
		QDomElement e = n.toElement();
		if( !e.isNull() )
			elementToExpression( e );
		if ( ! _error.isNull() )
			break;
		n = n.nextSibling();
	}
}

void RegExpTool::elementToExpression( const QDomElement& e )
{
	QString name;
	QString exp;
	Qt::CaseSensitivity cs = Qt::CaseInsensitive; //Expressions are case insensitive by default

	if ( e.tagName().compare( "item" ) != 0 ) {
		_error = i18n( "Expected tag 'item'" );
		return;
	}

	QDomNode n = e.firstChild();
	if ( n.isNull() ) {
		_error = i18n( "First child of 'item' is not a node" );
		return;
	}

	QDomElement el = n.toElement();
	if ( el.isNull() || el.tagName().compare( "name" ) != 0 ) {
		_error = i18n( "Expected tag 'name'" );
		return;
	}
	name = el.text();

	n = n.nextSibling();
	el = n.toElement();
	if ( el.isNull() || el.tagName().compare( "exp" ) != 0 ) {
		_error = i18n( "Expected tag 'exp'" );
		return;
	}
	exp = el.text();

	n = n.nextSibling();
	el = n.toElement();
	if ( ! el.isNull() )
		cs = Qt::CaseSensitive;

	kDebug(KBABEL) << "RegExpTool: Adding expression: " << name;
	_list.append( Expression( name, QRegExp( exp, cs ) ) );
}

#include "main.moc"
