# Copyright (C) 2013 Pino Toscano <pino@kde.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

def errorString_i18n():
  return i18nc("which check found errors", "English text in translation");

def errorString():
  return "english text in translation"

def validateItem(item):
  if not item.isUntranslated():
    # FIXME: Expand this to do substring matching of non-translation
    msgid = item.msgid
    msgstr = item.msgstr
    msglen = min(len(msgid), len(msgstr))
    for i in range(msglen):
      if msgid[i] == msgstr[i]:
        return False
  return True
