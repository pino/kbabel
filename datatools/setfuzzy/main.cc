/* This file is part of KBabel
   based Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
			2003	Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
   you do not wish to do so, delete this exception statement from
   your version.
  
*/

#include <resources.h>
#include "catalog.h"
#include "main.h"

#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( SetFuzzyToolFactory, registerPlugin< SetFuzzyTool >(); )
K_EXPORT_PLUGIN( SetFuzzyToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

SetFuzzyTool::SetFuzzyTool( QObject* parent, const QVariantList & )
    : KDataTool( parent )
{
}

bool SetFuzzyTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
    if ( command != "allfuzzy" )
    {
	kDebug(KBABEL) << "Fuzzy Toggling Tool does only accept the command 'allfuzzy'";
	kDebug(KBABEL) << "   The commands " << command << " is not accepted";
	return false;
    }
    
    // Check wether we can accept the data
    if ( datatype != "Catalog" )
    {
	kDebug(KBABEL) << "Fuzzy Toggling Tool  only accepts datatype Catalog";
	return false;
    }

    if ( mimetype != "application/x-kbabel-catalog" )
    {
	kDebug(KBABEL) << "Plural Forms Tool only accepts mimetype application/x-kbabel-catalog";
	return false;
    }
    
    if( command == "allfuzzy" )
    {
	Catalog* catalog = (Catalog*)(data);
	
	catalog->applyBeginCommand(0,Msgstr,0);
	
	for( uint index=0; index < catalog->numberOfEntries(); index++ )
	{
	    if( !catalog->isUntranslated(index) )
	    {
		catalog->setFuzzy(index,true);
	    }
	}

	catalog->applyEndCommand(0,Msgstr,0);
    }
    return true;
}

#include "main.moc"
