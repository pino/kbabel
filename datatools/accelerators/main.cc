/* This file is part of KBabel
   based Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
			2002	Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
   you do not wish to do so, delete this exception statement from
   your version.

*/

#include <resources.h>
#include "catalogitem.h"
#include "catalogsettings.h"
#include "main.h"

#include <kconfig.h>
#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( AcceleratorToolFactory, registerPlugin< AcceleratorTool >(); )
K_EXPORT_PLUGIN( AcceleratorToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

AcceleratorTool::AcceleratorTool( QObject* parent, const QVariantList & )
    : KDataTool( parent ), _cache_origin( 0 )
{
    // bogus translation just for allowing the translation
    i18nc("what check found errors","accelerator");
}

bool AcceleratorTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
    if ( command != "validate" )
    {
	kDebug(KBABEL) << "Accelerator Tool does only accept the command 'validate'";
	kDebug(KBABEL) << "   The commands " << command << " is not accepted";
	return false;
    }
    
    // Check wether we can accept the data
    if ( datatype != "CatalogItem" )
    {
	kDebug(KBABEL) << "Accelerator Tool only accepts datatype CatalogItem";
	return false;
    }

    if ( mimetype != "application/x-kbabel-catalogitem" )
    {
	kDebug(KBABEL) << "Accelerator Tool only accepts mimetype application/x-kbabel-catalogitem";
	return false;
    }
    
    if( command == "validate" )
    {
	CatalogItem* item = (CatalogItem*)(data);
	
	if( _cache_origin != item->project() )
	{
	    _context = item->project()->miscSettings().contextInfo;
	    _marker = item->project()->miscSettings().accelMarker;
	    _cache_origin = item->project();
	}
	
	bool hasError = false;

	if(!item->isUntranslated())
	{
		// FIXME: this should care about plural forms in msgid
		QString lineid=item->msgid().first();
		lineid.replace( _context, "");
		lineid.replace(QRegExp("\\n"),"");
		lineid.trimmed();
		QString regStr(_marker);
		regStr+="[^\\s]";
		QRegExp reg(regStr);
		
		QStringList str = item->msgstr(true);
		for( QStringList::Iterator form = str.begin() ; form != str.end(); form++ )
		{
		    QString linestr=(*form);
		    linestr.trimmed();

		    int n = lineid.count(reg);
		    if( _marker == '&' )
			n = n - lineid.count(QRegExp("(&[a-z,A-Z,\\-,0-9,#]*;)|(&&(?!&+))"));
		    int m = linestr.count(reg);
		    if( _marker == '&' )
			m = m - linestr.count(QRegExp("(&[a-z,A-Z,\\-,0-9,#]*;)|(&&(?!&+))"));
		    hasError = hasError || ( n<=1 &&  m != n);
		}
	}
	
	if(hasError)
	{
	    item->appendError( "Accelerator" );
	}
	else
	{
	    item->removeError( "Accelerator" );
	}
			
	return !hasError;
    }
    return false;
}

#include "main.moc"
