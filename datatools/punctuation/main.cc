/* This file is part of KBabel
   based Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
			2003	Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
   you do not wish to do so, delete this exception statement from
   your version.
	  
*/

#include <resources.h>
#include "catalogitem.h"
#include "catalogsettings.h"
#include "main.h"

#include <kconfig.h>
#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( PunctuationToolFactory, registerPlugin< PunctuationTool >(); )
K_EXPORT_PLUGIN( PunctuationToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

PunctuationTool::PunctuationTool( QObject* parent, const QVariantList & )
    : KDataTool( parent )
{
    // bogus translation just for allowing the translation
    i18nc("what check found errors","punctuation");
}

bool PunctuationTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
    if ( command != "validate" )
    {
	kDebug(KBABEL) << "Punctuation Tool does only accept the command 'validate'";
	kDebug(KBABEL) << "   The commands " << command << " is not accepted";
	return false;
    }
    
    // Check wether we can accept the data
    if ( datatype != "CatalogItem" )
    {
	kDebug(KBABEL) << "Punctuation Tool only accepts datatype CatalogItem";
	return false;
    }

    if ( mimetype != "application/x-kbabel-catalogitem" )
    {
	kDebug(KBABEL) << "Punctuation Tool only accepts mimetype application/x-kbabel-catalogitem";
	return false;
    }
    
    if( command == "validate" )
    {
	CatalogItem* item = (CatalogItem*)(data);
	
	bool hasError = false;

	if(!item->isUntranslated())
	{
		QString lineid=item->msgid().first();

		// lookup punctuation in original text
		QRegExp punc("[\\.!\\?:]+$");
		int i = lineid.indexOf( punc );
		
		QString t("");
		
		if( i != -1 ) t = lineid.right(lineid.length()-i);
		
		if( item->pluralForm() != NoPluralForm )
		{
		    // check, that both plural forms contain the same punctuation
		    QString pl = item->msgid().at(1);
		    int j = pl.indexOf( punc );
		    
		    QString tp("");
		    if( j != -1 ) tp = pl.right(pl.length()-j);
		    
		    if( tp != t )
		    {
			kWarning() << "Singular and plural form do not have the same punctuation";
		    }
		}
		
		QStringList forms = item->msgstr(true);
		if( item->pluralForm() == KDESpecific ) {
		    forms = item->msgstr(true).at(0).split( "\\n", QString::SkipEmptyParts );
		}

		for( QStringList::Iterator form = forms.begin() ; form != forms.end(); form++ )
		{
		    QString linestr=(*form);
		    
		    int j = linestr.indexOf( punc );
		    
		    // there is no punctuation in original, but one in the translation
		    if( i == -1 && j != i ) 
		    {
			hasError = true;
			break;
		    }
		    
		    // there is punctuation in original, but not same as in the translation
		    if( i != -1 && linestr.right(linestr.length()-j) != t )
		    {
			hasError = true;
			break;
		    }
		}
	}
	
	if(hasError)
	{
	    item->appendError( "punctuation" );
	}
	else
	{
	    item->removeError( "punctuation" );
	}
			
	return !hasError;
    }
    return false;
}

#include "main.moc"
