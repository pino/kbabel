/* This file is part of KBabel
   based Copyright (C) 1998, 1999 Torben Weis <weis@kde.org>
			2002	Stanislav Visnovsky <visnovsky@kde.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   In addition, as a special exception, the copyright holders give
   permission to link the code of this program with any edition of
   the Qt library by Trolltech AS, Norway (or with modified versions
   of Qt that use the same license as Qt), and distribute linked
   combinations including the two.  You must obey the GNU General
   Public License in all respects for all of the code used other than
   Qt. If you modify this file, you may extend this exception to
   your version of the file, but you are not obligated to do so.  If
   you do not wish to do so, delete this exception statement from
   your version.

*/

#include <resources.h>
#include "catalogitem.h"
#include "catalogsettings.h"
#include "main.h"

#include <kconfig.h>
#include <kdebug.h>
#include <kpluginfactory.h>
#include <klocale.h>
#include <kmessagebox.h>

/***************************************************
 *
 * Factory
 *
 ***************************************************/

K_PLUGIN_FACTORY( EquationsToolFactory, registerPlugin< EquationsTool >(); )
K_EXPORT_PLUGIN( EquationsToolFactory( "kbabeldatatool" ) )

using namespace KBabel;

EquationsTool::EquationsTool( QObject* parent, const QVariantList & )
    : KDataTool( parent), _equation("^[a-zA-Z0-9]+=.+")
{
    i18nc("what check found errors","equations");
}

bool EquationsTool::run( const QString& command, void* data, const QString& datatype, const QString& mimetype )
{
    if ( command != "validate" )
    {
	kDebug(KBABEL) << "Equations Tool does only accept the command 'validate' and 'shortcut'";
	kDebug(KBABEL) << "   The commands " << command << " is not accepted";
	return false;
    }
    
    // Check wether we can accept the data
    if ( datatype != "CatalogItem" )
    {
	kDebug(KBABEL) << "Equations Tool only accepts datatype CatalogItem";
	return false;
    }

    if ( mimetype != "application/x-kbabel-catalogitem" )
    {
	kDebug(KBABEL) << "Equations Tool only accepts mimetype application/x-kbabel-catalogitem";
	return false;
    }
    
    if( command == "validate" )
    {
	CatalogItem* item = (CatalogItem*)(data);
	
	bool hasError = false;

	if(!item->isUntranslated() && !item->msgid().first().contains('\n')
	     && item->msgid().first().contains(_equation))
	{
    	    int index = item->msgid().first().indexOf( '=' );
	    QString left = item->msgid().first().left(index);
	    index = item->msgstr().first().indexOf( '=' );
	    if(left != item->msgstr().first().left(index))
		hasError = true;
	}
	
	if(hasError)
	{
	    item->appendError( "equations" );
	}
	else
	{
	    item->removeError( "equations" );
	}
			
	return !hasError;
    }
    return false;
}

#include "main.moc"
