# Copyright (C) 2013 Pino Toscano <pino@kde.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

def errorString_i18n():
  return i18nc("which check found errors", "translation has inconsistent length")

def errorString():
  return "translation has inconsistent length"

def validateItem(item):
  if not item.isUntranslated():
    # Check for translations that are too short or too long
    # This may not be totally correct but we check both
    # the singular and plural forms against each translated plural.
    # FIXME: replace 10% check with configurable setting or a statistical
    # based expected length relationship
    msgid = item.msgid
    msgstr = item.msgstr
    msglen = min(len(msgid), len(msgstr))
    for i in range(msglen):
      idlen = len(msgid[i])
      strlen = len(msgstr[i])
      if (strlen < (0.1 * idlen)) or (strlen > (10 * idlen)):
        return False
  return True
