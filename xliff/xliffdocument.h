/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#ifndef XLIFFDOCUMENT_H
#define XLIFFDOCUMENT_H

#include <QtCore>
#include <QtXml>
#include <kbabel_export.h>
#include "kurl.h"
#include "xliffresourcecontainer.h"

class XliffFile;


class  KBXLIFF_EXPORT XliffDocument : public XliffResourceContainer
{
    Q_OBJECT

public:

    enum Version{V1_0, V1_1, V_Unknown};

    XliffDocument(QObject* parent=0);
    ~XliffDocument();

    QList<XliffFile *> files();
    QString fileName();
    void setFileName(const QString &name);
    Version version();
    void setVersion(Version version);
    QString xmlLang();

    static XliffDocument* parse(const QString & filename, QString * errorMsg = 0, int * errorLine = 0, int * errorColumn = 0 );

    virtual QList<XliffResourceContainer*> children();
    virtual QList<XliffResource*> resources();
    
    QDomDocument *xmlDoc();

private:
    XliffDocument(QDomDocument * doc, const QString & filename, QObject* parent=0);

    void initialize();

    QString m_fileName;
    QDomDocument * m_doc;
    Version m_version;
    QList<XliffFile*> m_files;
    QList<XliffResourceContainer*> m_children;
    QList<XliffResource*> m_resources;
};

#endif //XLIFFDOCUMENT_H
