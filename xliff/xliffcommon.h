/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#ifndef XLIFFCOMMON_H
#define XLIFFCOMMON_H


#define XLIFF_1_1_NSURI "urn:oasis:names:tc:xliff:document:1.1"
#define XML_NSURI "http://www.w3.org/XML/1998/namespace"



class QDomElement;

bool readApprovedAttr(const QDomElement &elem);
void setApprovedAttr(QDomElement &elem, bool approved);

bool readTranslateAttr(const QDomElement &elem, bool recursive = true);

QString readXmlSpaceAttr(const QDomElement &elem, bool recursive = true);

QString readXmlLangAttr(const QDomElement &elem, bool recursive = true);

#endif // XLIFFCOMMON_H
