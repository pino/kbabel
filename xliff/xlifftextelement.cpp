/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "xlifftextelement.h"

#include <QTextDocument>
#include <QTextCursor>
#include <QTextCharFormat>
#include <QTextBlock>
#include <QTextFragment>



XliffTextElement::XliffTextElement(QDomElement & elem, QObject *parent)
    : QObject(parent), m_elem(elem)
{
    m_doc = NULL;
    m_inlineCount = 0;
}


XliffTextElement::~XliffTextElement()
{
}

QTextDocument* XliffTextElement::content()
{
    if(m_doc == NULL){
        
        m_doc = new QTextDocument;
        QTextCursor cursor(m_doc);
        cursor.movePosition(QTextCursor::Start);
        
        QDomNode n = m_elem.firstChild();
        while (!n.isNull()) {
            if (n.isCharacterData()) {
                QDomCharacterData cData = n.toCharacterData();
                cursor.insertText(cData.data(), textFormat());
            }
            else if (n.isElement()) {
                QDomElement e = n.toElement();
                QDomNode nodeContent = e.firstChild();
                if(!nodeContent.isNull()){
                    if (nodeContent.isCharacterData()) {
                        QDomCharacterData cData = nodeContent.toCharacterData();
                        cursor.insertText(cData.data(),phFormat());
                        cursor.insertText(QString::number(++m_inlineCount),identifierFormat());
                    }
                }
            }
            n = n.nextSibling();
        }    
    }
    return m_doc;
}

QTextCharFormat XliffTextElement::phFormat()
{
    QTextCharFormat format;
    format.setFontWeight(QFont::Bold);
    format.setForeground(QBrush(Qt::darkBlue));
    return format;
}

QTextCharFormat XliffTextElement::identifierFormat()
{
    QTextCharFormat format;
    format.setVerticalAlignment(QTextCharFormat::AlignSuperScript);
    format.setForeground(QBrush(Qt::darkGreen));
    return format;
}

QTextCharFormat XliffTextElement::textFormat()
{
    QTextCharFormat format;
    return format;
}

void XliffTextElement::setContent(QTextDocument *content)
{
    m_doc= content->clone();
/*
    QTextBlock currentBlock = content->begin();

    while (currentBlock.isValid()) {

        QTextBlock::iterator it;
        for (it = currentBlock.begin(); !(it.atEnd()); ++it) {
            QTextFragment currentFragment = it.fragment();
            
            QTextCharFormat format = currentFragment.charFormat();
            if(format == textFormat()){
            }
            else if(format == phFormat()){
            }
            else if (format == identifierFormat()){
            
            }
            
            
        }



        currentBlock = currentBlock.next();
    }    

*/
}

#include "xlifftextelement.moc"
