/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "xliffbinunit.h"

#include "xliffcommon.h"
#include "xlifftransunit.h"

#include "kdebug.h"


XliffBinUnit::XliffBinUnit(QDomNode & node, XliffResourceContainer* parent)
    : XliffResourceContainer(parent), m_node(node)
{
    m_translate = -1;
    initialize();
}

XliffBinUnit::~XliffBinUnit()
{
}

bool XliffBinUnit::approved()
{
    return readApprovedAttr(m_node.toElement());
}

bool XliffBinUnit::translate()
{
    if(m_translate == -1){
        m_translate = readTranslateAttr(m_node.toElement()) ? 1 : 0;
    }
    
    return m_translate == 1;
}


QString XliffBinUnit::id()
{
     return m_node.toElement().attribute("id");
}

QString XliffBinUnit::restype()
{
     return m_node.toElement().attribute("restype");
}

QString XliffBinUnit::resname()
{
     return m_node.toElement().attribute("resname");
}




QList<XliffResource*> XliffBinUnit::resources()
{
    return m_transUnits;
}









void XliffBinUnit::initialize()
{
    kDebug() << "Initializing XLIFF <bin-unit> " << endl;
    
    XliffResource *resElem;
    QString nodeName;
    
    QDomNodeList binUnit = m_node.childNodes();
    
    for(int i=0;i<binUnit.count();i++){

        QDomNode currNode = binUnit.item(i);
         
        // TODO: Check namespace here
        nodeName = currNode.nodeName();
        kDebug() << "Found <" << nodeName << "> node" << endl;
        
        if( nodeName.compare("trans-unit") == 0){
            kDebug() << "Initializing <trans-unit> node" << endl;
            resElem = new XliffTransUnit(currNode, this);
            m_transUnits << resElem;
        } 
    }

}

#include "xliffbinunit.moc"
