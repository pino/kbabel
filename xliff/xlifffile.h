/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#ifndef XLIFFFILE_H
#define XLIFFFILE_H

#include <QtCore>
#include <QtXml>
#include <kbabel_export.h>
#include "xliffresourcecontainer.h"

class XliffResource;

class KBXLIFF_EXPORT XliffFile : public XliffResourceContainer
{
    Q_OBJECT

public:

    explicit XliffFile(QDomNode & node, XliffResourceContainer* parent=0);
    ~XliffFile();

    QString sourceLanguage();

    QString targetLanguage();

    QString datatype();

    QString originalFileName();
    
    virtual QList<XliffResource*> resources();
    
    virtual QList<XliffResourceContainer*> children();

    // XliffProductData * productData(); 

    // QDateTime date();

    // productData();

    // QString category();

    // QString toolId();

    // XliffTool * toolById(QString toolId);

    // XliffFileReference * skeleton();

    // XliffFileReference * reference();

    // QList<XliffCountGroup*> countGroups();

    // QList<XliffPropertyGroup*> propertyGroups();

    // QList<XliffTool*> tools();

    QString xmlSpace();

private:
    void initialize();
    QList<XliffResource*> m_resources; 
    QList<XliffResourceContainer*> m_children;
    QDomNode m_node;    
};

#endif //XLIFFFILE_H
