/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include "xlifffile.h"

#include <QtCore>

#include "xliffresource.h"
#include "xliffgroup.h"
#include "xlifftransunit.h"
#include "xliffbinunit.h"
#include "xliffresourcecontainer.h"

#include "xliffcommon.h"

#include <kdebug.h>

XliffFile::XliffFile(QDomNode & node, XliffResourceContainer* parent)
    : XliffResourceContainer(parent), m_node(node)

{
    initialize();
}

XliffFile::~XliffFile()
{
    // do nothing
}

QString XliffFile::datatype()
{
    return m_node.toElement().attribute("datatype");
}

QString XliffFile::originalFileName()
{
    return m_node.toElement().attribute("original");
}

QString XliffFile::sourceLanguage()
{
    return m_node.toElement().attribute("source-language");
}

QString XliffFile::targetLanguage()
{
    return m_node.toElement().attribute("target-language");
}

QList<XliffResource*> XliffFile::resources()
{
    return m_resources;
}

QList<XliffResourceContainer*> XliffFile::children()
{
    return m_children;
}

QString XliffFile::xmlSpace()
{
    return m_node.toElement().attributeNS(
        XML_NSURI, "space", "default");
}

void XliffFile::initialize()
{
    kDebug() << "Initializing XLIFF file " << endl;
    QDomNodeList file = m_node.childNodes();

    for(int i=0;i<file.count();i++){

        // TODO: Check namespace here
        QString nodeName = file.item(i).nodeName();
        
        kDebug() << "Found <" << nodeName << "> node" << endl;
        
        if( nodeName.compare("body") == 0){
            
            QDomNodeList grp = file.item(i).childNodes();
            
            for(int i=0;i<grp.count();i++){
        
                QDomNode currNode = grp.item(i); 
                
                // TODO: Check namespace here
                nodeName = currNode.nodeName();
                kDebug() << "Found <" << nodeName << "> node" << endl;
                
                if( nodeName.compare("group") == 0){
                    kDebug() << "Initializing <group> node" << endl;
                    XliffGroup *resElem = new XliffGroup(currNode, this);
                    m_resources << resElem;
                    
                    if(resElem->restype() != "x-gettext-plurals")
                        m_children << resElem;
                    
                } 
                else if( nodeName.compare("trans-unit") == 0){
                    kDebug() << "Initializing <trans-unit> node" << endl;
                    XliffTransUnit *resElem = new XliffTransUnit(currNode, this);
                    m_resources << resElem;
                } 
                else if( nodeName.compare("bin-unit") == 0){
                    kDebug() << "Initializing <bin-unit> node" << endl;
                    XliffBinUnit *resElem = new XliffBinUnit(currNode, this);
                    m_resources << resElem;
                    m_children << resElem;
                }
            }
        }
    }
}


#include "xlifffile.moc"
