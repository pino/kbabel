/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "xlifftransunit.h"
#include "xliffcommon.h"
#include "xliffsource.h"
#include "xlifftarget.h"

#include "kdebug.h"

XliffTransUnit::XliffTransUnit(QDomNode & node, XliffResourceContainer* parent)
    : XliffResource(parent), m_node(node)
{
    m_source = NULL;
    m_target = NULL;
    m_translate = -1;
    initialize();
}

XliffTransUnit::~XliffTransUnit()
{
}

QString XliffTransUnit::datatype()
{
     return m_node.toElement().attribute("datatype");
    
}

QString XliffTransUnit::xmlSpace()
{
    return readXmlSpaceAttr(m_node.toElement());
}

bool XliffTransUnit::approved()
{
    return readApprovedAttr(m_node.toElement());
}

void XliffTransUnit::setApproved(bool approved)
{
    QDomElement elem = m_node.toElement();
    return setApprovedAttr(elem,approved);
}


bool XliffTransUnit::translate()
{
    if(m_translate == -1){
        m_translate = readTranslateAttr(m_node.toElement()) ? 1 : 0;
    }
    
    return m_translate == 1;
}

XliffSource* XliffTransUnit::source()
{
    return m_source;
}

XliffTarget* XliffTransUnit::target()
{
    return m_target;
}


QString XliffTransUnit::id()
{
     return m_node.toElement().attribute("id");
}

QString XliffTransUnit::restype()
{
     return m_node.toElement().attribute("restype");
}

QString XliffTransUnit::resname()
{
     return m_node.toElement().attribute("resname");
}



void XliffTransUnit::initialize()
{
    kDebug() << "Initializing XLIFF <trans-unit> " << endl;
    
    QString nodeName;
    
    QDomNodeList grp = m_node.childNodes();
    QDomElement source;
    for(int i=0;i<grp.count();i++){

        // TODO: Check namespace here
        nodeName = grp.item(i).nodeName();
        kDebug() << "Found <" << nodeName << "> node" << endl;
        
        QDomElement elem = grp.item(i).toElement();
        
        if( nodeName.compare("source") == 0){
            kDebug() << "Initializing <source> node" << endl;
            m_source = new XliffSource(elem, this);
            source = elem;
        } 
        else if( nodeName.compare("target") == 0){
            kDebug() << "Initializing <target> node" << endl;
            m_target = new XliffTarget(elem, this);
        } 
    }
    
    if(!m_target){
        QDomElement target = m_node.ownerDocument().createElementNS(XLIFF_1_1_NSURI,"target");
        m_node.insertAfter(target, source);
        m_target = new XliffTarget(target, this);
    }

}

#include "xlifftransunit.moc"
