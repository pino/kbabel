/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include "xliffdocument.h"

#include <QtCore>

#include <kdebug.h>

#include "xlifffile.h"

#include "xliffcommon.h"

XliffDocument::XliffDocument(QObject* parent) :
    XliffResourceContainer()
{
    setParent(parent);
}

XliffDocument::XliffDocument(QDomDocument * doc, const QString & filename, QObject* parent) :
    XliffResourceContainer()
{
    m_doc = doc;
    m_fileName = filename;
    initialize();
}

QDomDocument *XliffDocument::xmlDoc()
{
    return m_doc;
}

XliffDocument::~XliffDocument()
{
    // do nothing
    delete m_doc;
}

QList<XliffFile *>  XliffDocument::files()
{
    return m_files;
}

QString XliffDocument::fileName()
{
    // do nothing
    return m_fileName;
}

void XliffDocument::setFileName(const QString & name)
{
    m_fileName = name;
}

XliffDocument::Version XliffDocument::version()
{
    if(!m_doc) return V_Unknown;

    QDomElement doc_elem = m_doc->documentElement();
    
    QString version = doc_elem.attribute("version");

    if(version.isEmpty())
        kDebug() << "No XLIFF version specified";

    if( version.compare("1.1") == 0)
        return V1_1;
    else if (version.compare("1.0") == 0)
        return V1_0;
    else
        return V_Unknown;

}


void XliffDocument::setVersion(Version version)
{
    m_version = version;
}


QString XliffDocument::xmlLang()
{
    return m_doc->documentElement().attributeNS(
        XML_NSURI, "lang");
}

XliffDocument* XliffDocument::parse(const QString & filename, QString * errorMsg, int * errorLine, int * errorColumn)
{
    if ( filename.isEmpty( ) ) {
        kDebug() << "fatal error: empty filename to open" << endl;
        return NULL; // NO_FILE;
    }
        
    QFileInfo info( filename );
    
    if ( !info.exists( ) || info.isDir( ) )
        return NULL; // NO_FILE;
        
    if ( !info.isReadable( ) )
        return NULL; // NO_PERMISSIONS;
        
    QFile file( filename );
    if ( !file.open( QIODevice::ReadOnly ) )
        return NULL; // NO_PERMISSIONS;
    
    QDomDocument * doc = new QDomDocument();

    if ( !doc->setContent( &file, true, errorMsg, errorLine, errorColumn ) ) {
        file.close( );
        kError() << "Parsing error at line " << errorLine << ", column " << errorColumn << ", error " << errorMsg << endl;
        return NULL; // PARSE_ERROR;
    }
    file.close( );

    XliffDocument * xlfdoc = new XliffDocument(doc,filename);

    return xlfdoc;
}

void XliffDocument::initialize()
{
    kDebug() << "Initializing XLIFF document" << endl;
    if(!m_doc) return;

    QDomNodeList files = m_doc->documentElement().childNodes();

    kDebug() << "  - found " << files.count() << " file(s)" << endl;
    for(int i=0;i<files.count();i++){
        QDomNode node = files.item(i);

        // TODO: Check namespace here
        if( node.nodeName().compare("file") == 0){
            XliffFile * xlf_file = new XliffFile(node, this);
            m_files << xlf_file;
            m_resources << xlf_file;
            m_children << xlf_file;
        }
    }
}

QList<XliffResourceContainer*> XliffDocument::children()
{
    return m_children;
}

QList<XliffResource*> XliffDocument::resources()
{
    return m_resources;
}

#include "xliffdocument.moc"
