/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#ifndef XLIFFGROUP_H
#define XLIFFGROUP_H

#include <QtCore>
#include <QtXml>
#include <kbabel_export.h>

#include "xliffresource.h"
#include "xliffresourcecontainer.h"

class KBXLIFF_EXPORT XliffGroup : public XliffResourceContainer
{
    Q_OBJECT

public:

    explicit XliffGroup(QDomNode & node, XliffResourceContainer* parent=0);
    ~XliffGroup();

     QString id();
    // QString ts();
     QString restype();
     QString resname();
    // QList<XliffContextGroup*> contextGroups();
    // QList<XliffCountGroup*> countGroups();
    // QList<XliffPropGroup*> propGroups();
    // QList<XliffNote*> notes();
    // QStringList reformat();



    // trans-unit / group common
    QString datatype();
    QString xmlSpace();
    // QString extradata();
    // QString helpId();
    // QString menu();
    // QString menuOption();
    // QString menuName();
    // QRect coord();
    // QString font();
    // QString cssStyle();
    // QString style();
    // QString exstyle();
    // QString extype();
    // ...constraints:
    // int maxbytes();
    // int minbytes();
    // QString sizeUnit();
    // int maxHeight();
    // int minHeight();
    // int maxWidth();
    // int minWidth();
    // QString charclass();


    virtual QList<XliffResource*> resources();
    virtual QList<XliffResourceContainer*> children();
    

private:
    QList<XliffResource*> m_resources;
    QList<XliffResourceContainer*> m_children;
    QDomNode m_node;        
    void initialize();
};

#endif //XLIFFGROUP_H
