/*****************************************************************************
  This file is part of KBabel

  Copyright (C) 2005 by KBabel Developers
        Asgeir Frimannsson <asgeirf@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */

#include "xliffcommon.h"

#include "kdebug.h"
#include <QtCore>
#include <QtXml>

bool readApprovedAttr(const QDomElement &elem)
{
    QString appr = elem.attribute("approved");
    
    if(appr.isEmpty())
        return false;
    else if( appr.compare("yes") == 0 )
        return true;
    else if( appr.compare("no") == 0 )
        return false;
    else{
        kDebug() << "Invalid XLIFF 'approved' attribute: " << appr << endl;
        return false;
    }

}

void setApprovedAttr(QDomElement &elem, bool approved)
{
    elem.setAttribute("approved", approved ? "yes" : "no");
}

bool readTranslateAttr(const QDomElement &elem, bool recursive){
    
    if(elem.isNull())
        return true;
    
    QString trAtt = elem.attribute("translate");
    
    if(trAtt.isEmpty()){
    
        if(!recursive) return true;
        
        QDomNode parent = elem.parentNode();
        
        if(parent.nodeName().compare("body") == 0)
            return true;
        
        kDebug() << "Recursing: " << parent.nodeName() << endl;
        return readTranslateAttr(parent.toElement(), recursive);
    }
    else if( trAtt.compare("yes") == 0 )
        return true;
    else if( trAtt.compare("no") == 0 )
        return false;
    else{
        kDebug() << "Invalid XLIFF 'translate' attribute: " << trAtt << endl;
        return false;
    }

}


QString readXmlSpaceAttr(const QDomElement &elem, bool recursive)
{

    QDomNode currNode;
    QDomElement currElem = elem;
    
    QString space = currElem.attributeNS(XML_NSURI, "space");;
    
    if(!recursive)
        return space.isEmpty() ? "default" : space;
    
    while (space.isEmpty()){
    
        currNode = currElem.parentNode();
        if(currNode.isNull()){
            return space.isEmpty() ? "default" : space;
        }
        
        currElem = currNode.toElement();
        space = currElem.attributeNS(XML_NSURI, "space");
        kDebug() << "Back to while loop in readXmlSpaceAttr: " << currElem.nodeName() << endl;
        
    } 
    
    return space;
}


QString readXmlLangAttr(const QDomElement &elem, bool recursive)
{

    QDomNode currNode;
    QDomElement currElem = elem;
    
    QString value = currElem.attributeNS(XML_NSURI, "lang");;
    
    if(!recursive)
        return value;
    
    while (value.isEmpty()){
    
        currNode = currElem.parentNode();
        if(currNode.isNull()){
            return value;
        }
        
        currElem = currNode.toElement();
        value = currElem.attributeNS(XML_NSURI, "space");
        kDebug() << "Back to while loop in readXmlLangAttr: " << currElem.nodeName() << endl;
        
    } 
    
    return value;
}


