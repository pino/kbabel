
#include <klocale.h>
#include <kcomponentdata.h>
#include <kaboutdata.h>
#include <kdebug.h>

#include "dbse_factory.h"
#include "KDBSearchEngine.h"

static KAboutData aboutData()
{
    KAboutData about("kdbsearchengine", QByteArray(),
                     ki18n("Translation Database"),
                     "0.3",
                     ki18n("A fast translation search engine based on databases"),
                     KAboutData::License_GPL,
                     ki18n("Copyright 2000-2001 by Andrea Rizzi"),
                     KLocalizedString(),
                     0,
                     "rizzi@kde.org");
    about.addAuthor(ki18n("Andrea Rizzi"), KLocalizedString(), "rizzi@kde.org");
    about.setProgramIconName("kbabeldict");
    return about;
}

K_PLUGIN_FACTORY_DEFINITION(DbSeFactory, registerPlugin<KDBSearchEngine>();)
K_EXPORT_PLUGIN(DbSeFactory(aboutData()))
