/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2002 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include <klocale.h>
#include <kcmdlineargs.h>
#include <k3staticdeleter.h>
#include <kdebug.h>
#include <kglobal.h>
#include <kcomponentdata.h>
#include <kio/netaccess.h>

#include <qtextstream.h>
#include <qtimer.h>
#include <QList>

#include "tmxcompendiumdata.h"
#include "tmxcompendium.h"
#include "preferenceswidget.h"
#include "pc_factory.h"

#include <catalogitem.h>

// ngram length and minimal matching for fuzzy search
#define NGRAM_LEN 3
#define LIM_NGRAM 50  

K_GLOBAL_STATIC(Q3Dict<TmxCompendiumData>, _compDict)

TmxCompendium::TmxCompendium(QObject *parent, const QVariantList& args)
        : SearchEngine(parent, args)
{
    prefWidget=0;
    data=0;
    error=false;
    stop=false;
    active=false;
    initialized=false;
    loading=false;

    langCode = KGlobal::locale()->language();

    caseSensitive = false;
    wholeWords=true;

    matchEqual = true;
    matchNGram = true;
    matchIsContained = false;
    matchContains = true;
    matchWords=true;


    loadTimer = new QTimer(this);
    connect(loadTimer,SIGNAL(timeout()),this,SLOT(slotLoadCompendium()));
}

TmxCompendium::~TmxCompendium()
{
    if(isSearching())
    {
        stopSearch();
    }

    unregisterData();
}

bool TmxCompendium::isReady() const
{
    return (isSearching() || !error);
}


bool TmxCompendium::isSearching() const
{
    return (active || loading);
}


void TmxCompendium::saveSettings(KConfigGroup &group)
{
    if(autoUpdate && prefWidget && prefWidget->settingsChanged())
    {
        applySettings();
    }
        
    group.writeEntry("CaseSensitive",caseSensitive);
    group.writeEntry("WholeWords", wholeWords);

    group.writeEntry("MatchEqual", matchEqual);
    group.writeEntry("MatchIsContained",matchIsContained);
    group.writeEntry("MatchContains", matchContains);
    group.writeEntry("MatchWords", matchWords);
    group.writeEntry("MatchNGram", matchNGram);  

    group.writeEntry("Compendium", url);
}

void TmxCompendium::readSettings(const KConfigGroup &group)
{    
    caseSensitive = group.readEntry("CaseSensitive", false);
    wholeWords = group.readEntry("WholeWords",true);

    matchEqual = group.readEntry("MatchEqual", true);
    matchIsContained = group.readEntry("MatchIsContained", false);
    matchContains = group.readEntry("MatchContains",true);
    matchWords = group.readEntry("MatchWords",true);
    matchNGram = group.readEntry("MatchNGram",true);  

    QString newPath = group.readEntry("Compendium","http://i18n.kde.org/po_overview/@LANG@.messages");
    if(!initialized)
    {
        url = newPath;
    }
    else if(newPath != url)
    {
        url = newPath;
        loadCompendium();
    }

            
    restoreSettings();
}

PrefWidget *TmxCompendium::preferencesWidget(QWidget *parent)
{
    prefWidget = new TmxCompendiumPreferencesWidget(parent);
    prefWidget->setObjectName("tmxcompendium_prefwidget");
    connect(prefWidget, SIGNAL(applySettings()), this, SLOT(applySettings()));
    connect(prefWidget, SIGNAL(restoreSettings())
                    , this, SLOT(restoreSettings()));

    restoreSettings();
    
    return prefWidget;
}

const KAboutData *TmxCompendium::about() const
{
    return PcFactory::componentData().aboutData();
}


QString TmxCompendium::name() const
{
    return i18n("TMX Compendium");
}

QString TmxCompendium::id() const
{
    return "tmxcompendium";
}

QString TmxCompendium::lastError()
{
    return errorMsg;
}


bool TmxCompendium::startSearch(const QString& text, uint pluralForm, const SearchFilter*)
{
    if(autoUpdate && prefWidget && prefWidget->settingsChanged())
    {
        applySettings();
    }
 
    if(isSearching())
        return false;

    clearResults();
    stop = false;
    active = true;

    if(!initialized)
    {
        if(loadTimer->isActive())
            loadTimer->stop();

        slotLoadCompendium();
    }
    
    if(error || !data)
    {
        active = false;
        return false;
    }

    if(data->active())
    {
        active = false;
        return true;
    }
   
    emit started();


    QList<int> foundIndices;
    QList<int> checkedIndices;
    uint checkCounter=0;
    
    const int *index = data->exactDict(text);
    if(index)
    {
        foundIndices.append(*index);

        SearchResult *result = new SearchResult;
        result->requested = text;
        result->found = QStringList(data->msgid(*index));
        result->translation = data->msgstr(*index);
        result->score = 100;

        TranslationInfo *info = new TranslationInfo;
        info->location = directory(realURL,0);
	// is this really necessary for a new allocated object?
        info->translator.clear();
        info->description.clear();
        result->descriptions.append(info);

        results.append(result);

        emit numberOfResultsChanged(results.count());
        emit resultFound(result);
    }

    QString searchStr=TmxCompendiumData::simplify(text);
    
           
    if(!caseSensitive)
    {
         searchStr = searchStr.toLower();
    }

    QString temp = searchStr;
    temp = temp.toLower();

    const QList<int> *indexList = data->allDict(temp);
    if(indexList)
    {
        QList<int>::ConstIterator it;
        for( it = indexList->begin(); it != indexList->end(); ++it )
        {
            if(foundIndices.contains(*it))
            {
                continue;
            }

            QString origStr = data->msgid(*it);
            origStr = TmxCompendiumData::simplify(origStr);
        
           
            if(!caseSensitive)
            {
                origStr = origStr.toLower();
            }

            if(origStr==searchStr)
            {
                foundIndices.append(*it);

                SearchResult *result = new SearchResult;
                result->requested = text;
                result->found = QStringList(data->msgid(*it));
                result->translation = data->msgstr(*it);
		// FIXME: handle plural forms properly
                result->score = score(result->requested,result->found.first());

                TranslationInfo *info = new TranslationInfo;
                info->location = directory(realURL,0);
                info->translator.clear();
                info->description.clear();
                result->descriptions.append(info);
 
                addResult(result);
            }
        }
    }
 
    QStringList wList = TmxCompendiumData::wordList(searchStr);
    for ( QStringList::Iterator wit = wList.begin()
               ; wit != wList.end(); ++wit ) 
    {
        if(stop)
            break;

        indexList = data->wordDict( (*wit).toLower() );
        if(indexList)
        {
            QList<int>::ConstIterator it;
            for( it = indexList->begin(); it != indexList->end(); ++it )
            {
                if(stop)
                    break;
                
                if(foundIndices.contains(*it))
                {
                    continue;
                }
              
                if(checkedIndices.contains(*it))
                {
                    continue;
                }
                
                checkedIndices.append(*it);
                checkCounter++;

                if( (100*(checkCounter+1))%data->numberOfEntries() < 100)
                {
                    emit progress( (100*(checkCounter+1))/data->numberOfEntries());
                }
            
                qApp->processEvents(QEventLoop::AllEvents, 100);

                QString origStr = data->msgid(*it);
                origStr = TmxCompendiumData::simplify(origStr);

           
                if(!caseSensitive)
                {
                    origStr = origStr.toLower();
                }

                bool found = false;
                if(matchWords)
                {
                    if(!caseSensitive)
                    {
                        found=true;
                    }
                    else
                    {
                        QString s=*wit;
                        QString o=origStr;
                        
                        if(wholeWords)
                        {
                            s=' '+*wit+' ';
                            o=' '+origStr+' ';
                        }
                        if(o.contains(s))
                        {
                            found=true;
                        }
                    }
                }

                
                if(!found && origStr==searchStr)
                {
                    found =true;
                }
                 
                // if there is an string which just contains ignored characters,
                // continue
                if(!found && origStr.isEmpty() || 
                    (origStr.length() == 1 && text.length() > 1))
                {
                    continue;
                }
            

                if(!found && matchContains && !wholeWords)
                {
                    QString s=maskString(searchStr);
                    QRegExp searchReg(s);

                    if( searchReg.indexIn( origStr ) >= 0 )
                        found=true;
                }
            
                if(!found && matchIsContained && !wholeWords)
                {
                    QString s=maskString(origStr);
                    QRegExp reg(s);

                    if( reg.indexIn( searchStr ) >= 0 )
                    {
                        found = true;
                    }
                }
                if(!found && matchWords && !wholeWords)
                {
                    QStringList list = TmxCompendiumData::wordList(searchStr);
                    
                    for ( QStringList::Iterator wit2 = list.begin()
                       ; wit2 != list.end(); ++wit2 ) 
                    {
                        QString s=maskString(*wit2);
                        QRegExp reg(s);

                        if( reg.indexIn( origStr ) >= 0 )
                        {
                            found = true;
                        }
                    }
                }

                if(found)
                {
                    foundIndices.append(*it);

                    SearchResult *result = new SearchResult;
                    result->requested = text;
                    result->found = QStringList(data->msgid(*it));
                    result->translation = data->msgstr(*it);
		    // FIXME: handle plural forms properly
                    result->score = score(result->requested,(result->found.first()));

                    TranslationInfo *info = new TranslationInfo;
                    info->location = directory(realURL,0);
                    info->translator.clear();
                    info->description.clear();
                    result->descriptions.append(info);
 
                    addResult(result);
                }
            }
        }
    }
 
    
    if( matchNGram || 
	(!wholeWords && (matchContains || matchIsContained || matchWords))
	)
      {
        QRegExp searchReg;
        if(matchContains)
        {
            QString s=maskString(searchStr);
            searchReg.setPattern(s);
        }

        
        bool breakLoop=false;
        int i=-1;
        while(!breakLoop)    
        {
            if(stop)
            {
                breakLoop=true;
                break;
            }

            i++;
            
            if(checkedIndices.contains(i))
            {
                continue;
            }

            checkedIndices.append(i);
            checkCounter++;
  
            if(foundIndices.contains(i))
            {
                 continue;
            }
       
            qApp->processEvents(QEventLoop::AllEvents, 100);

            if(i >= data->numberOfEntries())
            {
                 breakLoop = true;
                 break;
            }
        
            if( (100*(checkCounter+1))%data->numberOfEntries() < 100)
            {
                emit progress( (100*(checkCounter+1))/data->numberOfEntries());
            }

             QString origStr = data->msgid(i);
             origStr = TmxCompendiumData::simplify(origStr);
            
             if(!caseSensitive)
             {
                 origStr = origStr.toLower();
             }

 
            // if there is an string which just contains ignored characters,
            // continue
            if(origStr.isEmpty() || 
                    (origStr.length() == 1 && text.length() > 1))
            {
                continue;
            }
            
            bool found = false;
            if( matchContains && searchReg.indexIn( origStr ) >= 0 )
            {
                found=true;
            }

                    
            if(!found && matchIsContained)
            {
                QString s=maskString(origStr);
                QRegExp reg(s);

                if( reg.indexIn( searchStr ) >= 0 )
                {
                    found = true;
                }
            }
            if(!found && matchWords)
            {
                QStringList list = TmxCompendiumData::wordList(searchStr);
                    
                for ( QStringList::Iterator wit2 = list.begin()
                   ; wit2 != list.end(); ++wit2 ) 
                {
                    QString s=maskString(*wit2);

                    if(wholeWords)
                    {
                        origStr = ' '+origStr+' ';
                        s=' '+s+' ';
                    }
                    QRegExp reg(s);
                    
                    if( reg.indexIn( origStr ) >= 0 )
                    {
                        found = true;
                    }
                }
             }

	    if(!found && matchNGram)
	      {
		// to get more results one could
		// interchange searchStr and origStr when
		// the latter is shorter

		found = ( ngramMatch(searchStr,origStr,NGRAM_LEN) 
			  > LIM_NGRAM );
	      }
       
            if(found)
            {
                 foundIndices.append(i);

                 SearchResult *result = new SearchResult;
                 result->requested = text;
                 result->found = QStringList(data->msgid(i));
                 result->translation = data->msgstr(i);
		 // FIXME: handle plural forms properly
                 result->score = score(result->requested,(result->found.first()));

                 TranslationInfo *info = new TranslationInfo;
                 info->location = directory(realURL,0);
                 info->translator.clear();
                 info->description.clear();
                 result->descriptions.append(info);
    
                 addResult(result);
             }
        }
    } 
    
    if( (100*(checkCounter+1))/data->numberOfEntries() < 100 && !stop)
    {
        for(int i=(100*(checkCounter+1))/data->numberOfEntries(); i<=100; i++)
        {
            emit progress(i);
        }
    }
    

    active = false;
    stop = false;
    emit finished();
    
    return true;
}

void TmxCompendium::stopSearch()
{
    stop=true;
}


void TmxCompendium::applySettings()
{    
    if(!prefWidget)
        return;
    
    if(isSearching())
        stopSearch();
    
    caseSensitive = prefWidget->caseSensitive();
    wholeWords = prefWidget->wholeWords();

    matchEqual = prefWidget->matchEqual();
    matchNGram = prefWidget->matchNGram();
    matchIsContained = prefWidget->matchIsContained();
    matchContains = prefWidget->matchContains();
    matchWords = prefWidget->matchWords();

    
    bool needLoading=false;
    
    
    QString newPath = prefWidget->url();
    if(!initialized)
    {
        url = newPath;
    }
    else if(newPath != url)
    {
        url = newPath;
        needLoading = true;
    }

    if(needLoading)
    {
        loadCompendium();
        initialized=false;
    }
}

void TmxCompendium::restoreSettings()
{
    if(!prefWidget)
        return;
    
    prefWidget->setCaseSensitive(caseSensitive);
    prefWidget->setWholeWords(wholeWords);
    prefWidget->setURL(url);

    prefWidget->setMatchEqual(matchEqual);
    prefWidget->setMatchNGram(matchNGram);
    prefWidget->setMatchIsContained(matchIsContained);
    prefWidget->setMatchContains(matchContains);
    prefWidget->setMatchWords(matchWords);
}
    
void TmxCompendium::loadCompendium()
{
    if( !loading && !loadTimer->isActive() ) {
        loadTimer->setSingleShot( true );
        loadTimer->start( 100 );
    }
}

void TmxCompendium::slotLoadCompendium()
{
    if(loading)
        return;

    if(loadTimer->isActive())
        loadTimer->stop();
    
    loading = true;

    if(data)
    {
        unregisterData();
    }


    QString path=url;
   
    if(path.contains("@LANG@"))
    {
        path.replace("@LANG@",langCode);
    }
    KUrl u = KCmdLineArgs::makeURL( path.toLocal8Bit() );
    realURL = u.url();
  
    registerData();
    

    if(!data)
    {
        kError() << "no data object in tmxcompendium?";

        loading=false;
        return;
    }

    if(!data->initialized())
    {
        if(!data->active())
        {
            data->load(u,langCode);
            recheckData();
            if(error)
            {
                emit hasError(errorMsg);
            }
        }
        else
        {
            connect(data, SIGNAL(progressEnds()), this, SLOT(recheckData()));
        }
    }
    else
    {
        recheckData();
        if(error)
        {
            emit hasError(errorMsg);
        }
    }

    initialized=true;
}

void TmxCompendium::recheckData()
{
    if(data)
    {
        disconnect(data, SIGNAL(progressEnds()), this, SLOT(recheckData()));

        error = data->hasErrors();
        errorMsg = data->errorMsg();
    }

    loading=false;
}

QString TmxCompendium::maskString(QString s) const
{
    s.replace('\\',"\\\\");
    s.replace('?',"\\?");
    s.replace('[',"\\[");
    s.replace('.',"\\.");
    s.replace('*',"\\*");
    s.replace('+',"\\+");
    s.replace('^',"\\^");
    s.replace('$',"\\$");
    s.replace('(',"\\(");
    s.replace(')',"\\)");
    s.replace('{',"\\{");
    s.replace('}',"\\}");
    s.replace('|',"\\|");
    
    return s;
}

void TmxCompendium::addResult(SearchResult *result)
{
    if(!results.isEmpty() && results.last()->score >= result->score)
    {
        results.append(result);
    }
    else
    {
        QList<SearchResult *>::Iterator it = results.begin();
        while(it != results.end())
        {
            if((*it)->score < result->score)
            {
                it = results.insert(it, result);
                emit resultsReordered();
                break;
            }
            ++it;
        }

        if(it == results.end())
        {
             results.append(result);
        }
    }

    emit numberOfResultsChanged(results.count());
    emit resultFound(result);
}


void TmxCompendium::setLanguageCode(const QString& lang)
{   
    if(initialized && url.contains("@LANG@") && lang!=langCode 
            && !loadTimer->isActive() )
    {
        initialized=false;
    }
    
    langCode=lang;
}

QString TmxCompendium::translate(const QString& text, uint pluralForm)
{
    if(!initialized)
    {
        if(loadTimer->isActive())
            loadTimer->stop();

        slotLoadCompendium();
    }
    
    if(error || !data || data->active())
    {
        return QString();
    }
   

    const int *index = data->exactDict(text);

    if(index)
    {
        return data->msgstr(*index);
    }
   
    return QString();
}

QString TmxCompendium::fuzzyTranslation(const QString& text, int &score, uint pluralForm)
{
  if(!initialized)
    {
      if(loadTimer->isActive())
	loadTimer->stop();

      slotLoadCompendium();
    }
    
  if(error || !data || data->active())
    {
      return QString();
    }
    
  // try to find fuzzy string
  bool breakLoop = false;
  stop = false;
  int i=-1;
  int best_matching = -1;
  int best_match = 0;
  int total = data->numberOfEntries();
 
  QString searchStr = TmxCompendiumData::simplify(text);

  //kDebug(750) << "find best match for " << searchStr;

  while(!breakLoop)
    {

      // progress and loop control
      if(stop)
	{
	  breakLoop=true;
	  break;
	}

      i++;

      if(i >= total)
	{
	  breakLoop = true;
	  break;
	}

      if( (100*(i+1))%total < 100)
	{
	  emit progress( (100*(i+1))/total);
	}

      // get a message from the catalog
      QString origStr = data->msgid(i);
      origStr = TmxCompendiumData::simplify(origStr);

      // don't match too long strings for short search string
      if (origStr.length() > 2*searchStr.length())
	continue;
      //      kDebug(750) << i << ": matching " << origStr;

      int ngram_result = ngramMatch(searchStr,origStr);

      if (ngram_result > best_match) {
	best_match = ngram_result;
	best_matching = i;

	// kDebug(750) << "[" << ngram_result << "] " << text << "-"
	// << origStr;
      }
    }

  if (best_match > LIM_NGRAM) {
    score = best_match;
    return data->msgstr(best_matching);
  }
 
  return QString();
}


void TmxCompendium::unregisterData()
{
    if(data)
    { 
        disconnect(data, SIGNAL(progressStarts(const QString&)), this
            , SIGNAL(progressStarts(const QString&)));
        disconnect(data, SIGNAL(progressEnds()), this , SIGNAL(progressEnds()));
        disconnect(data, SIGNAL(progress(int)), this , SIGNAL(progress(int)));

        if(data->active())
        {
            disconnect(data,SIGNAL(progressEnds()),this,SLOT(recheckData()));
        }

        if(data->unregisterObject(this))
        {
            if(!data->active())
            {
                compendiumDict()->remove(realURL);
            }
            else
            {
                connect(data,SIGNAL(progressEnds()),this,SLOT(removeData()));
            }
        }

        data=0;
    }
}

void TmxCompendium::registerData()
{
    data = compendiumDict()->find(realURL);
    if(!data)
    {
        data = new TmxCompendiumData;
        compendiumDict()->insert(realURL,data);
    }

    data->registerObject(this);
    
    if(data->active())
    {
        emit progressStarts(i18n("Loading PO compendium"));
    }
    
    connect(data, SIGNAL(progressStarts(const QString&)), this
            , SIGNAL(progressStarts(const QString&)));
    connect(data, SIGNAL(progressEnds()), this , SIGNAL(progressEnds()));
    connect(data, SIGNAL(progress(int)), this , SIGNAL(progress(int)));
}

void TmxCompendium::removeData()
{
    const QObject *s=sender();
    if(s && s->inherits("TmxCompendiumData"))
    {
        const TmxCompendiumData *d=static_cast<const TmxCompendiumData*>(s);
        if(d)
        {
            Q3DictIterator<TmxCompendiumData> it(*compendiumDict());
            while(it.current())
            {
                if(it.current() == d)
                {
                    if(!d->hasObjects())
                    {
                        compendiumDict()->remove(it.currentKey());
                    }

                    break;
                }

                ++it;
            }
        }
        
    }
}

Q3Dict<TmxCompendiumData> *TmxCompendium::compendiumDict()
{
    if(!_compDict.exists())
    {
        _compDict->setAutoDelete(true);
    }

    return _compDict;
}



#include "tmxcompendium.moc"
