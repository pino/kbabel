/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2002 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include <qcheckbox.h>
#include <qlabel.h>
#include <qlayout.h>
#include <QVBoxLayout>

#include <kfiledialog.h>
#include <qpushbutton.h>

#include <klineedit.h>
#include <klocale.h>
#include <kurlrequester.h>

#include "preferenceswidget.h"

TmxCompendiumPreferencesWidget::TmxCompendiumPreferencesWidget(QWidget *parent)
		: PrefWidget(parent)
		, changed(false)
{
	setupUi( this );
	layout()->setMargin(0);

	connect(caseBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(equalBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(ngramBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(isContainedBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(containsBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(hasWordBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));
	connect(wholeBtn, SIGNAL(toggled(bool))
					, this, SLOT(setChanged()));

	connect(urlInput->lineEdit(),SIGNAL(textChanged(const QString&))
					, this, SLOT(setChanged()));

	connect(equalBtn, SIGNAL(toggled(bool))
					, this, SLOT(equalBtnToggled(bool)));
	connect(ngramBtn, SIGNAL(toggled(bool))
					, this, SLOT(ngramBtnToggled(bool)));
	connect(isContainedBtn, SIGNAL(toggled(bool))
					, this, SLOT(isContainedBtnToggled(bool)));
	connect(containsBtn, SIGNAL(toggled(bool))
					, this, SLOT(containsBtnToggled(bool)));
	connect(hasWordBtn, SIGNAL(toggled(bool))
					, this, SLOT(hasWordBtnToggled(bool)));


	QString whatsthis=i18n("<qt><p><b>Parameters</b></p>"
		"<p>Here you can fine-tune searching within the PO file. "
		"For example, if you want to perform a case sensitive search.</p></qt>" );
	caseBtn->setWhatsThis(whatsthis);
	wholeBtn->setWhatsThis(whatsthis);

	whatsthis = i18n("<qt><p><b>Comparison Options</b></p>"
		"<p>Choose here which messages you want to have treated as a matching "
		"message.</p></qt>");
	equalBtn->setWhatsThis(whatsthis);
	containsBtn->setWhatsThis(whatsthis);
	isContainedBtn->setWhatsThis(whatsthis);
	hasWordBtn->setWhatsThis(whatsthis);

	whatsthis = i18n("<qt><p><b>3-Gram-matching</b></p>"
			 "<p>A message matches another if most of its 3-letter groups are "
			 "contained in the other message. e.g. 'abc123' matches 'abcx123c12'.</p></qt>");
	ngramBtn->setWhatsThis(whatsthis);

	whatsthis = i18n("<qt><p><b>Location</b></p>"
					"<p>Configure here which file is to be used for searching."
					"</p></qt>");
	urlInput->setWhatsThis(whatsthis);
}

TmxCompendiumPreferencesWidget::~TmxCompendiumPreferencesWidget()
{
}


void TmxCompendiumPreferencesWidget::apply()
{
	emit applySettings();
}

void TmxCompendiumPreferencesWidget::cancel()
{
	emit restoreSettings();
}

void TmxCompendiumPreferencesWidget::standard()
{
	urlInput->setUrl(KUrl("http://i18n.kde.org/po_overview/@LANG@.messages"));
	caseBtn->setChecked(false);
	equalBtn->setChecked(true);
	ngramBtn->setChecked(true);
	isContainedBtn->setChecked(false);
	containsBtn->setChecked(false);
	wholeBtn->setChecked(true);
	hasWordBtn->setChecked(true);

	changed=true;
}

void TmxCompendiumPreferencesWidget::setURL(const QString url)
{
	urlInput->setUrl(url);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setCaseSensitive(bool on)
{
	caseBtn->setChecked(on);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setMatchEqual(bool on)
{
	equalBtn->setChecked(on);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setMatchNGram(bool on)
{
	ngramBtn->setChecked(on);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setMatchIsContained(bool on)
{
	isContainedBtn->setChecked(on);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setMatchContains(bool on)
{
	containsBtn->setChecked(on);
	changed=false;
}

void TmxCompendiumPreferencesWidget::setWholeWords(bool on)
{
	wholeBtn->setChecked(on);
	changed=false;
}


void TmxCompendiumPreferencesWidget::setMatchWords(bool on)
{
	hasWordBtn->setChecked(on);
	changed=false;
}



QString TmxCompendiumPreferencesWidget::url()
{
	changed=false;
	return urlInput->url().url();
}

bool TmxCompendiumPreferencesWidget::caseSensitive()
{
	changed=false;

	return caseBtn->isChecked();
}

bool TmxCompendiumPreferencesWidget::matchEqual()
{
	changed=false;

	return equalBtn->isChecked();
}

bool TmxCompendiumPreferencesWidget::matchNGram()
{
	changed=false;

	return ngramBtn->isChecked();
}

bool TmxCompendiumPreferencesWidget::matchIsContained()
{
	changed=false;

	return isContainedBtn->isChecked();
}

bool TmxCompendiumPreferencesWidget::matchContains()
{
	changed=false;

	return containsBtn->isChecked();
}

bool TmxCompendiumPreferencesWidget::wholeWords()
{
	changed=false;

	return wholeBtn->isChecked();
}


bool TmxCompendiumPreferencesWidget::matchWords()
{
	changed=false;

	return hasWordBtn->isChecked();
}



bool TmxCompendiumPreferencesWidget::settingsChanged() const
{
	return changed;
}

void TmxCompendiumPreferencesWidget::setChanged()
{
	changed=true;
}


void TmxCompendiumPreferencesWidget::equalBtnToggled(bool on)
{
	if(!on)
	{
		if(!isContainedBtn->isChecked()
		   && !ngramBtn->isChecked()
		   && !containsBtn->isChecked()
		   && !hasWordBtn->isChecked())
		{
			equalBtn->setChecked(true);
		}
	}
}

void TmxCompendiumPreferencesWidget::ngramBtnToggled(bool on)
{
	if(!on)
	{
		if(!isContainedBtn->isChecked()
		   && !equalBtn->isChecked()
		   && !containsBtn->isChecked()
		   && !hasWordBtn->isChecked())
		{
		  equalBtn->setChecked(true);
		}
	}
}

void TmxCompendiumPreferencesWidget::isContainedBtnToggled(bool on)
{
	if(!on)
	{
		if(!equalBtn->isChecked()
		   && !ngramBtn->isChecked()
		   && !containsBtn->isChecked()
		   && !hasWordBtn->isChecked())
		{
		  isContainedBtn->setChecked(true);
		}
	}
}

void TmxCompendiumPreferencesWidget::containsBtnToggled(bool on)
{
	if(!on)
	{
		if(!isContainedBtn->isChecked()
		   && !ngramBtn->isChecked()
		   && !equalBtn->isChecked()
		   && !hasWordBtn->isChecked())
		{
		  containsBtn->setChecked(true);
		}
	}
}

void TmxCompendiumPreferencesWidget::hasWordBtnToggled(bool on)
{
	if(!on)
	{
		if(!isContainedBtn->isChecked()
		   && !ngramBtn->isChecked()
		   && !equalBtn->isChecked()
		   && !containsBtn->isChecked())
		{
		  hasWordBtn->setChecked(true);
		}
	}
}



#include "preferenceswidget.moc"
