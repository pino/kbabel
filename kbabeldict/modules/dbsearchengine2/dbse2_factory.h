#ifndef DBSE2_FACTORY_H
#define DBSE2_FACTORY_H

#include <klibloader.h>
class KComponentData;
class KAboutData;

class DbSe2Factory : public KLibFactory
{
	Q_OBJECT
public:
	explicit DbSe2Factory( QObject *parent=0, const char *name=0);
	~DbSe2Factory();

	virtual QObject *createObject( QObject *parent=0, const char *name=0,
	                               const char *classname="QObject",
	                               const QStringList &args = QStringList());

	static KComponentData componentData();

private: 
	static KComponentData s_instance;
	static KAboutData *s_about;
};

#endif
