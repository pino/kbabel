
#include <klocale.h>
#include <kcomponentdata.h>
#include <kaboutdata.h>
#include <kdebug.h>

#include "dbse2_factory.h"
#include "KDBSearchEngine2.h"
//Added by qt3to4:


extern "C"
{
	KDE_EXPORT void *init_kbabeldict_dbsearchengine2()
//	void *init_libdbsearchengine2()
	{
		return new DbSe2Factory;
	}
};


KComponentData DbSe2Factory::s_instance = 0;
KAboutData *DbSe2Factory::s_about = 0;


DbSe2Factory::DbSe2Factory( QObject *parent, const char *name)
		: KLibFactory(parent,name)
{
}

DbSe2Factory::~DbSe2Factory()
{
	if(s_instance)
	{
		delete s_instance;
		s_instance=0;
	}

	if(s_about)
	{
		delete s_about;
		s_about=0;
	}
}


QObject *DbSe2Factory::createObject( QObject *parent, const char *name,
				    const char *classname, const QStringList &)
{
	if(QByteArray(classname) != "SearchEngine")
	{
		kError() << "not a SearchEngine requested" << endl;
		return 0;
	}
	
	KDBSearchEngine2 *se = new KDBSearchEngine2(parent,name);

	emit objectCreated(se);
	return se;
}


KComponentData DbSe2Factory::componentData()
{
	if(!s_instance)
	{

		s_about = new KAboutData( "kdbsearchengine2", 0,
			      ki18n("Translation Database")
				, "1.99"	,
ki18n("A fast translation search engine based on databases")
						, KAboutData::License_GPL
						, ki18n("Copyright 2000-2003 by Andrea Rizzi")
						,KLocalizedString(),0, "rizzi@kde.org");

		s_about->addAuthor(ki18n("Andrea Rizzi"),KLocalizedString(),"rizzi@kde.org");

		s_instance(s_about);
	}

	return s_instance;
}

#include "dbse2_factory.moc"
