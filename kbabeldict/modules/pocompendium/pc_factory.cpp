/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include <klocale.h>
#include <kcomponentdata.h>
#include <kaboutdata.h>
#include <kdebug.h>

#include "pc_factory.h"
#include "pocompendium.h"


static KAboutData aboutData()
{
    KAboutData about("pocompendium", QByteArray(),
                     ki18n("PO Compendium"),
                     "1.0",
                     ki18n("A module for searching in a PO file"),
                     KAboutData::License_GPL,
                     ki18n("Copyright 2000-2001, Matthias Kiefer"),
                     KLocalizedString(),
                     0,
                     "kiefer@kde.org");
    about.addAuthor(ki18n("Matthias Kiefer"), KLocalizedString(), "kiefer@kde.org");
    about.setProgramIconName("kbabeldict");
    return about;
}

K_PLUGIN_FACTORY_DEFINITION(PcFactory, registerPlugin<PoCompendium>();)
K_EXPORT_PLUGIN(PcFactory(aboutData()))
