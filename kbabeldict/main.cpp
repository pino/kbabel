/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2003 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than  
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.
		  
**************************************************************************** */
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <kcursor.h>
#include <klocale.h>
#include <kapplication.h>
#include <kwindowsystem.h>

#include <qtimer.h>

#include "kbabeldict.h"
#include <version.h>

class KBabelDictApplication : public KApplication
{
public:
    KBabelDictApplication();
    ~KBabelDictApplication();

private:
    KBabelDict *topLevel;
};

KBabelDictApplication::KBabelDictApplication()
    : KApplication()
    , topLevel(0)
{
    KApplication::setOverrideCursor(Qt::WaitCursor);
	
    topLevel = new KBabelDict();

	setTopWidget(topLevel);
    topLevel->show();

    KApplication::restoreOverrideCursor();

    QObject::connect( topLevel, SIGNAL( destroyed() ),
	     this, SLOT( quit() ) );
}

KBabelDictApplication::~KBabelDictApplication()
{
    delete(topLevel);
}


int main(int argc, char **argv)
{
    KLocale::setMainCatalog("kbabel");
    
    KAboutData about("kbabeldict", 0,ki18n("KBabel - Dictionary"),KBABEL_VERSION,
       ki18n("A dictionary for translators"),KAboutData::License_GPL,
       ki18n("(c) 2000,2001,2002,2003 The KBabeldict developers"),KLocalizedString(),"http://kbabel.kde.org");

    about.addAuthor(ki18n("Matthias Kiefer"),ki18n("Original author"),"kiefer@kde.org");
    about.addAuthor(ki18n("Stanislav Visnovsky"),ki18n("Current maintainer"),"visnovsky@kde.org");

    about.setTranslator(ki18nc("NAME OF TRANSLATORS", "Your names"),
	ki18nc("EMAIL OF TRANSLATORS", "Your emails"));

    // Initialize command line args
    KCmdLineArgs::init(argc, argv, &about);

    // Tell which options are supported

    KCmdLineOptions options;
    options.add("nosplash", ki18n("Disable splashscreen at startup"));
    KCmdLineArgs::addCmdLineOptions( options );

    // Add options from other components
    KCmdLineArgs::addStdCmdLineOptions();
    
    KBabelDictApplication app;

    return app.exec();
}
