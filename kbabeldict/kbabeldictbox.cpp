/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 2000 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2003-2005 by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */


#include <k3aboutapplication.h>

#include <kapplication.h>
#include "kbabeldictbox.h"
#include <version.h>
#include <resources.h>

#include <kaboutdata.h>
#include <k3aboutdialog.h>
#include <kconfig.h>
#include <kdialog.h>
#include <k3listview.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kstandarddirs.h>
#include <kglobal.h>
#include <kdebug.h>
#include <kwindowsystem.h>
#include <kservicetypetrader.h>
#include <kaboutapplicationdialog.h>

#include <qclipboard.h>
#include <qdir.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qpushbutton.h>
#include <q3textview.h>
#include <q3stylesheet.h>
#include <qtoolbutton.h>
#include "kbabeldictadaptor.h"
#include "kbabel_interface.h"

#include <qtimer.h>
#include <QWheelEvent>
#include <QGridLayout>
#include <Q3PtrList>
#include <QEvent>
#include <QHBoxLayout>
#include <QList>
#include <QVBoxLayout>
#include <ktoolinvocation.h>
#include <QSplitter>
#include <QtDBus>
#include <QTextDocument>
#include <kconfiggroup.h>
#define KBABELDICT 5321

using namespace KBabel;

class ResultListItem : public Q3ListViewItem
{
public:
    ResultListItem(Q3ListView *parent, const SearchResult& result,bool richText);

    virtual QString key(int column, bool ascending) const;
    const SearchResult* result() const;
    bool richText() const { return _richText; }

private:
    SearchResult _result;
    bool _richText;
};

ResultListItem::ResultListItem(Q3ListView *parent, const SearchResult& result
                , bool richText)
        : Q3ListViewItem(parent)
        , _result(result)
        , _richText(richText)
{
    int score=_result.score;
    if(score<0)
        score=0;
    else if(score>100)
        score=100;
    setText(0,QString::number(score));

    QString tmp;
    if(richText)
        tmp=_result.plainFound;
    else
	// FIXME: what about plural forms?
        tmp=result.found.first();

    bool cutted=false;
    int index = tmp.indexOf( '\n' );
    if(index > 0)
    {
        tmp=tmp.left(index);
        cutted=true;
    }
    if(tmp.length() > 30)
    {
        tmp=tmp.left(30);
        cutted=true;
    }
    tmp = tmp.trimmed();
    if(cutted)
        tmp+="...";

    setText(1,tmp);

    if(richText)
        tmp=_result.plainTranslation;
    else
        tmp=result.translation;

    cutted=false;
    index = tmp.indexOf( '\n' );
    if(index > 0)
    {
        tmp=tmp.left(index);
        cutted=true;
    }
    if(tmp.length() > 30)
    {
        tmp=tmp.left(30);
        cutted=true;
    }
    tmp = tmp.trimmed();
    if(cutted)
        tmp+="...";
    setText(2,tmp);


    if(!_result.descriptions.isEmpty())
    {
        TranslationInfo *ti = _result.descriptions.first();
        if(ti)
        {
            setText(3,ti->location);
        }
    }

}

QString ResultListItem::key(int column, bool ascending) const
{
    if(column==0)
    {
        QString result=QString::number(_result.score);
        result = result.rightJustified( 10, '0' );

        return result;
    }

    return Q3ListViewItem::key(column,ascending);
}

const SearchResult *ResultListItem::result() const
{
    return &_result;
}

/*
 *  Constructs a KBabelDictBox which is a child of 'parent'
 */
KBabelDictBox::KBabelDictBox( QWidget* parent )
    : QWidget( parent )
{
    new KbabeldictAdaptor(this);
    QDBusConnection::sessionBus().registerObject("/KBabelDict", this);

   active=-1;
   currentResult=0;
   currentInfo=0;


   QVBoxLayout *mainLayout = new QVBoxLayout(this);
   mainLayout->setMargin(0);

   QGridLayout *grid = new QGridLayout();
   mainLayout->addLayout( grid );

   QHBoxLayout *hbox = new QHBoxLayout;
   QLabel *label = new QLabel(i18n("Total:"),this);
   hbox->addWidget(label);
   totalResultsLabel = new QLabel("0",this);
   hbox->addWidget(totalResultsLabel);
   grid->addLayout(hbox,0,0);

   hbox = new QHBoxLayout;
   label = new QLabel(i18n("Current:"), this);
   hbox->addWidget(label);
   currentLabel = new QLabel("0",this);
   hbox->addWidget(currentLabel);
   grid->addLayout(hbox,1,0);


   hbox = new QHBoxLayout;
   label = new QLabel(i18n("Found in:"), this);
   hbox->addWidget(label);
   locationLabel = new QLabel(this);
   hbox->addWidget(locationLabel);
   hbox->setStretchFactor(locationLabel,2);
   grid->addLayout(hbox,0,1);

   hbox = new QHBoxLayout;
   label = new QLabel(i18n("Translator:"), this);
   hbox->addWidget(label);
   translatorLabel = new QLabel(this);
   translatorLabel->setMinimumSize(50,0);
   hbox->addWidget(translatorLabel);
   hbox->setStretchFactor(translatorLabel,2);
   grid->addLayout(hbox,1,1);

   grid->setColumnStretch( 1, 2 );


   hbox = new QHBoxLayout;
   label = new QLabel(i18n("Date:"),this);
   hbox->addWidget(label);
   dateLabel = new QLabel(this);
   dateLabel->setMinimumSize(50,0);
   hbox->addWidget(dateLabel);
   hbox->setStretchFactor(dateLabel,2);

   moreButton = new QPushButton( this );
   moreButton->setObjectName( "moreButton" );
   moreButton->setText(i18n("&More"));
   moreButton->setEnabled(false);
   moreButton->setAutoRepeat(true);
   hbox->addWidget(moreButton);

   mainLayout->addLayout(hbox);


   hbox = new QHBoxLayout;
   hbox->addStretch(1);
   listButton = new QToolButton(this);
   listButton->setArrowType(Qt::UpArrow);
   listButton->setFixedSize(20,15);
   listButton->setAutoRepeat(false);
   connect(listButton,SIGNAL(clicked()),this,SLOT(showListOnly()));
   hbox->addWidget(listButton);
   detailButton = new QToolButton(this);
   detailButton->setArrowType(Qt::DownArrow);
   detailButton->setFixedSize(20,15);
   detailButton->setAutoRepeat(false);
   connect(detailButton,SIGNAL(clicked()),this,SLOT(showDetailsOnly()));
   hbox->addWidget(detailButton);

   mainLayout->addLayout(hbox);


   resultSplitter = new QSplitter( Qt::Vertical, this );
   resultSplitter->setObjectName( "resultsplitter" );
   mainLayout->addWidget(resultSplitter);

   viewContainer = new QSplitter( Qt::Vertical, resultSplitter );
   viewContainer->setObjectName( "singleEntrySplitter" );
   QVBoxLayout *vbox = new QVBoxLayout(viewContainer);
   vbox->setSizeConstraint( QLayout::SetNoConstraint );
   origView = new Q3TextView(viewContainer,"origView");
   origView->setWordWrap( Q3TextEdit::WidgetWidth );
   origView->setMinimumSize(1,1);
   vbox->addWidget(origView);
   translationView = new Q3TextView(viewContainer,"translationView");
   translationView->setWordWrap( Q3TextEdit::WidgetWidth );
   translationView->setMinimumSize(1,1);
   vbox->addWidget(translationView);
   viewContainer->setMinimumSize(1,1);

   resultListView = new K3ListView( resultSplitter );
   resultListView->setObjectName("resultListView");
   resultListView->setMinimumSize(1,1);
   resultListView->addColumn( i18n( "Score" ) );
   resultListView->addColumn( i18n( "Original" ) );
   resultListView->addColumn( i18n( "Translation" ) );
   resultListView->addColumn( i18n( "Location" ) );

   resultListView->installEventFilter(this);
   connect(resultListView
           , SIGNAL(doubleClicked(Q3ListViewItem *,const QPoint&,int))
           , this, SLOT(editFile()));
   connect(resultListView, SIGNAL(returnPressed(Q3ListViewItem *))
           , this, SLOT(editFile()));
   connect(resultListView
           , SIGNAL(contextMenu(K3ListView *,Q3ListViewItem *,const QPoint&))
           , this
           , SLOT(showContextMenu(K3ListView *,Q3ListViewItem *,const QPoint&)));

   resultSplitter->setStretchFactor( /*viewContainer*/ 0, 0 );
   QList<int> sizes;
   sizes.append(50);
   sizes.append(50);
   resultSplitter->setSizes(sizes);


   hbox = new QHBoxLayout;
   hbox->addStretch(1);

   prevButton = new QPushButton(i18n("< &Previous"),this);
   prevButton->setEnabled(false);
   prevButton->setAutoRepeat(true);
   hbox->addWidget(prevButton);

   nextButton = new QPushButton(i18n("&Next >"),this);
   nextButton->setEnabled(false);
   nextButton->setAutoRepeat(true);
   hbox->addWidget(nextButton);

   hbox->addStretch(1);
   mainLayout->addLayout(hbox);

   totalResultsLabel->setNum(100000);
   totalResultsLabel->setFixedSize(totalResultsLabel->sizeHint());
   totalResultsLabel->setNum(0);
   currentLabel->setNum(100000);
   currentLabel->setFixedSize(currentLabel->sizeHint());
   currentLabel->setNum(0);

   setRMBMenu( new QMenu( this ) );
   QStringList fileList;
#if 0
   // try to find installed modules by looking into directories
   // kbabeldict/modules and getting all files *.rc
   QStringList dirList = KGlobal::dirs()->findDirs("data"
                                    ,"kbabeldict/modules");

   for ( QStringList::Iterator it = dirList.begin(); it != dirList.end()
                                             ; ++it )
   {
      QDir dir((*it),"*.rc");
      QStringList list = dir.entryList(QDir::Files|QDir::Readable);

      for ( QStringList::Iterator fit = list.begin(); fit != list.end()
                                             ; ++fit )
      {
         if(!fileList.contains((*fit)))
         {
            fileList.append((*fit));
         }
      }
   }
#endif

   // use locate to locate the actual file, because rcfiles in the users
   // directory is preferred for systemwide rc files
   QStringList rcList;
   for( QStringList::Iterator fit = fileList.begin(); fit != fileList.end();
                                             ++fit)
   {
      rcList.append(KStandardDirs::locate("data","kbabeldict/modules/"+(*fit)));
   }

   for( QStringList::Iterator rit = rcList.begin(); rit != rcList.end();
                                             ++rit)
   {
      KConfig rcConfig(( *rit), KConfig::NoGlobals );

      KConfigGroup group = rcConfig.group("SearchEngine");

      QStringList appList = group.readEntry("Applications",QStringList());
      KComponentData inst = KGlobal::mainComponent();
      if(inst.isValid() && !appList.isEmpty() && !appList.contains(inst.componentName()))
      {
          continue;
      }

      QString libName = group.readEntry("Lib");

      if(!libName.isNull())
      {
         kDebug(KBABELDICT) << "loading library " << libName;

         KPluginLoader loader( libName );
         KPluginFactory *factory = loader.factory();

         if(factory)
         {
            SearchEngine *e = factory->create<SearchEngine>(this);
            if(!e)
            {
               kError() << "searchengine not initialized";
            }
            else
            {
                e->setObjectName("searchengine");
		registerModule(e);
            }
         }
         else
         {
            kError() << "wasn't able to load library";
         }

      }
   }

   kDebug(KBABEL_SEARCH) << "Now using trader for " << KGlobal::mainComponent().componentName();

   // try to find installed modules by KTrader
   KService::List  offers = KServiceTypeTrader::self()->query("KBabelDictModule",
	    "('"+KGlobal::mainComponent().componentName()+"' in [Applications])");

   for(KService::List::ConstIterator it = offers.begin(); it != offers.end(); ++it )
   {
      QString error;
      SearchEngine *e = (*it)->createInstance<SearchEngine>(this, QVariantList(), &error);
      if(!e)
      {
         kError() << "wasn't able to load" << (*it)->library() << error;
      }
      else
      {
         e->setObjectName("searchengine");
         registerModule(e);
      }
   }

   kDebug(KBABEL_SEARCH) << "Now for any application";

   offers = KServiceTypeTrader::self()->query("KBabelDictModule",
	    "not ( exist Applications)");

   for(KService::List::ConstIterator it = offers.begin(); it != offers.end(); ++it )
   {
      QString error;
      SearchEngine *e = (*it)->createInstance<SearchEngine>(this, QVariantList(), &error);
      if(!e)
      {
         kError() << "wasn't able to load" << (*it)->library() << error;
      }
      else
      {
         e->setObjectName("searchengine");
         registerModule(e);
      }
   }

   connect(nextButton,SIGNAL(clicked()),this,SLOT(slotNextResult()));
   connect(prevButton,SIGNAL(clicked()),this,SLOT(slotPrevResult()));
   connect(moreButton,SIGNAL(clicked()),this,SLOT(nextInfo()));


   origView->installEventFilter(this);
   translationView->installEventFilter(this);

   resultListView->setSorting(0,false);
   resultListView->setAllColumnsShowFocus(true);

   connect(resultListView,SIGNAL(selectionChanged(Q3ListViewItem*))
           , this, SLOT(showResult(Q3ListViewItem*)));
}

/*
 *  Destroys the object and frees any allocated resources
 */
KBabelDictBox::~KBabelDictBox()
{
}

void KBabelDictBox::registerModule( SearchEngine* e )
{
    active = 0;
    moduleList.append(e);
    connect(e, SIGNAL(started()),this,SIGNAL(searchStarted()));
    connect(e, SIGNAL(finished()),this,SIGNAL(searchStopped()));
    connect(e, SIGNAL(finished()),this
        ,SLOT(clearModuleResults()));
    connect(e, SIGNAL(progress(int)),this,SIGNAL(progressed(int)));
    connect(e, SIGNAL(progressStarts(const QString&)),this
        , SIGNAL(progressStarts(const QString&)));
    connect(e, SIGNAL(progressEnds()),this,SIGNAL(progressEnds()));
    connect(e, SIGNAL(resultFound(const SearchResult*)), this
        , SLOT(addResult(const SearchResult*)));
    connect(e, SIGNAL(hasError(const QString&)), this
        , SIGNAL(errorInModule(const QString&)));
}

void KBabelDictBox::saveSettings(KConfigBase *config)
{
   KConfigGroup cs(config,"KBabelDict");

   cs.writeEntry("ResultSplitter",resultSplitter->sizes());

   SearchEngine *e;

   e = moduleList.at(active);
   if(e)
   {
      cs.writeEntry("ActiveModule",e->id());
   }

   foreach(e, moduleList)
   {
      KConfigGroup group = config->group(e->id());
      e->saveSettings(group);
   }


}

void KBabelDictBox::saveSettings(const QString& moduleId, KConfigGroup &group)
{
   foreach(SearchEngine *e, moduleList)
   {
      if(e->id() == moduleId)
      {
        e->saveSettings(group);
	break;
      }
   }
}

void KBabelDictBox::readSettings(KConfigBase *config)
{
   KConfigGroup cs(config,"KBabelDict");
   QList<int> sizes = cs.readEntry( "ResultSplitter", QList<int>() );
   if(!sizes.isEmpty())
       resultSplitter->setSizes(sizes);

   QString m = cs.readEntry("ActiveModule");
   if(!m.isEmpty())
   {
      setActiveModule(m);
   }

   foreach(SearchEngine *e, moduleList)
   {
      KConfigGroup group = config->group(e->id());
      e->readSettings(group);
   }
}

void KBabelDictBox::readSettings(const QString& moduleId, const KConfigGroup &group)
{
   foreach(SearchEngine *e, moduleList)
   {
      if(e->id() == moduleId)
      {
        e->readSettings(group);
	break;
      }
   }
}

void KBabelDictBox::setAutoUpdateOptions(bool on)
{
   foreach(SearchEngine *e, moduleList)
   {
      e->setAutoUpdateOptions(on);
   }
}


int KBabelDictBox::activeModule()
{
   return active;
}

/*
 * public slot
 */
void KBabelDictBox::setActiveModule(int a)
{
   if( a == active)
      return;

   if( a < moduleList.count())
   {
      SearchEngine *engine = moduleList.at(active);

      if(!engine)
      {
         kDebug(KBABELDICT) << "no module available";
      }
      else if(engine->isSearching())
      {
         engine->stopSearch();
         engine->clearResults();
      }

      engine = moduleList.at(a);
      if(engine)
      {
          active =  a;
          emit activeModuleChanged(active);
          emit activeModuleChanged(engine->isEditable());
      }


   }
}

void KBabelDictBox::setActiveModule(QString id)
{
   int i=0;

   foreach(SearchEngine *e, moduleList)
   {
      if(e->id() == id)
      {
         setActiveModule(i);
         break;
      }

      i++;
   }
}

/*
 * public slot
 */
void KBabelDictBox::startSearch(const QString text)
{
   clear();
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      if(engine->isSearching())
      {
         engine->stopSearch();
         connect(this, SIGNAL(searchStopped()), this
                     , SLOT(startDelayedSearch()));

         searchText=text;
      }
      else engine->startSearch(text);
   }
}

/*
 * public slot
 */
void KBabelDictBox::startTranslationSearch(const QString text)
{
   clear();
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      if(engine->isSearching())
      {
         engine->stopSearch();
         connect(this, SIGNAL(searchStopped()), this
                     , SLOT(startDelayedTranslationSearch()));

         searchText=text;
      }
      else engine->startSearchInTranslation(text);
   }
}

void KBabelDictBox::startDelayedSearch(const QString text)
{
   clear();
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      searchText=text;

      if(engine->isSearching())
      {
         engine->stopSearch();

         connect(this, SIGNAL(searchStopped()), this
            , SLOT(startDelayedSearch()));

      }
      else
      {
         QTimer::singleShot(5,this,SLOT(startDelayedSearch()));
      }
   }
}

void KBabelDictBox::startDelayedTranslationSearch(const QString text)
{
   clear();
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      searchText=text;

      if(engine->isSearching())
      {
         engine->stopSearch();

         connect(this, SIGNAL(searchStopped()), this
            , SLOT(startDelayedTranslationSearch()));

      }
      else
      {
         QTimer::singleShot(5,this,SLOT(startDelayedTranslationSearch()));
      }
   }
}

QString KBabelDictBox::translate(const QString text)
{
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
      return QString();
   }
   else
   {
      if(engine->isSearching())
      {
         engine->stopSearch();
      }

      return engine->translate(text);
   }
}

QString KBabelDictBox::fuzzyTranslation(const QString text, int &score)
{
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
      return QString();
   }
   else
   {
      if(engine->isSearching())
      {
         engine->stopSearch();
      }

      return engine->fuzzyTranslation(text, score);
   }
}

QString KBabelDictBox::searchTranslation(const QString text, int &score)
{
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
      return QString();
   }
   else
   {
      if(engine->isSearching())
      {
         engine->stopSearch();
      }

      return engine->searchTranslation(text, score);
   }
}

void KBabelDictBox::startDelayedSearch()
{
   clear();

   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      disconnect(this, SIGNAL(searchStopped()), this
                     , SLOT(startDelayedSearch()));


      engine->startSearch(searchText);
   }
}

void KBabelDictBox::startDelayedTranslationSearch()
{
   clear();

   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      disconnect(this, SIGNAL(searchStopped()), this
                     , SLOT(startDelayedTranslationSearch()));


      engine->startSearchInTranslation(searchText);
   }
}

/*
 * public slot
 */
void KBabelDictBox::stopSearch()
{
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
   }
   else
   {
      engine->stopSearch();
   }

}

bool KBabelDictBox::isSearching()
{
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
      kDebug(KBABELDICT) << "no module available";
      return false;
   }

   return engine->isSearching();
}


QStringList KBabelDictBox::moduleNames()
{
   QStringList list;

   foreach(SearchEngine *e, moduleList)
   {
      list.append(e->name());
   }

   return list;
}

QStringList KBabelDictBox::modules()
{
   QStringList list;

   foreach(SearchEngine *e, moduleList)
   {
      list.append(e->id());
   }

   return list;
}

QList<ModuleInfo *> KBabelDictBox::moduleInfos()
{
   QList<ModuleInfo *> list;

   foreach(SearchEngine *e, moduleList)
   {
      ModuleInfo *info = new ModuleInfo;
      info->id=e->id();
      info->name=e->name();
      info->editable=e->isEditable();

      list.append(info);
   }

   return list;
}


QList<PrefWidget *> KBabelDictBox::modPrefWidgets(QWidget *parent)
{
   QList<PrefWidget *> list;

   foreach(SearchEngine *e, moduleList)
   {
      list.append(e->preferencesWidget(parent));
   }

   return list;

}


void KBabelDictBox::showResult(Q3ListViewItem *item)
{
   ResultListItem *resultItem = static_cast<ResultListItem*>(item);

   if(!item)
   {
      kError(KBABELDICT) << "no item";
      if(rmbPopup)
      {
        editFileAction->setText( i18n( "Edit File" ) );
        editFileAction->setEnabled( false );
      }
   }
   else
   {
      const SearchResult *result= resultItem->result();
      if(!result)
         return;

      resultListView->ensureItemVisible(item);

      currentResult = resultListView->itemIndex(item);
      currentInfo = 0;

      bool richText=resultItem->richText();
      if(richText)
      {
         // FIXME: what about plural forms?
         origView->setText(result->found.first());
         translationView->setText(result->translation);
      }
      else
      {
         // FIXME: what about plural forms?
         origView->setText(Qt::convertFromPlainText(result->found.first()));
         translationView->setText(
               Qt::convertFromPlainText(result->translation));
      }

      if(result->descriptions.count() > 0)
      {
         Q3PtrListIterator<TranslationInfo> it(result->descriptions);
         TranslationInfo *info=it.current();
         if(info)
         {
            if(info->lastChange.isValid())
            {
               dateLabel->setText(KGlobal::locale()->formatDate(
                    info->lastChange.date(), KLocale::ShortDate));
            }
            else
            {
                dateLabel->setText("");
            }
            locationLabel->setText(info->location);
            translatorLabel->setText(info->translator);

            if(rmbPopup)
            {
                if(!info->filePath.isEmpty())
                {
                    editFileAction->setText( i18n( "Edit File %1", info->location ) );
                    editFileAction->setEnabled( true );
                }
                else
                {
                    editFileAction->setText( i18n( "Edit File" ) );
                    editFileAction->setEnabled( false );
                }
             }
         }
      }
      else
      {
         dateLabel->setText("");
         locationLabel->setText("");
         translatorLabel->setText("");

         editFileAction->setText( i18n( "Edit File" ) );
         editFileAction->setEnabled( false );
      }

      moreButton->setEnabled((result->descriptions.count() > 1));

      currentLabel->setText(QString::number(currentResult+1));

      prevButton->setEnabled(currentResult > 0);
      nextButton->setEnabled(currentResult+1 < total);
   }

}

void KBabelDictBox::nextResult()
{
    Q3ListViewItem *item=resultListView->selectedItem();
    if(item)
    {
        item=item->itemBelow();
        if(item)
        {
            resultListView->setSelected(item,true);
        }
    }
}


void KBabelDictBox::prevResult()
{
    Q3ListViewItem *item=resultListView->selectedItem();
    if(item)
    {
        item=item->itemAbove();
        if(item)
        {
            resultListView->setSelected(item,true);
        }
    }

}

void KBabelDictBox::addResult(const SearchResult* result)
{
   SearchEngine *e;

   e = moduleList.at(active);
   if(!e)
   {
       kError(KBABELDICT) << "no module available";
       return;
   }

   Q3ListViewItem *item=resultListView->selectedItem();
   int index=0;
   if(item)
   {
       index=resultListView->itemIndex(item);
   }

   new ResultListItem(resultListView, *result,e->usesRichTextResults());
   total++;
   totalResultsLabel->setText(QString::number(total));

   if(total==1)
   {
      resultListView->setSelected(resultListView->firstChild(),true);
   }
   else
   {
      nextButton->setEnabled((currentResult+1) < total);
      item=resultListView->itemAtIndex(index);
      if(item)
      {
          resultListView->setSelected(item,true);
      }
   }
}

void KBabelDictBox::clear()
{
    dateLabel->setText("");
    locationLabel->setText("");
    translatorLabel->setText("");
    currentLabel->setText(QString::number(0));
    totalResultsLabel->setText(QString::number(0));
    origView->setText("");
    translationView->setText("");
    currentResult=0;
    currentInfo=0;
    total=0;

    resultListView->clear();
    clearModuleResults();

    moreButton->setEnabled(false);
    prevButton->setEnabled(false);
    nextButton->setEnabled(false);

    if(rmbPopup)
    {
        editFileAction->setText( i18n( "Edit File" ) );
        editFileAction->setEnabled( false );
    }
}

void KBabelDictBox::nextInfo()
{
   ResultListItem *item = static_cast<ResultListItem*>(resultListView->selectedItem());

   if(!item)
   {
      kDebug(KBABELDICT) << "no item available";
   }
   else
   {
      const SearchResult *result = item->result();
      if(!result)
         return;

      if(result->descriptions.count() > 0)
      {
         currentInfo++;
         TranslationInfo *info;
         if(currentInfo == (int)result->descriptions.count())
         {
            Q3PtrListIterator<TranslationInfo> it(result->descriptions);
            info = it.current();
            currentInfo = 0;
         }
         else
         {
            Q3PtrListIterator<TranslationInfo> it(result->descriptions);
            for(int i=0; i < currentInfo; i++)
            {
                ++it;
            }
            info=*it;
         }

         if(info->lastChange.isValid())
         {
             dateLabel->setText(KGlobal::locale()->formatDate(
                           info->lastChange.date(), KLocale::ShortDate));
         }
         else
         {
             dateLabel->setText("");
         }

         locationLabel->setText(info->location);
         translatorLabel->setText(info->translator);

         if(rmbPopup)
         {
            if(!info->filePath.isEmpty())
            {
                editFileAction->setText( i18n( "Edit File %1", info->location ) );
                editFileAction->setEnabled( true );
            }
            else
            {
                editFileAction->setText( i18n( "Edit File" ) );
                editFileAction->setEnabled( false );
            }
         }
      }
   }

}

void KBabelDictBox::showListOnly()
{
   int h=resultSplitter->height();
   QList<int> sizes;
   sizes.append(1);
   sizes.append(h-1);
   resultSplitter->setSizes(sizes);
}

void KBabelDictBox::showDetailsOnly()
{
   int h=resultSplitter->height();
   QList<int> sizes;
   sizes.append(h-1);
   sizes.append(h);
   resultSplitter->setSizes(sizes);
}

void KBabelDictBox::clearModuleResults()
{
    SearchEngine *engine = moduleList.at(active);
    if(engine)
        engine->clearResults();
}

void KBabelDictBox::about()
{
   K3AboutApplication *aboutDlg = new K3AboutApplication(0L,this);

   foreach(SearchEngine *e, moduleList)
   {
      K3AboutContainer *page = aboutDlg->addScrolledContainerPage(e->name());

      const KAboutData *aboutData = e->about();
      if(aboutData)
      {

         QString text = aboutData->programName() + ' ' +
                      aboutData->version() + '\n';

         text += '\n'+aboutData->shortDescription()+'\n';

         if(!aboutData->homepage().isEmpty())
         {
            text += '\n' + aboutData->homepage() + '\n';
         }
         if(!aboutData->otherText().isEmpty())
         {
            text += '\n' + aboutData->otherText() + '\n';
         }
         if(!aboutData->copyrightStatement().isEmpty())
         {
            text += '\n' + aboutData->copyrightStatement() + '\n';
         }

         if(aboutData->bugAddress() != "submit@bugs.kde.org")
         {
            text += '\n' + i18n("Send bugs to %1",
                   aboutData->bugAddress()) +'\n';
         }

         QLabel *label = new QLabel(text,0);
         page->addWidget(label);

         int authorCount = aboutData->authors().count();
         if(authorCount)
         {
            if(authorCount==1)
                  text=i18n("Author:");
            else
                  text=i18n("Authors:");

            label = new QLabel(text,0);
            page->addWidget(label);

            foreach(KAboutPerson person, aboutData->authors())
            {
               page->addPerson( person.name(), person.emailAddress(),
                        person.webAddress(), person.task() );
            }
         }
         int creditsCount = aboutData->credits().count();
         if(creditsCount)
         {
            text = i18n("Thanks to:");
            label = new QLabel(text,0);
            page->addWidget(label);

            foreach(KAboutPerson person, aboutData->credits())
            {
               page->addPerson( person.name(), person.emailAddress(),
                        person.webAddress(), person.task() );
            }

         }
      }
      else
      {
         QString text = i18n("No information available.");
         QLabel *label = new QLabel(text,0);
         page->addWidget(label);
      }

   }

   aboutDlg->setInitialSize(QSize(400,1));
   aboutDlg->exec();

   delete aboutDlg;
}

void KBabelDictBox::aboutActiveModule()
{
    SearchEngine *engine = moduleList.at(active);
    if(!engine)
        return;

    aboutModule(engine->id());
}

void KBabelDictBox::aboutModule(const QString& id)
{
   SearchEngine *e = 0;

   foreach(e, moduleList)
   {
      if(e->id() == id)
      {
          break;
      }
   }

   if(!e)
       return;

   KAboutApplicationDialog *aboutDlg = new KAboutApplicationDialog(e->about(), this);
   aboutDlg->exec();

   delete aboutDlg;

}


void KBabelDictBox::slotNextResult()
{
   nextResult();
}

void KBabelDictBox::slotPrevResult()
{
   prevResult();
}


void KBabelDictBox::slotStopSearch()
{
   stopSearch();
}

void KBabelDictBox::slotStartSearch(const QString& text)
{
   startSearch(text);
}

void KBabelDictBox::setEditedPackage(const QString& name)
{
   foreach(SearchEngine *e, moduleList)
   {
      e->setEditedPackage(name);
   }
}


void KBabelDictBox::setEditedFile(const QString& path)
{
   foreach(SearchEngine *e, moduleList)
   {
      e->setEditedFile(path);
   }
}

void KBabelDictBox::setLanguage(const QString& languageCode,
        const QString& languageName)
{
   foreach(SearchEngine *e, moduleList)
   {
      e->setLanguage(languageCode,languageName);
      e->setLanguageCode(languageCode);
   }
}

void KBabelDictBox::copy()
{
   if(origView->hasSelectedText())
   {
      origView->copy();
   }
   else if(translationView->hasSelectedText())
   {
      translationView->copy();
   }
   else
   {
      QClipboard *cb = KApplication::clipboard();
      cb->setText(translation());
   }
}

QString KBabelDictBox::translation()
{
   QString trans;

   Q3ListViewItem *item=resultListView->selectedItem();
   if(item)
   {
       ResultListItem *r=static_cast<ResultListItem*>(item);
       if(r)
       {
           const SearchResult *sr=r->result();
           if(sr)
           {
               if(r->richText())
                   trans=sr->plainTranslation;
               else
                   trans=sr->translation;
           }
       }
   }

   return trans;
}

void KBabelDictBox::setRMBMenu( QMenu *popup )
{
    if( popup ) {

        if( popup->actions().count() > 0 )
            popup->addSeparator();

    editFileAction = popup->addAction( i18n( "Edit File" ) );
    connect( editFileAction, SIGNAL( triggered() ), this, SLOT( editFile() ) );

    editFileAction->setEnabled( false );

    origView->setContextMenuPolicy( Qt::ActionsContextMenu );
    origView->addActions( popup->actions() );

    origView->viewport()->setContextMenuPolicy( Qt::ActionsContextMenu );
    origView->viewport()->addActions(popup->actions());

    translationView->setContextMenuPolicy( Qt::ActionsContextMenu );
    translationView->addActions( popup->actions() );

    translationView->viewport()->setContextMenuPolicy( Qt::ActionsContextMenu );
    translationView->viewport()->addActions( popup->actions() );

    this->setContextMenuPolicy( Qt::ActionsContextMenu );
    this->addActions( popup->actions() );

    //KContextMenuManager::insert(resultListView->viewport(),popup);

    rmbPopup = popup;
    }
}

bool KBabelDictBox::hasSelectedText() const
{
   bool have=false;
   if(origView->hasSelectedText())
      have=true;
   else if(translationView->hasSelectedText())
      have=true;
   else if(resultListView->selectedItem() )
      have=true;

   return have;
}

QString KBabelDictBox::selectedText() const
{
   QString text;
   if(origView->hasSelectedText())
      text=origView->selectedText();
   else if(translationView->hasSelectedText())
      translationView->selectedText();

   return text;
}



void KBabelDictBox::configure(const QString& id, bool modal)
{

   QWidget* w = prefDialogs.value(id);
   if(w)
   {
#ifdef Q_OS_UNIX	   
       KWindowSystem::activateWindow( w->winId() );
#endif
       return;
   }

   foreach(SearchEngine *e, moduleList)
   {
      if(e->id() == id)
      {
         QString caption = i18n("Configure Dictionary %1", e->name());
         KDialog *dialog = new KDialog(this);
         dialog->setButtons( KDialog::Ok|KDialog::Apply|KDialog::Cancel|KDialog::Default );
         dialog->setDefaultButton( KDialog::Ok );
         dialog->setModal( modal );
         dialog->setCaption( caption );
         dialog->setWhatsThis("");

         PrefWidget *prefWidget = e->preferencesWidget(dialog);
         dialog->setMainWidget(prefWidget);

         connect(dialog, SIGNAL(okClicked()),prefWidget,SLOT(apply()));
         connect(dialog, SIGNAL(applyClicked()),prefWidget,SLOT(apply()));
         connect(dialog, SIGNAL(defaultClicked()),prefWidget,SLOT(standard()));
         connect(dialog, SIGNAL(cancelClicked()),prefWidget,SLOT(cancel()));

         connect(dialog, SIGNAL(finished()),this,SLOT(destroyConfigDialog()));

         prefDialogs.insert(id,dialog);

         if( modal ) dialog->exec();
	 else dialog->show();

         break;
      }
   }

}


void KBabelDictBox::destroyConfigDialog()
{
    const QObject *obj = sender();
    if(obj && obj->inherits("KDialog"))
    {
       KDialog *dialog = (KDialog*)obj;
       if(dialog)
       {
           dialog->delayedDestruct();

           const QString id = prefDialogs.key(dialog);
           if (!id.isEmpty())
           {
               prefDialogs.remove(id);
           }
       }
    }
}

void KBabelDictBox::edit(const QString& id)
{
   foreach(SearchEngine *e, moduleList)
   {
      if(e->id() == id)
      {
         if(e->isEditable())
             e->edit();

         break;
      }
   }
}

void KBabelDictBox::edit()
{
    SearchEngine *engine = moduleList.at(active);
    if(!engine)
        return;

    if(engine->isEditable())
        engine->edit();
}

void KBabelDictBox::setTextChanged(const QStringList& orig,
        const QString& translation, uint pluralForm, const QString& description)
{
   foreach(SearchEngine *e, moduleList)
   {
      e->stringChanged(orig,translation,pluralForm,description);
   }
}


void KBabelDictBox::wheelEvent(QWheelEvent *we)
{
    if(we->delta() > 0)
    {
        prevResult();
    }
    else
    {
        nextResult();
    }

    we->accept();
}

bool KBabelDictBox::eventFilter(QObject *o, QEvent *e)
{
    if(e->type() == QEvent::Wheel)
    {
        QWheelEvent *we = static_cast<QWheelEvent*>(e);
        if(we)
        {
            wheelEvent(we);
            return true;
        }
    }
    else if(e->type() == QEvent::Resize && o == resultListView)
    {
        if(resultListView->height() < 2)
        {
           detailButton->setEnabled(false);
           listButton->setEnabled(true);
        }
        else if(resultListView->height() > resultSplitter->height()-10)
        {
           detailButton->setEnabled(true);
           listButton->setEnabled(false);
        }
        else
        {
           detailButton->setEnabled(true);
           listButton->setEnabled(true);
        }
    }

    return false;
}

void KBabelDictBox::editFile()
{
    ResultListItem *item = static_cast<ResultListItem*>(resultListView->currentItem());

   if(!item)
   {
      kDebug(KBABELDICT) << "no item available";
   }
   else
   {
      const SearchResult *result = item->result();
      if(!result)
         return;

      if(!result->descriptions.isEmpty())
      {
         TranslationInfo *info;
         Q3PtrListIterator<TranslationInfo> it(result->descriptions);
         for(int i=0; i < currentInfo; i++)
         {
             ++it;
         }
         info=*it;

         if(!info->filePath.isEmpty())
         {
             QString url = info->filePath;
             QString msgid;

             if( item->richText() )
             {
                msgid = result->plainFound;
             }
             else
             {
                 msgid = result->found.first();
             }

             if ( !QDBusConnection::sessionBus().interface()->isServiceRegistered("org.kde.kbabel") )
             {
                 kDebug(KBABELDICT) << "kbabel is not registered";

                 QString error;
                 QStringList argList;
                 argList.append("--nosplash");
                 argList.append("--gotomsgid");
                 argList.append( msgid.toLocal8Bit() );
                 argList.append( url.toLocal8Bit() );
                 KToolInvocation::kdeinitExec("kbabel",argList,&error);
                 if(!error.isNull())
                 {
                     KMessageBox::sorry(this
                             ,i18n("There was an error starting KBabel:\n%1",
                                     error));
                     return;
                 }
             }
             else
             {
		org::kde::kbabel::kbabel kbabel("org.kde.kbabel", "/KBabel", QDBusConnection::sessionBus());
		QDBusReply<void> reply = kbabel.gotoFileEntry(url.toUtf8(), msgid.toUtf8() );
                 if (!reply.isValid())
                 {
                     KMessageBox::sorry(this
                             ,i18n("There was an error using D-Bus."));
                 }
             }
          }
       }
   }
}

void KBabelDictBox::showContextMenu(K3ListView *,Q3ListViewItem *,const QPoint& p)
{
    if(rmbPopup)
    {
        rmbPopup->exec(p);
    }
}


bool KBabelDictBox::messagesForPackage(const QString& package
           , QList<DiffEntry>& resultList, QString& error)
{
   setActiveModule("dbsearchengine");
   SearchEngine *engine = moduleList.at(active);

   if(!engine)
   {
       KMessageBox::sorry(this
          ,i18n("The \"Translation Database\" module\n"
              "appears not to be installed on your system."));
       return false;
   }

   QList<SearchResult> rList;

   SearchFilter* filter = new SearchFilter();
   filter->setLocation(package);

   bool success = engine->messagesForFilter(filter,rList,error);

   if(success)
   {
       QList<SearchResult>::Iterator it;
       for(it=rList.begin(); it != rList.end(); ++it)
       {
    	   // FIXME: what about plural forms?
           DiffEntry e;
           e.msgid = (*it).found.first();
           e.msgstr = (*it).translation;
           resultList.append(e);
       }
   }

   return success;
}

#include "kbabeldictbox.moc"

