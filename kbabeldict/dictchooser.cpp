/* ****************************************************************************
  This file is part of KBabel

  Copyright (C) 1999-2001 by Matthias Kiefer
                            <matthias.kiefer@gmx.de>
		2003	  by Stanislav Visnovsky
			    <visnovsky@kde.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

  In addition, as a special exception, the copyright holders give
  permission to link the code of this program with any edition of
  the Qt library by Trolltech AS, Norway (or with modified versions
  of Qt that use the same license as Qt), and distribute linked
  combinations including the two.  You must obey the GNU General
  Public License in all respects for all of the code used other than
  Qt. If you modify this file, you may extend this exception to
  your version of the file, but you are not obligated to do so.  If
  you do not wish to do so, delete this exception statement from
  your version.

**************************************************************************** */
#include "dictchooser.h"
#include "resources.h"

#include <qlayout.h>
#include <qpushbutton.h>
#include <QVBoxLayout>
#include <qlistwidget.h>
#include <qtoolbutton.h>
#include <kconfig.h>
#include <klocale.h>
#include <ktemporaryfile.h>
#include <kactionselector.h>
#include <kicon.h>

static const int ModuleRole = Qt::UserRole + 1;

Q_DECLARE_METATYPE(ModuleInfo*);

class ModuleInfoItem : public QListWidgetItem
{
public:
    ModuleInfoItem(ModuleInfo *info)
        : QListWidgetItem(info->name)
    {
        setData(ModuleRole, QVariant::fromValue(info));
    }
};

static QAbstractButton* hackButtonInActionSelector(KActionSelector *selector)
{
    QLayout *lay = selector->layout();
    if (!lay || !qobject_cast<QHBoxLayout *>(lay))
        return 0;
    QLayoutItem *item = lay->itemAt(lay->count() - 1);
    lay = item->layout();
    if (!lay || !qobject_cast<QVBoxLayout *>(lay))
        return 0;
    QWidget *refButton = 0;
    for (int i = 0; i < lay->count(); ++i)
    {
        item = lay->itemAt(i);
        if(item->widget() && qobject_cast<QAbstractButton *>(item->widget()))
            refButton = item->widget();
    }
    if (!refButton || lay->indexOf(refButton) == -1)
        return 0;

    int index = lay->indexOf(refButton) + 1;
    if (index == lay->count())
        index = -1;

    QToolButton *button = new QToolButton(selector);
    button->setIcon(KIcon("configure"));
    qobject_cast<QBoxLayout *>(lay)->insertWidget(index, button);
    return button;
}

DictChooser::DictChooser(KBabelDictBox*b, QStringList selected, QWidget *parent)
    : QWidget( parent ), box( b )
{
    dictList = box->moduleInfos();
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setMargin(0);

    actionSelector = new KActionSelector(this);
    actionSelector->setAvailableInsertionPolicy(KActionSelector::Sorted);
    actionSelector->setAvailableLabel(i18nc("dictionary to not use", "Do not use:"));
    actionSelector->setSelectedInsertionPolicy(KActionSelector::AtBottom);
    actionSelector->setSelectedLabel(i18nc("dictionary to use", "Use:"));
    actionSelector->setMoveOnDoubleClick(true);
    layout->addWidget(actionSelector);

    if (!(configureBtn = hackButtonInActionSelector(actionSelector)))
    {
        QHBoxLayout *hlay = new QHBoxLayout();
        hlay->addStretch(1);
        configureBtn = new QPushButton(i18n("Con&figure..."), this);
        hlay->addWidget(configureBtn);
        hlay->addItem(new QSpacerItem(40, 4, QSizePolicy::Fixed, QSizePolicy::Minimum));
        layout->addLayout(hlay);
    }
    configureBtn->setEnabled(false);

    for(QStringList::Iterator it=selected.begin(); it!=selected.end();
            ++it)
    {
        foreach(ModuleInfo *mi, dictList)
        {
            if(mi->id==*it)
            {
                actionSelector->selectedListWidget()->addItem(new ModuleInfoItem(mi));
            }
        }
    }
    foreach(ModuleInfo *mi, dictList)
    {
        if(!selected.contains(mi->id))
        {
            actionSelector->availableListWidget()->addItem(new ModuleInfoItem(mi));
        }
    }

    connect(actionSelector->selectedListWidget(), SIGNAL(currentRowChanged(int)),
            this, SLOT(slotSelectedChanged(int)));
    connect(configureBtn,SIGNAL(clicked()), this
            , SLOT(configureSelected()));

    int min = minimumHeight();
    if(min < 100)
        setMinimumHeight(100);
}


DictChooser::~DictChooser()
{
    qDeleteAll(tempConfig);
    qDeleteAll(dictList);
}


QStringList DictChooser::selectedDicts()
{
    QStringList selected;

    QListWidget *list = actionSelector->selectedListWidget();
    for(int i = 0; i < list->count(); i++)
    {
        QListWidgetItem *item = list->item(i);
        selected.append(item->data(ModuleRole).value<ModuleInfo *>()->id);
    }

    return selected;
}


void DictChooser::slotSelectedChanged(int current)
{
    configureBtn->setEnabled(current != -1);
}

void DictChooser::configureSelected()
{
    QListWidgetItem *item = actionSelector->selectedListWidget()->currentItem();
    if(!item)
        return;
    ModuleInfo *mi = item->data(ModuleRole).value<ModuleInfo *>();

    // do backup first
    if(!tempConfig.value(mi->id))
    {
        KTemporaryFile* tmp=new KTemporaryFile();
        tmp->open();
        tempConfig.insert(mi->id ,tmp);

        kDebug(KBABEL_SEARCH) << "Temp file:" << tmp->fileName();
        KConfig config(tmp->fileName());
        KConfigGroup group = config.group(mi->id);
        box->saveSettings(mi->id, group);
    }

    // now let user change configuration
    box->configure(mi->id,true);
}

void DictChooser::restoreConfig()
{
    kDebug( KBABEL_SEARCH ) << "Cleanup";
    // cleanup
    QHashIterator<QString, KTemporaryFile *> it(tempConfig);
    while(it.hasNext())
    {
        it.next();
        KConfig config(it.value()->fileName());
        KConfigGroup group = config.group(it.key());
        box->readSettings(it.key(), group);
    }

    // there is no temporary configs
    qDeleteAll(tempConfig);
    tempConfig.clear();
}

#include "dictchooser.moc"
